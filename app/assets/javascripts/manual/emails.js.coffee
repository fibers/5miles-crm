# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'page:change', ->
  $('#manual_email_test').on 'mouseover', ->
    $('#manual_email_send').removeAttr('data-disable-with');
    $('input[name="immediate"]').removeAttr('required');
    $(this).attr('data-disable-with', 'Sending ...');
    $('input#test_user_id').attr('required', 'required');

  $('#manual_email_test').on 'mouseleave', ->
    $(this).removeAttr('data-disable-with');
    $('input#test_user_id').removeAttr('required');

  $('#manual_email_send').on 'mouseover', ->
    $('#manual_email_test').removeAttr('data-disable-with');
    $(this).attr('data-disable-with', 'Sending ...');
    $('input[name="immediate"]').attr('required', 'required');
    $('input#test_user_id').removeAttr('required');

  $('#manual_email_send').on 'mouseleave', ->
    $(this).removeAttr('data-disable-with');

  $('a#manual_email_preview').on 'mouseover', ->
    $('div#emailPreview').html($('textarea#email_content').val())
