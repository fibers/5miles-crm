# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'page:change', ->
  $('#productTab a[href="#location"]').click (e) ->
    e.preventDefault()
    $(this).tab 'show'
    center = window.map.getCenter()
    setTimeout ->
      google.maps.event.trigger window.map, 'resize'
      window.map.setCenter center
    , 500

  $('a[data-target="#disapprove"]').on 'click', (e) ->
    $('div#disapprove textarea#reason').attr('required', 'required')

  $('div#disapprove').on 'hide.bs.modal', ->
    $('div#disapprove textarea#reason').removeAttr('required')

  $('a[data-target="#approveRisk"]').on 'click', (e) ->
    $('div#approveRisk textarea#risk').attr('required', 'required')

  $('div#approveRisk').on 'hide.bs.modal', ->
    $('div#approveRisk textarea#risk').removeAttr('required')
