# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'page:change', ->
  $('#tag_status').change ->
    if $(this).children('option:selected').val() == '0'
      $('#invalidSelected').show();
    else
      $('#invalidSelected').hide();
