# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'page:change', ->
  $('.cloudinary-fileupload').on 'change', ->
    $('input#main_image_local_path').val($(this).val());

  $('#manual_recommend_items_test').on 'mouseover', ->
    $('#manual_recommend_items_send').removeAttr('data-disable-with');
    $('input[name="immediate"]').removeAttr('required');
    $(this).attr('data-disable-with', 'Sending ...');
    $('input#test_user_id').attr('required', 'required');

  $('#manual_recommend_items_test').on 'mouseleave', ->
    $(this).removeAttr('data-disable-with');
    $('input#test_user_id').removeAttr('required');

  $('#manual_recommend_items_send').on 'mouseover', ->
    $('#manual_recommend_items_test').removeAttr('data-disable-with');
    $('input#test_user_id').removeAttr('required');
    $(this).attr('data-disable-with', 'Sending ...');
    $('input[name="immediate"]').attr('required', 'required');

  $('#manual_recommend_items_send').on 'mouseleave', ->
    $(this).removeAttr('data-disable-with');

