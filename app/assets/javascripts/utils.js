/**
 * Created by lvyi on 15/4/16.
 */
//String.prototype.replaceAll = function(reallyDo, replaceWith, ignoreCase) {
//    if (!RegExp.prototype.isPrototypeOf(reallyDo)) {
//        return this.replace(new RegExp(reallyDo, (ignoreCase ? "gi": "g")), replaceWith);
//    } else {
//        return this.replace(reallyDo, replaceWith);
//    }
//}
String.prototype.replaceAll = function(search, replace){
    var regex = new RegExp(search, "g");
    return this.replace(regex, replace);
}

var DateFormat = {};

(function($) {
    var daysInWeek          = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var shortDaysInWeek     = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    var shortMonthsInYear   = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var longMonthsInYear    = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'];
    var shortMonthsToNumber = { 'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06',
        'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12' };

    var YYYYMMDD_MATCHER = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.?\d{0,3}[Z\-+]?(\d{2}:?\d{2})?/;

    $.format = (function() {
        function numberToLongDay(value) {
            // 0 to Sunday
            // 1 to Monday
            return daysInWeek[parseInt(value, 10)] || value;
        }

        function numberToShortDay(value) {
            // 0 to Sun
            // 1 to Mon
            return shortDaysInWeek[parseInt(value, 10)] || value;
        }

        function numberToShortMonth(value) {
            // 1 to Jan
            // 2 to Feb
            var monthArrayIndex = parseInt(value, 10) - 1;
            return shortMonthsInYear[monthArrayIndex] || value;
        }

        function numberToLongMonth(value) {
            // 1 to January
            // 2 to February
            var monthArrayIndex = parseInt(value, 10) - 1;
            return longMonthsInYear[monthArrayIndex] || value;
        }

        function shortMonthToNumber(value) {
            // Jan to 01
            // Feb to 02
            return shortMonthsToNumber[value] || value;
        }

        function parseTime(value) {
            // 10:54:50.546
            // => hour: 10, minute: 54, second: 50, millis: 546
            // 10:54:50
            // => hour: 10, minute: 54, second: 50, millis: ''
            var time = value,
                hour,
                minute,
                second,
                millis = '',
                delimited,
                timeArray;

            if(time.indexOf('.') !== -1) {
                delimited = time.split('.');
                // split time and milliseconds
                time   = delimited[0];
                millis = delimited[delimited.length - 1];
            }

            timeArray = time.split(':');

            if(timeArray.length === 3) {
                hour   = timeArray[0];
                minute = timeArray[1];
                // '20 GMT-0200 (BRST)'.replace(/\s.+/, '').replace(/[a-z]/gi, '');
                // => 20
                // '20Z'.replace(/\s.+/, '').replace(/[a-z]/gi, '');
                // => 20
                second = timeArray[2].replace(/\s.+/, '').replace(/[a-z]/gi, '');
                // '01:10:20 GMT-0200 (BRST)'.replace(/\s.+/, '').replace(/[a-z]/gi, '');
                // => 01:10:20
                // '01:10:20Z'.replace(/\s.+/, '').replace(/[a-z]/gi, '');
                // => 01:10:20
                time = time.replace(/\s.+/, '').replace(/[a-z]/gi, '');
                return {
                    time:    time,
                    hour:    hour,
                    minute:  minute,
                    second:  second,
                    millis:  millis
                };
            }

            return { time : '', hour : '', minute : '', second : '', millis : '' };
        }


        function padding(value, length) {
            var paddingCount = length - String(value).length;
            for(var i = 0; i < paddingCount; i++) {
                value = '0' + value;
            }
            return value;
        }

        return {

            parseDate: function(value) {
                var values,
                    subValues;

                var parsedDate = {
                    date:       null,
                    year:       null,
                    month:      null,
                    dayOfMonth: null,
                    dayOfWeek:  null,
                    time:       null
                };

                if(typeof value == 'number') {
                    return this.parseDate(new Date(value));
                } else if(typeof value.getFullYear == 'function') {
                    parsedDate.year       = String(value.getFullYear());
                    // d = new Date(1900, 1, 1) // 1 for Feb instead of Jan.
                    // => Thu Feb 01 1900 00:00:00
                    parsedDate.month      = String(value.getMonth() + 1);
                    parsedDate.dayOfMonth = String(value.getDate());
                    parsedDate.time       = parseTime(value.toTimeString() + "." + value.getMilliseconds());
                } else if(value.search(YYYYMMDD_MATCHER) != -1) {
                    /* 2009-04-19T16:11:05+02:00 || 2009-04-19T16:11:05Z */
                    values = value.split(/[T\+-]/);
                    parsedDate.year       = values[0];
                    parsedDate.month      = values[1];
                    parsedDate.dayOfMonth = values[2];
                    parsedDate.time       = parseTime(values[3].split('.')[0]);
                } else {
                    values = value.split(' ');
                    if(values.length === 6 && isNaN(values[5])) {
                        // values[5] == year
                        /*
                         * This change is necessary to make `Mon Apr 28 2014 05:30:00 GMT-0300` work
                         * like `case 7`
                         * otherwise it will be considered like `Wed Jan 13 10:43:41 CET 2010
                         * Fixes: https://github.com/phstc/jquery-dateFormat/issues/64
                         */
                        values[values.length] = '()';
                    }
                    switch (values.length) {
                        case 6:
                            /* Wed Jan 13 10:43:41 CET 2010 */
                            parsedDate.year       = values[5];
                            parsedDate.month      = shortMonthToNumber(values[1]);
                            parsedDate.dayOfMonth = values[2];
                            parsedDate.time       = parseTime(values[3]);
                            break;
                        case 2:
                            /* 2009-12-18 10:54:50.546 */
                            subValues = values[0].split('-');
                            parsedDate.year       = subValues[0];
                            parsedDate.month      = subValues[1];
                            parsedDate.dayOfMonth = subValues[2];
                            parsedDate.time       = parseTime(values[1]);
                            break;
                        case 7:
                        /* Tue Mar 01 2011 12:01:42 GMT-0800 (PST) */
                        case 9:
                        /* added by Larry, for Fri Apr 08 2011 00:00:00 GMT+0800 (China Standard Time) */
                        case 10:
                            /* added by Larry, for Fri Apr 08 2011 00:00:00 GMT+0200 (W. Europe Daylight Time) */
                            parsedDate.year       = values[3];
                            parsedDate.month      = shortMonthToNumber(values[1]);
                            parsedDate.dayOfMonth = values[2];
                            parsedDate.time       = parseTime(values[4]);
                            break;
                        case 1:
                            /* added by Jonny, for 2012-02-07CET00:00:00 (Doctrine Entity -> Json Serializer) */
                            subValues = values[0].split('');
                            parsedDate.year       = subValues[0] + subValues[1] + subValues[2] + subValues[3];
                            parsedDate.month      = subValues[5] + subValues[6];
                            parsedDate.dayOfMonth = subValues[8] + subValues[9];
                            parsedDate.time       = parseTime(subValues[13] + subValues[14] + subValues[15] + subValues[16] + subValues[17] + subValues[18] + subValues[19] + subValues[20]);
                            break;
                        default:
                            return null;
                    }
                }

                if(parsedDate.time) {
                    parsedDate.date = new Date(parsedDate.year, parsedDate.month - 1, parsedDate.dayOfMonth, parsedDate.time.hour, parsedDate.time.minute, parsedDate.time.second, parsedDate.time.millis);
                } else {
                    parsedDate.date = new Date(parsedDate.year, parsedDate.month - 1, parsedDate.dayOfMonth);
                }

                parsedDate.dayOfWeek = String(parsedDate.date.getDay());

                return parsedDate;
            },

            date : function(value, format) {
                try {
                    var parsedDate = this.parseDate(value);

                    if(parsedDate === null) {
                        return value;
                    }

                    var year       = parsedDate.year,
                        month      = parsedDate.month,
                        dayOfMonth = parsedDate.dayOfMonth,
                        dayOfWeek  = parsedDate.dayOfWeek,
                        time       = parsedDate.time;
                    var hour;

                    var pattern      = '',
                        retValue     = '',
                        unparsedRest = '',
                        inQuote      = false;

                    /* Issue 1 - variable scope issue in format.date (Thanks jakemonO) */
                    for(var i = 0; i < format.length; i++) {
                        var currentPattern = format.charAt(i);
                        // Look-Ahead Right (LALR)
                        var nextRight      = format.charAt(i + 1);

                        if (inQuote) {
                            if (currentPattern == "'") {
                                retValue += (pattern === '') ? "'" : pattern;
                                pattern = '';
                                inQuote = false;
                            } else {
                                pattern += currentPattern;
                            }
                            continue;
                        }
                        pattern += currentPattern;
                        unparsedRest = '';
                        switch (pattern) {
                            case 'ddd':
                                retValue += numberToLongDay(dayOfWeek);
                                pattern = '';
                                break;
                            case 'dd':
                                if(nextRight === 'd') {
                                    break;
                                }
                                retValue += padding(dayOfMonth, 2);
                                pattern = '';
                                break;
                            case 'd':
                                if(nextRight === 'd') {
                                    break;
                                }
                                retValue += parseInt(dayOfMonth, 10);
                                pattern = '';
                                break;
                            case 'D':
                                if(dayOfMonth == 1 || dayOfMonth == 21 || dayOfMonth == 31) {
                                    dayOfMonth = parseInt(dayOfMonth, 10) + 'st';
                                } else if(dayOfMonth == 2 || dayOfMonth == 22) {
                                    dayOfMonth = parseInt(dayOfMonth, 10) + 'nd';
                                } else if(dayOfMonth == 3 || dayOfMonth == 23) {
                                    dayOfMonth = parseInt(dayOfMonth, 10) + 'rd';
                                } else {
                                    dayOfMonth = parseInt(dayOfMonth, 10) + 'th';
                                }
                                retValue += dayOfMonth;
                                pattern = '';
                                break;
                            case 'MMMM':
                                retValue += numberToLongMonth(month);
                                pattern = '';
                                break;
                            case 'MMM':
                                if(nextRight === 'M') {
                                    break;
                                }
                                retValue += numberToShortMonth(month);
                                pattern = '';
                                break;
                            case 'MM':
                                if(nextRight === 'M') {
                                    break;
                                }
                                retValue += padding(month, 2);
                                pattern = '';
                                break;
                            case 'M':
                                if(nextRight === 'M') {
                                    break;
                                }
                                retValue += parseInt(month, 10);
                                pattern = '';
                                break;
                            case 'y':
                            case 'yyy':
                                if(nextRight === 'y') {
                                    break;
                                }
                                retValue += pattern;
                                pattern = '';
                                break;
                            case 'yy':
                                if(nextRight === 'y') {
                                    break;
                                }
                                retValue += String(year).slice(-2);
                                pattern = '';
                                break;
                            case 'yyyy':
                                retValue += year;
                                pattern = '';
                                break;
                            case 'HH':
                                retValue += padding(time.hour, 2);
                                pattern = '';
                                break;
                            case 'H':
                                if(nextRight === 'H') {
                                    break;
                                }
                                retValue += parseInt(time.hour, 10);
                                pattern = '';
                                break;
                            case 'hh':
                                /* time.hour is '00' as string == is used instead of === */
                                hour = (parseInt(time.hour, 10) === 0 ? 12 : time.hour < 13 ? time.hour
                                    : time.hour - 12);
                                retValue += padding(hour, 2);
                                pattern = '';
                                break;
                            case 'h':
                                if(nextRight === 'h') {
                                    break;
                                }
                                hour = (parseInt(time.hour, 10) === 0 ? 12 : time.hour < 13 ? time.hour
                                    : time.hour - 12);
                                retValue += parseInt(hour, 10);
                                // Fixing issue https://github.com/phstc/jquery-dateFormat/issues/21
                                // retValue = parseInt(retValue, 10);
                                pattern = '';
                                break;
                            case 'mm':
                                retValue += padding(time.minute, 2);
                                pattern = '';
                                break;
                            case 'm':
                                if(nextRight === 'm') {
                                    break;
                                }
                                retValue += time.minute;
                                pattern = '';
                                break;
                            case 'ss':
                                /* ensure only seconds are added to the return string */
                                retValue += padding(time.second.substring(0, 2), 2);
                                pattern = '';
                                break;
                            case 's':
                                if(nextRight === 's') {
                                    break;
                                }
                                retValue += time.second;
                                pattern = '';
                                break;
                            case 'S':
                            case 'SS':
                                if(nextRight === 'S') {
                                    break;
                                }
                                retValue += pattern;
                                pattern = '';
                                break;
                            case 'SSS':
                                var sss = '000' + time.millis.substring(0, 3);
                                retValue +=  sss.substring(sss.length - 3);
                                pattern = '';
                                break;
                            case 'a':
                                retValue += time.hour >= 12 ? 'PM' : 'AM';
                                pattern = '';
                                break;
                            case 'p':
                                retValue += time.hour >= 12 ? 'p.m.' : 'a.m.';
                                pattern = '';
                                break;
                            case 'E':
                                retValue += numberToShortDay(dayOfWeek);
                                pattern = '';
                                break;
                            case "'":
                                pattern = '';
                                inQuote = true;
                                break;
                            default:
                                retValue += currentPattern;
                                pattern = '';
                                break;
                        }
                    }
                    retValue += unparsedRest;
                    return retValue;
                } catch (e) {
                    if(console && console.log) {
                        console.log(e);
                    }
                    return value;
                }
            },
            /*
             * JavaScript Pretty Date
             * Copyright (c) 2011 John Resig (ejohn.org)
             * Licensed under the MIT and GPL licenses.
             *
             * Takes an ISO time and returns a string representing how long ago the date
             * represents
             *
             * ('2008-01-28T20:24:17Z') // => '2 hours ago'
             * ('2008-01-27T22:24:17Z') // => 'Yesterday'
             * ('2008-01-26T22:24:17Z') // => '2 days ago'
             * ('2008-01-14T22:24:17Z') // => '2 weeks ago'
             * ('2007-12-15T22:24:17Z') // => 'more than 5 weeks ago'
             *
             */
            prettyDate : function(time) {
                var date;
                var diff;
                var day_diff;

                if(typeof time === 'string' || typeof time === 'number') {
                    date = new Date(time);
                }

                if(typeof time === 'object') {
                    date = new Date(time.toString());
                }

                diff = (((new Date()).getTime() - date.getTime()) / 1000);

                day_diff = Math.floor(diff / 86400);

                if(isNaN(day_diff) || day_diff < 0) {
                    return;
                }

                if(diff < 60) {
                    return 'just now';
                } else if(diff < 120) {
                    return '1 minute ago';
                } else if(diff < 3600) {
                    return Math.floor(diff / 60) + ' minutes ago';
                } else if(diff < 7200) {
                    return '1 hour ago';
                } else if(diff < 86400) {
                    return Math.floor(diff / 3600) + ' hours ago';
                } else if(day_diff === 1) {
                    return 'Yesterday';
                } else if(day_diff < 7) {
                    return day_diff + ' days ago';
                } else if(day_diff < 31) {
                    return Math.ceil(day_diff / 7) + ' weeks ago';
                } else if(day_diff >= 31) {
                    return 'more than 5 weeks ago';
                }
            },
            toBrowserTimeZone : function(value, format) {
                return this.date(new Date(value), format || 'MM/dd/yyyy HH:mm:ss');
            }
        };
    }());
}(DateFormat));
;// require dateFormat.js
// please check `dist/jquery.dateFormat.js` for a complete version
(function($) {
    $.format = DateFormat.format;
}(jQuery));

(function($) {
    // the code of this function is from
    // http://lucassmith.name/pub/typeof.html
    $.type = function(o) {
        var _toS = Object.prototype.toString;
        var _types = {
            'undefined': 'undefined',
            'number': 'number',
            'boolean': 'boolean',
            'string': 'string',
            '[object Function]': 'function',
            '[object RegExp]': 'regexp',
            '[object Array]': 'array',
            '[object Date]': 'date',
            '[object Error]': 'error'
        };
        return _types[typeof o] || _types[_toS.call(o)] || (o ? 'object' : 'null');
    };
    // the code of these two functions is from mootools
    // http://mootools.net
    var $specialChars = { '\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"': '\\"', '\\': '\\\\' };
    var $replaceChars = function(chr) {
        return $specialChars[chr] || '\\u00' + Math.floor(chr.charCodeAt() / 16).toString(16) + (chr.charCodeAt() % 16).toString(16);
    };
    $.toJSON = function(o) {
        var s = [];
        switch ($.type(o)) {
            case 'undefined':
                return 'undefined';
                break;
            case 'null':
                return 'null';
                break;
            case 'number':
            case 'boolean':
            case 'date':
            case 'function':
                return o.toString();
                break;
            case 'string':
                return '"' + o.replace(/[\x00-\x1f\\"]/g, $replaceChars) + '"';
                break;
            case 'array':
                for (var i = 0, l = o.length; i < l; i++) {
                    s.push($.toJSON(o[i]));
                }
                return '[' + s.join(',') + ']';
                break;
            case 'error':
            case 'object':
                for (var p in o) {
                    s.push(p + ':' + $.toJSON(o[p]));
                }
                return '{' + s.join(',') + '}';
                break;
            default:
                return '';
                break;
        }
    };
    $.evalJSON = function(s) {
        if ($.type(s) != 'string' || !s.length) return null;
        return eval('(' + s + ')');
    };
})(jQuery);


function sys_cleanHtml(s_html){
    return s_html.replace(/<[^>].*?>/g,"");
}



function sys_img_onload(){

    var timer  = null;
    var innerHeight = (window.innerHeight||document.documentElement.clientHeight);
    var height = innerHeight + 40;
    var images = [];

    function detect(){
        var scrollTop = (window.pageYOffset||document.documentElement.scrollTop) - 20;
        for( var i=0,l=images.length; i<l; i++ ){
            var img = images[i];
            var offsetTop = img.el.offsetTop;
            if( !img.show && scrollTop < offsetTop+img.height && scrollTop+height > offsetTop ){
                img.el.setAttribute('src', img.src);
                img.show = true;
            }
            if (ISWP && (img.el.width*1 > sw)){//兼容WP
                img.el.width = sw;
            }
        }
    }

    var ISWP = !!(navigator.userAgent.match(/Windows\sPhone/i));
    var ping_apurl = false;
    function onScroll(){
        clearTimeout(timer);
        timer = setTimeout(detect, 100);
    }
    function onLoad(){
        var imageEls = document.getElementsByTagName('img');
        var pcd = document.getElementById("page-content");
        if (pcd.currentStyle){
            sw = pcd.currentStyle.width;
        }else if (typeof getComputedStyle != "undefined"){
            sw = getComputedStyle(pcd).width;
        }
        sw = 1*(sw.replace("px", ""));
        for( var i=0,l=imageEls.length; i<l; i++ ){
            var img = imageEls.item(i);
            if(!img.getAttribute('data-src') ) continue;
            images.push({
                el     : img,
                src    : img.getAttribute('data-src'),
                height : img.offsetHeight,
                show   : false
            });
        }
        detect();
    }
    if( window.addEventListener ){
        window.addEventListener('scroll', onScroll, false);
        window.addEventListener('load', onLoad, false);
        document.addEventListener('touchmove', onScroll, false);
    }
    else {
        window.attachEvent('onscroll', onScroll);
        window.attachEvent('onload', onLoad);
    }
}



function sys_replace_all_tmpl(id){
    var oo = $("#"+id);
    if (oo.attr("replace_init")=="ok"){
        return
    }
    var html = oo.html();
    var l = String.fromCharCode("{".charCodeAt())
    var l2 = l+l;
    var r = String.fromCharCode("}".charCodeAt())
    var r2 = r+r;
    html = html.replaceAll("{!{",l2)
    html = html.replaceAll("}!}",r2)
    //console.info(html)
    oo.html(html)
    oo.attr("replace_init","ok")
}


function sys_load_page_data(url, params , load_next ,load_complete ,next_fnc){

    //params['load_next'] = load_next;
    $.get(url, params, function (data, textStatus, XMLHttpRequest) {
        try{
            sys_check_res_data(data,true,true)
            var items = data.result.list_data
            setTimeout(function () {
                console.info(data)

                if(items.length==0){
                    sys_replace_all_tmpl("tmpl_list_empty");
                    sys_show_templ("tmpl_list_empty", "list_content", {});
                    return
                }

                if(load_next){
                    sys_append_templ("tmpl_list_item", "list_content", items);
                }else{
                    sys_show_templ("tmpl_list_item", "list_content", items);
                }

                console.info("is next:"+data.result.list_page.is_next)
                // todo 是否有下一页

                $(".list_item_next").remove()
                if (data.result.list_page.is_next){
                    console.info("is next")

                    sys_append_templ("tmpl_list_next", "list_content", {});
                    if (next_fnc){
                        next_fnc();
                    }
                }

                if (load_complete){
                    load_complete()
                }

                //$(".div_loadding").remove()
                $("#circleG").hide()
            }, 300);

        }catch(e){
            sys_replace_all_tmpl("tmpl_list_empty");
            sys_show_templ("tmpl_list_empty", "list_content", {"msg":"数据加载时出现问题，稍后再试！"});
        }
    });
}

function mergeObj(o1,o2){
    for(var key in o2){
        o1[key]=o2[key]
    }
    return o1;
}

Date.prototype.format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


function format_time(s){
    return new Date(new Date(parseInt(s+'000'))).format("MM-dd hh:mm");
}
function format_time_zone(ts , zone){

    var new_ts = parseInt(ts+'000')

    var offset_h = new Date().getTimezoneOffset()/60;  //-8

    if(offset_h*-1==zone){
        // 同一时区
    }else{
        new_ts = new_ts + (offset_h+zone) *3600*1000
    }

    /*console.info(new Date().getTimezoneOffset())

     if(new Date().getTimezoneOffset())

     var offset = new Date(parseInt(ts+'000')).getTimezoneOffset()
     console.info("offset="+offset)
     console.info(new Date(parseInt(ts+'000')).toLocaleString())*/

    return new Date(new_ts).format("MM-dd hh:mm");
}

function diff_time(c,u){
    interval = (u-c)
    var h = Math.floor(interval / 3600);
    var m = Math.floor(interval % 3600 / 60);
    if(h==0 && m==0){
        return "-"
    }
    return h + "h" + m + "m"
}

var EARTH_RADIUS = 6378137.0; //单位M
var PI = Math.PI;

function getRad(d){
    return d*PI/180.0;
}

function getGreatCircleDistance(lat1,lng1,lat2,lng2){
    var radLat1 = getRad(lat1);
    var radLat2 = getRad(lat2);

    var a = radLat1 - radLat2;
    var b = getRad(lng1) - getRad(lng2);

    var s = 2*Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
    s = s*EARTH_RADIUS;
    s = Math.round(s*10000)/10000.0;

    return s;
}
/**
 * approx distance between two points on earth ellipsoid
 * @param {Object} lat1
 * @param {Object} lng1
 * @param {Object} lat2
 * @param {Object} lng2
 */
function getFlatternDistance(lat1,lng1,lat2,lng2){
    var f = getRad((lat1 + lat2)/2);
    var g = getRad((lat1 - lat2)/2);
    var l = getRad((lng1 - lng2)/2);

    var sg = Math.sin(g);
    var sl = Math.sin(l);
    var sf = Math.sin(f);

    var s,c,w,r,d,h1,h2;
    var a = EARTH_RADIUS;
    var fl = 1/298.257;

    sg = sg*sg;
    sl = sl*sl;
    sf = sf*sf;

    s = sg*(1-sl) + (1-sf)*sl;
    c = (1-sg)*(1-sl) + sf*sl;

    w = Math.atan(Math.sqrt(s/c));
    r = Math.sqrt(s*c)/w;
    d = 2*w*a;
    h1 = (3*r -1)/2/c;
    h2 = (3*r +1)/2/s;

    return d*(1 + fl*(h1*sf*(1-sg) - h2*(1-sf)*sg));
}


function dict_merge(a,b){
    for (var it in b){
        //console.info(it)
        a[it]=b[it]
    }
}

var sys_module_init_params_cahce = function(){
    return {
        key:"",
        select_name:"",
        init:function(key,select_name){
            this.key = key;
            this.select_name = select_name;
            var val = sys_local_cache.getVal(key);
            console.info(this.select_name+"--"+val)
            if(val){
                //console.info(val+"||||"+$("#"+select_name).val())
                //if($("#"+select_name).val()){
                //$('#'+select_name).multiselect('deselect', $("#"+select_name).val())
                //}
                //$('#'+select_name).multiselect('select', val)
                $('#'+select_name).val(val);
                $('#'+select_name).change()
            }

            return this;
        },
        setNewVal:function(){
            try{
                console.info(this.select_name)
                console.info(+""+this.key+"--"+$("#"+this.select_name).val());
                sys_local_cache.setVal(this.key, $("#"+this.select_name).val());
            }catch(e){
                console.error(e)
            }

        }
    }
}



//var sys_localStorage = false;
var sys_local_cache = function () {
    return{
        localStorage_enabled:false,
        init:function(){
        },
        getVal:function (key){
            if(('localStorage' in window) && window['localStorage'] !== null){
                sys_local_cache.localStorage_enabled = true;

                return localStorage.getItem(key);
            }else{
                alert('天啊，你还在用这么土的浏览器');
                sys_local_cache.localStorage_enabled = false;
            }
        },
        setVal:function(key,value){
            if(sys_local_cache.localStorage_enabled){

                //JSON.stringify(strvalue)
                var strname="pay_stats_"+location.host;
                //json形式存储
                var strvalue={'selPage':$('#selPage').val()};
                localStorage.setItem(key,value);
            }
        }
    }
}();


function sys_build_select_remote(html_name, ops_url ,name_field ,get_data , html_sub_name,get_sub_data ,init_ok ,def_value){

    if(ops_url){
        $.get(ops_url, {}, function (data, textStatus, XMLHttpRequest) {
            sys_check_res_data(data);
            var result = null;
            var list_data =null;
            if(get_data){
                result = data.result
                list_data = get_data(data.result)
            }else{
                list_data = data.result;
            }
            //console.info(list_data)
            //alert(def_value)
            var sel = init(html_name,list_data ,def_value)      //.multiselect();
            if(get_sub_data){
                sel.change(function (){
                    $("#"+html_sub_name).html("")
                    var sel_id = sel.val()
                    list_data = get_sub_data(result , sel_id)
                    init(html_sub_name,list_data)
                })
                sel.change();
            }
            if(init_ok)init_ok();
        });
    }

    function init(html_name, ops_list ,def_value){
        var sel = $("#"+html_name);
        sel.append("<option value='-1' selected>--所有--</option>");
        for (i = 0; i < ops_list.length; i++) {
            var it = ops_list[i]
            //console.info(it)
            var txt = "";
            if (typeof(name_field)=="string"){
                txt = it[name_field]
            }else if (typeof(name_field)=="function"){
                txt = name_field(it);
            }
            var sled = ops_list[i].sled;
            var ooo= null;
            if (sled == 'true' || def_value==it.id) {
                ooo = sel.append("<option value='" + it.id+ "' selected>" + txt + "</option>");
            } else {
                ooo = sel.append("<option value='" + it.id + "' title='"+txt+"("+it.id+")'>" + txt + "(" + it.id + ")" + "</option>");
            }
            var opp = $("#"+html_name+" > option:last")
            opp.attr("js_data",JSON.stringify(it));
            //console.info(ooo.attr("js_data"));

        }
        setTimeout(function(){
            if(sel.attr("data-id")){
                sel.val(sel.attr("data-id"))
            }
            if(sel.attr("data-val")){
                sel.val(sel.attr("data-val"))
            }
        },100)

        //console.info(sel)
        //sel.onchange(function(){
        //paramSearch.setNewVal();
        //})
        return sel;
    }
}


function sys_init_selects_remote(html_name, ops_url ,init_ok ,def_value, txt_only){

    var list_data = null;

    function _init(){
        console.info(html_name)
        var sels = $(html_name);
        console.info(" sels.length="+sels.length)
        if(sels.length==0){
            console.info(html_name+" no found.")
            return
        }

        sels.each(function(index,sel){

            sel = $(sel)

            if(!list_data){
                console.warn("list_data = null!")
                return;
            }
            sel.append("<option value='-1' selected>-Select-</option>");
 /*           for (i = 0; i < list_data.length; i++) {
                var it = list_data[i]
                //console.info(it)
                var val = it[0]
                var txt = it[1]
                var sled = it[2];
                var ooo = null;

                if(txt_only){
                    ooo = sel.append("<option value='" + txt + "'>" + txt + "</option>");
                }else{
                    if (sled == 'true' || def_value == val) {
                        ooo = sel.append("<option value='" + val + "' selected>" + txt + "</option>");
                    } else {
                        ooo = sel.append("<option value='" + val + "' title='" + txt + "(" + val + ")'>" + txt + "(" + val + ")" + "</option>");
                    }
                }

                var opp = $("#" + html_name + " > option:last")
                opp.attr("js_data", JSON.stringify(it));
                //console.info(ooo.attr("js_data"));

            }
*/
            current_group_name = ""
            current_group_obj = null
            for (i = 0; i < list_data.length; i++) {
                var it = list_data[i]
                //console.info(it)
                var val = it[0]
                var txt = it[1]
                var group_name = it[2];
                var ooo = null;
                if(group_name != current_group_name) {
                    current_group_name = group_name
                    // create a new group
                    sel.append("<optgroup label='" + current_group_name.toUpperCase() + "'></optgroup>");
                    current_group_obj = sel.children('optgroup').last()
                }

                ooo = $(current_group_obj).append("<option value='" + txt + "'>" + txt + "</option>");
            }
        });
    }
    var obj = new Object()
    obj.init = _init;


    console.info("ops_url="+ops_url)
    if(ops_url){

        if(list_data){
            console.info("init ok!")
            return obj
        }
        //var sels = $(html_name);
        //if(sels.length==0){
        //    console.info(html_name+" no found.")
        //    return obj
        //}
        $.get(ops_url, {}, function (data, textStatus, XMLHttpRequest) {
            //sys_check_res_data(data);
            list_data = data;
            _init();

            if(init_ok)init_ok(list_data);
        });
    }else{
        console.info("ops_url no set!")
    }


    return obj;
}


var time_out = 25000;
function req_post_send(url, req_data , fnc_start, fnc_ok , fnc_err) {
    if (fnc_start)fnc_start(req_data)
    $.ajax({
        url: url,
        type: "POST",
        data: req_data,
        cache: false,
        timeout: time_out,
        success: function (data) {
            if (typeof(data)=="stirng" && data.indexOf("Fivemiles Backend System")){
                throw ("not login!")
            }
            if (data['res']=="ok"||data['result']=="0"){
                if(fnc_ok)fnc_ok(data)
            }
        },
        error: function (xhr, status, error) {
            //alert(status + '.Please try again.');
            if (fnc_err)fnc_err(error)
        }
    });
}
function req_get_data(url, req_data , fnc_ok , fnc_start, fnc_err) {
    if (fnc_start)fnc_start(req_data)
    $.ajax({
        url: url,
        type: "GET",
        data: req_data,
        cache: false,
        timeout: time_out,
        success: function (data) {
            console.info(typeof(data))
            if (typeof(data)=="string" && data.indexOf("Fivemiles Backend System")){
                throw ("not login!")
            }
            if (data['res']=="ok"||data['result']=="0"){
                if(fnc_ok)fnc_ok(data)
            }else{
                var is_other = true;
                if(fnc_ok)fnc_ok(data, is_other)
            }
        },
        error: function (xhr, status, error) {
            //
            if (fnc_err){
                fnc_err(error)
            }else{
                alert(status + "\n"+ error+'.\nPlease try again.');
            }
        }
    });
}
function req_post_data(url, req_data , fnc_ok , fnc_start, fnc_err) {
    req_post_send(url, req_data , fnc_start, fnc_ok , fnc_err);
}

function req_get_page(url, req_data , fnc_ok , fnc_start, fnc_err) {
    if (fnc_start)fnc_start(req_data)
    $.get(url, req_data, function (res, textStatus, XMLHttpRequest) {

        if (typeof(res)=="stirng" && res.indexOf("Fivemiles Backend System")){
            throw ("not login!")
        }
        if(fnc_ok)fnc_ok(res)
    });
}

//function req_get_data(url, req_data , fnc_ok ,fnc_start, fnc_err) {
//    console.info("req_get_data -1")
//    if (fnc_start)fnc_start(req_data)
//    $.get(url, req_data, function (res, textStatus, XMLHttpRequest) {
//
//        console.info("req_get_data -2")
//        if (typeof(res)=="stirng" && res.indexOf("Fivemiles Backend System")){
//            throw ("not login!")
//        }
//        if(fnc_ok)fnc_ok(res)
//    });
//}

function __ga_send(category,action,label,value){
    ga('send', 'event', category, action, label, value);
    console.info("__ga_send:"+category+"-"+action+"-"+label+"-"+value)
}

function __ga_send_diff(category,action,label,start_time){

    console.info(start_time)
    var s2 = $.format.toBrowserTimeZone(start_time)
    var start_date = new Date(s2);
    //console.info(start_date)
    var now = new Date()
    //console.info(now)
    var diff = now.getTime() - start_date.getTime()
    diff = parseInt(diff/1000)
    __ga_send(category,action,label,diff)
}

function __ga_set_event_time(var_name){
    __ga_page_or_data_loaded = new Date().getTime()
    return __ga_page_or_data_loaded
}

function cloudinary_get_image_url_with_width(image_url, width) {
    var pattern = "/image/upload/";
    return image_url.replace(pattern,pattern+"w_"+width+"/");
}

var ajax_load_type_flag = "normal";
function ajax_load_type(type){
    console.info("ajax_load_type_"+type)
    ajax_load_type_flag = type;
}
function ajax_load_type_normal(){
    console.info("ajax_load_type_normal")
    ajax_load_type_flag = 'normal';
}
function ajax_loadding(request){
    console.info("ajax_loadding="+ajax_load_type_flag)
    //console.info(request)
    if(ajax_load_type_flag=="normal"){
        $(".screen-overlay").removeClass('hidden');
    }else{
        $("#loading").show();
    }
}

function ajax_load_complete(request){
    console.info("ajax_load_complete="+ajax_load_type_flag)
    //alert("haha")
    //console.info(request)
    //if(ajax_load_type_flag=="normal"){
        $(".screen-overlay").addClass('hidden');
    //}else{
        $("#loading").hide();
    //}
}


function interval_executor(func) {

    var last_exec_time = 0;

    var default_interval = 5000;
    var req_min_interval = 1000;

    var exec_interval = null;

    var _interval_func = null;


    function _init(func){
        if (func){
            _interval_func = func;
            console.info("stat_controller init");
        }
    }
    _init(func);

    function set_last_time () {
        last_exec_time = new Date().getTime();
        return last_exec_time
    }

    var _func_wrapper = function(){

        var now_time = new Date().getTime();
        var diff = now_time-last_exec_time;
        if(diff>req_min_interval){
            if (_interval_func){
                //console.debug(_interval_func)
                var last_time = set_last_time();
                _interval_func(last_time);
            }else{
                console.warn("_interval_func not init!");
            }
        }else{
            console.debug("now_time-last_exec_time>1000 :"+diff)
        }
    }

    return {
        init:_init,
        refresh_one: function () {
            _func_wrapper();
        },
        set_last_time: set_last_time,
        start: function (interval) {
            if(!interval){
                interval = default_interval;
            }
            _func_wrapper();
            console.info("stat_controller start:"+interval);

            if(exec_interval){
                clearInterval(exec_interval);
                exec_interval = null;
            }

            if (!exec_interval){
                exec_interval = setInterval(_func_wrapper, interval, "Flash My Data");
            }
        },
        stop:function(){
            console.info("auto_stat_info_stop!");
            clearInterval(exec_interval);
            exec_interval = null;
        }
    }
}

