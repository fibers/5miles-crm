$(document).on 'page:change', ->
  $('ul#country_tree').on 'click', ->
    checked_counties = {}
    country_tree = $.fn.zTree.getZTreeObj("country_tree")
    root_nodes = country_tree.getNodesByParam('level', 0, null)
    if root_nodes[0] and root_nodes[0].getCheckStatus().checked
      checked_counties[root_nodes[0].name] = {}
    else if root_nodes[1] and root_nodes[1].getCheckStatus().checked
      root_node = root_nodes[1]
      checked_counties[root_node.name] = {}
      if root_node.getCheckStatus().half
        checked_nodes = country_tree.getNodesByParam('checked', true, root_node)
        if checked_nodes[0] and checked_nodes[0].level == 1
          if checked_nodes[0].getCheckStatus().half == false
            for l1_node in checked_nodes
              do (l1_node) ->
                if l1_node.getCheckStatus().half == false and l1_node.level == 1
                  checked_counties[root_node.name][l1_node.name] = {}
          else
            l1_node = checked_nodes.shift()
            l1_node_name = l1_node.name
            checked_counties[root_node.name][l1_node_name] = {}
            if checked_nodes[0] and checked_nodes[0].level == 2
              if checked_nodes[0].getCheckStatus().half == false
                for l2_node in checked_nodes
                  do (l2_node) ->
                    if l2_node.getCheckStatus().half == false and l2_node.level == 2
                      checked_counties[root_node.name][l1_node_name][l2_node.name] = {}
              else
                l2_node = checked_nodes.shift()
                l2_node_name = l2_node.name
                checked_counties[root_node.name][l1_node_name][l2_node_name] = {}
                if checked_nodes[0] and checked_nodes[0].level == 3
                  for l3_node in checked_nodes
                    do (l3_node) ->
                      if l3_node.getCheckStatus().half == false and l3_node.level == 3
                        checked_counties[root_node.name][l1_node_name][l2_node_name][l3_node.name] = {}
    $(this).next().val(JSON.stringify(checked_counties))
