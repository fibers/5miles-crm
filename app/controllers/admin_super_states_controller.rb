class AdminSuperStatesController < ApplicationController

  before_action :find_admin, only: [:destroy, :create]

  def create
    log_info("Promote the admin '#{@admin.username}'.")
    if @admin.promote
      flash[:success] = "Promote the admin '#{@admin.username}' successfully."
    else
      flash[:error] = "Failed to Promote the admin '#{@admin.username}'."
    end
    redirect_to :back
  end

  def destroy
    log_info("Demote the admin '#{@admin.username}'.")
    if @admin.demote
      flash[:success] = "Demote the admin '#{@admin.username}' successfully."
    else
      flash[:error] = "Failed to Demote the admin '#{@admin.username}'."
    end
    redirect_to :back
  end

  private
  def find_admin
    @admin = Admin.find(params[:admin_id])
  end
end
