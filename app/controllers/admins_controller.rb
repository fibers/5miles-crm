class AdminsController < ApplicationController

  before_action :find_admin, except: [:index, :new, :create, :edit_item_risk_keywords, :update_item_risk_keywords, :edit_white_list, :update_white_list]
  before_action :is_super_admin, only: [:index, :new, :create, :edit, :show, :update, :destroy]
  before_action :is_admin_owner, only: [:password_edit, :password_update]
  before_action :get_item_risk_keywords, only: [:edit_item_risk_keywords]
  before_action :get_white_list, only: [:edit_white_list]

  def index
    @admins_grid = initialize_grid(session[:is_root_admin] ? Admin : Admin.where.not(username: "root"),
                                   per_page: Settings.USERS_PER_PAGE)
  end

  def new
    @admin = Admin.new
  end

  def create
    @admin = Admin.new(create_params)
    log_info("Create an admin '#{@admin.username}'.")
    if @admin.save
      redirect_to admins_path
    else
      render 'new'
    end

  end

  def edit

  end

  def show

  end

  def update
    if @admin.update(create_params)
      redirect_to admin_path(@admin)
    else
      render 'edit'
    end
  end

  def destroy
    log_info("Disable the admin '#{@admin.username}'.")
    if @admin.update(state: Settings.BACKEND_USER.States.Disabled)
      flash[:success] = "Disable the admin '#{@admin.username}' successfully."
    else
      flash[:error] = "Failed to Disable the admin '#{@admin.username}'."
    end
    redirect_to :back
  end

  def password_edit
    render 'password'
  end

  def password_update
    log_info("Update password of the admin '#{@admin.username}'.") unless @admin.nil?
    if @admin && @admin.authenticate(params[:old_password])
      @admin.password_confirmation = params[:password_confirmation]
      @admin.password = params[:password]
      if @admin.save
        flash[:success] = "Change password successfully"
        redirect_to index_path
        return
      else
        flash[:error] = "The two password fields didn't match"
      end
    else
      flash[:error] = "Invaild old password"
    end

    render 'password'
  end

  def edit_item_risk_keywords

  end

  def update_item_risk_keywords
    begin
      if params[:item_risk_keywords].blank? or params[:item_risk_keywords][:content].blank?
        flash[:error] = "Please input item risk keywords firstly."
        break
      end
      ret = BackendConfiguration.update_configuration(Settings.BackendConfigurations.ItemRiskKeywords, params[:item_risk_keywords][:content])
      if ret
        flash[:success] = "Update item risk keywords successfully"
      else
        flash[:error] = "Failed to update item risk keywords"
      end
    end while false

    redirect_to item_risk_keywords_admins_path
  end

  def edit_white_list

  end

  def update_white_list
    begin
      if params[:white_list].blank? or params[:white_list][:content].blank?
        flash[:error] = "Please input user id(s) firstly."
        break
      end
      ret = BackendConfiguration.update_white_list(Settings.BackendConfigurations.WhiteList, params[:white_list][:content])
      if ret
        flash[:success] = "Update white list successfully."
      else
        flash[:error] = "Failed to update white list."
      end
    end while false

    redirect_to white_list_admins_path
  end

  private

  def find_admin
    @admin = Admin.find(params[:id])
  end

  def create_params
    admin = params.require(:admin).permit(:username, :password, :is_super_admin, :user_type, :review_roles, :email)
    if params[:is_bi_user].present?
      admin['bi_level'] = 1
    else
      admin['bi_level'] = 0
    end
    return admin
  end

  def is_super_admin
    unless session[:is_super_admin]
      flash[:error] = "You do not have permission to access this page"
      redirect_to root_path
    end
  end

  def is_admin_owner
    unless session[:current_admin_id] == @admin.id
      flash[:error] = "You do not have permission to access this page"
      redirect_to root_path
    end
  end

  def get_item_risk_keywords
    @item_risk_keywords = BkConfiguration.find_by_name(Settings.BackendConfigurations.ItemRiskKeywords)
  end

  def get_white_list
    @white_list = BkConfiguration.find_by_name(Settings.BackendConfigurations.WhiteList)
  end
end
