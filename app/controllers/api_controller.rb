class ApiController < ApplicationController

  def country
    position_data = []
    parent_id = nil
    isParent = true

    case params[:level].to_i
      when 0
        parent_id = -1
        isParent = true
      when 1
        parent_id = params[:id].to_i
        isParent = true
      when 2
        parent_id = params[:id].to_i
        isParent = false
    end

    if parent_id.present?
      position_data = Position.where(parent_id: parent_id).select(:id, :parent_id, :name).order(display_order: :desc).order(:name).map do |position|
        {id: position.id, pId: position.parent_id, name: position.name, isParent: isParent}
      end
    end

    puts position_data.inspect

    respond_to do |format|
      format.json {
        render :json => position_data
      }
    end
  end

  def category
    ret = {}
    case params[:event]
      when Settings.RZTREE.EventType.TYPE_RENAME
        if EntityCategory.find_by_title(params[:newName])
          ret['error'] = "Category \"#{params[:newName]}\" had already existed."
        else
          category_params = {}
          category_params['id'] = params[:id].to_i
          category_params['parent_id'] = params[:pId].to_i
          category_params['title'] = params[:name]
          log_info("Rename Event: category name(#{category_params.inspect}) successfully")
          categories = EntityCategory.where(category_params)
          if categories.present?
            if categories[0].update(title: params[:newName])
              log_info("Update category name(\"#{params[:name]}\" => \"#{params[:newName]}\") successfully")
            else
              ret['error'] = "Failed to update category name(\"#{params[:name]}\" => \"#{params[:newName]}\")"
            end
          else
            ret['error'] = "Category \"#{params[:name]}\" dose not found."
          end
        end
      when Settings.RZTREE.EventType.TYPE_DROP
        src_nodes = params[:src_nodes].values()
        change_id_list = src_nodes.map { |node| node['id'] }
        target_node = params[:target_node]
        target_node_id = target_node['id']
        move_type = params[:move_type]
        is_copy = params[:is_copy]
        category_hash = {}
        category_id_list = []
        EntityCategory.unscoped.all.order(:order_num).each do |category|
          key = category.id.to_s
          category_hash[key] = category
          category_id_list << key
        end
        log_info("Before update: category_id_list = #{category_id_list.inspect}")
        category_id_list = category_id_list - change_id_list
        case move_type
          when Settings.RZTREE.MoveType.TYPE_INNER
            ret['error'] = "No support \"#{'inner type'}\""
          when Settings.RZTREE.MoveType.TYPE_PREV
            change_id_list << target_node_id
            puts 'prev type'
          when Settings.RZTREE.MoveType.TYPE_NEXT
            change_id_list.unshift(target_node_id)
            puts 'next type'
          else
            ret['error'] = "No move type(\"#{move_type}\")"
        end
        if ret['error'].blank?
          category_ordered_id_list = []
          category_id_list.each do |id|
            if id == target_node_id
              category_ordered_id_list += change_id_list
            else
              category_ordered_id_list << id
            end
          end
          category_ordered_id_list.each_with_index do |id, index|
            category_hash[id].order_num = index + 1
          end
          begin
            EntityCategory.transaction do
              for category in category_hash.values()
                category.save!()
              end
            end
            log_info("Update order number of category successfully.")
          rescue Exception => e
            ret['error'] = "Failed to update order number of category"
          end
        end
      # when Settings.RZTREE.EventType.TYPE_ADD
      #   pid = params[:pId]
      #   prev_id = params[:prevId]
      #   name = params[:name]
      #   category_params = {}
      #   category_params['parent_id'] = pid.to_i
      #   category_params['title'] = name
      #   categories = EntityCategory.where(category_params)
      #   if categories.blank?
      #     category_params['short_url'] = ''
      #     category_params['gender'] = Settings.USER_GENDER.Unknown
      #     category_params['weight'] = 0
      #     begin
      #       EntityCategory.transaction do
      #         category_hash = {}
      #         category_id_list = []
      #         EntityCategory.unscope(:order).all.order(:order_num).each do |category|
      #           key = category.id.to_s
      #           category_hash[key] = category
      #           category_id_list << key
      #         end
      #         max_id = EntityCategory.unscope(:order).order(id: :desc).limit(1).pluck(:id)
      #         if max_id.present?
      #           max_id = max_id[0]
      #           category_params['id'] = max_id + 1
      #         end
      #         new_category = EntityCategory.create!(category_params)
      #         category_ordered_id_list = []
      #         category_id_list.each do |id|
      #           category_ordered_id_list << id
      #           if id == prev_id
      #             category_ordered_id_list << new_category.id.to_s
      #           end
      #         end
      #         category_hash[new_category.id.to_s] = new_category
      #         category_ordered_id_list.each_with_index do |id, index|
      #           category_hash[id].order_num = index + 1
      #         end
      #         for category in category_hash.values()
      #           category.save!()
      #         end
      #         url = URI('/products')
      #         ret['data'] = EntityCategory.select(:id, :parent_id, :title).valid.map do |category|
      #           url.query = URI.encode_www_form([['grid[f][entity_category.id][]',category.id]])
      #           {id: category.id, pId: category.parent_id, name: category.title, target: '_blank', url: url.to_s}
      #         end
      #         ret['data'].unshift({id: Settings.TOP_CATEGORY['Top Category'], pId: nil, name: 'Root', open: true})
      #       end
      #     rescue Exception => e
      #       ret['error'] = "Failed to create category(\"#{name}\")"
      #     end
      #   else
      #     ret['error'] = "\"#{params[:event]}\" had already existed"
      #   end
      else
        ret['error'] = "No event(\"#{params[:event]}\")"
    end

    if ret['error'].present?
      log_info(ret['error'])
    end

    respond_to do |format|
      format.json {
        render :json => ret
      }
    end
  end
end
