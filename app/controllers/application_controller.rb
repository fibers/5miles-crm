class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  #http://dev.maxmind.com/geoip/legacy/codes/iso3166/
  @@COUNTRIES = [Settings.INTERNAL_USERS_STRING, Settings.ALL_USERS_STRING] + Country.all.map(&:first).sort

  rescue_from ActionController::InvalidAuthenticityToken do |exception|
    respond_to do |format|
      flash[:warning] = "No operation too long time, please try again."
      format.html { redirect_to :back }
      format.js { render 'csrf_token_error' }
    end
  end

  # rescue_from ActiveRecord::StatementInvalid do |exception|
  #   respond_to do |format|
  #     format.js { render 'active_record_error' }
  #   end
  # end if Rails.env != 'development'

  protect_from_forgery with: :exception

  before_action :trace_log_before_action

  before_action :login?, except: [:login, :create, :fetch_product_feed] # if Rails.env != 'development'

  before_action :set_current_employee

  after_action :trace_log_after_action

  private
  def login?
    if request.path != login_path and session[:current_admin_id].blank?
      if request.get?
        session[:previous_url] = request.fullpath
      elsif request.referer.present? and request.referer != login_path
        session[:previous_url] = request.referer
      end
      redirect_to login_path
    end
  end

  def raven_capture_exception_not_raise(&block)
    begin
      block.call
    rescue Exception => e
      Raven.capture_exception(e)
      raise if Rails.env == 'development'
    end
  end

  def set_current_employee
    EmployeeManager.instance.current_employee_id = session[:current_admin_id].to_i
    EmployeeManager.instance.current_employee_name = session[:current_admin_username]
  end

  def trace_log_prefix
    user_id = nil
    method = nil
    if request.request_parameters.present? and request.request_parameters['_method']
      method = request.request_parameters['_method'].upcase
    else
      method = request.method
    end
    if defined?(session) and session[:current_admin_id]
      user_id = session[:current_admin_id]
    else
      user_id = 'NoLogIn'
    end
    "User(#{user_id}) #{method} #{request.fullpath} from #{request.ip} at #{Time.now.utc.iso8601}"
  end

  def log_info(message)
    Rails.logger.info "log_info #{trace_log_prefix} #{message}"
  end

  def trace_log_before_action
    if request.post? or request.put? or request.patch? or request.delete?
      Rails.logger.info "trace_log_before_action #{trace_log_prefix}"
    end
  end

  def trace_log_after_action
    if request.post? or request.put? or request.patch? or request.delete?
      if flash.present?
        flash.keys.each do |key|
          Rails.logger.info "trace_log_after_action #{trace_log_prefix} #{flash[key]}"
        end
      end
    end
  end

  def new_hash(array, default_value)
    r = {}
    array.each { |key| r[key] = default_value }
    return r
  end

  def ztree_country_tree_get_countries(ztree_country_tree_data)
    target_countries = {}
    if ztree_country_tree_data.present?
      data = JSON.load(ztree_country_tree_data)
      if data.keys().include?(Settings.INTERNAL_USERS_STRING)
        target_countries[Settings.INTERNAL_USERS_STRING] = true
      elsif data.keys().include?(Settings.ALL_USERS_STRING)
        countries = data[Settings.ALL_USERS_STRING].keys()
        if countries.blank?
          target_countries[Settings.ALL_USERS_STRING] = true
        else
          target_countries['country'] = countries
          target_countries['region'] = []
          target_countries['city'] = []
          for country in countries
            regions = data[Settings.ALL_USERS_STRING][country].keys()
            target_countries['region'] |= regions
            for region in regions
              cities = data[Settings.ALL_USERS_STRING][country][region].keys()
              target_countries['city'] |= cities
            end
          end
          if target_countries['city'].present?
            bcs = BUSINESS_CIRCLE_DATA.keys()
            union_bcs = target_countries['city'] & bcs
            if union_bcs.present?
              target_countries['city'] -= union_bcs
              for bc in union_bcs
                target_countries['city'] |= BUSINESS_CIRCLE_DATA[bc]
              end
            end
          end
        end
      end
      log_info("Target country, region and city: #{target_countries.inspect}")
    end
    return target_countries
  end

  def generate_pagination_params(total, offset_current_page, offset_next_page, limit_per_page)
    offset_previous_page = offset_current_page - limit_per_page
    if offset_previous_page < 0
      offset_previous_page = 0
    end
    pagination_data = {}
    pagination_data['pagination'] = {}
    pagination_data['pagination']['total'] = total
    pagination_data['pagination']['current']= {}
    pagination_data['pagination']['current']['offset'] = offset_current_page
    pagination_data['pagination']['previous'] = {}
    pagination_data['pagination']['previous']['offset'] = offset_previous_page
    pagination_data['pagination']['next'] = {}
    pagination_data['pagination']['next']['offset'] = offset_next_page
    return pagination_data
  end

end
