class CategoriesController < ApplicationController

  before_action :find_category, only: [:edit, :update, :show, :publish_category, :draft_category, :disable_category, :enable_category]
  before_action :all_category_for_select, only: [:index, :edit, :new]

  def index
    @categories_grid = initialize_grid(
        EntityCategory,
        order: 'entity_category.order_num',
        order_direction: 'asc',
        include: [:parent],
        per_page: Settings.CATEGORIES_PER_PAGE)
    @all_category_for_display = @all_category_for_select.invert
  end

  def treeview
    url = URI('/products')
    @category_tree_data = EntityCategory.select(:id, :parent_id, :title).valid.map do |category|
      url.query = URI.encode_www_form([['grid[f][entity_category.id][]', category.id]])
      {id: category.id, pId: category.parent_id, name: category.title, target: '_blank', url: url.to_s}
    end
    @category_tree_data.unshift({id: Settings.TOP_CATEGORY['Top Category'], pId: nil, name: 'Root', open: true})
  end

  def new
    @category = EntityCategory.new
  end

  def create
    category_params = get_category_params()
    @category = EntityCategory.new(category_params)
    if @category.save
      flash[:success] = "Created '#{@category.title}' successfully."
      redirect_to category_path(@category)
    else
      flash[:error] = "Failed to create '#{@category.title}'"
      render 'new'
    end
  end

  def update
    category_params = get_category_params()
    log_info("Update the category '" + @category.title + "'")

    weight = category_params.delete(:weight)
    if weight.present?
      weight = weight.to_i
      if weight < Settings.WEIGHT.Min or weight > Settings.WEIGHT.Max
        flash[:warning] = 'Weight value is invalid.'
      end
    end

    if flash[:warning].present?
      render 'edit'
    else
      if @category.update(category_params)
        if weight != @category.weight
          ret = BackendApiREST.batch_update_category_weight(@category.id, weight)
          if ret[1].present?
            ret = JSON.load(ret[1])
            if ret["meta"]["result"] == "ok"
              flash[:success] = "Update the category '" + @category.title + "' successfully."
            else
              flash[:error] = "Failed to update some product's category weight(ids: #{ret["objects"]})."
            end
            redirect_to category_index_path
          else
            flash[:error] = "Failed to update category weight attribute"
            render 'edit'
          end
        else
          flash[:success] = "Update the category '" + @category.title + "' successfully."
          redirect_to category_index_path
        end
      else
        render 'edit'
      end
    end
  end

  def edit
  end

  def publish_category
    begin
      if @category.status == Settings.CATEGORY_STATUSES.Published
        flash[:success] = "Category '#{@category.title}' had been in publish status."
      else
        if @category.update(status: Settings.CATEGORY_STATUSES.Published, publish_date: Time.current)
          flash[:success] = "Publish category '#{@category.title}' successfully."
        else
          flash[:error] = "Failed to publish category '#{@category.title}'"
        end
      end
    end while false
    redirect_to :back
  end

  def draft_category
    begin
      if @category.status == Settings.CATEGORY_STATUSES.Draft
        flash[:success] = "Category '#{@category.title}' had been in draft status."
      else
        if @category.update(status: Settings.CATEGORY_STATUSES.Draft)
          flash[:success] = "Draft category '#{@category.title}' successfully."
        else
          flash[:error] = "Failed to draft category '#{@category.title}'"
        end
      end
    end while false
    redirect_to :back
  end

  def disable_category
    begin
      if @category.status == Settings.CATEGORY_STATUSES.Invalid
        flash[:success] = "Category '#{@category.title}' had been in disable status."
      else
        if @category.update(status: Settings.CATEGORY_STATUSES.Invalid)
          flash[:success] = "Disable category '#{@category.title}' successfully."
        else
          flash[:error] = "Failed to disable category '#{@category.title}'"
        end
      end
    end while false
    redirect_to :back
  end

  def enable_category
    begin
      if @category.status != Settings.CATEGORY_STATUSES.Invalid
        flash[:error] = "Category '#{@category.title}' had not been in disable status."
      else
        if @category.update(status: Settings.CATEGORY_STATUSES.Draft)
          flash[:success] = "Enable category '#{@category.title}' successfully."
        else
          flash[:error] = "Failed to enable category '#{@category.title}'"
        end
      end
    end while false
    redirect_to :back
  end

  private
  def find_category
    @category = EntityCategory.find(params[:id])
  end

  def all_category_for_select
    @all_category_for_select = Settings.TOP_CATEGORY.merge(EntityCategory.pluck(:title, :id).to_h)
  end

  def get_category_params
    params.require(:category).permit(:title, :description, :gender, :weight, :icon)
  end

end
