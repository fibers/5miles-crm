require 'net/http'
require 'uri'
require 'json'

require 'http_utils'

class EsController < ApplicationController

  @@url = 'http://52.64.55.110:8080/fivemiles/_search'
  #@@url = 'http://127.0.0.1:9200/fivemiles/_search'

  def index
    render "index" #,layout: false
  end

  def list

    query = params[:q]
    query_type = params[:query_type]
    lat = params[:lat]
    lon = params[:lon]
    type = params[:type]
    now_ts = Time.current.to_i #1436776425
    puts now_ts
    cat_id = params[:cat_id]

    if params[:limit].present?
      limit = params[:limit].to_f
    else
      limit = 20
    end
    if params[:offset].present?
      offset = params[:offset].to_f
    else
      offset = 0
    end

    weight0_query_body = <<-EOF
      {"filter": {"and": [{"range": {"weight": {"lte": 0}}}]}}
    EOF

    ctr_query_body = <<-EOF

      {"sort": [
        {
          "user_ctr": {
            "order": "desc"
          }
        }
      ],"filter": {"and": [{
            "range": {
              "created_at": {
                "gte": 1427777328
              }
            }
          },
      {
            "exists": {
              "field": "user_ctr"
            }
          }
      ]}}

    EOF

    q1_query_body = <<-EOF
      {
          "query": {
            "filtered": {
              "query": {
                "multi_match": {
                  "query": "a",
                  "type": "best_fields",
                  "fields": [
                    "title^10",
                    "brand_name",
                    "city"
                  ]
                }
              }
            }
          }
      }
    EOF

    # "tie_breaker": 0.3,
    # "minimum_should_match": "75%",

    q2_query_body = <<-EOF
      {
          "query": {
            "filtered": {
              "query": {
                "multi_match": {
                  "query": "a",
                  "type": "most_fields",
                  "fields": [
                    "title^10",
                    "brand_name",
                    "city"
                  ]
                }
              }
            }
          }
      }

    EOF

    cat_query_body = <<-EOF
      {"sort": [], "query": {"function_score": {"query": {
      "filtered": {"filter": {"and": [{"range": {"weight": {"gt": 0}}}, {"term": {"cat_id":  #{cat_id}}},
        {"term": {"owner.is_robot": 0}}]}}},
      "functions": [{"gauss": {"weight": {"origin": 10, "decay": 0.2, "scale": 3, "offset": 5}}},
        {"gauss": {"updated_at": {"origin": #{now_ts}, "scale": "172800"}}},
        {"gauss": {"location": {"origin": "#{lat},#{lon}", "decay": 0.22, "scale": "20mi", "offset": "20mi"}}}]}},
      "from": 40, "aggs": {"group_by_refind": {"terms": {"field": "cat_id"}}}, "size": 20}
    EOF

    if type=='weight_0'
      query_body = weight0_query_body
      query_json = JSON.parse(query_body)
    elsif type=='query_cat'
      query_body = cat_query_body
      puts query_body
      query_json = JSON.parse(query_body)
    elsif type=='ctr'
      query_body = ctr_query_body
      query_json = JSON.parse(query_body)
    elsif type=='query_best_fields'
      query_body = q1_query_body
      query_json = JSON.parse(query_body)
      query_json["query"]["filtered"]["query"]["multi_match"]["query"]=query
    elsif type=='query_most_fields'
      query_body = q2_query_body
      query_json = JSON.parse(query_body)
      query_json["query"]["filtered"]["query"]["multi_match"]["query"]=query
    end


    query_json['size'] = limit
    query_json['from'] = offset

    puts query_json.to_json

    next_val = "?offset=#{offset+limit}"

    res = HttpUtils.send_data(@@url, query_json, query_json.to_json)
    res_json = JSON.parse(res)

    puts res_json.to_json

    total = res_json['hits']['total']
    objects = JSON.parse(res)['hits']['hits']

    #render :json => res, :callback => 'show'
    render :json => {"meta"=>{"next"=>next_val,"total"=>total}, "objects"=>objects}

  end


end
