class Fmmc::CronsController < ApplicationController

  before_action :find_cron, only: [:edit, :update, :enable, :disable]

  def index
    @cron_grid = initialize_grid(FmmcCron,
                                 include: [:frequency, :task],
                                 order: 'fmmc_cron.status',
                                 order_direction: 'desc',
                                 per_page: Settings.FMMC_PER_PAGE)
  end


  def edit

  end

  def new
    @cron = FmmcCron.new
  end

  def update
    if @cron.update(update_params)
      redirect_to fmmc_crons_path
    else
      render 'edit'
    end
  end

  def create
    @cron = FmmcCron.new(create_params)
    log_info("Create a cron '" + @cron.name + "'.")

    if @cron.save
      redirect_to fmmc_crons_path
    else
      render 'new'
    end
  end

  def enable
    @cron.status = Settings.FMMC_STATUSES.Enabled
    if @cron.save
      flash[:success] = "Enable the cron template '#{@cron.name}' successfully."
    else
      flash[:error] = "Failed to Enable the cron template '#{@cron.name}'."
    end
    redirect_to :back
  end

  def disable
    @cron.status = Settings.FMMC_STATUSES.Disabled
    if @cron.save
      flash[:success] = "Disable the cron template '#{@cron.name}' successfully."
    else
      flash[:error] = "Failed to disable the cron template '#{@cron.name}'."
    end
    redirect_to :back

  end

  private
  def find_cron
    @cron = FmmcCron.find(params[:id])
  end

  def create_params
    params.require(:cron).permit(:name, :status, :comment, :frequency_period_id, :frequency_quantity, :at, :tz, :task_id)
  end

  def update_params
    params.require(:cron).permit(:name, :status, :comment, :frequency_period_id, :frequency_quantity, :at, :tz, :task_id)
  end

end
