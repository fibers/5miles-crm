class Fmmc::TasksController < ApplicationController

  before_action :find_task, only: [:edit, :update, :enable, :disable, :show]
  before_action :options_for_country_select, only: [:new, :edit, :create]

  def index
    @task_grid = initialize_grid(FmmcTask.normal_scope,
                                 include: [:mail_template, :push_template, :system_message_template],
                                 order: 'fmmc_task.status',
                                 order_direction: 'desc',
                                 per_page: Settings.FMMC_PER_PAGE)
  end


  def edit

  end

  def new
    @task = FmmcTask.new
  end

  def update
    if @task.update(update_params)
      redirect_to fmmc_tasks_path
    else
      render 'edit'
    end
  end

  def create
    @task = FmmcTask.new(create_params)
    log_info("Create a task '" + @task.name + "'.")

    if @task.save
      redirect_to fmmc_tasks_path
    else
      render 'new'
    end
  end

  def show

  end

  def enable
    @task.status = Settings.FMMC_STATUSES.Enabled
    if @task.save
      flash[:success] = "Enable the task template '#{@task.name}' successfully."
    else
      flash[:error] = "Failed to Enable the task template '#{@task.name}'."
    end
    redirect_to :back
  end

  def disable
    @task.status = Settings.FMMC_STATUSES.Disabled
    if @task.save
      flash[:success] = "Disable the task template '#{@task.name}' successfully."
    else
      flash[:error] = "Failed to disable the task template '#{@task.name}'."
    end
    redirect_to :back

  end

  private
  def find_task
    @task = FmmcTask.find(params[:id])
  end

  def create_params
    params.require(:task).permit(:name, :clazz, :status, :comment, :mail_template_id, :push_template_id, :system_message_template_id, :target_country)
  end

  def update_params
    params.require(:task).permit(:name, :clazz, :status, :comment, :mail_template_id, :push_template_id, :system_message_template_id, :target_country)
  end

  def options_for_country_select
    @options_for_country_select = [Settings.INTERNAL_USERS_STRING] + Country.all.map(&:first).sort
  end
end
