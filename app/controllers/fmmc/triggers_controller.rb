class Fmmc::TriggersController < ApplicationController

  before_action :find_trigger, only: [:edit, :update, :enable, :disable]

  def index
    @trigger_grid = initialize_grid(FmmcTrigger.normal_scope,
                                    order: 'fmmc_trigger.updated_at',
                                    order_direction: 'desc',
                                    include: [:period, :task],
                                    per_page: Settings.FMMC_PER_PAGE)
  end


  def edit

  end

  def new
    @trigger = FmmcTrigger.new
  end

  def update
    if @trigger.update(update_params)
      redirect_to fmmc_triggers_path
    else
      render 'edit'
    end
  end

  def create
    @trigger = FmmcTrigger.new(create_params)
    log_info("Create a trigger '" + @trigger.name + "'.")

    if @trigger.save
      redirect_to fmmc_triggers_path
    else
      render 'new'
    end
  end

  def enable
    @trigger.status = Settings.FMMC_STATUSES.Enabled
    if @trigger.save
      flash[:success] = "Enable the trigger template '#{@trigger.name}' successfully."
    else
      flash[:error] = "Failed to Enable the trigger template '#{@trigger.name}'."
    end
    redirect_to :back
  end

  def disable
    @trigger.status = Settings.FMMC_STATUSES.Disabled
    if @trigger.save
      flash[:success] = "Disable the trigger template '#{@trigger.name}' successfully."
    else
      flash[:error] = "Failed to disable the trigger template '#{@trigger.name}'."
    end
    redirect_to :back

  end

  private
  def find_trigger
    @trigger = FmmcTrigger.find(params[:id])
  end

  def create_params
    params.require(:trigger).permit(:name, :task_id, :status, :comment, :interval_period_id, :interval_quantity, :at, :trigger_type)
  end

  def update_params
    params.require(:trigger).permit(:name, :task_id, :status, :comment, :interval_period_id, :interval_quantity, :at, :trigger_type)
  end
end
