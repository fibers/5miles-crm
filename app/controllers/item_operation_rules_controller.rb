class ItemOperationRulesController < ApplicationController

  before_action :get_item_operation_rule, only: [:show, :edit, :update, :disable, :enable]

  def index
    @rules_grid = initialize_grid(
        ItemOperationRule,
        per_page: Settings.TASKS_PER_PAGE)
  end

  def new
    @rule = ItemOperationRule.new()
    @auto_complete_params = {}
    @show_type_checked_states = populate_checked_states(0)
  end

  def create
    @rule = ItemOperationRule.new(rule_params)

    if @rule.save()
      redirect_to item_operation_rules_path
    else
      render 'new'
    end
  end

  def show

  end

  def edit
    @auto_complete_params = {delay_time: @rule.delay_time,
                             action_type: @rule.action_type,
                             target_weight: @rule.target_weight,
                             target_rule_id: @rule.target_rule_id}

    # populate the check box checked_state
    @show_type_checked_states = populate_checked_states(@rule.show_type)
  end

  def update
    if @rule.update(rule_params)
      redirect_to item_operation_rule_path(@rule)
    else
      render 'edit'
    end
  end

  def disable
    @rule.status = 0
    @rule.save()

    redirect_to item_operation_rules_path
  end

  def enable
    @rule.status = 1
    @rule.save()

    redirect_to item_operation_rules_path
  end

  private
  def rule_params
    rule = params.require(:rule).permit(:name, :is_keep_audit_state,
                                        :action, :lower_weight_value,
                                        :notify_user, :push_message,
                                        :email_title, :email_and_system_message,
                                        :delay_time, :action_type,
                                        :target_weight, :target_rule_id)
    rule[:target_weight] = nil unless rule.has_key?(:target_weight)
    rule[:target_rule_id] = nil unless rule.has_key?(:target_rule_id)

    # transform show type name to value (default: 0)
    rule[:show_type] = trans_show_type_value(params[:show_type_names])

    return rule
  end

  def get_item_operation_rule
    @rule = ItemOperationRule.find(params[:id])
  end

  def trans_show_type_value(show_type_names)
    if show_type_names.nil?
      return 0
    else
      return ((show_type_names[:do_not_show].nil? ? 0 : show_type_names[:do_not_show].to_i) +
          (show_type_names[:image_audit].nil? ? 0 : show_type_names[:image_audit].to_i) +
          (show_type_names[:often_use].nil? ? 0 : show_type_names[:often_use].to_i))
    end
  end

  def populate_checked_states(show_type_value)
    show_type_checked_states = {}
    value = show_type_value.to_i
    if value == 0
      show_type_checked_states = {do_not_show: false, image_audit: false, often_use: false}
    else
      show_type_checked_states[:do_not_show] = value & Settings.ItemOperationRules.ShowType.DoNotShow == 0 ? false : true
      show_type_checked_states[:image_audit] = value & Settings.ItemOperationRules.ShowType.ImageAudit == 0 ? false : true
      show_type_checked_states[:often_use] = value & Settings.ItemOperationRules.ShowType.OftenUse == 0 ? false : true
    end
    show_type_checked_states
  end
end
