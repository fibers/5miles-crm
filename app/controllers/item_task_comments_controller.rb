class ItemTaskCommentsController < ApplicationController

  def create
    puts 'comment params:', comment_params

    # render plain: params.to_s
    @task = ItemTask.find(params[:item_task_id])
    @comment = @task.comments.create(comment_params)

    redirect_to item_task_path(@task)
  end

  private
  def comment_params
    params[:item_task_comment]['commenter'] = session['current_admin_username']
    params.require(:item_task_comment).permit(:commenter, :comment)
  end

end
