class ItemTasksController < ApplicationController

  def index
    @task_type_counts = new_hash(ItemOperationRule.operation_types, 0)
    counts = ItemTask.where.not(state: Settings.ITEM_TASK_STATE.Completed).group(:task_type).count
    @task_type_counts = @task_type_counts.merge(counts)

    @pending_process_task_count = ItemTask.where(state: [Settings.ITEM_TASK_STATE.ActionExecuted, Settings.ITEM_TASK_STATE.Created]).select(:id).count()

    prev_24_hours_datetime = Time.current - 24.hours
    @execute_action_more_24h_task_count = ItemTask.where(state: Settings.ITEM_TASK_STATE.ActionExecuted)
                                              .where("item_tasks.execute_action_at < ?", prev_24_hours_datetime).count()
    if params[:execute_action_more_24h_tasks].present?
      params[:grid][:f][:execute_action_at] = {}
      params[:grid][:f][:execute_action_at][:to] = prev_24_hours_datetime - 1.day
    end

    @item_tasks_grid = initialize_grid(
        self.get_products_by_state(),
        order: 'item_tasks.created_at',
        order_direction: 'desc',
        per_page: Settings.TASKS_PER_PAGE)
  end

  def get_products_by_state
    if params[:grid] and params[:grid][:f] and params[:grid][:f][:state]
      return ItemTask.includes(:item, :user)
    else
      return ItemTask.includes(:item, :user).where.not(state: Settings.ITEM_TASK_STATE.Completed)
    end
  end

  def create
    rule = ItemOperationRule.find(params[:operation_rule_id])
    is_success_flag, message, task = Utils::BkItemTaskOps.create(rule,
                                                                 params[:product_id],
                                                                 session['current_admin_username'])
    if is_success_flag
      flash[:success] = message
      redirect_to item_task_path(task)
    else
      flash[:error] = message
      redirect_to :back
    end
  end

  def create_and_execute
    rule = ItemOperationRule.find(params[:operation_rule_id])
    is_success_flag, message, task = Utils::BkItemTaskOps.create(rule,
                                                                 params[:product_id],
                                                                 session['current_admin_username'])
    unless is_success_flag
      flash[:error] = message
      redirect_to :back
    else
      push_message = task.rule.push_message
      email_title = task.rule.email_title
      email_and_system_message = task.rule.email_and_system_message
      is_success_flag, message = Utils::BkItemTaskOps.execute_action(task, session['current_admin_username'], push_message, email_title, email_and_system_message)
      if is_success_flag
        flash[:success] = message
      else
        flash[:error] = message
      end
      redirect_to item_task_path(task)
    end
  end

  def show
    @task = ItemTask.find(params[:id])
    @product = @task.item
    if @product.state == Settings.PRODUCT_STATUSES.Unavailable and @task.state != Settings.ITEM_TASK_STATE.Completed
      #   如果用户删除了该商品，那么自动关闭该任务
      @is_alter_flag = true
      comment_text = 'User has deleted this item.This task is completed automatically.'
      Utils::BkItemTaskOps.create_task_comment(@task, comment_text, session[:current_admin_username])
      @task.complete(session[:current_admin_username])
    end
    nickname = @task.user.present? ? @task.user.nickname : "No User"
    values = {item_name: @task.item.title[0..10], user_name: nickname}
    @push_message = populate_message(@task.rule.push_message.to_s, values)
    @email_title = populate_message(@task.rule.email_title.to_s, values)
    @email_and_system_message = populate_message(@task.rule.email_and_system_message.to_s, values)
    @operation_rules = ItemOperationRule.where(status: true)
                           .where("show_type & #{Settings.ItemOperationRules.ShowType.DoNotShow} != #{Settings.ItemOperationRules.ShowType.DoNotShow}")
                           .order(:name).pluck(:name, :id)

    # item detail information
    @item_details = {
        current: {},
        previous: {}
    }
    item = @task.item
    # current_bk_item_copy = BkItemCopy.get_latest(item.id)
    # @item_details[:current] = JSON.load(current_bk_item_copy.content).symbolize_keys! if current_bk_item_copy.present?
    @item_details[:current] = item.generate_copy.symbolize_keys!
    if @task.bk_item_copy.present?
      @item_details[:previous] = JSON.load(@task.bk_item_copy.content).symbolize_keys!
    else
      @item_details[:previous] = @item_details[:current]
    end
    # 旧的快照里面可能没有如下的信息，所以这里进行补充，方便显示
    additional_item_details = {
        local_price: item.local_price,
        country: item.country,
        region: item.region,
        city: item.city,
    }
    if @item_details[:current].present?
      @item_details[:current] = additional_item_details.merge(@item_details[:current])
      @item_details[:previous] = additional_item_details.merge(@item_details[:previous])
    end

    @other_item_details = {}
    category = item.categories[0]
    @other_item_details[:category] = category.blank? ? '' : category.title
    @other_item_details[:owner] = item.user.nickname
    ret = BackendApiREST.get_offer_count_by_item_ids(Array(item.id))
    log_info('product_offer_count: %s' %[ret])
    offer_count = JSON.parse(ret[1])["objects"][0]["count"].to_i
    @other_item_details[:offers] = offer_count
    @other_item_details[:reviews] = ReviewReview.where(item_id: item.id).select(:id).count()
    bk_item = BkItem.find_by_id(item.id)
    audit_priority = bk_item.blank? ? 0 : bk_item.audit_priority
    @other_item_details[:priority] = audit_priority
    @other_item_details[:state] = Settings.PRODUCT_STATUSES.invert[item.state]
    edit_after_review = bk_item.blank? ? false : bk_item.is_edited
    @other_item_details[:edit_after_review] = edit_after_review

  end

  def execute_action
    @task = ItemTask.includes(:item, :rule).find(params[:id])
    push_message = params[:push_message]
    email_title = params[:email_title]
    email_and_system_message = params[:email_and_system_message]
    is_success_flag, message = Utils::BkItemTaskOps.execute_action(@task, session['current_admin_username'], push_message, email_title, email_and_system_message)
    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end
    redirect_to @task
  end

  def complete
    is_success_flag = false
    message = ""
    @task = ItemTask.includes(:item, :rule).find(params[:id])
    if params[:is_ok] == "1"
      if params[:is_change_weight].present?
        is_success_flag, message = Utils::BkItemTaskOps.complete(@task,
                                                                 session[:current_admin_username],
                                                                 is_ok: true,
                                                                 comment: params[:comment],
                                                                 weight: params[:weight])
      else
        is_success_flag, message = Utils::BkItemTaskOps.complete(@task,
                                                                 session[:current_admin_username],
                                                                 is_ok: true,
                                                                 comment: params[:comment])
      end
    else
      @rule = ItemOperationRule.find(params[:operation_rule_id])
      is_success_flag, message = Utils::BkItemTaskOps.complete(@task, session[:current_admin_username], item_operation_rule: @rule, is_execute: true)
    end

    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end

    redirect_to @task
  end

  private

  def populate_message(message, params)
    message = message.clone
    params.each { |key, value| message = message.gsub '${%s}' % key, value.to_s }
    return message
  end

end
