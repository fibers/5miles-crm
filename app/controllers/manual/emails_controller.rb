class Manual::EmailsController < ApplicationController

  def index
    @emails_grid = initialize_grid(EmailRecommendation.includes(:email_template),
                                   order: 'email_recommendations.id',
                                   order_direction: 'desc',
                                   per_page: Settings.USERS_PER_PAGE)
  end

  def new
  end

  def create
    @is_success_flag = false
    @content = nil
    @email = nil
    email = params[:email]
    timestamp = Time.now.to_i
    @send_time = nil

    begin
      if params[:email].nil?
        @content = 'Email content is empty'
        break
      end

      if email[:subject].blank?
        @content = 'Email subject is empty'
        break
      end

      if email[:subject].length > 100
        @content = 'Email subject is too long'
        break
      end

      if email[:content].blank?
        @content = 'Email content is empty'
        break
      end

      if email[:from].present? and email[:from].length > 50
        @content = "The from attribute of email is too long"
        break
      end

      @email = FmmcMailTemplate.new
      @email.name = "#{Settings.MANUAL_MESSAGES_PREFIX.EMAIL_COMPOSE_PREFIX}_#{timestamp}"
      @email.content = email[:content]
      @email.subject = email[:subject]
      @email.status = Settings.EMAIL_TEMP_STATUSES.Valid
      @email.comment = email[:remarks] || ''
      @email.from_address = email[:from] || ''
      @email.notification_type = email[:type].to_i

      log_info @email.inspect

      if params[:commit] == 'Send for test'
        if params[:test_user_id].blank?
          @content = 'Error: User ID for test is empty.'
          break
        end
        user_id = params[:test_user_id].to_i
        user = EntityUser.find_by_id(user_id)
        if user.blank?
          @content = 'Error: User does not exist.'
          break
        end

        user_name = user.nickname
        if user_name.blank?
          @content = 'Error: User nickname is empty.'
          break
        end
        if user.email.blank?
          @content = 'Error: User email is empty.'
          break
        end

        email_params = {}
        email_params['from'] = @email.from_address
        email_params['text'] = @email.content
        email_params['subject'] = @email.subject
        log_info('Send email recommendation to %d for test' %[user_id])

        if FmmcREST.send_mail(user_id.to_s, email_params)
          @is_success_flag = true
          @content = 'Has sent email to %s successfully' % [user_name]
        else
          @content = 'Fail to send email to %s' %[user_name]
        end

      elsif params[:commit] == 'Send'
        if params[:immediate].blank?
          @content = "Please select start time"
          break
        end

        if params[:immediate] == '0'
          datetime_h = params[:date].to_h
          datetime_string = Time.mktime(*datetime_h.values).to_s(:db)
          @send_time = Time.zone.parse(datetime_string)
          if @send_time < Time.current
            @content = "The time(#{@send_time}) has expired"
            break
          end
        end
        targets = ztree_country_tree_get_countries(email[:to])
        if targets.blank?
          @content = 'Error: To parameter is empty'
          break
        end

        if email[:gender].blank?
          @content = 'Error: Gender is empty'
          break
        end

        if params[:registered_time].blank?
          @content = 'Error: Registered Time is empty'
          break
        end

        registered_time = params[:registered_time]
        is_registered_time = registered_time[:type].present? and registered_time[:value].present? and registered_time[:unit].present? ? true : false
        # "registered_time"=>{"type"=>"1", "value"=>"2", "unit"=>"5"}
        if targets[Settings.INTERNAL_USERS_STRING].present?
          @email.subject = "#{"(Internal)"}#{@email.subject}"
        end

        unless @email.save
          @content = "Error: Fail to save email template"
          break
        end

        gender = email[:gender]
        target_gender = gender.sort.join(',')
        task = FmmcTask.new
        task.name = "#{Settings.MANUAL_MESSAGES_PREFIX.TASK_COMPOSE_PREFIX}_#{timestamp}"
        task.clazz = "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TASK_CLAZZ}"
        task.status = Settings.FMMC_STATUSES.Enabled
        task.mail_template = @email if @email.present?
        task.comment = email[:remarks] || ''
        if targets[Settings.INTERNAL_USERS_STRING].present?
          task.target_country = Settings.INTERNAL_USERS_STRING
          task.target_region = ''
          task.target_city = ''
        elsif targets[Settings.ALL_USERS_STRING].present?
          task.target_country = ''
          task.target_region = ''
          task.target_city = ''
        else
          task.target_country = targets['country'].present? ? targets['country'].join(',') : ''
          task.target_region = targets['region'].present? ? targets['region'].join(',') : ''
          task.target_city = targets['city'].present? ? targets['city'].join(',') : ''
        end
        task.target_gender = target_gender
        if is_registered_time
          task.target_register_interval_period_id = registered_time[:unit]
          task.target_register_interval_quantity = registered_time[:value]
          task.target_register_interval_type = registered_time[:type]
        end
        unless task.save
          @content = "Error: Fail to save task template"
          break
        end

        trigger = FmmcTrigger.new
        trigger.name = "#{Settings.MANUAL_MESSAGES_PREFIX.TRIGGER_COMPOSE_PREFIX}_#{timestamp}"
        trigger.task = task
        trigger.status = Settings.FMMC_STATUSES.Enabled
        trigger.comment = email[:remarks] || ''
        trigger.at = ''
        if params[:immediate] == '1'
          trigger.trigger_type = Settings.TRIGGER_TYPES.Instant
        else
          trigger.interval_period_id = FmmcFrequencyPeriod.get_id_by_frequency(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)
          interval_quantity = (@send_time - Time.current + 1.send(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)).to_i / 1.send(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)
          trigger.interval_quantity = interval_quantity > 0 ? interval_quantity : 1
          trigger.trigger_type = Settings.TRIGGER_TYPES.Delayed
        end
        unless trigger.save
          @content = "Error: Fail to save trigger template"
          break
        end

        trigger_params = {}
        if FmmcREST.notify(trigger.name, trigger_params)
          @is_success_flag = true
          @content = "Add Task Successfully"
        else
          @content = "Failed to Add task"
        end
      end
    end while false

    respond_to do |format|
      format.js {}
    end

  end

  def new_good_items
    @template_options_for_select = FmmcMailTemplate.where("name LIKE ?", Settings.MANUAL_MESSAGES_PREFIX.EMAIL_GOOD_ITEMS_PREFIX + "%").select(:name, :id)

  end

  def create_good_items
    @is_success_flag = false
    @content = nil
    @send_time = nil
    products = []
    email_template = nil
    timestamp = Time.now.to_i
    trigger_params = {}

    begin
      if params[:to].blank?
        @content = 'Error: "To(Country)" is empty'
        break
      end
      if params[:email_template].blank?
        @content = 'Error: "Email Template" is empty'
        break
      end

      params[:product_ids].each do |id|
        if id.present?
          p = EntityItem.includes(:images).find_by_id(id)
          if p
            products << p
          else
            @content = 'Error: Item ID(%s) dose not exist.' % [id]
            # skip params[:product_ids].each
            break
          end
        end
      end
      if @content.present?
        # 'Error: Item ID(%s) dose not exist.'
        break
      end
      if products.empty?
        @content = 'Error: "Item ID" is empty'
        break
      end

      email_template = FmmcMailTemplate.find_by_id(params[:email_template])
      if email_template.blank?
        @content = 'Error: template does not exist.'
        break
      end

      data = {'items' => []}
      products.each do |p|
        item = {}
        item['title'] = p.title
        item['price'] = p.local_price
        item['image_url'] = p.images[0].image_path
        item['url'] = '%s/%s?utm_source=%s&utm_medium=email&utm_campaign=EDM' % [Settings.APP_SITE_ITEM_URI_2_PREFIX, p.fuzzy_item_id, email_template.name]
        data['items'] << item
      end
      data['first_paragraph'] = ''
      if params[:first_paragraph].present?
        data['first_paragraph'] = params[:first_paragraph]
      end
      data['mail_subject'] = params[:subject] if params[:subject].present?

      if params[:commit] == 'Send for test'
        if params[:test_user_id].blank?
          @content = 'Error: User ID for test is empty.'
          break
        end
        user_id = params[:test_user_id].to_i
        user = EntityUser.find_by_id(user_id)
        if user.blank?
          @content = 'Error: User ID for test is not present'
          break
        end
        user_name = user.nickname
        if user.email.blank?
          @content = 'Error: Email for test is not present'
          break
        end

        log_info('Send email recommendation to %d for test' %[user_id])
        email_params = {}
        email_params['template'] = email_template.name
        email_params['subject'] = params[:subject] if params[:subject].present?
        email_params['data'] = data
        if FmmcREST.send_mail(user_id.to_s, email_params)
          @is_success_flag = true
          @content = 'Has sent email to %s successfully' % [user_name]
        else
          @content = 'Fail to send email to %s' %[user_name]
        end

      elsif params[:commit] == 'Send'
        if params[:immediate].blank?
          @content = "Please select start time"
          break
        end

        if params[:immediate] == '0'
          datetime_h = params[:date].to_h
          datetime_string = Time.mktime(*datetime_h.values).to_s(:db)
          @send_time = Time.zone.parse(datetime_string)
          if @send_time < Time.current
            @content = "The time(#{@send_time}) has expired"
            break
          end
        end
        targets = ztree_country_tree_get_countries(params[:to])
        if targets.blank?
          @content = 'Error: To parameter is empty'
          break
        end
        if params[:gender].blank?
          @content = 'Error: Gender is empty'
          break
        end
        gender = params[:gender]
        target_gender = gender.sort.join(',')

        if params[:registered_time].blank?
          @content = 'Error: Registered Time is empty'
          break
        end
        registered_time = params[:registered_time]
        is_registered_time = registered_time[:type].present? and registered_time[:value].present? and registered_time[:unit].present? ? true : false
        # "registered_time"=>{"type"=>"1", "value"=>"2", "unit"=>"5"}
        task = FmmcTask.new
        task.name = "#{Settings.MANUAL_MESSAGES_PREFIX.TASK_COMPOSE_PREFIX}_#{timestamp}"
        task.clazz = "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TASK_CLAZZ}"
        task.status = Settings.FMMC_STATUSES.Enabled
        task.mail_template = email_template
        task.comment = params[:remarks] || ''
        if targets[Settings.INTERNAL_USERS_STRING].present?
          task.target_country = Settings.INTERNAL_USERS_STRING
          task.target_region = ''
          task.target_city = ''
        elsif targets[Settings.ALL_USERS_STRING].present?
          task.target_country = ''
          task.target_region = ''
          task.target_city = ''
        else
          task.target_country = targets['country'].present? ? targets['country'].join(',') : ''
          task.target_region = targets['region'].present? ? targets['region'].join(',') : ''
          task.target_city = targets['city'].present? ? targets['city'].join(',') : ''
        end
        task.target_gender = target_gender
        if is_registered_time
          task.target_register_interval_period_id = registered_time[:unit]
          task.target_register_interval_quantity = registered_time[:value]
          task.target_register_interval_type = registered_time[:type]
        end
        unless task.save
          log_info "Fail to save task"
          @content = "Fail to save task"
          break
        end

        trigger = FmmcTrigger.new
        trigger.name = "#{Settings.MANUAL_MESSAGES_PREFIX.TRIGGER_COMPOSE_PREFIX}_#{timestamp}"
        trigger.task = task
        trigger.status = Settings.FMMC_STATUSES.Enabled
        trigger.comment = params[:remarks] || ''
        trigger.at = ''
        if params[:immediate] == '1'
          trigger.trigger_type = Settings.TRIGGER_TYPES.Instant
        else
          trigger.interval_period_id = FmmcFrequencyPeriod.get_id_by_frequency(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)
          interval_quantity = (@send_time - Time.current + 1.send(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)).to_i / 1.send(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)
          trigger.interval_quantity = interval_quantity > 0 ? interval_quantity : 1
          trigger.trigger_type = Settings.TRIGGER_TYPES.Delayed
        end
        unless trigger.save
          log_info "Fail to save trigger"
          @content = "Fail to save trigger"
          break
        end
        trigger_params = {}
        if targets[Settings.INTERNAL_USERS_STRING].present?
          if data['mail_subject'].present?
            data['mail_subject'] = "#{"(Internal)"}#{data['mail_subject']}"
          else
            data['mail_subject'] = "#{"(Internal)"}#{email_template.subject}"
          end
        end
        trigger_params['data'] = data
        if FmmcREST.notify(trigger.name, trigger_params)
          @is_success_flag = true
          @content = "Add Task Successfully"
        else
          @content = "Failed to Add task"
        end
      end
    end while false

    respond_to do |format|
      format.js {}
    end

  end

end
