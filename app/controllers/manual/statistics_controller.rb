class Manual::StatisticsController < ApplicationController
  def index
    @manual_grid = initialize_grid(FmmcTrigger.manual_scope.includes(task: [:frequency, :system_message_template, :push_template, :mail_template]),
                                   order: 'fmmc_trigger.updated_at',
                                   order_direction: 'desc',
                                   include: [:period],
                                   per_page: Settings.MANUAL_PER_PAGE)
  end
end
