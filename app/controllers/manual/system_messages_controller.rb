class Manual::SystemMessagesController < ApplicationController

  def index

  end

  def new
    search_action_with_banner = {}
    search_action_with_banner['Search(with banner)'] = 's2'
    @system_actions = Settings.SYSTEM_MESSAGE_ACTIONS.merge(search_action_with_banner).sort
    search_action_param_with_banner = {}
    search_action_param_with_banner['s2'] = 'Keyword'
    @system_action_params = Settings.SYSTEM_MESSAGE_ACTION_PARAMS.merge(search_action_param_with_banner)

  end

  def create
    @is_success_flag = false
    @content = nil
    @system_message = nil
    @push = nil
    @send_time = nil
    timestamp = Time.now.to_i

    begin
      system_message = params[:system_message]
      system_message_action = ''
      push_action = ''
      if system_message[:action].present?
        if Settings.SYSTEM_MESSAGE_ACTION_PARAMS[system_message[:action]]
          system_message_action = "#{system_message[:action]}:#{system_message[:action_param]}"
        else
          system_message_action = system_message[:action]
        end
      end
      if system_message_action == 's2'
        if system_message['s2']['image']['url'].blank? or system_message['s2']['image']['width'].blank? or system_message['s2']['image']['height'].blank?
          @content = "Please update banner image"
          @is_success_flag = false
          break
        end
        search_with_banner_campaign = CampaignCampaign.new
        search_with_banner_campaign.name = "#{Settings.CAMPAIGN.SEARCH_WITH_BANNER}_#{timestamp}"
        search_with_banner_campaign.campaign_type = Settings.CAMPAIGN.TYPES.SearchWithBanner
        search_with_banner_campaign.click_action = ''
        search_with_banner_campaign.begin_at = Time.current
        search_with_banner_campaign.end_at = Time.current + 60.years
        search_with_banner_campaign.comment = ''
        search_with_banner_campaign.image_url = system_message['s2']['image']['url']
        search_with_banner_campaign.image_width = system_message['s2']['image']['width']
        search_with_banner_campaign.image_height = system_message['s2']['image']['height']
        search_with_banner_campaign.save!

        system_message_action = "#{system_message_action}:#{system_message[:action_param]},cid:#{search_with_banner_campaign.id}"
        push_action = system_message_action
      end
      @system_message = FmmcSystemMessageTemplate.new
      @system_message.name = "#{Settings.MANUAL_MESSAGES_PREFIX.SYSTEM_MESSAGE_COMPOSE_PREFIX}_#{timestamp}"
      @system_message.content = system_message[:content] || ''
      @system_message.status = Settings.TEMP_SYSTEM_MESSAGE_STATUSES.Valid
      @system_message.comment = params[:remarks] || ''
      @system_message.image = params[:image] || ''
      @system_message.action = system_message_action
      @system_message.desc = ''

      log_info @system_message.inspect

      if params[:is_with_push].to_i == 1 and params[:push]
        push = params[:push]
        if push_action.blank?
          push_action = 'n'
          if Settings.SYSTEM_MESSAGE_ACTION_PARAMS[push[:action]]
            push_action = "#{push[:action]}:#{push[:action_param]}"
          else
            push_action = push[:action]
          end
        end

        payload = {"t" => "s", "a" => push_action}.to_json
        @push = FmmcPushTemplate.new
        @push.name = "#{Settings.MANUAL_MESSAGES_PREFIX.PUSH_COMPOSE_PREFIX}_#{timestamp}"
        @push.content = push[:content] || ''
        @push.status = Settings.PUSH_TEMP_STATUSES.Valid
        @push.comment = params[:remarks] || ''
        @push.payload = payload
        @push.title = push[:title]
        @push.flag = push[:mute].present? ? Settings.PUSH_REMIND_TYPES.Vibration.to_i : Settings.PUSH_REMIND_TYPES.Ring.to_i
        @push.notification_type = push[:type]

        log_info @push.inspect
      end

      if params[:commit] == "Send for test"
        if params[:test_user_id].blank?
          @content = 'Error: User ID for test is empty.'
          break
        end
        user_id = params[:test_user_id].to_i
        user = EntityUser.find_by_id(user_id)
        if user.blank?
          @content = 'Error: User ID for test is not present'
          break
        end
        ret = send_system_message_with_push_for_each(user_id, @system_message, @push)
        if ret["system_message"]
          if ret["push"].blank?
            @is_success_flag = true
            @content = "Has sent system message to #{user_id} successfully"
          elsif ret["push"]
            @is_success_flag = true
            @content = "Has sent system message and push to #{user_id} successfully"
          else
            @content = "Has sent system message to #{user_id} successfully, but push failed"
          end
        else
          @content = "Fail to send system message to #{user_id}"
        end
      elsif params[:commit] == "Send"
        if params[:immediate].blank?
          @content = "Please select start time"
          break
        end

        if params[:immediate] == '0'
          datetime_h = params[:date].to_h
          datetime_string = Time.mktime(*datetime_h.values).to_s(:db)
          @send_time = Time.zone.parse(datetime_string)
          if @send_time < Time.current
            @content = "The time(#{@send_time}) has expired"
            break
          end
        end
        targets = ztree_country_tree_get_countries(params[:to])
        if targets.blank?
          @content = 'Error: To parameter is empty'
          break
        end
        with_push = nil
        if @push.present?
          with_push = "(with push)"
        end
        if params[:gender].blank?
          @content = 'Error: Gender is empty'
          break
        end
        gender = params[:gender]
        target_gender = gender.sort.join(',')
        uid_suffix = params[:uid_suffix] || []
        target_uid_suffix = uid_suffix.join(',')

        if params[:registered_time].blank?
          @content = 'Error: Registered Time is empty'
          break
        end

        registered_time = params[:registered_time]
        is_registered_time = registered_time[:type].present? and registered_time[:value].present? and registered_time[:unit].present? ? true : false
        # "registered_time"=>{"type"=>"1", "value"=>"2", "unit"=>"5"}

        if targets[Settings.INTERNAL_USERS_STRING].present?
          @system_message.content = "(#{Settings.INTERNAL_USERS_STRING})#{@system_message.content}"
          @push.content = "(#{Settings.INTERNAL_USERS_STRING})#{@push.content}" if @push.present?
        end
        unless @system_message.save
          @content = "Fail to save system message template"
          log_info @system_message.errors.full_messages
          break
        end
        if @push.present?
          unless @push.save
            @content = "Fail to save push template"
            log_info @push.errors.full_messages
            break
          end
        end

        task = FmmcTask.new
        task.name = "#{Settings.MANUAL_MESSAGES_PREFIX.TASK_COMPOSE_PREFIX}_#{timestamp}"
        task.clazz = "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TASK_CLAZZ}"
        task.status = Settings.FMMC_STATUSES.Enabled
        task.system_message_template = @system_message if @system_message.present?
        task.push_template = @push if @push.present?
        task.comment = params[:remarks] || ''
        if targets[Settings.INTERNAL_USERS_STRING].present?
          task.target_country = Settings.INTERNAL_USERS_STRING
          task.target_region = ''
          task.target_city = ''
        elsif targets[Settings.ALL_USERS_STRING].present?
          task.target_country = ''
          task.target_region = ''
          task.target_city = ''
        else
          task.target_country = targets['country'].present? ? targets['country'].join(',') : ''
          task.target_region = targets['region'].present? ? targets['region'].join(',') : ''
          task.target_city = targets['city'].present? ? targets['city'].join(',') : ''
        end
        if Settings.INTERNAL_USERS_STRING != task.target_country
          task.target_uid_tail = target_uid_suffix
        end
        task.target_gender = target_gender
        if is_registered_time
          task.target_register_interval_period_id = registered_time[:unit]
          task.target_register_interval_quantity = registered_time[:value]
          task.target_register_interval_type = registered_time[:type]
        end
        unless task.save
          @content = "Error: Fail to save task template"
          break
        end

        trigger = FmmcTrigger.new
        trigger.name = "#{Settings.MANUAL_MESSAGES_PREFIX.TRIGGER_COMPOSE_PREFIX}_#{timestamp}"
        trigger.task = task
        trigger.status = Settings.FMMC_STATUSES.Enabled
        trigger.comment = params[:remarks] || ''
        trigger.at = ''
        if params[:immediate] == '1'
          trigger.trigger_type = Settings.TRIGGER_TYPES.Instant
        else
          trigger.interval_period_id = FmmcFrequencyPeriod.get_id_by_frequency(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)
          interval_quantity = (@send_time - Time.current + 1.send(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)).to_i / 1.send(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)
          trigger.interval_quantity = interval_quantity > 0 ? interval_quantity : 1
          trigger.trigger_type = Settings.TRIGGER_TYPES.Delayed
        end
        unless trigger.save
          @content = "Error: Fail to save trigger template"
          break
        end

        if @push.present?
          payload = JSON.load(@push.payload)
          payload['r'] = "P#{trigger.task_id}"
          @push.update(payload: payload.to_json)
        end

        trigger_params = {}
        if FmmcREST.notify(trigger.name, trigger_params)
          @is_success_flag = true
          @content = "Add Task Successfully"
        else
          @content = "Failed to Add task"
        end
      end
    end while false
  end

  private
  def send_system_message_with_push_for_each(uid, system_message, push)
    results = {}
    if system_message.present?
      log_info("Send system message to #{uid}")
      results["system_message"] = false
      sm = {}
      sm['text'] = system_message.content if system_message.content.present?
      sm['image'] = system_message.image if system_message.image.present?
      sm['action'] = system_message.action if system_message.action.present?
      sm['desc'] = system_message.desc if system_message.desc.present?
      if FmmcREST.system_message(uid, sm)
        results["system_message"] = true
        if push.present?
          log_info("Send push to #{uid}")
          results["push"] = false
          pm = {}
          pm['title'] = push.title if push.title.present?
          pm['text'] = push.content if push.content.present?
          pm['payload'] = push.payload if push.payload.present?
          pm['flag'] = push.flag if push.flag.present?
          if FmmcREST.push(uid, pm)
            results["push"] = true
          end
        end
      end
    end
    return results
  end
end
