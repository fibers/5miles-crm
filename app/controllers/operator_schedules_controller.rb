class OperatorSchedulesController < ApplicationController
  before_action :get_schedule_params, only: [:create, :update]
  before_action :find_schedule, only: [:edit, :update]
  before_action :get_from_url, except: [:index]

  def index
    @schedules_grid = initialize_grid(BkOperatorSchedule,
                                      include: [:operator],
                                      order: 'bk_operator_schedules.id',
                                      order_direction: 'desc')
  end

  def new
    @schedule = BkOperatorSchedule.new
    @schedule.operator_id = params[:operator_id].to_i if params[:operator_id].present?
  end

  def create
    @schedule = BkOperatorSchedule.new(@schedule_params)
    if @schedule.save
      flash[:success] = "Create schedule successfully"
      redirect_to @from_url
    else
      flash[:error] = "Failed to create schedule"
      render 'new'
    end
  end

  def edit

  end

  def update
    if @schedule.update(@schedule_params)
      flash[:success] = "Update schedule successfully"
      redirect_to @from_url
    else
      flash[:error] = "Failed to update schedule"
      render 'edit'
    end
  end

  private

  def find_schedule
    @schedule = BkOperatorSchedule.find(params[:id])
  end

  def get_schedule_params
    schedule = params[:schedule]
    @schedule_params = {}
    @schedule_params['operator_id'] = schedule[:operator_id]
    @schedule_params['state'] = schedule[:state]
    start_time_array = []
    schedule.keys.sort.grep(/start_time/).each do |key|
      start_time_array << schedule[key]
    end
    end_time_array = []
    schedule.keys.sort.grep(/end_time/).each do |key|
      end_time_array << schedule[key]
    end
    @schedule_params['start_time'] = Time.zone.parse(Time.new(*start_time_array).to_s(:db))
    @schedule_params['end_time'] = Time.zone.parse(Time.new(*end_time_array).to_s(:db))
  end

  def get_from_url
    @from_url = params[:from_url]
  end

end
