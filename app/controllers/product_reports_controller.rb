class ProductReportsController < ApplicationController

  before_action :find_product_report, only: [:edit, :update, :create_zendesk_ticket]

  def index
    @product_reports_grid = initialize_grid(
        SpamItemreportrecord,
        include: [:product, :reporter],
        per_page: Settings.REPORTS_PER_PAGE)

    @unprocessed_reports = SpamItemreportrecord.where(is_processed: 0).size
    @processed_reports = SpamItemreportrecord.where(is_processed: 1).size

    @report_reason_options_for_select = SpamReportreason.item_report_reasons()
  end

  def edit
    trigger = FmmcTrigger.find_by_name(Settings.TRIGGER_TEMP_NAMES.ProductReport)
    @template_contents = {}
    @template_contents['push_content'] = Utils::BkOps.render_template_content(trigger.task.push_template.content,
                                                                              push_content: nil)
    @template_contents['system_message_content'] = Utils::BkOps.render_template_content(trigger.task.system_message_template.content,
                                                                                        system_message_content: nil)
  end

  def update

    begin
      reply_content = ''
      is_send_flag = params[:is_send_flag] == '1' ? true : false
      if is_send_flag
        if params[:push].blank? or params[:push][:content].blank?
          flash[:error] = "Push content is empty"
          break
        end

        if params[:system_message].blank? or params[:system_message][:content].blank?
          flash[:error] = "System message content is empty"
          break
        end
        reply_content = params[:system_message][:content]
      end

      if @product_report.update(is_processed: 1, reply_content: reply_content)
        if is_send_flag
          trigger_params = {}
          trigger_params['target_user'] = @product_report.reporter_id.to_s
          trigger_params['data'] = {}
          trigger_params['data']['push_content'] = params[:push][:content]
          trigger_params['data']['system_message_content'] = params[:system_message][:content]
          trigger_params['data']['fuzzy_item_id'] = @product_report.product.fuzzy_item_id
          FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ProductReport, trigger_params)
        end

        flash[:success] = "Process feedback successfully."
      else
        flash[:error] = "Failed to process feedback"
      end
    end while false

    redirect_to product_reports_path(grid: {f: {is_processed: ['f']}})
  end

  def create_zendesk_ticket
    is_success_flag, message = Utils::ItemReportOps.create_zendesk_ticket(@product_report)
    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end

    redirect_to :back
  end

  private
  def find_product_report
    @product_report = SpamItemreportrecord.find(params[:id])
  end

end
