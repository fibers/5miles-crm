class ProductStatesController < ApplicationController

  def create
    @is_success_flag = false
    @message = ""
    @is_risk = params[:is_risk]
    begin
      if params[:is_risk].nil?
        @is_success_flag, @message = Utils::ItemOps.approve(params[:product_id].to_i, session[:current_admin_username])
        unless @is_success_flag
          break
        end

        if params[:is_change_weight].present? and params[:is_change_weight].to_i == 1
          product_ids = []
          product_ids << params[:product_id].to_i
          weight = params[:weight].to_i
          ret = BackendApiREST.batch_update_item_weight(product_ids, weight)
          puts ret.inspect
          result = JSON.load(ret[1])
          unless result["meta"]["result"] == "ok"
            @is_success_flag = false
            @message = "Approve the product successfully, but failed to update weight to #{weight}."
            break
          end
        end
        if params[:clear_user_risk].blank?
          @is_success_flag = true
          @message = "Approve the product successfully."
          break
        end
        # 清除用户risk信息
        @product = EntityItem.find(params[:product_id].to_i)
        reason = "Normalize user when approve item(id:#{params[:product_id].to_i})"
        @is_success_flag, @message = Utils::UserOps.normalize(@product.user_id, session[:current_admin_id], session[:current_admin_username], reason)
        unless @is_success_flag
          break
        end
        @message = "Approve the product successfully."
      else
        if params[:risk].blank?
          @is_success_flag = false
          @message = "Please input risk firstly"
          break
        end
        @is_success_flag, @message = Utils::ItemOps.approve_risk(params[:product_id].to_i, session[:current_admin_username], params[:risk])
        unless @is_success_flag
          break
        end

        if params[:set_user_risk].blank?
          break
        end
        @product = EntityItem.find(params[:product_id].to_i)
        reason = params[:user_risk]
        @is_success_flag, @message = Utils::UserOps.risk(@product.user_id, session[:current_admin_id], session[:current_admin_username], reason)
        if @is_success_flag
          @message = "Approve the product #{@product.title} on risk(#{reason}) and edit owner risk successfully."
        end
      end
    end while false

    respond_to do |format|
      format.html do
        if @is_success_flag
          flash[:success] = @message
        else
          flash[:error] = @message
        end
        redirect_to :back
      end
      format.js do
        if @product.blank?
          @product = EntityItem.find(params[:product_id].to_i)
        end
      end
    end
  end

  def destroy
    #   is_success_flag = false
    #   message = ""
    #
    #   begin
    #     if params[:reason].blank?
    #       is_success_flag = false
    #       message = "Please input reason firstly"
    #       break
    #     end
    #     is_success_flag, message = Utils::ItemOps.disapprove(params[:product_id].to_i, session[:current_admin_username], params[:reason])
    #   end while false
    #
    #   if is_success_flag
    #     flash[:success] = message
    #   else
    #     flash[:error] = message
    #   end
    #
    redirect_to :back
  end

end
