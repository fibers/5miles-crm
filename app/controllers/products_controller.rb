class ProductsController < ApplicationController

  before_action :find_product, only: [:show, :images, :update_tags, :offers]
  before_action :selected_ids, only: [:update_weight, :update_categories, :update_categories_new]

  #before_action :all_tags, :product_tag_ids, only: [:show, :update_tags]
  before_action :all_categories, only: [:show, :index]
  before_action :product_category_id, only: [:show]
  before_action :product_offer_count, only: [:offers, :show]
  before_action :product_offers, only: [:offers]
  before_action :get_bk_item_audit_histories, only: [:show, :offers]
  before_action :detail_action_params, only: [:show, :offers]

  def index
    @products_grid = initialize_grid(
        EntityItem.includes(:user, :categories, :images).references(:entity_user),
        order: 'entity_item.id',
        order_direction: 'desc',
        per_page: Settings.PRODUCTS_PER_PAGE)

    sql = "select sum(case when state = #{Settings.PRODUCT_STATUSES.Listing} and internal_status = #{Settings.PRODUCT_INTERNAL_STATUSES.PendingReview} then 1 else 0 end) as  pending_review_listing_count
, sum(case when state = #{Settings.PRODUCT_STATUSES.Listing} and internal_status = #{Settings.PRODUCT_INTERNAL_STATUSES.ApproveRisk} then 1 else 0 end) as  approved_risking_listing_count
, sum(case when state = #{Settings.PRODUCT_STATUSES.Listing} and internal_status = #{Settings.PRODUCT_INTERNAL_STATUSES.Approve} then 1 else 0 end) as  approved_normal_listing_count
, sum(case when internal_status = #{Settings.PRODUCT_INTERNAL_STATUSES.InReview} then 1 else 0 end) as  in_review_count
, sum(case when state = #{Settings.PRODUCT_STATUSES.Unapproved} then 1 else 0 end) as  unapproved_count from entity_item t"
    # return the hash result
    ret = EntityItem.connection.select_all(sql)[0]
    @pending_review_listing_count = ret["pending_review_listing_count"].to_i
    @in_review_count = ret["in_review_count"].to_i
    @approved_normal_listing_count = ret["approved_normal_listing_count"].to_i
    @unapproved_count = ret["unapproved_count"].to_i
    @approved_risking_listing_count = ret["approved_risking_listing_count"].to_i
  end

  def show
    @active_tab = params[:active_tab].blank? ? 'overview' : params[:active_tab]

    bk_item = BkItem.find_by_item_id(@product.id)
    Utils::BkItemOps.update_item_state(bk_item, @product)
  end

  def map
    @products = []
    @force_load_selected_products = params.delete(:force_load_selected_products)
    @products_grid = initialize_grid(
        EntityItem.includes(:user, :categories).references(:entity_user),
        per_page: 1)
    begin
      if params[:grid].blank? or params[:grid][:f].blank?
        break
      end
      @has_filter_flag = true
      @products_grid.with_resultset do |active_relation|
        puts "total_count: #{@products_grid.resultset.total_count}"
        if @products_grid.resultset.total_count <= Settings.PRODUCT.Map.Display.Max or @force_load_selected_products.present?
          active_relation.find_each do |record|
            product_params = {}
            product_params['id'] = record.id.to_s
            product_params['lat'] = record.lat
            product_params['lon'] = record.lon
            @products << product_params
          end
        end
      end
    end while false
  end

  def images
    respond_to do |format|
      format.html { redirect_to products_path }
      format.js {}
    end
  end

  def approve
    # log_info('Batch approve the products(%s) on risk(%s:%s)' %[@selected.to_s, params[:is_risk].nil? ? 'false' : 'true', params[:risk]])
    # is_error_flag = false
    # if params[:is_risk].nil?
    #   EntityItem.where(id: @selected).find_each do |product|
    #     unless product.approve
    #       is_error_flag = true
    #       break
    #     end
    #   end
    # else
    #   EntityItem.includes(:user, :images).where(id: @selected).find_each do |product|
    #     send_offer_user_email_flag = false
    #     if product.state == Settings.PRODUCT_STATUSES.Listing.to_i
    #       send_offer_user_email_flag = true
    #     end
    #     if params[:risk].blank? or product.user.nil?
    #       next
    #     end
    #     data = {}
    #     data['message'] = params[:risk]
    #     data['item_image'] = product.images[0].image_path
    #     data['item_title'] = product.title
    #     data['item_price'] = product.local_price
    #     data['fuzzy_item_id'] = product.fuzzy_item_id
    #
    #     unless product.user.is_robot?
    #       seller_data = {}
    #       seller_data['target_user'] = product.user.id.to_s
    #       seller_data['data'] = {}
    #       seller_data['data']['item_url'] = "#{Settings.APP_SITE_ITEM_URI_PREFIX}#{product.fuzzy_item_id}?from=#{Settings.EMAIL_TEMP_NAMES.ItemApproveRiskSeller}"
    #       seller_data['data'].merge!(data)
    #       FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemApproveRiskSeller, seller_data)
    #     end
    #     if send_offer_user_email_flag
    #       buyer_data = {}
    #       buyer_data['data'] = {}
    #       buyer_data['data']['item_url'] = "#{Settings.APP_SITE_ITEM_URI_PREFIX}#{product.fuzzy_item_id}?from=#{Settings.EMAIL_TEMP_NAMES.ItemApproveRiskBuyer}"
    #       buyer_data['data']['item_id'] = product.id
    #       buyer_data['data'].merge!(data)
    #       FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemApproveRiskBuyer, buyer_data)
    #     end
    #     unless product.approve_risk
    #       is_error_flag = true
    #       break
    #     end
    #   end
    # end
    #
    # if is_error_flag
    #   flash[:error] = 'Failed to batch approve the products.'
    # else
    #   flash[:success] = 'Batch approve the products successfully.'
    # end
    redirect_to :back
  end

  def disapprove
    # log_info('Batch disapprove the products(%s) on reason(%s)' %[@selected.to_s, params[:reason]])
    # is_error_flag = false
    # EntityItem.includes(:user, :images).where(id: @selected).find_each do |product|
    #   send_offer_user_email_flag = false
    #   if product.state == Settings.PRODUCT_STATUSES.Listing.to_i
    #     send_offer_user_email_flag = true
    #   end
    #   if params[:reason].blank? or product.user.nil?
    #     next
    #   end
    #   data = {}
    #   data['message'] = params[:reason]
    #   data['item_image'] = product.images[0].image_path
    #   data['item_title'] = product.title
    #   data['item_price'] = product.local_price
    #   data['fuzzy_item_id'] = product.fuzzy_item_id
    #
    #   unless product.user.is_robot?
    #     seller_data = {}
    #     seller_data['target_user'] = product.user.id.to_s
    #     seller_data['data'] = {}
    #     seller_data['data'].merge!(data)
    #     FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemDisapproveSeller, seller_data)
    #   end
    #   if send_offer_user_email_flag
    #     buyer_data = {}
    #     buyer_data['data'] = {}
    #     buyer_data['data']['item_id'] = product.id
    #     buyer_data['data'].merge!(data)
    #     FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemDisapproveBuyer, buyer_data)
    #   end
    #   unless product.disapprove
    #     is_error_flag = true
    #     break
    #   end
    # end
    #
    # if is_error_flag
    #   flash[:error] = 'Failed to batch disapprove the products.'
    # else
    #   flash[:success] = 'Batch disapprove the products successfully.'
    # end
    redirect_to :back
  end

  def update_tags
    log_info('Update tags')

    # tag_ids = params[:tag_ids]
    # unless tag_ids
    #   tag_ids = []
    # end
    #
    # tag_ids = tag_ids.map { |id| id.to_i }
    # add_tag_ids = tag_ids - @product_tag_ids
    # del_tag_ids = @product_tag_ids - tag_ids
    #
    # add_tag_ids.each do |tid|
    #   @product.related_tags.create(tag_id: tid)
    # end
    #
    # del_tag_ids.each do |tid|
    #   @product.related_tags.find_by(tag_id: tid).destroy
    # end
    #
    # if add_tag_ids.any? or del_tag_ids.any?
    #   # 下面的需要api提供接口，不能直接修改
    #   # @product.update_time = Time.now
    #   # @product.save
    # end

    respond_to do |format|
      format.js {}
    end
  end


  def update_categories_new

    category_id = params[:category_id]

    log_info('Update category to id(%s) on products(%s)' %[category_id, @selected.to_s])

    err = ''
    if category_id
      category_id = category_id.to_i
      @selected.each do |id|
        unless BackendApiREST.update_product_category(id.to_i, category_id)
          err = 'Failed to update product category'
        end
      end
    end

    res_flag = 'err'
    if err
      res_flag = 'ok'
    else
      err= ''
    end
    res = {"res"=>res_flag , "msg"=>err, "body"=>nil}
    render json: res

  end

  def update_categories

    category_id = params[:category_id]

    log_info('Update category to id(%s) on products(%s)' %[category_id, @selected.to_s])

    if category_id
      category_id = category_id.to_i
      @selected.each do |id|
        unless BackendApiREST.update_product_category(id.to_i, category_id)
          flash[:error] = 'Failed to update product category'
          break
        end
      end
    end

    if params[:id]
      @product = EntityItem.find(params[:id])
      @product_category_id = @product.related_categories.pluck(:cat_id)
      respond_to do |format|
        format.js {}
      end
    else
      if flash[:error].nil?
        flash[:success] = 'Update product categories successfully .'
      end
      redirect_to :back
    end

  end

  def offers
    @seller = nil
    EntityUser.where(fuzzy_user_id: @product_offers["meta"]["id"]).each do |u|
      @seller = u if u.fuzzy_user_id == @product_offers["meta"]["id"]
    end
    @buyers = {}
    @product_offers["objects"].each do |offer|
      fuzzy_user_id = offer["buyer"]["id"]
      if fuzzy_user_id
        if @buyers[fuzzy_user_id].nil?
          @buyers[fuzzy_user_id] = {"offers" => []}
        end
        @buyers[fuzzy_user_id]["offers"] << offer
      end
    end

    EntityUser.where(fuzzy_user_id: @buyers.keys()).select(:id, :fuzzy_user_id, :nickname).each do |u|
      if @buyers[u.fuzzy_user_id].present?
        @buyers[u.fuzzy_user_id]["user"] = u
      end
    end

    @reviews = {}
    ReviewReview.where(item_id: params[:id].to_i).each do |r|
      key = r.reviewer_id == @seller.id ? r.target_user : r.reviewer_id
      @reviews[key] = [] if @reviews[key].blank?
      @reviews[key] << r
    end
  end

  def index_weight
    @products_grid = initialize_grid(EntityItem,
                                     order: 'entity_item.weight',
                                     order_direction: 'desc',
                                     per_page: Settings.PRODUCTS_PER_PAGE)
  end

  def update_weight
    product_ids = []
    @selected.each { |id| product_ids << id.to_i }

    weight = params[:weight].to_i

    log_info("Update weight to #{weight} about products(#{product_ids}).")
    if product_ids.present?
      if weight < Settings.WEIGHT.Min or weight > Settings.WEIGHT.Max
        flash[:warning] = 'Weight value is invalid.'
      else
        ret = BackendApiREST.batch_update_item_weight(product_ids, weight)
        puts ret.inspect
        result = JSON.load(ret[1])
        if params[:id].blank?
          if result["meta"]["result"] == "ok"
            flash[:success] = 'Batch update weight successfully.'
          else
            flash[:error] = "Some user failed(ids: #{result["objects"]})."
          end
        end
      end
    else
      flash[:warning] = 'Please select product firstly.'
    end

    if params[:id].present?
      @product = EntityItem.find_by_id(params[:id])
      respond_to do |format|
        format.js {}
      end
    else
      redirect_to :back
    end
  end

  def index_hot_items
    @hot_items_dimensions_for_select = {'发offer人数' => 0, 'PV数' => 1}
    @item_pv_and_offer = {}
    @offer_ratio = 1
    @pv_ratio = 10

    @dimension = params[:dimension].present? ? params[:dimension].to_i : 0
    @data_from = params[:from].present? ? params[:from] : (Time.current.to_date - 14.days).to_s #default 15 days
    @date_to = params[:to].present? ? params[:to] : Time.current.to_date.to_s
    @limit_count = params[:count].present? ? params[:count].to_i : Settings.HOT_ITEMS_PER_PAGE
    params_sql = {from: @data_from, to: @date_to, limit: @limit_count}
    @default_order = []

    if Time.zone.parse(@date_to) - Time.zone.parse(@data_from) <= 1.month
      if @dimension == 0
        @default_order << [9, 'desc']
        find_by_offer_sql = "SELECT offer.item_id, count(DISTINCT offer.user_id) AS offer_count \
                      FROM bizdb.ods_offer AS offer \
                      WHERE offer.report_date >= :from AND offer.report_date < :to AND offer.offer_type = 0 \
                      GROUP BY offer.item_id DESC \
                      ORDER BY offer_count DESC \
                      LIMIT :limit \
                      ;"
        find_sql = []
        find_sql << find_by_offer_sql
        find_sql << params_sql
        OdsOffer.find_by_sql(find_sql).each do |offer|
          @item_pv_and_offer[offer.item_id] = {}
          @item_pv_and_offer[offer.item_id]['offer'] = offer
        end
        params_sql[:item_ids] = @item_pv_and_offer.keys()
        find_by_pv_sql = "SELECT pv.item_id, count(DISTINCT pv.user_id) AS pv_count \
                      FROM bizdb.ods_page_view AS pv \
                      WHERE pv.report_date >= :from AND pv.report_date < :to AND pv.visit_type = 0 AND pv.item_id in (:item_ids) \
                      GROUP BY pv.item_id \
                      ;"
        find_sql = []
        find_sql << find_by_pv_sql
        find_sql << params_sql
        OdsPageView.find_by_sql(find_sql).each do |pv|
          @item_pv_and_offer[pv.item_id]['pv'] = pv
        end

      else
        @default_order << [8, 'desc']
        find_by_pv_sql = "SELECT pv.item_id, count(DISTINCT pv.user_id) AS pv_count \
                      FROM bizdb.ods_page_view AS pv \
                      WHERE pv.report_date >= :from AND pv.report_date < :to AND pv.visit_type = 0 \
                      GROUP BY pv.item_id DESC \
                      ORDER BY pv_count DESC \
                      LIMIT :limit \
                      ;"
        find_sql = []
        find_sql << find_by_pv_sql
        find_sql << params_sql
        OdsPageView.find_by_sql(find_sql).each do |pv|
          @item_pv_and_offer[pv.item_id] = {}
          @item_pv_and_offer[pv.item_id]['pv'] = pv
        end
        params_sql[:item_ids] = @item_pv_and_offer.keys()
        find_by_offer_sql = "SELECT offer.item_id, count(DISTINCT offer.user_id) AS offer_count \
                      FROM bizdb.ods_offer AS offer \
                      WHERE offer.report_date >= :from AND offer.report_date < :to AND offer.offer_type = 0 AND offer.item_id in (:item_ids) \
                      GROUP BY offer.item_id DESC \
                      ;"
        find_sql = []
        find_sql << find_by_offer_sql
        find_sql << params_sql
        OdsOffer.find_by_sql(find_sql).each do |offer|
          @item_pv_and_offer[offer.item_id]['offer'] = offer
        end
      end
      EntityItem.includes(:user, :categories, :images).where(id: @item_pv_and_offer.keys()).find_each do |item|
        @item_pv_and_offer[item.id]['item'] = item
      end
    end
  end

  def product_ajax
    case params[:fn]
      when "approve_window"
        @product = EntityItem.find(params[:id])
        @is_remote = params[:is_remote].blank? ? false : true
        respond_to do |format|
          format.html { render "approve_window", layout: false }
        end
      when "approve_risk_window"
        @product = EntityItem.find(params[:id])
        @user_risk = ''
        @user_info = @product.user.user_info
        if @user_info.present? and @user_info.internal_status == Settings.USER_INTERNAL_STATUSES.Risk
          @user_risk = @user_info.reason
        end
        @is_remote = params[:is_remote].blank? ? false : true
        respond_to do |format|
          format.html { render "approve_risk_window", layout: false }
        end
      when "set_first_image"
        @product = EntityItem.find(params[:id])
        image_id = params[:image_id].to_i
        if BackendApiREST.set_item_first_image(@product.id, image_id)
          flash[:success] = 'Update product first image successfully.'
        else
          flash[:error] = "Failed to update product first image."
        end
        respond_to do |format|
          format.html { render '_item_images_content', layout: false }
        end
      when "batch_change_weight_window"
        @selected_ids = params[:selected_ids]
        respond_to do |format|
          format.html { render 'batch_change_weight_window', layout: false }
        end
      when "batch_modify_category_window"
        all_categories()
        @selected_ids = params[:selected_ids]
        respond_to do |format|
          format.html { render 'batch_modify_category_window', layout: false }
        end
    end
  end

  private
  def find_product
    @product = EntityItem.includes(:images, :user).find(params[:id])
  end

  def selected_ids
    @selected = []
    if params[:id]
      @selected << params[:id]
    elsif params[:grid] and params[:grid][:selected]
      @selected = params[:grid][:selected]
    elsif params[:selected_ids]
      @selected = params[:selected_ids]
    end
  end

  # def all_tags
  #   @tags = CommodityTag.valid.select(:id, :title)
  # end

  # def product_tag_ids
  #   @product_tag_ids = @product.related_tags.pluck(:tag_id)
  # end

  def product_category_id
    @product_category_id = @product.related_categories.pluck(:cat_id)
  end

  def all_categories
    @categories = EntityCategory.published.select(:id, :slug, :title)
  end

  def product_offer_count
    ret = BackendApiREST.get_offer_count_by_item_ids(Array(params[:id].to_i))
    log_info('product_offer_count: %s' %[ret])
    @offer_count = JSON.parse(ret[1])["objects"][0]["count"].to_i
  end

  def product_offers
    ret = BackendApiREST.get_offers_by_item_id(params[:id].to_i)
    log_info('product_offers: %s' %[ret])
    @product_offers = JSON.parse(ret[1])
  end

  def get_bk_item_audit_histories
    @bk_item_audit_histories = BkItemAuditHistory.where(item_id: params[:id]).order(id: :desc)
  end

  def detail_action_params
    @operation_rules = ItemOperationRule.where(status: true)
                           .where("show_type & #{Settings.ItemOperationRules.ShowType.DoNotShow} != #{Settings.ItemOperationRules.ShowType.DoNotShow}")
                           .order(:name).pluck(:name, :id)
    @opened_review_tasks = nil
    if ItemTask.is_has_opened_task(@product.id)
      @opened_review_tasks = ItemTask.where.not(state: Settings.ITEM_TASK_STATE.Completed).where(item_id: @product.id)
    end
  end
end
