class SmartAds::BoardController < ApplicationController

  @@url = "#{Settings.ES_SERVER_URL_PREFIX}/fivemiles/_search"

  def index
    respond_to do |format|
      format.html do
        render 'all'
      end
    end
  end


  def list_items
    respond_to do |format|
      format.json do

        query = params[:q]

        lat = params[:lat]
        lon = params[:lon]

        cat_id = params[:cat_id]
        city = params[:city]
        query_type = params[:query_type]
        date_from = params[:date_from]
        date_to = params[:date_to]

        now_ts = Time.current.to_i
        #1436776425
        puts now_ts

        if params[:limit].present?
          limit = params[:limit].to_f
        else
          limit = 20
        end
        if params[:offset].present?
          offset = params[:offset].to_f
        else
          offset = 0
        end

        time_start = Time.current.to_i - 3600*24*7

        ctr_query_body = <<-EOF

          {"sort": [
              {
                "user_ctr": {
                  "order": "desc"
                }
              }
            ],"filter": {"and": [
                {
                  "term": {
                    "city": "#{city}"
                  }
                },
                {
                  "exists": {
                    "field": "user_ctr"
                  }
                }
            ]}
          }

        EOF

        query_json = {}

        query_type='ctr_list'

        #if query_type=='ctr_list'
          query_body = ctr_query_body
          query_json = JSON.parse(query_body)

          filter_list = query_json['filter']['and']
          if cat_id
            filter_list<<{"term"=>{"cat_id"=> cat_id}}
          end
          #
        # {
        #     "range": {
        #     "created_at": {
        #     "gte": #{time_start}
        # }
        # }
        # }
          if date_to
            filter_list<<{"range"=>{"created_at"=>{"gte"=>date_to}}}
          end

          if date_from
             filter_list<<{"range"=>{"created_at"=>{"lte"=>date_from}}}
          end
        #end


        query_json['size'] = limit
        query_json['from'] = offset

        puts query_json.to_json

        puts @@url


        res = HttpUtils.send_data(@@url, query_json, query_json.to_json)
        res_json = JSON.parse(res)

        puts res_json.to_json

        total = res_json['hits']['total']
        objects = JSON.parse(res)['hits']['hits']
        next_val = "?offset=#{(offset+limit).to_i}"

        #render :json => res, :callback => 'show'
        render :json => {"meta"=>{"next"=>next_val,"total"=>total}, "objects"=>objects}

        end

    end

  end

  def update_info
    respond_to do |format|
      format.json do
        city_name = params["city"]
        res={}
        sql = "select updated_at, count(1) item_cnt from bk_fb_dpa_items"
        sql += " where city_name = '#{city_name}' group by updated_at order by updated_at desc"
        rows = BkFbDpaItem.connection.select_all(sql).rows
        last_update_time = ""
        last_update_cnt = 0
        total_item_cnt = 0
        if rows.size > 0
          last_update_time = "#{rows[0][0]} (#{((Time.current - rows[0][0]) / 60 / 60).round(1)} hours ago)"
          last_update_cnt = rows[0][1]
          rows.each do |row|
            total_item_cnt += row[1]
          end
        end

        res["data"] = {"last_update_time" => last_update_time, "last_update_cnt" => last_update_cnt, "selected_cnt" => total_item_cnt}

        puts "stat_info = #{res}"
        render json: res
      end
    end
  end

  def fetch_product_feed
    city_name = params["city_name"]
    source = params["source"].blank? ? "local" : params["source"]
    Rails.logger.info("Fetch product feed request from #{source}: city_name=#{city_name}")

    fetch_start_time = Time.zone.now().to_s
    is_success_flag, item_count, output_string = FbDPA::FbProductCategory.generate_fb_product_feed(city_name)

    fetch_end_time = Time.zone.now().to_s

    fetch_log = BkFbFetchLog.new()
    fetch_log.city_name = city_name
    fetch_log.fetch_start_time =  fetch_start_time
    fetch_log.fetch_end_time = fetch_end_time
    fetch_log.deal_status = is_success_flag ? "S" : "X"
    fetch_log.fetch_count = item_count
    fetch_log.save()

    if is_success_flag
      send_data output_string
    else
      send_data ""
    end
  end

  def clear_product_feed
    respond_to do |format|
      format.json do
        city_name = params["city"]
        Rails.logger.info("Clear product feed: city_name=#{city_name}")

        is_success_flag, message = FbDPA::FbProductCategory.clear_product_feed(city_name)

        if is_success_flag
          render json: {"res" => "ok", "data" => {}}
        else
          render json: {"res" => "ng", "data" => {message: message}}
        end
      end
    end
  end

  def add_product_feed
    respond_to do |format|
      format.json do
        city_name = params["city"]
        Rails.logger.info("Add product feed: city_name=#{city_name}")

        selected_list = params["selected_list"]
        item_list = []
        if selected_list.present?
          item_list = selected_list.values()
        end

        is_success_flag, message, success_item_cnt = FbDPA::FbProductCategory.add_product_feed_batch(city_name, item_list)

        if is_success_flag
          render json: {"res" => "ok", "data" => {success_item_cnt: success_item_cnt}}
        else
          render json: {"res" => "ng", "data" => {message: message}}
        end
      end

    end
  end
end
