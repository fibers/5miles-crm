class SmartAudit::BoardController < ApplicationController

  def index
    respond_to do |format|
      format.html do
        render 'all'
      end
    end
  end


  def list_images
    respond_to do |format|
      format.json do
        operator_id = params[:operator_id].blank? ? -1 : params[:operator_id].to_i
        offset = params[:offset].blank? ? 0 : params[:offset].to_i
        limit = params[:limit].blank? ? 100 : params[:limit].to_i
        res= SmartAuditManager.instance.list_images(operator_id, offset: offset, limit: limit)
        render json: {"result" => 0, "body" => res}
      end
    end

  end

  def list_items
    respond_to do |format|
      format.json do
        operator_id = params[:operator_id].blank? ? -1 : params[:operator_id].to_i
        offset = params[:offset].blank? ? 0 : params[:offset].to_i
        limit = params[:limit].blank? ? 100 : params[:limit].to_i
        res= SmartAuditManager.instance.list_items(operator_id, offset: offset, limit: limit)
        res={}
        case params[:fn]
          when 'description_audit'
            res = SmartAuditManager.instance.list_items(operator_id, offset: offset, limit: limit)
          when 'all_pending'
            res = SmartAuditManager.instance.list_all_pending_items(offset: offset, limit: limit)
          when 'unassigned_pending'
            res= SmartAuditManager.instance.list_unassigned_pending_items(offset: offset, limit: limit)
          when 'assigned_pending'
            res= SmartAuditManager.instance.list_assigned_pending_items(offset: offset, limit: limit)
        end
        render json: {"result" => 0, "body" => res}
      end
    end
  end

  def stat_info
    respond_to do |format|
      format.json do
        operator_id = params[:operator_id].blank? ? -1 : params[:operator_id].to_i
        login_op_id = params[:login_op_id].blank? ? -1 : params[:login_op_id].to_i

        my_group = SmartAuditManager.instance.get_my_group(operator_id)
        puts "bk_items_group=#{my_group.to_json}"

        bk_items_group = SmartAuditManager.instance.get_all_pending_group()
        puts "bk_items_group=#{bk_items_group.to_json}"


        audit_operators = SmartAuditManager.instance.get_audit_operators(operator_id)

        # checkin_status
        checkin_status = 0
        agent = AgentManager.instance.get_agent_info(login_op_id)
        if not agent.nil?
          checkin_status = agent.state
        end

        assign_check(agent, audit_operators, login_op_id, operator_id)

        res={}
        res['data'] = my_group.merge(bk_items_group.merge({'operators' => audit_operators.values()})).merge({'checkin_status' => checkin_status})

        res['data'] = res['data']
        puts "stat_info = #{res}"
        render json: res
      end
    end
  end

  def assign_check(agent, audit_operators, login_op_id, operator_id)
    Rails.logger.info "assign_check agent #{login_op_id} #{agent}"
    begin
      # not checked_in

      if agent.nil? or (agent.blank? && agent.state == 0)
        Rails.logger.info "assign_check break: agent.state #{login_op_id} "
        break
      end

      # protect fresh frequently
      break if !$global_redis.set("bk:login_id:#{login_op_id}", 1, ex: 5, nx: true)

      last_dispatch_time = $global_redis.hget("agent:#{login_op_id}", "last_dispatch_time")
      if last_dispatch_time.nil?
        Rails.logger.info "assign_check break: last_dispatch_time.nil #{login_op_id} "
        break
      end

      op_info = audit_operators[login_op_id]
      diff = Time.now.utc - last_dispatch_time.to_time

      # 正审核本人的商品，且无已待审核商品，且距离上次分配时间超过60秒，触发已完成的分配
      if operator_id == login_op_id and op_info.nil? and diff > 60
        SmartAuditManager.instance.assign_data(login_op_id)
        Rails.logger.info "The operator #{login_op_id} has finished all assignment, finish_assigned_trigger called.(assign_check)"
      else
        Rails.logger.info "assign_check cancel #{login_op_id} "
      end

    end while false
    Rails.logger.info "assign_check end #{login_op_id} "
  end

  # 图片审核通过
  def pass_images
    # 当前图片通过，保存audit_tag=img_ok,
    respond_to do |format|
      format.json do
        # 图片审核任务归属
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i
        # 当前登陆者
        login_op_id = params[:login_op_id].blank? ? nil : params[:login_op_id].to_i

        #SmartviewManager.instance.items_imgs_ok(operator_id)
        item_ids = params[:item_ids]
        Rails.logger.info "PASS-IMG-START：login_op_id=#{login_op_id}， operator_id=#{operator_id} , item_ids=#{item_ids}"

        res = nil
        if operator_id
          res = BkItem.where(item_id: item_ids)
                    .where(img_assigned_to: operator_id)
                    .update_all("img_audit_tag='#{Settings.AUDIT_TAG_IMG.ImgOk}',img_op_id=#{login_op_id}")
        end

        Rails.logger.info "PASS-IMG-END： res=#{res}"

        render json: {"res" => "ok", "data" => res}
      end
    end
  end

  def assign_data
    respond_to do |format|
      format.json do
        operator_id = params[:operator_id].blank? ? -1 : params[:operator_id].to_i

        SmartAuditManager.instance.assign_data(operator_id)

        render json: {"res" => "ok", "data" => {}}
      end
    end
  end

  def approve
    respond_to do |format|
      format.json do
        options = {}
        item_id = params[:item_id].to_i
        options[:operator_id] = EmployeeManager.instance.current_employee_id
        options[:operator_name] = EmployeeManager.instance.current_employee_name
        if params[:is_change_weight] == 'true'
          options[:is_change_weight] = true
          options[:weight] = 5
        end
        if params[:item_task_id].present?
          options[:item_task_id] = params[:item_task_id].to_i
          options[:is_ok] = true
        end

        is_success_flag = false
        if options[:item_task_id].blank? and ItemTask.is_has_opened_task(item_id)
          is_success_flag = true
        else
          is_success_flag, message = Audit::ManualAuditService.approve(item_id, options)
        end

        # TODO: finish render logic
        data = {item_id: item_id, item_task_id: options[:item_task_id]}
        if is_success_flag
          render json: {res: "ok", data: data}
        else
          data[:message] = message
          render json: {res: "ng", data: data}
        end
      end
    end
  end

  def item_task_execute
    respond_to do |format|
      format.json do
        options = {}
        is_success_flag, message, task = false, "", nil

        options[:operator_id] = EmployeeManager.instance.current_employee_id
        options[:operator_name] = EmployeeManager.instance.current_employee_name
        item_id = params[:item_id].to_i
        item_task_type = params[:item_task_type]
        item_task_id = nil
        if params[:item_task_id].present?
          item_task_id = params[:item_task_id].to_i
        end

        # Whether exists old_task or not?
        if item_task_id.nil?
          # Create a new task
          is_success_flag, message, item_task = Audit::ManualAuditService.item_task_create(item_id, item_task_type, options)
          if is_success_flag
            # Execute the task when create a task successfully
            is_success_flag, message = Audit::ManualAuditService.item_task_execute(item_task.id, options)
          end
        else
          # Change the item task
          is_success_flag, message = Audit::ManualAuditService.item_task_change_rule_execute(item_task_id, item_task_type, options)
        end

        # TODO: finish render logic
        if is_success_flag
          render json: {"res" => "ok", "data" => {}}
        else
          render json: {"res" => "ng", "data" => {message: message}}
        end
      end
    end
  end

  def set_first_image
    respond_to do |format|
      format.json do
        item_id = params[:item_id].to_i
        image_id = params[:image_id].to_i
        begin
          BackendApiREST.set_item_first_image(item_id, image_id)
        rescue Exception => e
        end

        images = []
        EntityImage.where(item_id: item_id)
            .order(is_first: :desc)
            .select(:id, :item_id, :image_path).each do |image|
          item_image = {}
          item_image['img_url'] = image.image_path
          item_image['img_id'] = image.id
          item_image['is_new'] = false
          images << item_image
        end

        if params[:item_task_id].present?
          item_task = ItemTask.find(params[:item_task_id].to_i)
          if item_task.present? and item_task.bk_item_copy.present?
            item_copy = JSON.load(item_task.bk_item_copy.content).symbolize_keys!
            images.each do |item_image|
              if item_copy[:images].include?(item_image['img_url'])
                item_image['is_new'] = false
              else
                item_image['is_new'] = true
              end
            end
          end
        end

        render json: {res: 'ok', data: {images: images}}
      end
    end
  end

end
