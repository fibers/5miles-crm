class SmartAudit::OperatorsController < ApplicationController
  def index
    # TODO 获取所有check in的operator的信息, 如下:
    @operator_audit_info = []
    audit_operators = SmartAuditManager.instance.get_audit_operators(0);
    checkin_ids = AgentManager.instance.get_checkin_agent_ids;

    #有待审核任务的操作者的统计信息
    audit_operators.each do |operator_id, value|
      checked_in = checkin_ids.include?(operator_id)
      checkin_ids.delete(operator_id)
      value["is_my"] = checked_in ? "Y" : "N"
      @operator_audit_info << value.symbolize_keys
    end
    # 没有待审核任务但check_in的操作者
    checkin_ids.each do |id|
      empl_name = EmployeeManager.instance.get_empl_name(id)
      @operator_audit_info << {item_cnt: 0, id: id, username: empl_name, is_my: "Y"}
    end
  end

  def update
    id = params[:id].to_i
    is_reassign = params[:reassign] == "1" ? true : false
    # clear all items assigned to operator
    if is_reassign
      SmartAuditManager.instance.reassign_data(id)
    end

    # checkout the operator
    AgentManager.instance.checkout(id)


    redirect_to smart_audit_operators_path
  end


  def check_in
    respond_to do |format|
      format.json do
        operator_id = params[:operator_id].blank? ? -1 : params[:operator_id].to_i

        AgentManager.instance.checkin(operator_id)
        render json: {"result"=>0,"body"=>{}}
      end
    end
  end

  def check_out
    respond_to do |format|
      format.json do
        operator_id = params[:operator_id].blank? ? -1 : params[:operator_id].to_i

        AgentManager.instance.checkout(operator_id)
        render json: {"result"=>0,"body"=>{}}
      end
    end
  end
end
