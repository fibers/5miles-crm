class SmartAudit::TasksController < ApplicationController

  def index

  end

  def get_rules
    respond_to do |format|
      format.json do
        @operation_rules = ItemOperationRule.where(status: true)
                               .where("show_type & #{Settings.ItemOperationRules.ShowType.DoNotShow} != #{Settings.ItemOperationRules.ShowType.DoNotShow}")
                               .order(:action, :name).pluck(:id,:name,:action)  #

        #puts @operation_rules

        #render json: {"res"=>"ok","data"=>@operation_rules}
        render json: @operation_rules.to_json
      end
    end
  end

  def get_img_audit_rules
    respond_to do |format|
      format.json do
        @operation_rules = ItemOperationRule.where(status: true)
                               .where("show_type & #{Settings.ItemOperationRules.ShowType.ImageAudit} = #{Settings.ItemOperationRules.ShowType.ImageAudit}")
                               .order(:action, :name).pluck(:id,:name,:action)  #
        render json: @operation_rules.to_json
      end
    end
  end

  def get_often_use_rules
    respond_to do |format|
      format.json do
        @operation_rules = ItemOperationRule.select(:name, :id).where(status: true)
                               .where("show_type & #{Settings.ItemOperationRules.ShowType.OftenUse} = #{Settings.ItemOperationRules.ShowType.OftenUse}")
                               .order(:name)
        render json: @operation_rules.to_json
      end
    end
  end

  def quick_task_exec
    # 图片不好，快速创建task，并且执行
    respond_to do |format|
      format.json do
        puts params

        fetch_size = 30
        item_id = params[:id]
        task_type = params[:task_type]
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i

        #SmartviewManager.instance.right_down(operator_id)
        puts "operator_id=#{operator_id}, item_id=#{item_id}"

        TaskManager.instance.quick_task_exec(task_type, item_id, operator_id, nil)

        # 标记
        res = nil
        if operator_id
          #.where.not(audit_tag:'img_right_down')
          res = BkItem.where(item_id: item_id)
                    .update_all("img_audit_tag='#{Settings.AUDIT_TAG_IMG.ImgTask}', img_op_id=#{operator_id}")
        end
        render json: {"res"=>"ok","data"=>res}
      end
    end
  end

  def query_task_state
    respond_to do |format|
      format.json do
        item_id = params[:item_id].blank? ? nill : params[:item_id]

        task_complete = false

        ret = BkItem.select(:review_state).find_by(item_id: item_id)
        if not Settings.PRODUCT_INTERNAL_STATUSES.PendingReview.eql? ret["review_sate"]
          task_complete = true
        end

        render json: {"res"=>"ok","data"=>{"task_complete"=>task_complete}}
      end
      end
  end

  def item_task_ajax
    case params[:fn]
      when 'task_complete_action'
        data = item_task_complete_action(params[:data].to_h)
        respond_to do |format|
          format.json do
            render json: data
          end
        end
    end
  end

  private

  def item_task_complete_action(options={})
    options.symbolize_keys!
    data = {}
    task = ItemTask.includes(:item, :rule).find(options[:id])
    current_admin_username = session[:current_admin_username]
    is_success_flag = false
    message = ''
    case options[:action_type]
      when 'approve_complete_action'
        approve_complete_options = {is_ok: true}
        if options[:is_change_weight] == 'true'
          approve_complete_options[:weight] = 5
        end
        is_success_flag, message = Utils::BkItemTaskOps.complete(task,
                                                                 current_admin_username,
                                                                 approve_complete_options)
      when 'change_rule_action'
        rule = ItemOperationRule.find_by_name(options[:target_rule_name])
        is_success_flag, message = Utils::BkItemTaskOps.complete(task, current_admin_username, item_operation_rule: rule, is_execute: true)
    end

    if is_success_flag
      data[:res] = 'ok'
      if task.item.internal_status != Settings.PRODUCT_INTERNAL_STATUSES.PendingReview
        data[:delete_row] = true
      end
      is_edit_after_audit = false
      bk_item = BkItem.select(:id, :item_id, :is_edited).find_by_item_id(task.item_id)
      if bk_item.present?
        is_edit_after_audit = bk_item.is_edited
      end
      data[:is_edit_after_audit] = is_edit_after_audit
    else
      data[:res] = 'error'
      data[:error_message] = message
    end

    return data
  end

end
