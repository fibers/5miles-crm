class SmartReview::BkImagesController < ApplicationController

  def index
    respond_to do |format|
      format.html do
        render 'smart_review/bk_items/images'
      end

      format.json do
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i

        list_order = params[:list_order]

        offset = params[:offset]
        if not offset
          offset = 0
        elsif
          offset = offset.to_i
        end

        limit = params[:limit]
        if not limit
          limit = 10
        elsif
          limit = limit.to_i
        end
        puts list_order

        case params[:fn]
          when 'images_data'
            render json: SmartviewManager.instance.get_image_data(operator_id, offset, limit, list_order)
          #when 'audited_imgs'
          #  render json: SmartviewManager.instance.get_audited_imgs_data(offset:offset, limit:limit)
        end
      end
    end
  end
  def stat_info

    respond_to do |format|
      format.json do
        bk_items_group  = BkItem.select("img_op_id, count(id) as item_cnt")
                              .where(operator_id: 0)
                              .where(img_audit_tag: '')
                              .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                              .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                              .group(:img_op_id)
        puts "bk_items_group=#{bk_items_group.to_json}"

        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i

        all_pending_cnt = 0
        rest_pending_cnt = 0
        for it in bk_items_group
          all_pending_cnt = all_pending_cnt+it.item_cnt

          if it.img_op_id == 0
            rest_pending_cnt = rest_pending_cnt+it.item_cnt
          end
        end

        img_operators = SmartviewManager.instance.get_img_operators(operator_id)
        res={}
        res['data'] = {'all_pending_cnt'=>all_pending_cnt,
                       'rest_pending_cnt'=>rest_pending_cnt,
                        'operators'=>img_operators.values()}

        puts "stat_info = #{res}"
        render json: res
      end
    end
  end

  def fetch_img_items
    # 当前图片通过，保存audit_tag=img_ok,
    respond_to do |format|
      format.json do
        fetch_size = 30
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i
        list_order = params[:list_order]

        #SmartviewManager.instance.fetch_img_items(operator_id)

        res = nil
        puts "operator_id=#{operator_id}"
        if operator_id
          res = BkItem.where(img_op_id: 0)
                    .where(operator_id: 0)
                    .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                    .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                    .order(audit_priority: :asc , modified_at: list_order.to_sym)
                    .limit(fetch_size)
                    .update_all("img_op_id=#{operator_id}")
        end

        render json: {"res"=>"ok", "data"=>res}
      end
    end
  end

  def set_items_imgs_pass
    # 当前图片通过，保存audit_tag=img_ok,
    respond_to do |format|
      format.json do
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i
        #SmartviewManager.instance.items_imgs_ok(operator_id)
        item_ids = params[:item_ids]
        puts "operator_id=#{operator_id} , item_ids=#{item_ids}"

        res = nil
        if operator_id
          res = BkItem.where(img_op_id: operator_id)
                    .where(item_id: item_ids)
                    .where(img_audit_tag: '')
                    .update_all("img_audit_tag='#{Settings.AUDIT_TAG_IMG.ImgOk}'")
        end

        puts "res=#{res}"

        render json: {"res"=>"ok", "data"=>res}
      end
    end
  end


  def take_down
    # 图片不好，直接降权，不发消息，记录历史
    respond_to do |format|
      format.json do
        puts params

        fetch_size = 30
        item_id = params[:id]
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i

        #SmartviewManager.instance.right_down(operator_id)
        puts "operator_id=#{operator_id}, item_id=#{item_id}"

        # 标记
        res = nil
        if operator_id
          #.where.not(audit_tag:'img_right_down')
          res = BkItem.where(item_id: item_id)
                        .update_all("img_audit_tag='#{Settings.AUDIT_TAG_IMG.ImgTaskDown}'")
        end

        puts "right_down res=#{res}"

        # todo 直接降权,不发消息
        new_weight = 1
        ret = BackendApiREST.batch_update_item_weight([item_id.to_i], new_weight)
        result = JSON.load(ret[1])
        if result.present? && result["meta"]["result"] == "ok"
          Rails.logger.info 'update weight successfully.'
        else
          Rails.logger.info "update weight failed: #{ret}"
        end

        # todo 记录历史
        op_user = EmployeeManager.instance.get_empl_name(operator_id)
        if not op_user
          op_user_name = operator_id
        else
          op_user_name = op_user['empl_name']
        end

        bk_item_audit_history = BkItemAuditHistory.new
        bk_item_audit_history.item_id = item_id
        bk_item_audit_history.admin = op_user_name
        bk_item_audit_history.action_type = Settings.PRODUCT_INTERNAL_STATUSES.invert[Settings.PRODUCT_INTERNAL_STATUSES.UpdateWeight]
        bk_item_audit_history.action_type_id = Settings.PRODUCT_INTERNAL_STATUSES.UpdateWeight
        bk_item_audit_history.reason = "Image not good, set weght=1."
        #bk_item_audit_history.prev_state = current_state
        bk_item_audit_history.save!

        render json: {"res"=>"ok","data"=>res}
      end
    end
  end


end
