class SmartReview::BkItemsController < ApplicationController

  def index
    respond_to do |format|
      format.html do
        #render 'index', layout: false
        render 'index'
      end

      format.json do
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i
        puts "fn= #{params[:fn]} operator_id=#{operator_id}"
        offset = params[:offset]
        if not offset
          offset = 0
        elsif
        offset = offset.to_i
        end

        limit = params[:limit]
        if not limit
          limit = 10
        elsif
        limit = limit.to_i
        end
        #puts "aaa=#{limit}, #{offset}"

        case params[:fn]
          when 'data'
            res =SmartviewManager.instance.get_smart_review_data(operator_id, offset:offset, limit:limit)
          when 'rest_pending'
            res =SmartviewManager.instance.get_rest_pending_data(offset:offset, limit:limit)
          when 'all_pending'
            res= SmartviewManager.instance.get_all_pending_data(offset:offset, limit:limit)
          when 'all_pending_for_image'
            res= SmartviewManager.instance.get_all_pending_data(offset:offset, limit:limit)
          when 'all_pending_for_desc'
            res= SmartviewManager.instance.get_all_pending_data(offset:offset, limit:limit)
          when 'double_check'
            res = SmartviewManager.instance.get_double_check_data(offset:offset, limit:limit)
          when 'today_check'
            res = SmartviewManager.instance.get_today_check_data(offset:offset, limit:limit)
          when 'pending_review_info'

            assigned_list = SmartviewManager.instance.get_assign_info(operator_id)

            all_pending_img_cnt  = SmartviewManager.instance.get_all_pending_img_cnt()
            all_pending_desc_cnt  = SmartviewManager.instance.get_all_pending_desc_cnt()
            rest_pending_cnt  = SmartviewManager.instance.get_rest_pending_cnt()

            #check_cnt  = SmartviewManager.instance.get_check_cnt()
            today_cnt = SmartviewManager.instance.get_today_check_cnt()

            task_cnt  = TaskManager.instance.opened_task_cnt()
            img_other_task_cnt  = TaskManager.instance.img_other_task_cnt()

            res={}
            #res["check_cnt"]=check_cnt
            res["today_cnt"]=today_cnt
            res["task_cnt"]=task_cnt
            res["img_other_task_cnt"] = img_other_task_cnt
            res["all_pending_img_cnt"]=all_pending_img_cnt
            res["all_pending_desc_cnt"]=all_pending_desc_cnt
            res["rest_pending_cnt"]=rest_pending_cnt
            res["data"]=assigned_list.values()

            puts "check_cnt = #{res}"
        end
        render json: res
      end

    end
  end

  def show

  end

  def get_operator_window
    @operators = Utils::BkOps.get_operators()
    @bk_item = BkItem.find(params[:bk_item_id])

    respond_to do |format|
      format.html { render 'get_operator_window', layout: false }
    end
  end

  def fetch_items

    respond_to do |format|
      format.json do

        fetch_size = params[:fetch_size]   #.default(fetch_size=15)
        #operator_id = params.default(operator_id)
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i
        res = SmartviewManager.instance.fetch_items(fetch_size, operator_id)
        render json: {"res"=>res}
      end
    end

  end



  def change_operator
    begin
      if params[:reason].blank?
        flash[:error] = "Assigning reason is empty."
        break
      end
      @bk_item = BkItem.find_by_id(params[:bk_item_id])
      if @bk_item.blank?
        flash[:error] = "This product does not exist."
      elsif @bk_item.update(operator_id: params[:operator_id], assign_reason: params[:reason])
        flash[:success] = "Modify operator successfully"
      else
        flash[:error] = "Failed to modify operator."
      end
    end while false

    redirect_to :back
  end

  def change_category

  end

  def change_first_image

  end


end
