class SmartReview::TasksController < ApplicationController

  def index

  end

  def get_rules
    respond_to do |format|
      format.json do
        @operation_rules = ItemOperationRule.select(:name, :id).where(status: true).order(:name).pluck(:id,:name)  #

        #puts @operation_rules

        #render json: {"res"=>"ok","data"=>@operation_rules}
        render json: @operation_rules.to_json
      end
    end
  end

  def quick_task_exec
    # 图片不好，快速创建task，并且执行
    respond_to do |format|
      format.json do
        puts params

        fetch_size = 30
        item_id = params[:id]
        task_type = params[:task_type]
        operator_id = params[:operator_id].blank? ? nil : params[:operator_id].to_i

        #SmartviewManager.instance.right_down(operator_id)
        puts "operator_id=#{operator_id}, item_id=#{item_id}"

        TaskManager.instance.quick_task_exec(task_type, item_id, operator_id, nil)

        # 标记
        res = nil
        if operator_id
          #.where.not(audit_tag:'img_right_down')
          res = BkItem.where(item_id: item_id)
                    .update_all("img_audit_tag='#{Settings.AUDIT_TAG_IMG.ImgTask}'")
        end
        render json: {"res"=>"ok","data"=>res}
      end
    end
  end
end
