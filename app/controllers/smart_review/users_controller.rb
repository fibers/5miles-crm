class SmartReview::UsersController < ApplicationController
  def index
    respond_to do |format|
      format.json do
        user_info_data = {}

        EntityUser.where(id: params[:owner_ids]).select(:id, :nickname, :portrait).each do |user|
          user_id = user.id
          user_info_data[user_id] = {}
          user_info_data[user_id]['id'] = user_id
          user_info_data[user_id]['nickname'] = user.nickname
          user_info_data[user_id]['portrait'] = user.portrait
          user_info_data[user_id]['item_total'] = 0
          user_info_data[user_id]['item_listing_count'] = 0
          user_info_data[user_id]['item_sold_count'] = 0
          user_info_data[user_id]['item_unapproved_count'] = 0
          user_info_data[user_id]['item_unavailable_count'] = 0
          user_info_data[user_id]['item_unlisted_count'] = 0
        end

        EntityItem.where(user_id: user_info_data.keys).select(:id, :user_id, :state).find_each do |item|
          user_id = item.user_id
          user_info_data[user_id]['item_total'] += 1
          case item.state
            when Settings.PRODUCT_STATUSES.Listing
              user_info_data[user_id]['item_listing_count'] += 1
            when Settings.PRODUCT_STATUSES.Sold
              user_info_data[user_id]['item_sold_count'] += 1
            when Settings.PRODUCT_STATUSES.Unapproved
              user_info_data[user_id]['item_unapproved_count'] += 1
            when Settings.PRODUCT_STATUSES.Unavailable
              user_info_data[user_id]['item_unavailable_count'] += 1
            when Settings.PRODUCT_STATUSES.Unlisted
              user_info_data[user_id]['item_unlisted_count'] += 1
          end
        end

        User.where(user_id: user_info_data.keys)
            .where(internal_status: Settings.USER_INTERNAL_STATUSES.Risk)
            .select(:id, :user_id, :reason).find_each do |user|
          user_id = user.user_id
          user_info_data[user_id]['risk'] = user.reason
        end

        render json: user_info_data.values()
      end
    end
  end
end
