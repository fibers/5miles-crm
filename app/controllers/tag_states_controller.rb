class TagStatesController < ApplicationController

  before_action :find_tag, only: [:destroy, :create]

  def create
    log_info("Publish the tag '" + @tag.title + "'.")
    if @tag.publish
      flash[:success] = "Publish the tag '" + @tag.title + "' successfully."
    else
      flash[:error] = "Failed to publish the tag '" + @tag.title + "'."
    end
    redirect_to :back
  end

  def destroy
    log_info("Make the tag '" + @tag.title + "' to draft.")
    if @tag.draft
      flash[:success] = "Make the tag '" + @tag.title + "' to draft successfully."
    else
      flash[:error] = "Failed to make the tag '" + @tag.title + "' to draft."
    end
    redirect_to :back
  end

  private
  def find_tag
    @tag = CommodityTag.find(params[:commodity_tag_id])
  end

end
