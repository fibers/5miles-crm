class TagsController < ApplicationController

  before_action :find_tag, only: [:edit, :update, :order]

  def index
    @tags_grid = initialize_grid(
        CommodityTag,
        order: 'commodity_tag._order',
        order_direction: 'asc',
        per_page: Settings.TAGS_PER_PAGE)
  end

  def new
    @tag = CommodityTag.new
  end

  def create
    @tag = CommodityTag.new(create_params)
    @tag.publish_date = Time.now
    log_info("Create a tag '" + @tag.title + "'.")

    if @tag.save
      redirect_to commodity_tags_path
    else
      render 'new'
    end
  end

  def update
    log_info("Update the tag '" + @tag.title + "'.")
    if @tag.update(update_params)
      # if update_params[:status] == Settings.TAG_STATUSES.Invalid.to_s
      #   @tag.products.find_each do |p|
      #     # # 下面的需要api提供接口，不能直接修改
      #     # p.update_time = Time.now
      #     # p.save
      #   end
      #   @tag.related_tags.find_each do |rt|
      #     rt.destroy
      #   end
      # end

      redirect_to commodity_tags_path
    else
      render 'edit'
    end
  end

  def edit
  end

  def order
    @tag.update(order_params)
    redirect_to commodity_tags_path
  end


  private
  def create_params
    params.require(:tag).permit(:title, :status, :_order)
  end

  def update_params
    params.require(:tag).permit(:title, :status, :_order)
  end

  def order_params
    params.permit(:_order)
  end

  def find_tag
    @tag = CommodityTag.find(params[:id])
  end

end
