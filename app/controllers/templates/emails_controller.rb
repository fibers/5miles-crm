class Templates::EmailsController < ApplicationController
  before_action :find_email, only: [:edit, :show, :update, :enable, :disable]

  def index
    @emails_grid = initialize_grid(FmmcMailTemplate.normal_scope,
                                   order: 'fmmc_mail_template.updated_at',
                                   order_direction: 'desc', per_page: Settings.TEMP_EMAILS_PER_PAGE)
  end

  def new
    @email = FmmcMailTemplate.new
  end

  def create
    @email = FmmcMailTemplate.new(email_template_params)
    log_info("Create the Email template '#{@email.name}'.")
    if @email.save
      flash[:success] = "Create the Email template '#{@email.name}' successfully."
      redirect_to templates_email_path(@email)
    else
      flash[:error] = "Failed to create the Email template '#{@email.name}'."
      render :new
    end
  end

  def show

  end

  def edit

  end

  def update
    log_info("Update the Email template '#{@email.name}'.")
    if @email.update(email_template_params)
      flash[:success] = "Update the Email template '#{@email.name}' successfully."
      redirect_to templates_email_path(@email)
    else
      flash[:error] = "Failed to update the Email template '#{@email.name}'."
      redirect_to edit_templates_email_path(@email)
    end
  end

  # def destroy
  #   log_info("Delete the Email template '#{@email.name}'.")
  #   if @email.destroy
  #     flash[:success] = "Delete the Email template '#{@email.name}' successfully."
  #   else
  #     flash[:error] = "Failed to delete the Email template '#{@email.name}'."
  #   end
  #   redirect_to templates_emails_path
  # end

  def enable
    @email.status = Settings.EMAIL_TEMP_STATUSES.Valid
    if @email.save
      flash[:success] = "Enable the Email template '#{@email.name}' successfully."
    else
      flash[:error] = "Failed to Enable the Email template '#{@email.name}'."
    end
    redirect_to :back
  end

  def disable
    @email.status = Settings.EMAIL_TEMP_STATUSES.Disable
    if @email.save
      flash[:success] = "Disable the Email template '#{@email.name}' successfully."
    else
      flash[:error] = "Failed to disable the Email template '#{@email.name}'."
    end
    redirect_to :back

  end

  private
  def find_email
    @email = FmmcMailTemplate.find(params[:id])
  end

  def email_template_params
    params.require(:email).permit(:name, :subject, :content, :comment, :status, :from_address, :notification_type)
  end

end
