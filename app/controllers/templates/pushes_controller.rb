class Templates::PushesController < ApplicationController
  before_action :find_push, only: [:edit, :enable, :disable, :show, :update]

  def index
    @pushes_grid = initialize_grid(FmmcPushTemplate.normal_scope,
                                   order: 'fmmc_push_template.updated_at',
                                   order_direction: 'desc', per_page: Settings.TEMP_PUSHES_PER_PAGE)
  end

  def new
    @push = FmmcPushTemplate.new
  end

  def create
    @push = FmmcPushTemplate.new(push_params)
    log_info("Create the PUSH template '#{@push.name}'.")
    if @push.save
      flash[:success] = "Create the PUSH template '#{@push.name}' successfully."
      redirect_to templates_push_path(@push)
    else
      flash[:error] = "Failed to create the PUSH template '#{@push.name}'."
      render :new
    end

  end

  def show

  end

  def edit

  end

  def update
    log_info("Update the PUSH template '#{@push.name}'.")
    if @push.update(push_params)
      flash[:success] = "Update the PUSH template '#{@push.name}' successfully."
      redirect_to templates_push_path(@push)
    else
      flash[:error] = "Failed to delete the PUSH template '#{@push.name}'."
      redirect_to edit_templates_push_path(@push)
    end
  end

  # def destroy
  #   log_info("Delete the PUSH template '#{@push.name}'.")
  #   if @push.destroy
  #     flash[:success] = "Delete the PUSH template '#{@push.name}' successfully."
  #   else
  #     flash[:error] = "Failed to delete the PUSH template '#{@push.name}'."
  #   end
  #   redirect_to templates_pushes_path
  #
  # end

  def enable
    @push.status = Settings.PUSH_TEMP_STATUSES.Valid
    if @push.save
      flash[:success] = "Enable the PUSH template '#{@push.name}' successfully."
    else
      flash[:error] = "Failed to Enable the PUSH template '#{@push.name}'."
    end
    redirect_to :back
  end

  def disable
    @push.status = Settings.PUSH_TEMP_STATUSES.Disable
    if @push.save
      flash[:success] = "Disable the PUSH template '#{@push.name}' successfully."
    else
      flash[:error] = "Failed to disable the PUSH template '#{@push.name}'."
    end
    redirect_to :back

  end

  private
  def find_push
    @push = FmmcPushTemplate.find(params[:id])
  end

  def push_params
    params.require(:push).permit(:name, :content, :comment, :status, :payload, :title, :flag, :notification_type, :daily_quota)
  end

end
