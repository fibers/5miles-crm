class Templates::SmsesController < ApplicationController

  before_action :find_sms, only: [:edit, :enable, :disable, :show, :update]

  def index
    @smses_grid = initialize_grid(FmmcSmsTemplate.normal_scope,
                                  order: 'fmmc_sms_template.updated_at',
                                  order_direction: 'desc', per_page: Settings.FMMC_PER_PAGE)
  end

  def new
    @sms = FmmcSmsTemplate.new
  end

  def create
    @sms = FmmcSmsTemplate.new(sms_params)
    log_info("Create the SMS template '#{@sms.name}'.")
    if @sms.save
      flash[:success] = "Create the SMS template '#{@sms.name}' successfully."
      redirect_to templates_sms_path(@sms)
    else
      flash[:error] = "Failed to create the SMS template '#{@sms.name}'."
      render :new
    end
  end

  def show

  end

  def edit

  end

  def update
    log_info("Update the SMS template '#{@sms.name}'.")
    if @sms.update(sms_params)
      flash[:success] = "Update the SMS template '#{@sms.name}' successfully."
      redirect_to templates_sms_path(@sms)
    else
      flash[:error] = "Failed to delete the SMS template '#{@sms.name}'."
      redirect_to edit_templates_sms_path(@sms)
    end
  end

  def enable
    @sms.status = Settings.FMMC_STATUSES.Enabled
    if @sms.save
      flash[:success] = "Enable the SMS template '#{@sms.name}' successfully."
    else
      flash[:error] = "Failed to Enable the SMS template '#{@sms.name}'."
    end
    redirect_to :back
  end

  def disable
    @sms.status = Settings.FMMC_STATUSES.Disabled
    if @sms.save
      flash[:success] = "Disable the SMS template '#{@sms.name}' successfully."
    else
      flash[:error] = "Failed to disable the SMS template '#{@sms.name}'."
    end
    redirect_to :back

  end

  private
  def find_sms
    @sms = FmmcSmsTemplate.find(params[:id])
  end

  def sms_params
    params.require(:sms).permit(:name, :content, :comment, :status)
  end
end
