class Templates::SystemMessagesController < ApplicationController
  before_action :find_system_message, only: [:show, :edit, :update, :enable, :disable]

  def index
    @system_messages_grid = initialize_grid(FmmcSystemMessageTemplate.normal_scope,
                                            order: 'fmmc_system_message_template.updated_at',
                                            order_direction: 'desc', per_page: Settings.TEMP_SYSTEM_MESSAGES_PER_PAGE)
  end

  def new
    @system_message = FmmcSystemMessageTemplate.new
  end

  def create
    @system_message = FmmcSystemMessageTemplate.new(system_message_params)
    log_info("Create the system message template '#{@system_message.name}'.")
    if @system_message.save
      flash[:success] = "Create the system message template '#{@system_message.name}' successfully."
      redirect_to templates_system_message_path(@system_message)
    else
      flash[:error] = "Failed to create the system message template '#{@system_message.name}'."
      render :new
    end

  end

  def edit

  end

  def update
    log_info("Update the system message template '#{@system_message.name}'.")
    if @system_message.update(system_message_params)
      flash[:success] = "Update the system message template '#{@system_message.name}' successfully."
      redirect_to templates_system_message_path(@system_message)
    else
      flash[:error] = "Failed to delete the system message template '#{@system_message.name}'."
      redirect_to edit_templates_system_message_path(@system_message)
    end
  end

  # def destroy
  #   log_info("Delete the system message template '#{@system_message.name}'.")
  #   if @system_message.destroy
  #     flash[:success] = "Delete the system message template '#{@system_message.name}' successfully."
  #   else
  #     flash[:error] = "Failed to delete the system message template '#{@system_message.name}'."
  #   end
  #   redirect_to templates_system_messages_path
  #
  # end

  def enable
    @system_message.status = Settings.TEMP_SYSTEM_MESSAGE_STATUSES.Valid
    if @system_message.save
      flash[:success] = "Enable the system message template '#{@system_message.name}' successfully."
    else
      flash[:error] = "Failed to Enable the system message template '#{@system_message.name}'."
    end
    redirect_to :back
  end

  def disable
    @system_message.status = Settings.TEMP_SYSTEM_MESSAGE_STATUSES.Disable
    if @system_message.save
      flash[:success] = "Disable the system message template '#{@system_message.name}' successfully."
    else
      flash[:error] = "Failed to disable the system message template '#{@system_message.name}'."
    end
    redirect_to :back

  end

  def show

  end

  private
  def find_system_message
    @system_message = FmmcSystemMessageTemplate.find(params[:id])
  end

  def system_message_params
    params.require(:system_message).permit(:name, :content, :status, :comment, :image, :action, :desc, :sm_type)
  end

end
