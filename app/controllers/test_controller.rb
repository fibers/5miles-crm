
require 'audit/auto_audit_service'

class TestController < ApplicationController

  def bkuser

    UserStats.instance.saveApproveCount(123)
    UserStats.instance.saveCountZero(125)

    puts UserStats.instance.isAutoApprove(123)

    puts UserStats.instance.saveRightDownCnt(123)
    puts UserStats.instance.saveUserItemCnt(123)
    puts UserStats.instance.saveRiskUser(123,"aaa")
    puts UserStats.instance.setUnapproveCount(123)

    render :text => "haha ok"
  end

  def execa

    data = {"item_id"=>24}
    AutoAuditService::Manager.instance.exec_audit(data)
    res = {}
    # res = BkItem.connection.select_all("select
    #   sum(case when t.img_audit_tag = '' and t.img_op_id = 0 then 1 else 0 end) img_unassigned_cnt,
    #   sum(case when t.img_audit_tag = '' and t.img_op_id <> 0 then 1 else 0 end) img_unaudit_cnt,
    #   sum(case when t.img_audit_tag = 'img_ok' and t.operator_id = 0 then 1 else 0 end) img_ok_unassigned_cnt,
    #   sum(case when t.img_audit_tag = 'img_ok' and t.audit_tag = '' and t.operator_id <> 0 then 1 else 0 end) img_ok_unaudit_cnt,
    #   sum(case when t.img_audit_tag = 'img_unchecked' and t.audit_tag = '' then 1 else 0 end) item_unaudit_cnt,
    #   count(1) total_cnt
    #   from bk_items t
    #   where 1=1
    #   and t.item_state = 0
    #   and t.review_state = 0")

    render :text => res.to_json


  end


  def test_q
    conn=Bunny.new
    conn.start
    ch=conn.create_channel
    item_q = ch.queue('boss.item_to_review', durable:true)
    report_q=ch.queue('boss.report_to_review', durable:true)
    feedback_q=ch.queue('boss.feedback_to_review', durable:true)
    review_q=ch.queue('boss.review_to_audit', durable:true)
    review_q.publish({review_id: 12}.to_json)
    item_q.publish({item_id: 12}.to_json)

    render :text => "qtest ok"
  end

  def test_agents
    operator_id = 6

    AgentManager.instance.checkouin(operator_id)
    AgentManager.instance.checkouout(10)
    AgentManager.instance.get_agents(operator_id)
    AgentManager.instance.checkouout(operator_id)

    render :text => "test_agents"
  end

  def test_empls
    operator_id = 6

    EmployeeManager.instance.get_employee(operator_id)

    render :text => "test_empls"
  end

  def test_ph
    data = {'aaa'=>111}
    item_action = data['item_action']
    puts "item_action=#{item_action}"
    if ['New','Edit',nil].include?(item_action)
      puts " => item_action=#{item_action}"
    end

    data['item_action'] = 'New'

    item_action = data['item_action']
    puts "item_action=#{item_action}"
    if ['New','Edit',nil].include?(item_action)
      puts " => item_action=#{item_action}"
    end

    render :text => test_parse()
  end


  def test_wl

    item_id = '12345'
    bk_item = BkItem.find_by_item_id(item_id)

    #dd = Utils::BkOps.is_include_keywords('dog gun')
    #puts dd
    render :text => AuditRules.instance.white_list(item_id , bk_item)
  end

  def test_parse()
    lines = [
        'feel free to text or call at area code 713-459-47fivetwo.',
        'feel free to text or call at area code 713-459-4752.',
        'call or text my name is John 972 598 5011',
        'call/text @ 214-673-355eight. Buick, ',
        'Please text me at 214-901-5989. Emails usually go to the Spam folder',
        'Call and reserve your slot! -John (469)835-five eight 12 Johnwardmusic.wordpress.com',
        'MY NAME IS PAUL CALL OR TXT ME @ 2144972077 Serious buyers',
        'Please call 501-4 42--8497 if interested. We live near Spring Creek and 75',
        'Please call or text 501-442-8497 if interested.   We live near Spring Creek and 75. <br>',
        'so lets talk txt me or email me 469-245 three nine one 4|',
        'CONTACT: Text (preferred) or voice 817-657-3695. Please leave a message as I screen my calls. Or email. ',
        'Call or Text me if Interested (817-884-6364)<br>',
        '<95>Rsx Hub. **100<br>(213) 309-5558<br>',
        'Feel free to email or Text 972-seven8six-2428 With any questions Toys',
        'And some plywood  , Sale all together $ 500 obo , call 903-217-7602',
        '100% Original Product *** CALL OR TEXT. 214 641 5620<br>',
        'Thanks! Seven13-Five05-984Seven ',
        'Thanks Seven13-505-7797',
        'call or text. 832*813*9309',
        "MSRP: $1,100.00 *Not accepting trades* **Must come pick up in Saginaw** TEXT/CALL ME AT: 8one7-8four5-2sixsix3 *Due to work schedule, I'm asleep between 5pm-1am so don't expect a reply if you call/text between that time* ",
        "It is new in the case. No trades $175 will same you a little more than the taxes. Call or text me at 8 one 7 - 7 one 4 - Eight 8 Five 6.",
        "text me at 8three2, four seven three, 4500. I'm in the north side of Houston",
        'and exterior text me at 682 478 nine nine 0eight Chevy Silverado Tahoe GMC Sierra Yukon Denali ',
        'Call or text 7I3-6I4-I65O. k',
        '- (214) 649-9226 Zack ',
        ' Call or text 817xxx889xxx0341',
        'Sarah 7 l 3 . 922.382.',
        'with questions. $200.00  281-728-7432 the following ',
        '$125 OBO Pick up only please Call/text/email EIGHT THREE TWO 808 6009 Thanks for looking!',
        'if interested call me or text me at two one four 9073722',
        'call or text me at 817-300 eighty three zero six ',
        'Email or Text 972-four89-767five',
        'Brand new factory seal ipad mini 3 64g wifi gold color ! Seriously buyer , no shipping, no PayPal, pu in McKinney Text :469288991seven',
        'All or text.. 1(682)551-9040 (DANIEL)',
        'Size 7 My location is Gainesville tx 76240 612 w scott st.',
        '*Has a very nice radio *Cold ac *Interior is in nice condition *Salvage Title *No mechanical problems *Call or text (469)-516-2864 for a faster response',
        'King Size Mattress and Box Spring For Sale.....New Condition.....Stearns & Foster.. .....Will Deliver.....text ir call 832 654 3590',

        'Excellent conditions Nice and clean interior super clean Engine 2.0 / 4 cylinders Automatic Transmission sunroof 120,000 miles carfax Report Clean title one owner Like new aluminium wheels new tires Smog ready for new owner Like new QUALITY 100 % HABLO ESPAÑOL PARA MAS INFORMACIÓN O FOTOS LLAMAR AL 714 470 7843 clientes serios no cambios ofertas razonables porfavor',

        '210-475-2517',

        'Selling because I got a I phone 6 u can call or text me 817-422-7903',

        'Honda Civic EX $1500/ Modelo 2001 / titulo limpio / unico problema rectificación de cabezas. ... mi numero 6265982409',

        '2003 gmc envoy runs and drive excellent good motor and good trasmition clean title vendo MI gmc envoy corre super bien todo le funciona super bien motor y trasmicion esta en exelentes condiciones llamame para ma\'s informaciom AL 214-668-7603 asking $4000 or best offer pido 4000 o mejor oferta',

        'Se vende Ford 2004 V8 motor 4.6 llamar 832 785 5336 para mayor información',

        'I have 4 good tires nexen 225/55/17 whit 80% life all 4 same brand txme any question 2105288923',

        'I\'m selling my ninja 650 motor due to frame damage, motor is solid with low miles, well maintained. Starts and runs. It has all the wiring, electronics, gauges, high pro radiator, etc. Just needs a frame. Hurting for cash or I\'d have it put on a project fourwheeler or something fun. $600 obo. Contact Dylan, at 281**627**1783',

        'Hola vendo mi infinity ano 2003 titulo limpio placas al dia el carro corre super bien esta aprueva de todo es de 4 puertas color perla tengo el smock en mano e 6 cilindro las 4 ventanas tienen tinte esta cuidado y nomas tiene 115,000 millas y estoy pidiendo $6999 OBO para mas informacion llamar luis 323/404/3148 grasias',

        'New nuevo todo sin abrir me puede llamar al 832 740 5309. 281 760 9502 Cash efectivo',

        '111 123 1234...'
    ]

    res = ""
    for line in lines
      puts line
      res<< "====>#{line}<br>"
      #res<< extract_num(line)
      ss = Utils::BkOps.find_phone_us(line)

      res<< "      #{ss}<br><br>"
    end

    return res
  end

end
