class Tools::HomeBannerController < ApplicationController
  before_action :get_home_banner, only: [:edit, :update, :destroy]

  def edit
  end

  def update
    begin
      if params[:campaign].blank?
        break
      end

      campaign = params[:campaign]
      @delete_prev_image_url = nil

      new_home_banner = CampaignCampaign.new
      new_home_banner.name = Settings.CAMPAIGN.HOME_BANNER_NAME
      new_home_banner.campaign_type = Settings.CAMPAIGN.TYPES.HomeBanner
      invalid_datetime = Time.at(0)
      new_home_banner.image_width = Settings.CAMPAIGN.IMAGE.HomeBanner.With
      new_home_banner.image_height = Settings.CAMPAIGN.IMAGE.HomeBanner.Height

      click_action = ""
      if params[:push].present?
        push = params[:push]
        if push[:action].blank?
          click_action = ''
        elsif push[:action_param].blank?
          if Settings.SYSTEM_MESSAGE_ACTION_PARAMS.keys().include?(push[:action])
            flash[:error] = "Action params must not be empty."
            break
          end
          click_action = push[:action]
        else
          click_action = "#{push[:action]}:#{push[:action_param]}"
        end
      end
      new_home_banner.click_action = click_action

      if campaign[:begin_at].present?
        begin_time = Time.zone.parse(campaign[:begin_at])
        new_home_banner.begin_at = begin_time
      else
        new_home_banner.begin_at = invalid_datetime
      end

      if campaign[:end_at].present?
        end_time = Time.zone.parse(campaign[:end_at]) + 1.day - 1.second
        new_home_banner.end_at = end_time
      else
        new_home_banner.end_at = invalid_datetime
      end

      new_home_banner.comment = campaign[:comment] || ''

      if campaign[:image_url].present?
        new_home_banner.image_url = campaign[:image_url]
      else
        # 如果没有上传图片，说明图片不需要更新
        new_home_banner.image_url = @home_banner.image_url
      end

      if new_home_banner.image_url.blank?
        flash[:error] = "Image url must not be empty."
      else
        log_info(new_home_banner.inspect)
        begin
          ActiveRecord::Base.transaction do
            unless @home_banner.new_record?
              @delete_prev_image_url = @home_banner.image_url
              @home_banner.destroy!
            end
            new_home_banner.save!
            @home_banner = new_home_banner
          end
          flash[:success] = "Update successfully"
        rescue Exception => e
          Rails.logger.error 'Exception: %s' %(e)
          flash[:error] = "Failed to update"
        end
      end

      if flash[:success].present?
        if @delete_prev_image_url.present?
          Cloudinary::Uploader.destroy(@delete_prev_image_url.sub(Settings.IMAGE_URI_PREFIX, ''))
        end
      end

    end while false
    redirect_to edit_tools_home_banner_path
  end

  def destroy
    if @home_banner.id.present?
      invalid_time = Time.at(0)
      @home_banner.begin_at = invalid_time
      @home_banner.end_at = invalid_time
      @home_banner.save
    end
    redirect_to edit_tools_home_banner_path
  end

  private
  def get_home_banner
    @home_banner = CampaignCampaign.find_by_name(Settings.CAMPAIGN.HOME_BANNER_NAME) || CampaignCampaign.new
  end

end
