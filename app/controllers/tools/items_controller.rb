class Tools::ItemsController < ApplicationController
  def index
    @items = []
    @next_page_url_params = {}
    @previous_page_url_params = {}
    @position_to_view = {lon: nil, lat: nil}
    @city = nil

    if params[:city].present?
      @city = params[:city]
    end

    #通过表单提交的情况
    if params[:position].present?
      position = params[:position]
      @position_to_view = position
      get_item_data(position)
    elsif params[:lon].present? and params[:lat].present?
      position = {}
      position['lon'] = params[:lon]
      position['lat'] = params[:lat]
      position['offset'] = params[:offset] if params[:offset].present?
      position['limit'] = params[:limit] if params[:limit].present?

      @position_to_view[:lon] = params[:lon]
      @position_to_view[:lat] = params[:lat]

      get_item_data(position)
    end
  end

  private

  def get_item_data(options={})
    options = options.to_h
    parameters_default = {lat: 0, lon: 0, offset: 0, limit: 20}
    parameters = parameters_default.merge(options.symbolize_keys!)
    uri=URI(Settings.AUCTION.URI)
    uri.query = URI.encode_www_form(parameters)
    log_info uri
    res = Net::HTTP.get_response(uri)
    if res.is_a?(Net::HTTPSuccess)
      data = JSON.parse(res.body)
      @items = data["objects"]
      meta = data["meta"]
      next_url = meta["next"]
      previous_url = meta["previous"]

      if next_url.present?
        next_params = url_query_to_hash(next_url)
        if next_params.present?
          @next_page_url_params = parameters.merge(next_params)
          if @city.present?
            @next_page_url_params.merge!({city: @city})
          end
        end
      end

      if previous_url.present?
        previous_params = url_query_to_hash(previous_url)
        if previous_params.present?
          @previous_page_url_params = parameters.merge(previous_params)
          if @city.present?
            @previous_page_url_params.merge!({city: @city})
          end
        end
      end
    end

    # log_info @items
    log_info @next_page_url_params
    log_info @previous_page_url_params
  end

  def url_query_to_hash(url)
    url_query = URI(url).query
    url_query_hash = URI.decode_www_form(url_query).to_h
    if url_query_hash.present?
      url_query_hash.symbolize_keys!
    end
    return url_query_hash
  end

end
