class Tools::RecommendItemsController < ApplicationController
  def new

  end

  def create
    @is_success_flag = false
    @content = nil
    @send_time = nil
    @system_message = nil
    @push = nil
    @email_subject = nil
    @data = nil
    @email_template = nil
    timestamp = Time.now.to_i
    product_ids = ''

    begin
      if params[:recommend_items][:main_image_url].blank?
        @content = "Main image must upload firstly."
        break
      end

      if params[:is_with_email].to_i == 1
        if params[:email].present? and params[:email][:subject].present?
          @email_subject = params[:email][:subject]
        else
          @content = "Email subject is empty"
          break
        end
      end

      if params[:product_ids].present? and params[:product_ids].join('').present?
        pids_s = []
        params[:product_ids].each { |pid| pids_s << pid if pid.to_i > 0 }
        pids_r = EntityItem.where(id: pids_s).select(:id)
        if pids_r.size > 0
          product_ids = pids_s.join(',')
        else
          @content = "Error: Item IDs(#{params[:product_ids]}) dose not exist."
          break
        end
      else
        @content = 'Error: "Item ID" is empty'
        break
      end

      recommend_items = params[:recommend_items]

      featured_items = EntityFeatureditems.new
      featured_items.title = recommend_items[:title] || ''
      featured_items.banner_url = recommend_items[:main_image_url]
      featured_items.banner_width = recommend_items[:main_image_width].to_i
      featured_items.banner_height = recommend_items[:main_image_height].to_i
      featured_items.items = product_ids
      featured_items.comment = params[:remarks] || ''

      unless featured_items.save
        @content = 'Failed to save items information'
        break
      end

      action = "f:#{featured_items.id}"

      system_message = params[:system_message]
      @system_message = FmmcSystemMessageTemplate.new
      @system_message.name = "#{Settings.MANUAL_MESSAGES_PREFIX.SYSTEM_MESSAGE_COMPOSE_PREFIX}_#{timestamp}"
      @system_message.content = system_message[:content] || ''
      @system_message.status = Settings.TEMP_SYSTEM_MESSAGE_STATUSES.Valid
      @system_message.comment = params[:remarks] || ''
      @system_message.image = system_message[:image] || ''
      @system_message.action = action
      @system_message.desc = ''

      log_info @system_message.inspect

      if params[:is_with_push].to_i == 1 and params[:push].present?
        push = params[:push]
        payload = {"t" => "s", "a" => action}.to_json
        @push = FmmcPushTemplate.new
        @push.name = "#{Settings.MANUAL_MESSAGES_PREFIX.PUSH_COMPOSE_PREFIX}_#{timestamp}"
        @push.content = push[:content] || ''
        @push.status = Settings.PUSH_TEMP_STATUSES.Valid
        @push.comment = params[:remarks] || ''
        @push.payload = payload
        @push.title = push[:title] || ''
        @push.flag = push[:mute].present? ? Settings.PUSH_REMIND_TYPES.Vibration.to_i : Settings.PUSH_REMIND_TYPES.Ring.to_i
        @push.notification_type = Settings.USER_SUBSCRIPTIONS.NOTIFICATION_TYPE.Recommendations

        log_info @push.inspect
      end

      if @email_subject.present?
        @email_template = FmmcMailTemplate.select(:id).find_by_name(Settings.EMAIL_TEMP_NAMES.RecommendItems)
        if @email_template.blank?
          @content = "Email template(#{Settings.EMAIL_TEMP_NAMES.RecommendItems}) dose not exist."
          break
        end
      end

      with_push_or_email = nil
      if @push.present? and @email_subject.blank?
        with_push_or_email = '(with push)'
      elsif @push.blank? and @email_subject.present?
        with_push_or_email = '(with email)'
      elsif @push.present? and @email_subject.present?
        with_push_or_email = '(with push and email)'
      end

      @data = {'items' => []}
      banner = {}
      banner['image_url'] = params[:recommend_items][:main_image_url].gsub(Settings.IMAGE_URI_PREFIX, Settings.RECOMMEND_ITEMS_EMAIL_BANNER_URI_PREFIX)
      @data['banner'] = banner
      pids = product_ids.split(',')
      pids.each do |id|
        p = EntityItem.find_by_id(id)
        if p.present?
          item = {}
          item['title'] = p.title
          item['price'] = p.local_price
          item['original_price'] = format_original_price(p.local_price, p.original_price) if p.original_price.present?
          item['place'] = p.city if p.city.present?
          item['image_url'] = p.images[0].image_path
          item['url'] = "#{Settings.APP_SITE_ITEM_URI_2_PREFIX}#{p.fuzzy_item_id}?utm_source=#{Settings.EMAIL_TEMP_NAMES.RecommendItems}&utm_medium=email&utm_campaign=EDM"
          @data['items'] << item
        end
      end

      if @email_subject.present?
        @data['mail_subject'] = @email_subject # used to send email by group
      end

      if params[:commit] == "Send for test"
        if params[:test_user_id].blank?
          @content = 'Error: User ID for test is empty.'
          break
        end
        user_id = params[:test_user_id].to_i
        user = EntityUser.find_by_id(user_id)
        if user.blank?
          @content = 'Error: User ID for test is not present'
          break
        end
        result = send_for_test_to_user(user_id, @system_message, @push, @email_subject, @data)
        if result.blank?
          @is_success_flag = true
          @content = "Has sent system message#{with_push_or_email} to #{user_id} successfully"
        else
          @content = result
        end
      elsif params[:commit] == "Send"
        if params[:immediate].blank?
          @content = "Please select start time"
          break
        end
        if params[:immediate] == '0'
          datetime_h = params[:date].to_h
          datetime_string = Time.mktime(*datetime_h.values).to_s(:db)
          @send_time = Time.zone.parse(datetime_string)
          if @send_time < Time.current
            @content = "The time(#{@send_time}) has expired"
            break
          end
        end
        targets = ztree_country_tree_get_countries(params[:to])
        if targets.blank?
          @content = 'Error: To parameter is empty'
          break
        end
        if params[:gender].blank?
          @content = 'Error: Gender is empty'
          break
        end

        gender = params[:gender]
        target_gender = gender.sort.join(',')

        if params[:registered_time].blank?
          @content = 'Error: Registered Time is empty'
          break
        end
        registered_time = params[:registered_time]
        is_registered_time = registered_time[:type].present? and registered_time[:value].present? and registered_time[:unit].present? ? true : false
        # "registered_time"=>{"type"=>"1", "value"=>"2", "unit"=>"5"}

        unless @system_message.save
          @content = "Fail to save system message template"
          break
        end
        if @push.present?
          unless @push.save
            @content = "Fail to save push template"
            break
          end
        end

        task = FmmcTask.new
        task.name = "#{Settings.MANUAL_MESSAGES_PREFIX.TASK_COMPOSE_PREFIX}_#{timestamp}"
        task.clazz = "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TASK_CLAZZ}"
        task.status = Settings.FMMC_STATUSES.Enabled
        task.system_message_template = @system_message if @system_message.present?
        task.push_template = @push if @push.present?
        task.mail_template = @email_template if @email_template.present?
        task.comment = params[:remarks] || ''
        if targets[Settings.INTERNAL_USERS_STRING].present?
          task.target_country = Settings.INTERNAL_USERS_STRING
          task.target_region = ''
          task.target_city = ''
        elsif targets[Settings.ALL_USERS_STRING].present?
          task.target_country = ''
          task.target_region = ''
          task.target_city = ''
        else
          task.target_country = targets['country'].present? ? targets['country'].join(',') : ''
          task.target_region = targets['region'].present? ? targets['region'].join(',') : ''
          task.target_city = targets['city'].present? ? targets['city'].join(',') : ''
        end
        task.target_gender = target_gender
        if is_registered_time
          task.target_register_interval_period_id = registered_time[:unit]
          task.target_register_interval_quantity = registered_time[:value]
          task.target_register_interval_type = registered_time[:type]
        end
        unless task.save
          @content = "Error: Fail to save task template"
          break
        end

        trigger = FmmcTrigger.new
        trigger.name = "#{Settings.MANUAL_MESSAGES_PREFIX.TRIGGER_COMPOSE_PREFIX}_#{timestamp}"
        trigger.task = task
        trigger.status = Settings.FMMC_STATUSES.Enabled
        trigger.comment = params[:remarks] || ''
        trigger.at = ''
        if params[:immediate] == '1'
          trigger.trigger_type = Settings.TRIGGER_TYPES.Instant
        else
          trigger.interval_period_id = FmmcFrequencyPeriod.get_id_by_frequency(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)
          interval_quantity = (@send_time - Time.current + 1.send(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)).to_i / 1.send(Settings.MANUAL_MESSAGES_PREFIX.MANUAL_TRIGGER_FREQUENCY)
          trigger.interval_quantity = interval_quantity > 0 ? interval_quantity : 1
          trigger.trigger_type = Settings.TRIGGER_TYPES.Delayed
        end
        unless trigger.save
          @content = "Error: Fail to save trigger template"
          break
        end

        if @push.present?
          payload = JSON.load(@push.payload)
          payload['r'] = "P#{trigger.task_id}"
          @push.update(payload: payload.to_json)
        end

        trigger_params = {}
        trigger_params['data'] = @data
        if FmmcREST.notify(trigger.name, trigger_params)
          @is_success_flag = true
          @content = "Add Task Successfully"
        else
          @content = "Failed to Add Task"
        end
      end
    end while false
  end

  private
  def format_original_price(local_price, original_price)
    original_price.to_s(:currency, unit: local_price.sub(/\d.*/, ''))
  end

  def send_for_test_to_user(uid, system_message, push, email_subject, data)
    results = nil

    log_info("Send system message to #{uid}")
    sm = {}
    sm['text'] = system_message.content if system_message.content.present?
    sm['image'] = system_message.image if system_message.image.present?
    sm['action'] = system_message.action if system_message.action.present?
    sm['desc'] = system_message.desc if system_message.desc.present?
    if FmmcREST.system_message(uid, sm)
      if push.present?
        log_info("Send push to #{uid}")
        pm = {}
        pm['title'] = push.title if push.title.present?
        pm['text'] = push.content if push.content.present?
        pm['payload'] = push.payload if push.payload.present?
        pm['flag'] = push.flag if push.flag.present?
        unless FmmcREST.push(uid, pm)
          results = "Failed to send push to #{uid}"
        end
      end
      if email_subject.present?
        log_info("Send email to #{uid}")
        email_params = {}
        email_params['template'] = Settings.EMAIL_TEMP_NAMES.RecommendItems
        email_params['data'] = data
        unless FmmcREST.send_mail(uid.to_s, email_params)
          results = "Failed to send email to #{uid}"
        end
      end
    else
      results = "Failed to send system message to #{uid}"
    end
    return results
  end

end
