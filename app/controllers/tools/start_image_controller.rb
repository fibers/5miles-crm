class Tools::StartImageController < ApplicationController

  before_action :get_launch, only: [:edit, :update, :destroy]

  def edit
  end

  def update
    begin
      if params[:campaign].blank?
        break
      end
      campaign = params[:campaign]
      @delete_prev_image_url = nil

      new_launch = CampaignCampaign.new
      new_launch.name = Settings.CAMPAIGN.LAUNCH_NAME
      new_launch.campaign_type = Settings.CAMPAIGN.TYPES.Launch
      invalid_datetime = Time.at(0)
      new_launch.image_width = Settings.CAMPAIGN.IMAGE.Launch.With
      new_launch.image_height = Settings.CAMPAIGN.IMAGE.Launch.Height

      click_action = ""
      # if params[:push].present?
      #   push = params[:push]
      #   if push[:action].blank?
      #     click_action = ''
      #   elsif push[:action_param].blank?
      #     if Settings.SYSTEM_MESSAGE_ACTION_PARAMS.keys().include?(push[:action])
      #       flash[:error] = "Action params must not be empty."
      #       break
      #     end
      #     click_action = push[:action]
      #   else
      #     click_action = "#{push[:action]}:#{push[:action_param]}"
      #   end
      # end
      new_launch.click_action = click_action

      if campaign[:begin_at].present?
        begin_time = Time.zone.parse(campaign[:begin_at])
        new_launch.begin_at = begin_time
      else
        new_launch.begin_at = invalid_datetime
      end

      if campaign[:end_at].present?
        end_time = Time.zone.parse(campaign[:end_at]) + 1.day - 1.second
        new_launch.end_at = end_time
      else
        new_launch.end_at = invalid_datetime
      end

      new_launch.comment = campaign[:comment] || ''

      if campaign[:image_url].present?
        new_launch.image_url = campaign[:image_url]
      else
        # 如果没有上传图片，说明图片不需要更新
        new_launch.image_url = @launch.image_url
      end

      if new_launch.image_url.blank?
        flash[:error] = "Image url must not be empty."
      else
        log_info(new_launch.inspect)
        begin
          ActiveRecord::Base.transaction do
            unless @launch.new_record?
              @delete_prev_image_url = @launch.image_url
              @launch.destroy!
            end
            new_launch.save!
            @launch = new_launch
          end
          flash[:success] = "Update successfully"
        rescue Exception => e
          Rails.logger.error 'Exception: %s' %(e)
          flash[:error] = "Failed to update"
        end
      end

      if flash[:success].present?
        if @delete_prev_image_url.present?
          Cloudinary::Uploader.destroy(@delete_prev_image_url.sub(Settings.IMAGE_URI_PREFIX, ''))
        end
      end
    end while false

    redirect_to edit_tools_start_image_path
  end

  def destroy
    if @launch.id.present?
      invalid_time = Time.at(0)
      @launch.begin_at = invalid_time
      @launch.end_at = invalid_time
      @launch.save
    end
    redirect_to edit_tools_start_image_path
  end

  private
  def get_launch
    @launch = CampaignCampaign.find_by_name(Settings.CAMPAIGN.LAUNCH_NAME) || CampaignCampaign.new
  end

end
