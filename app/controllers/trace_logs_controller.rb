class TraceLogsController < ApplicationController
  def index
    @log_grid = initialize_grid(TraceLog,
                                include: [:operator],
                                per_page: Settings.BACKEND_TRACE_LOG_PER_PAGE)
  end
end
