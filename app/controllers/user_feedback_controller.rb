class UserFeedbackController < ApplicationController

  before_action :find_user_feedback, only: [:edit, :update, :create_zendesk_ticket]

  def index
    @user_feedback_grid = initialize_grid(FeedbackUserfeedback,
                                          include: [:user],
                                          order: 'feedback_userfeedback.id',
                                          order_direction: 'desc',
                                          per_page: Settings.USERS_PER_PAGE)
    @unprocessed_feedback_count = FeedbackUserfeedback.where(state: 0).size
    @processed_feedback_count = FeedbackUserfeedback.where(state: 1).size
    @is_display_reply_content = true
    if params[:grid] and params[:grid][:f] and params[:grid][:f][:state]
      is_processed = params[:grid][:f][:state]
      if is_processed == ['f']
        @is_display_reply_content = false
      end
    end
  end

  def edit
    trigger = FmmcTrigger.find_by_name(Settings.TRIGGER_TEMP_NAMES.UserFeedbackReporter)
    @template_contents = {}
    @template_contents['push_content'] = Utils::BkOps.render_template_content(trigger.task.push_template.content,
                                                                              push_content: nil)
    @template_contents['system_message_content'] = Utils::BkOps.render_template_content(trigger.task.system_message_template.content,
                                                                                        system_message_content: nil)
  end

  def update

    begin
      reply_content = ''
      is_send_flag = params[:is_send_flag] == '1' ? true : false
      if is_send_flag
        if params[:push].blank? or params[:push][:content].blank?
          flash[:error] = "Push content is empty"
          break
        end

        if params[:system_message].blank? or params[:system_message][:content].blank?
          flash[:error] = "System message content is empty"
          break
        end
        reply_content = params[:system_message][:content]
      end

      if @user_feedback.update(state: true, reply_content: reply_content)
        if is_send_flag
          trigger_params = {}
          trigger_params['target_user'] = @user_feedback.user_id.to_s
          trigger_params['data'] = {}
          trigger_params['data']['push_content'] = params[:push][:content]
          trigger_params['data']['system_message_content'] = params[:system_message][:content]
          FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.UserFeedbackReporter, trigger_params)
        end

        flash[:success] = "Process feedback successfully."
      else
        flash[:error] = "Failed to process feedback"
      end
    end while false

    redirect_to user_feedback_index_path(grid: {f: {state: ['f']}})

  end

  def create_zendesk_ticket
    is_success_flag, message = Utils::UserFeedbackOps.create_zendesk_ticket(@user_feedback)
    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end

    redirect_to :back
  end

  private
  def find_user_feedback
    @user_feedback = FeedbackUserfeedback.find(params[:id])
  end
end
