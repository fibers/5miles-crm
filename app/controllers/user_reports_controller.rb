require 'fmmc_api'
require 'fmmc_constants'
require 'fmmc_types'

class UserReportsController < ApplicationController

  before_action :find_user_report, only: [:edit, :update, :create_zendesk_ticket]

  def index
    @user_reports_grid = initialize_grid(
        SpamUserreportrecord,
        include: [:user, :reporter],
        per_page: Settings.REPORTS_PER_PAGE)

    @unprocessed_reports = SpamUserreportrecord.where(is_processed: 0).size
    @processed_reports = SpamUserreportrecord.where(is_processed: 1).size
    @report_reason_options_for_select = SpamReportreason.user_report_reasons()
  end

  def edit
    trigger = FmmcTrigger.find_by_name(Settings.TRIGGER_TEMP_NAMES.UserReport)
    @template_contents = {}
    @template_contents['push_content'] = Utils::BkOps.render_template_content(trigger.task.push_template.content,
                                                                              push_content: nil)
    @template_contents['system_message_content'] = Utils::BkOps.render_template_content(trigger.task.system_message_template.content,
                                                                                        system_message_content: nil)
  end

  def update

    begin
      reply_content = ''
      is_send_flag = params[:is_send_flag] == '1' ? true : false
      if is_send_flag
        if params[:push].blank? or params[:push][:content].blank?
          flash[:error] = "Push content is empty"
          break
        end

        if params[:system_message].blank? or params[:system_message][:content].blank?
          flash[:error] = "System message content is empty"
          break
        end
        reply_content = params[:system_message][:content]
      end

      if @user_report.update(is_processed: 1, reply_content: reply_content)
        if is_send_flag
          trigger_params = {}
          trigger_params['target_user'] = @user_report.reporter_id.to_s
          trigger_params['data'] = {}
          trigger_params['data']['push_content'] = params[:push][:content]
          trigger_params['data']['system_message_content'] = params[:system_message][:content]
          trigger_params['data']['fuzzy_user_id'] = @user_report.user.fuzzy_user_id
          FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.UserReport, trigger_params)
        end

        flash[:success] = "Process feedback successfully."
      else
        flash[:error] = "Failed to process feedback"
      end
    end while false

    redirect_to user_reports_path(grid: {f: {is_processed: ['f']}})
  end

  def create_zendesk_ticket
    is_success_flag, message = Utils::UserReportOps.create_zendesk_ticket(@user_report)
    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end

    redirect_to :back
  end

  private
  def find_user_report
    @user_report = SpamUserreportrecord.find(params[:id])
  end

end
