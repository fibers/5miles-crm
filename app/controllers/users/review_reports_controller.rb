class Users::ReviewReportsController < ApplicationController

  before_action :get_review_report, only: [:show, :update, :create_zendesk_ticket]

  def index
    @review_reports_grid = initialize_grid(
        SpamReviewreportrecord,
        include: [:review],
        order: 'spam_reviewreportrecord.id',
        order_direction: 'desc',
        per_page: Settings.USER_REVIEWS_PER_PAGE)

    @unprocessed_reports = SpamReviewreportrecord.where(is_processed: 0).size
    @processed_reports = SpamReviewreportrecord.where(is_processed: 1).size
    @report_reason_options_for_select = SpamReportreason.review_report_reasons()
  end

  def show
    @reporter = EntityUser.select(:id, :nickname).find(@review_report.reporter_id)

  end

  def update
    is_success_flag = false
    message = ""
    begin
      is_send_flag = params[:is_send_flag] == '1' ? true : false
      is_ok = params[:is_ok] == '1' ? true : false
      is_success_flag, message = Utils::UserReviewReportOps.process(@review_report,
                                                                    session[:current_admin_id],
                                                                    session[:current_admin_username],
                                                                    params[:system_message_content],
                                                                    params[:push_content],
                                                                    is_send_flag,
                                                                    is_ok)
    end while false

    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end
    redirect_to users_review_reports_path(
                    wice_grid_custom_filter_params(
                        grid_name: 'grid', attribute: 'is_processed', value: 'f'
                    ))
  end

  def create_zendesk_ticket
    is_success_flag, message = Utils::UserReviewReportOps.create_zendesk_ticket(@review_report)
    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end

    redirect_to :back
  end

  def review_report_ajax
    case params[:fn]
      when "sm_push_template_content"
        trigger_name = ''
        case params[:is_ok]
          when '1'
            trigger_name = Settings.TRIGGER_TEMP_NAMES.UserReviewReportOkReporter
          when '0'
            trigger_name = Settings.TRIGGER_TEMP_NAMES.UserReviewReportDeleteReporter
        end
        data = Utils::UserReviewReportOps.get_template_contents(trigger_name)
        respond_to do |format|
          format.json { render json: data }
        end
      when "processors"
        data = BkUserReviewReportAuditHistory.where(review_report_id: params[:report_ids])
                   .pluck(:review_report_id, :admin)
                   .to_h
        respond_to do |format|
          format.json { render json: data }
        end
    end
  end

  private
  def get_review_report
    @review_report = SpamReviewreportrecord.find(params[:id])
  end
end
