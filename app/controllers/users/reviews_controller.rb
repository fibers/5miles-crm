class Users::ReviewsController < ApplicationController
  def index
    @user_reviews_grid = initialize_grid(ReviewReview,
                                         include: [:reviewer, :reviewee],
                                         order: 'review_review.id',
                                         order_direction: 'desc',
                                         per_page: Settings.USER_REVIEWS_PER_PAGE)
  end

  def update
    is_send_reviewee = false
    if params[:is_send_reviewee] == '1'
      is_send_reviewee = true
    end
    review = ReviewReview.find(params[:id])
    is_success_flag, message = Utils::UserReviewOps.approve(review,
                                                            session[:current_admin_id],
                                                            session[:current_admin_username],
                                                            params[:system_message_content],
                                                            params[:push_content],
                                                            is_send_reviewee)
    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end

    redirect_to request.referer
  end

  def destroy
    review = ReviewReview.find(params[:id])
    is_success_flag, message = Utils::UserReviewOps.disapprove(review,
                                                               session[:current_admin_id],
                                                               session[:current_admin_username],
                                                               params[:reason])
    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end

    redirect_to request.referer
  end

  def review_ajax
    @review = ReviewReview.find(params[:id])
    case params[:fn]
      when "approve_window"
        trigger_name = Settings.TRIGGER_TEMP_NAMES.UserReviewReviewee
        template_contents = Utils::UserReviewOps.get_template_contents(trigger_name, @review)
        @push_template_content = template_contents['push_content']
        @system_message_template_content = template_contents['system_message_content']
        respond_to do |format|
          format.html { render "approve_window", layout: false }
        end
      when "disapprove_window"
        respond_to do |format|
          format.html { render "disapprove_window", layout: false }
        end
    end
  end

end
