class Users::SubscriptionsController < ApplicationController
  before_action :find_subscription, only: [:edit, :update]

  def index
    @subscriptions = initialize_grid(FmmcUserSubscription,
                                     include: [:user],
                                     order: 'fmmc_user_subscription.updated_at',
                                     order_direction: 'desc',
                                     per_page: Settings.USER_SUBSCRIPTIONS_PER_PAGE)
  end

  def new
    @subscription = FmmcUserSubscription.new
  end

  def create
    @subscription = FmmcUserSubscription.new(subscription_params)
    log_info("Create the user subscription #{@subscription.to_json}.")
    if FmmcUserSubscription.where(query_subscription_params).present?
      log_info('The user subscription has already exist.')
      flash[:warning] = 'The user subscription has already exist, please confirm its state.'
      render :new
    else
      if @subscription.save
        flash[:success] = 'Create the user subscription successfully.'
        redirect_to users_subscriptions_path
      else
        flash[:error] = 'Failed to create the user subscription.'
        render :new
      end
    end
  end

  def edit

  end

  def update
    log_info("Update the user subscription #{@subscription.to_json}.")
    if @subscription.update(subscription_params)
      flash[:success] = 'Update the user subscription successfully.'
      redirect_to users_subscriptions_path
    else
      flash[:error] = 'Failed to update user subscription'
      render :edit
    end
  end

  private
  def find_subscription
    @subscription = FmmcUserSubscription.find_by_id(params[:id])
  end

  def query_subscription_params
    params.require(:subscription).permit(:user_id, :notification_type, :notification_method)
  end

  def subscription_params
    params.require(:subscription).permit(:user_id, :notification_type, :notification_method, :state, :comment)
  end

end
