class UsersController < ApplicationController

  before_action :find_user, only: [:show, :portrait, :email]
  before_action :selected_ids, only: [:enable, :disable, :update_weight]
  before_action :get_bk_user_audit_histories, only: [:show]


  def index
    @users_grid = initialize_grid(EntityUser.includes(:products),
                                  order: 'entity_user.id',
                                  order_direction: 'desc',
                                  per_page: Settings.USERS_PER_PAGE)
  end

  def show
    @active_tab = params[:active_tab].blank? ? 'info' : params[:active_tab]
  end

  def map
    @profiles = EntityUser.where(is_robot: 0).select(:id, :nickname, :latitude, :longitude)
  end

  def enable
    # log_info('Batch enable the users(%s).' %[@selected.to_s])
    # is_error_flag = false
    # EntityUser.where(id: @selected).find_each do |user|
    #   unless user.enable
    #     is_error_flag = true
    #     break
    #   end
    # end
    #
    # if is_error_flag
    #   flash[:error] = 'Failed to batch enable the users'
    # else
    #   flash[:success] = 'Batch enable the users successfully.'
    # end
    redirect_to :back
  end

  def disable
    is_success_flag = false
    message = ""
    is_success_flag, message = Utils::UserOps.disable(@selected[0], session[:current_admin_id], session[:current_admin_username], @reason)
    respond_to do |format|
      format.html do
        if is_success_flag
          flash[:success] = message
        else
          flash[:error] = message
        end

        redirect_to :back
      end

      format.json do
        data = {}
        if is_success_flag
          data[:is_error] = false
        else
          data[:is_error] = true
        end
        data[:message] = message

        render json: {res: 'ok', data: data}
      end
    end
  end

  def update_weight
    user_ids = []
    @selected.each { |id| user_ids << id.to_i }

    weight = params[:weight].to_i

    log_info("Update weight to #{weight} about users(#{@selected}).")
    if user_ids.present?
      if weight < Settings.WEIGHT.Min or weight > Settings.WEIGHT.Max
        flash[:warning] = 'Weight value is invalid.'
      else
        ret = BackendApiREST.batch_update_user_weight(user_ids, weight)
        puts ret.inspect
        result = JSON.load(ret[1])
        if params[:id].blank?
          if result["meta"]["result"] == "ok"
            flash[:success] = 'Batch update weight successfully.'
          else
            flash[:error] = "Some user failed(ids: #{result["objects"]})."
          end
        end
      end
    else
      flash[:warning] = 'Please select user firstly.'
    end

    if params[:id].present?
      @user = EntityUser.find_by_id(params[:id])
      respond_to do |format|
        format.js {}
      end
    else
      redirect_to :back
    end
  end

  def portrait
    respond_to do |format|
      format.js {}
    end

  end

  def index_disabled_users
    @users_grid = initialize_grid(DisabledUser,
                                  order_direction: 'desc',
                                  per_page: Settings.USERS_PER_PAGE)
  end

  def index_risk_users
    @risk_users_grid = initialize_grid(User.risk_scope,
                                       order_direction: 'desc',
                                       per_page: Settings.USERS_PER_PAGE)
  end

  def index_bad_users
    @bad_users_grid = initialize_grid(BkBadUser.gt_1_scope,
                                      order_direction: 'desc',
                                      per_page: Settings.USERS_PER_PAGE)
  end

  def user_ajax
    case params[:fn]
      when "set_risk"
        @user_id = params[:id].to_i
        @user_info = User.find_by_user_id(params[:id])
        if @user_info.blank?
          @is_risk_user = false
          @reason = ''
        elsif @user_info.internal_status == Settings.USER_INTERNAL_STATUSES.Risk
          @is_risk_user = true
          @reason = @user_info.reason
        end

        respond_to do |format|
          format.html { render "set_risk_window", layout: false }
        end

      when 'clear_portrait'
        respond_to do |format|
          format.json do
            data = {}
            user_id = params[:id]
            begin
              if user_id.blank?
                data[:status] = 'error'
                data[:message] = "User id is empty"
                break
              end

              ret = BackendApiREST.clear_portrait(user_id)
              unless ret
                data[:status] = 'error'
                data[:message] = "Api error"
                break
              end

              user = EntityUser.select(:id, :portrait).find(user_id)
              data[:status] = 'ok'
              data[:url] = user.portrait
            end while false

            render json: data
          end
        end

      when 'user_tab'
        respond_to do |format|
          format.json do
            options = {}
            options[:user_id] = params[:id]
            options[:user_tab_type] = params[:user_tab_type]
            options[:offset] = params[:offset]
            data = get_user_tab_information(options)
            render json: data
          end
        end
    end
  end

  def user_risk
    is_success_flag = false
    message = ""
    begin
      if params["is_set_risk"] == "0"
        is_success_flag, message = Utils::UserOps.normalize(params[:id], session[:current_admin_id], session[:current_admin_username], "OK")
      else
        is_success_flag, message = Utils::UserOps.risk(params[:id], session[:current_admin_id], session[:current_admin_username], params[:user_risk])
      end
    end while false

    if is_success_flag
      flash[:success] = message
    else
      flash[:error] = message
    end
    redirect_to :back
  end

  private
  def find_user
    @user = EntityUser.includes(products: :images).find(params[:id])
  end

  def selected_ids
    @selected = []
    if params[:id]
      @selected << params[:id]
    elsif params[:grid] && params[:grid][:selected]
      @selected = params[:grid][:selected]
    end
    @reason = params[:id].present? ? params[:reason] : params[:batchReason]
  end

  def get_bk_user_audit_histories
    @bk_user_audit_histories = BkUserAuditHistory.where(user_id: params[:id]).order(id: :desc)
  end

  def user_attr_key_humanize(key)
    case key
      when 'os'
        'register_os'
      when 'os_version'
        'register_os_version'
      when 'app_version'
        'register_app_version'
      when 'device'
        'register_device'
      else
        key
    end.humanize
  end

  def get_user_information(user_id)
    user = EntityUser.find(user_id)
    data = {}
    data['content'] = []
    user.attributes.each_pair do |key, value|
      if key != 'password'
        attr_hash = {}
        attr_hash['key'] = user_attr_key_humanize(key)
        attr_hash['value'] = value
        data['content'] << attr_hash
      end
    end
    if user.user_info.present? and user.user_info.internal_status == Settings.USER_INTERNAL_STATUSES.Risk
      attr_hash = {}
      attr_hash['key'] = 'risk_reason'.humanize
      attr_hash['value'] = user.user_info.reason
      data['content'] << attr_hash
    end
    return data
  end

  def get_user_items(user_id, offset)
    data = {}
    data['content'] = []
    limit_per_page = 10
    item_offset_current_page = offset
    if item_offset_current_page < 0
      item_offset_current_page = 0
    end
    items_current_page = EntityItem.where(user_id: user_id).offset(item_offset_current_page).limit(limit_per_page).order(id: :desc)

    item_ids = []
    item_data = {}
    items_current_page.each do |item|
      item_id = item.id
      item_ids << item_id
      item_data[item_id] = {}
      item_data[item_id]['id'] = item_id
      item_data[item_id]['images'] = item.images.order(is_first: :desc).pluck(:image_path)
      item_data[item_id]['title'] = item.title
      item_data[item_id]['created_time'] = item.created_at.to_s
      item_data[item_id]['censored'] = Settings.PRODUCT_INTERNAL_STATUSES.invert[item.internal_status]
      item_data[item_id]['state'] = Settings.PRODUCT_STATUSES.invert[item.state]
      item_data[item_id]['offer_count'] = 0
      item_data[item_id]['review_count'] = 0
    end
    if item_ids.present?
      ret = BackendApiREST.get_offer_count_by_item_ids(item_ids)
      if ret[0]
        JSON.load(ret[1])['objects'].each do |item|
          item_data[item['item_id'].to_i]['offer_count'] = item['count'].to_i
        end
      end
      ReviewReview.where(item_id: item_ids).group(:item_id).count().each_pair do |item_id, count|
        item_data[item_id]['review_count'] = count
      end
    end
    data['content'] = item_data.values()

    item_offset_next_page = item_offset_current_page + items_current_page.size
    item_total = EntityItem.where(user_id: user_id).size

    ret = generate_pagination_params(item_total, item_offset_current_page, item_offset_next_page, limit_per_page)
    data['pagination'] = ret['pagination']
    return data
  end

  def get_user_zendesk_ticket(user_email, offset)
    data = {}
    data['content'] = []
    limit_per_page = 10
    ticket_offset_current_page = offset
    if ticket_offset_current_page < 0
      ticket_offset_current_page = 0
    end
    tickets_current_page = BkZendeskTicket.where(req_email: user_email).offset(ticket_offset_current_page).limit(limit_per_page).order(tid: :desc)

    ticket_ids = []
    ticket_data = {}
    tickets_current_page.each do |ticket|
      ticket_id = ticket.tid
      ticket_ids << ticket_id
      ticket_data[ticket_id] = {}
      ticket_data[ticket_id]['tid'] = ticket_id
      ticket_data[ticket_id]['via'] = ticket.via
      ticket_data[ticket_id]['subject'] = ticket.subject
      ticket_data[ticket_id]['created_at'] = ticket.created_at.to_s
      ticket_data[ticket_id]['status'] = ticket.status
      ticket_data[ticket_id]['current_tags'] = ticket.current_tags
      ticket_data[ticket_id]['assignee_name'] = ticket.assignee_name
    end
    data['content'] = ticket_data.values()

    ticket_offset_next_page = ticket_offset_current_page + tickets_current_page.size
    tickets_total = BkZendeskTicket.where(req_email: user_email).size

    ret = generate_pagination_params(tickets_total, ticket_offset_current_page, ticket_offset_next_page, limit_per_page)
    data['pagination'] = ret['pagination']
    return data
  end

  def get_offered_items(user_id)
    user_id = user_id.to_i
    data = {}
    data['content'] = []
    offered_items = []
    ret = BackendApiREST.get_offered_items_by_user_id(user_id)
    if ret[0]
      offered_items = JSON.load(ret[1])
    end
    if offered_items.present?
      temp_data = {}
      offered_items.each do |item|
        item_id = item["item_id"].to_i
        temp_data[item_id] = {}
        temp_data[item_id]["id"] = item_id
        temp_data[item_id]["last_offered_at"] = Time.zone.at(item["last_timestamp"].to_i).to_s
      end
      item_ids = temp_data.keys
      seller_info = {}
      item_audit_states = Settings.PRODUCT_INTERNAL_STATUSES.invert
      item_states = Settings.PRODUCT_STATUSES.invert
      EntityItem.select(:id, :user_id, :title, :internal_status, :state).where(id: item_ids).find_each do |item|
        if seller_info[item.user_id].blank?
          seller_info[item.user_id] = {}
          seller_info[item.user_id]["item_ids"] = []
        end
        seller_info[item.user_id]["item_ids"] << item.id
        temp_data[item.id]["seller_id"] = item.user_id
        temp_data[item.id]["title"] = item.title
        temp_data[item.id]["audit_state"] = item_audit_states[item.internal_status]
        temp_data[item.id]["state"] = item_states[item.state]
        temp_data[item.id]["images"] = item.images.order(is_first: :desc).pluck(:image_path)
      end

      seller_ids = seller_info.keys
      EntityUser.select(:id, :nickname).where(id: seller_ids).find_each do |user|
        seller_info[user.id]["item_ids"].each do |item_id|
          temp_data[item_id]["seller_nickname"] = user.nickname
        end
      end

      data['content'] = temp_data.values
    end

    return data
  end

  def get_offer_messages(user_id)
    user_id = user_id.to_i
    user_id_str = user_id.to_s
    data = {}
    data['content'] = []
    offer_messages = []
    ret=BackendApiREST.get_offers_by_user_id(user_id)
    if ret[0]
      offer_messages = JSON.load(ret[1])
    end
    if offer_messages.present?
      offer_messages.each do |message|
        temp_data = {}
        if message['to'] == user_id_str
          temp_data['direction'] = 1
          temp_data['user_id'] = message['from'].to_i
        else
          temp_data['direction'] = 0
          temp_data['user_id'] = message['to'].to_i
        end

        temp_data['item_id'] = message['item_id'].to_i
        temp_data['owner_id'] = message['owner'].to_i
        temp_data['current_user_id'] = user_id
        temp_data['offer_type'] = message['offer_type'].to_i
        payload = {}
        begin
          payload = JSON.load(message['payload'])
          if payload.nil?
            # "payload"=>""
            payload = {}
            payload['text'] = ''
          end
          unless payload.is_a?(Hash)
            # "payload"=>"45"
            payload = {}
            payload['text'] = message['payload']
          end
        rescue
          # "payload"=>"Thanks"}
          payload['text'] = message['payload']
        end
        temp_data['time'] = Time.zone.parse(message['timestamp']).to_s
        temp_data['payload'] = payload

        flags = {}
        flags['score_flag'] = payload['score'].nil? ? 0 : 1
        flags['text_flag'] = payload['text'].nil? ? 0 : 1
        flags['address_flag'] = payload['address_map_thumb'].nil? ? 0 : 1
        flags['picture_flag'] = payload['picture_url'].nil? ? 0 : 1
        flags['other_flag'] = (flags['score_flag'] | flags['text_flag'] | flags['address_flag'] | flags['picture_flag']) ^ 1
        temp_data['flags'] = flags

        # 针对新增加的类型
        if flags['other_flag'] == 1
          temp_data['payload'] = payload.inspect
        end

        data['content'] << temp_data
      end
    end

    return data
  end

  def get_user_statistic_info(user_id)
    data = {}
    data['content'] = []
    ret = BackendApiREST.get_user_statistic_data(user_id)
    user_action_statistics = JSON.load(ret[1])["objects"]
    table_data = []
    Settings.USER_ACTION_STATISTICS_MAP.First.each do |key, value|
      tmp_data = {}
      tmp_data['key'] = value
      tmp_data['value'] = user_action_statistics[key]
      table_data << tmp_data
    end
    data['content'] << table_data
    table_data = []
    Settings.USER_ACTION_STATISTICS_MAP.Second.each do |key, value|
      tmp_data = {}
      tmp_data['key'] = value
      tmp_data['value'] = user_action_statistics[key]
      table_data << tmp_data
    end
    data['content'] << table_data
    return data
  end

  def get_user_audit_info(user_id)
    data = {}
    data['content'] = []
    bk_user_audit_histories = BkUserAuditHistory.where(user_id: user_id).order(id: :desc)
    operators = {}
    bk_user_audit_histories.each do |audit|
      operators[audit.admin_id] = ''
    end
    Admin.select(:id, :username).where(id: operators.keys()).each do |op|
      operators[op.id] = op.username
    end
    bk_user_audit_histories.each do |audit|
      tmp_audit = {}
      tmp_audit['action'] = Settings.USER_INTERNAL_STATUSES.invert[audit.action_type]
      tmp_audit['operator'] = operators[audit.admin_id]
      tmp_audit['audit_time'] = audit.created_at.to_s
      tmp_audit['reason'] = audit.reason
      data['content'] << tmp_audit
    end
    return data
  end

  def get_user_user_report(user_id)
    data = {}
    data['content'] = []
    user_reports = SpamUserreportrecord.where(user_id: user_id).order(id: :desc)
    reporters = {}
    user_reports.each do |ur|
      reporters[ur.reporter_id] = ''
    end
    EntityUser.select(:id, :nickname).where(id: reporters.keys()).each do |r|
      reporters[r.id] = r.nickname
    end
    report_reason_options_for_select = SpamReportreason.user_report_reasons()
    user_reports.each do |ur|
      tmp_ur = {}
      tmp_ur['id'] = ur.id
      tmp_ur['reporter_name'] = reporters[ur.reporter_id]
      tmp_ur['reporter_id'] = ur.reporter_id
      tmp_ur['reason'] = report_reason_options_for_select.invert[ur.reason]
      tmp_ur['reason_content'] = ur.reason_content
      tmp_ur['created_time'] = ur.created_at.to_s
      data['content'] << tmp_ur
    end

    return data
  end

  def get_user_item_report(user_id, item_ids)
    data = {}
    data['content'] = []
    item_reports = SpamItemreportrecord.where(item_id: item_ids).order(id: :desc)
    reporters = {}
    item_reports.each do |ir|
      reporters[ir.reporter_id] = ''
    end
    EntityUser.select(:id, :nickname).where(id: reporters.keys()).each do |r|
      reporters[r.id] = r.nickname
    end
    reported_items = {}
    item_reports.each do |ir|
      reported_items[ir.item_id.to_i] = ''
    end
    EntityItem.select(:id, :title).where(id: reported_items.keys()).each do |i|
      reported_items[i.id] = i.title
    end
    report_reason_options_for_select = SpamReportreason.item_report_reasons()
    item_reports.each do |ir|
      tmp_ir = {}
      tmp_ir['id'] = ir.id
      tmp_ir['item_id'] = ir.item_id
      tmp_ir['item_title'] = reported_items[ir.item_id.to_i]
      tmp_ir['reporter_id'] = ir.reporter_id
      tmp_ir['reporter_name'] = reporters[ir.reporter_id]
      tmp_ir['reason'] = report_reason_options_for_select.invert[ir.reason]
      tmp_ir['reason_content'] = ir.reason_content
      tmp_ir['created_time'] = ir.created_at.to_s
      data['content'] << tmp_ir
    end

    return data
  end

  def get_user_tab_information(options={})
    data = {}
    user_id = options[:user_id].to_i
    user_tab_type = options[:user_tab_type]
    item_ids = EntityItem.where(user_id: user_id).pluck(:id)
    user_email = EntityUser.select(:email).find(user_id).email
    data['header'] = {}
    data['header']['items'] = item_ids.size
    data['header']['audits'] = BkUserAuditHistory.where(user_id: user_id).count
    data['header']['user_reports'] = SpamUserreportrecord.where(user_id: user_id).count()
    data['header']['item_reports'] = SpamItemreportrecord.where(item_id: item_ids).count()
    data['header']['offered_items'] = 0
    data['header']['offer_messages'] = 0
    data['header']['zendesk_ticket'] = BkZendeskTicket.where(req_email: user_email).size
    ret = BackendApiREST.get_offered_item_count_by_user_id(user_id)
    if ret[0]
      offered_item_count = JSON.load(ret[1])['count']
      data['header']['offered_items'] = offered_item_count
    end
    ret=BackendApiREST.get_offer_count_by_user_id(user_id)
    if ret[0]
      offer_message_count = JSON.load(ret[1])['count']
      data['header']['offer_messages'] = offer_message_count
    end
    case user_tab_type
      when 'info'
        ret = get_user_information(user_id)
        data['content'] = ret['content']

      when 'items'
        ret = get_user_items(user_id, options[:offset].to_i)
        data['content'] = ret['content']
        data['pagination'] = ret['pagination']

      when 'offered_items'
        ret = get_offered_items(user_id)
        data['content'] = ret['content']

      when 'offer_messages'
        ret = get_offer_messages(user_id)
        data['content'] = ret['content']

      when 'zendesk_ticket'
        ret = get_user_zendesk_ticket(user_email, options[:offset].to_i)
        data['content'] = ret['content']
        data['pagination'] = ret['pagination']

      when 'statistics'
        ret = get_user_statistic_info(user_id)
        data['content'] = ret['content']

      when 'audits'
        ret = get_user_audit_info(user_id)
        data['content'] = ret['content']

      when 'user_reports'
        ret = get_user_user_report(user_id)
        data['content'] = ret['content']

      when 'item_reports'
        ret = get_user_item_report(user_id, item_ids)
        data['content'] = ret['content']
    end
    return data
  end

end
