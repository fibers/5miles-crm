class WelcomeController < ApplicationController

  layout false, only: [:login, :create]

  def index

  end

  def login

  end

  def create
    log_info('%s tried to login.' %[params[:username]])
    admin = Admin.find_by_username(params[:username])
    if admin && admin.is_enabled? && admin.authenticate(params[:password])
      previous_url = (session[:previous_url] != login_path and session[:previous_url] != logout_path) ? session[:previous_url] : nil
      session[:previous_url] = nil
      session[:current_admin_id] = admin.id
      session[:current_admin_username] = admin.username
      session[:is_super_admin] = admin.is_super_admin
      session[:is_root_admin] = admin.username.eql?("root")
      flash[:success] = 'Login successfully'
      log_info('Login successfully')
      redirect_to previous_url.present? ? previous_url : index_path
    else
      flash[:error] = 'Invalid name/password combination'
      render 'login'
    end
  end

  def destroy
    log_info('Logout successfully')
    session[:current_admin_id] = nil
    session[:current_admin_username] = nil
    session[:is_super_admin] = nil
    reset_session
    flash[:success] = 'Logout successfully'
    redirect_to login_path
  end

end
