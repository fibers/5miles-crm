module AdminsHelper
  def admin_attr_value(admin, attr)
    case attr
      when 'state'
        Settings.BACKEND_USER.States.invert[admin.state]
      when 'user_type'
        Settings.BACKEND_USER.Types.invert[admin.user_type]
      else
        admin.send(attr)
    end
  end
end
