module ApplicationHelper

  def wice_grid_custom_filter_params_days(opts = {})
    options = {
        :grid_name => 'grid',
        :attribute => nil,
        :days => 0
    }
    options.merge!(opts)

    [:attribute, :days].each do |key|
      raise ::Wice::WiceGridArgumentError.new("wice_grid_custom_filter_params_days: :#{key} is a mandatory argument") unless options[key]
    end

    attr_name = options[:attribute]
    days = options[:days]
    date_range = {
        fr: (Date.current - days.days).to_s(:db),
        to: Date.current.to_s(:db)
    }

    {"#{options[:grid_name]}[f][#{attr_name}]" => date_range}

  end

  def is_manual_template(template)
    template.name =~ /^#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}/ ? true : false
  end

  def image_tag_content_100_100(options={})
    options = options.symbolize_keys.merge(height: 100)
    image_tag_content_with_options(options)
  end

  def image_tag_content_200_200(options={})
    options = options.symbolize_keys.merge(width: 200)
    image_tag_content_with_options(options)
  end

  def image_tag_content_400_400(options={})
    options = options.symbolize_keys.merge(width: 400)
    image_tag_content_with_options(options)
  end

  def attr_value(item, attr_name)
    return item.send(attr_name)
  end

  def is_campaign_valid(campaign)
    invalid_time = Time.at(0)
    if campaign.new_record? or campaign.begin_at == invalid_time or campaign.end_at == invalid_time
      return false
    else
      return true
    end
  end

  def campaign_status(campaign)
    if not is_campaign_valid(campaign)
      return 'Disabled'
    elsif Time.current > campaign.end_at
      return 'Expired'
    else
      return 'Valid'
    end
  end

  def campaign_click_action_tag(click_action_params)
    select_tag_params = {}
    select_tag_params['name'] = 'push[action]'
    select_tag_params['option_tags'] = options_for_select(Settings.SYSTEM_MESSAGE_ACTIONS.to_a)
    select_tag_params['options'] = {}
    select_tag_params['options']['prompt'] = 'Please select an action'
    select_tag_params['options']['class'] = 'form-control'
    select_tag_params['options']['style'] = "width: 50%;display: inline-block;"

    input_tag_params = {}
    input_tag_params['name'] = 'push[action_param]'
    input_tag_params['value'] = nil
    input_tag_params['options'] = {}
    input_tag_params['options']['class'] = 'form-control hidden'
    input_tag_params['options']['style'] = "width: 45%;display: inline-block;float: right;"


    click_action_params = '' if click_action_params.blank?
    click_action = click_action_params.split(':')

    if click_action.size > 1
      input_tag_params['value'] = click_action[1..-1].join('')
      input_tag_params['options']['class'] = 'form-control'
      input_tag_params['options']['placeholder'] = Settings.SYSTEM_MESSAGE_ACTION_PARAMS[click_action[0]]
      input_tag_params['options']['required'] = 'required'
    end
    click_action_select_tag = select_tag(select_tag_params['name'], select_tag_params['option_tags'], select_tag_params['options'].symbolize_keys)
    click_action_input_tag = text_field_tag(input_tag_params['name'], input_tag_params['value'], input_tag_params['options'].symbolize_keys)
    click_action_script_tag = content_tag(:script) do
      raw <<-EOF
  #{'var push_action_params =  ' + Settings.SYSTEM_MESSAGE_ACTION_PARAMS.to_json};
  #{'var push_action_params_default_key = "' + click_action[0] + '";' if click_action.size > 0}
  #{'var push_action_params_default_value = "' + input_tag_params['value'] + '";' if click_action.size > 1}
  #{'$(document).ready(function(){ $(\'select#push_action option[value="'+ click_action[0] +'"]\').attr("selected", "selected"); });' if click_action.size > 0}
      EOF
    end
    content_tag(:div) do
      click_action_select_tag + click_action_input_tag + click_action_script_tag
    end
  end

  def pagination_tag()
    html_content = <<-EOF
    <script id="tmpl_pagination" type="text/x-jquery-tmpl">
      <ul class="pager">
        {{if pagination['current']['offset'] > 0}}
          <li>
            <a data-pagination-offset=${pagination['previous']['offset']}>&larr; Previous</a>
          </li>
        {{else}}
          <li class="disabled">
            <a>&larr; Previous</a>
          </li>
        {{/if}}

          <li>
            {{if pagination['total'] == 0}}
              0 - 0 / 0
            {{else}}
              ${pagination['current']['offset']+1} - ${pagination['next']['offset']} / ${pagination['total']}
            {{/if}}
          </li>

        {{if pagination['next']['offset'] < pagination['total']}}
          <li>
            <a data-pagination-offset=${pagination['next']['offset']}>Next &rarr;</a>
          </li>
        {{else}}
          <li class="disabled">
            <a>Next &rarr;</a>
          </li>
        {{/if}}
      </ul>
    </script>
    <script>
      function pagination_get_data(request_url, request_data, success_callback, failed_callback) {
          $('ul.pager > li[class!="disabled"] > a').click(function (e) {
              console.log($(this));
              e.preventDefault();
              var offset = $(this).data('pagination-offset');
              var tmp_data = {};
              console.log(offset);
              $.extend(tmp_data, request_data);
              tmp_data['offset'] = offset;
              $.ajax({
                  url: request_url,
                  type: "GET",
                  data: tmp_data,
                  cache: false,
                  timeout: #{Settings.AJAX.Timeout},
                  success: function (data) {
                      success_callback(data);
                  },
                  error: function (xhr, status, error) {
                      failed_callback(xhr, status, error);
                  }
              });
          });
      }
    </script>
    EOF

    raw html_content
  end

  def ztree_country_tree_tag(name)
    country_tree_tag = <<-EOF
<ul id="country_tree" class="ztree"></ul>
<input id="checked_countries" name="#{name}" type="text" value="{}" hidden>
    EOF

    raw country_tree_tag
  end

  def ztree_country_tree_js_tag()
    country_tree_js_tag = <<-EOF
<script>
    var setting = {
        async: {
            enable: true,
            url: "/api/country.json",
            autoParam: ["name", "level", "tId", "parentTId", "id", "pId"]
        },
        callback: {
            beforeCheck: ztree_country_before_check,
            onAsyncError: ztree_country_async_error,
            onAsyncSuccess: ztree_country_async_success,
            onCheck: ztree_country_on_check
        },
        check: {
            enable: true
        },
        data: {
        },
        edit: {
        },
        view: {
            enable: true,
            showIcon: false
        }
    };

    function ztree_country_async_error(event, treeId, node, XMLHttpRequest, textStatus, errorThrown) {
        alert("Failed to get information, Please try again");
    }

    function ztree_country_async_success(event, treeId, node, msg) {
//        console.log(event, treeId, node, msg);
          var country_tree = $.fn.zTree.getZTreeObj("country_tree");
          country_tree.refresh();
    }

    function ztree_country_before_check(treeId, node) {
        console.log(treeId, node);
        var status = node.getCheckStatus();
        var ret = true;
        var country_tree = $.fn.zTree.getZTreeObj("country_tree");
        var checked_nodes = country_tree.getCheckedNodes(true);
        if (status.checked === true) {
            ret = confirm("Will clean all checked selection.\\nAre you confirm?");
        } else if (checked_nodes.length > 0) {
            var is_need_alert = false;
            do {
                var last_checked_node = checked_nodes[checked_nodes.length - 1];
                if (node.parentTId == last_checked_node.tId) {
                    is_need_alert = false;
                    break;
                }
                var tree_node = node.getParentNode();
                while (tree_node) {
                    if (tree_node.parentTId == last_checked_node.tId) {
                        is_need_alert = false;
                        break;
                    }
                    tree_node = tree_node.getParentNode();
                }
                if (tree_node) {
                    break;
                }
                if (node.level == 3 && node.parentTId == last_checked_node.parentTId) {
                    is_need_alert = false;
                } else {
                    is_need_alert = true;
                }
            } while (false);
            if (is_need_alert) {
                alert("You can only select one country or state!");
                ret = false;
            }
        }
        console.log(ret);
        return ret;
    }

    function ztree_country_on_check(event, treeId, node) {
        console.log(event, treeId, node);
        var country_tree = $.fn.zTree.getZTreeObj("country_tree");

        if (node.checked === true) {
            var ztree_node = node;
            while (ztree_node != null) {
                if (ztree_node.checked == false) {
                    country_tree.checkNode(ztree_node, true, true);
                }
                if (ztree_node.level < 3) {
                    for (var peer_node = ztree_node.getPreNode(); peer_node != null; peer_node = peer_node.getPreNode()) {
                        if (peer_node.checked) {
                            country_tree.checkNode(peer_node, false, true);
                        }
                    }

                    for (var peer_node = ztree_node.getNextNode(); peer_node != null; peer_node = peer_node.getNextNode()) {
                        if (peer_node.checked) {
                            country_tree.checkNode(peer_node, false, true);
                        }
                    }
                }
                ztree_node = ztree_node.getParentNode();
            }
        } else {
            country_tree.checkAllNodes(false);
        }
    }

    var zNodes = #{ [{name: Settings.INTERNAL_USERS_STRING, isParent: false}, {name: Settings.ALL_USERS_STRING, isParent: true}].to_json };

    $(document).ready(function () {
        $.fn.zTree.init($("#country_tree"), setting, zNodes);
    });
</script>
    EOF

    raw country_tree_js_tag
  end

  def ztree_category_tree_tag()
    category_tree_tag = <<-EOF
<div style="float: right !important; margin: 5px;" >
    <label for="is_edit_category_tree" style="cursor: pointer;">
        <input type="checkbox" id="is_edit_category_tree">Edit Category Tree
    </label>
</div>
<ul id="category_tree" class="ztree"></ul>
    EOF

    raw category_tree_tag
  end

  def ztree_category_tree_js_tag(category_data_json)
    category_tree_js_tag = <<-EOF
<script>
    var setting = {
        async: {
            enable: true,
            url: "/api/category.json",
            autoParam: ["name", "level", "tId", "parentTId"]
        },
        callback: {
            beforeDrag: before_drag,
            beforeDrop: before_drop,
            beforeRename: before_rename_category,
            onRename: rename_category,
            onAsyncError: async_error,
            onAsyncSuccess: async_success
        },
        check: {},
        data: {
            simpleData: {
                enable: true
            }
        },
        edit: {
            enable: false,
            showRemoveBtn: false
        },
        view: {
            selectedMuti: false
        }
    };

    function category_sync_handler(data) {
        var result = {is_success: false};
        var _ztree = $.fn.zTree.getZTreeObj('category_tree');
        result.is_success = confirm("Press OK to confirm.");
        if (result.is_success) {
            $.ajax({
                async: false,
                contentType: _ztree.setting.async.contentType,
                type: _ztree.setting.async.type,
                url: _ztree.setting.async.url,
                data: data,
                dataType: _ztree.setting.async.dataType,
                success: function (msg) {
                    var ret = JSON.parse(msg)
                    if (ret['error']) {
                        alert(ret['error']);
                        result.is_success = false;
                    } else {
                        result.is_success = true;
                        result['data'] = ret['data']
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert('There are some errors, please try again');
                    result.is_success = false;
                }
            });
        }
        return result;
    }

    function async_error(event, treeId, node, XMLHttpRequest, textStatus, errorThrown) {
        alert("Failed to get information, Please try again");
    };

    function async_success(event, treeId, node, msg) {
//        console.log(event, treeId, node, msg);
    };

    function before_rename_category(treeId, node, newName, isCancel) {
        if (isCancel !== true) {
            var is_success = false;
            if (newName.length > 0) {
                if (newName == node.name) {
                    is_success = true;
                } else {
                    var data = {};
                    data['event'] = "#{Settings.RZTREE.EventType.TYPE_RENAME}";
                    data['id'] = node.id;
                    data['pId'] = node.pId;
                    data['name'] = node.name;
                    data['level'] = node.level;
                    data['tId'] = node.tId;
                    data['parentTId'] = node.parentTId;
                    data['newName'] = newName;
                    var ret = category_sync_handler(data);
                    is_success = ret.is_success;
                }

            } else {
                alert('Category name cant not be empty.');
                is_success = false;
            }
            if (is_success === false) {
                setTimeout($.fn.zTree.getZTreeObj('category_tree').cancelEditName(), 1000);
            }
            return is_success;
        }
    };

    function rename_category(event, treeId, treeNode, isCancel) {
    };

    function before_drag(treeId, nodes) {
    };

    function before_drop(treeId, nodes, dragTargetNode, moveType, isCopy) {
        var src_nodes = [];
        var data = {}, target_node = {};
        var is_success = false;
        if (moveType == "#{Settings.RZTREE.MoveType.TYPE_INNER}") {
            return false;
        }
        for (var i=0; i<nodes.length; i++) {
            var node = {};
            node['id'] = nodes[i].id;
            node['pId'] = nodes[i].pId;
            node['name'] = nodes[i].name;
            node['level'] = nodes[i].level;
            node['tId'] = nodes[i].tId;
            node['parentTId'] = nodes[i].parentTId;
            src_nodes[i] = node;
        }
        target_node['id'] = dragTargetNode.id;
        target_node['pId'] = dragTargetNode.pId;
        target_node['name'] = dragTargetNode.name;
        target_node['level'] = dragTargetNode.level;
        target_node['tId'] = dragTargetNode.tId;
        target_node['parentTId'] = dragTargetNode.parentTId;
        data['event'] = "#{Settings.RZTREE.EventType.TYPE_DROP}";
        data['src_nodes'] = src_nodes;
        data['target_node'] = target_node;
        data['move_type'] = moveType;
        data['is_copy'] = isCopy;
        var ret = category_sync_handler(data);
        is_success = ret.is_success;
        return is_success;
    };

//    function add_hover_dom(treeId, treeNode) {
//        var sObj = $("#" + treeNode.tId + "_span");
//        if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
//        var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
//                + "' title='add node' onfocus='this.blur();'></span>";
//        sObj.after(addStr);
//        var btn = $("#addBtn_"+treeNode.tId);
//        if (btn) btn.bind("click", function(){
//            var new_category_name = prompt("Please input new category name:");
//            console.log(new_category_name);
//            if (new_category_name) {
//                console.log('add node');
//                var data = {};
//                data['event'] = "#{Settings.RZTREE.EventType.TYPE_ADD}";
//                data['prevId'] = treeNode.id
//                data['pId'] = treeNode.pId;
//                data['name'] = new_category_name;
//                var ret = category_sync_handler(data);
//                if (ret.is_success) {
//                    var _ztree = $.fn.zTree.init($("#category_tree"), setting, ret.data);
//                    _ztree.setting.view.addHoverDom = add_hover_dom;
//                    _ztree.setting.view.removeHoverDom = remove_hover_dom;
//                    _ztree.setEditable(true);
//                }
//            }
//            return false;
//        });
//    };
//
//    function remove_hover_dom(treeId, treeNode) {
//        $("#addBtn_"+treeNode.tId).unbind().remove();
//    };

    var zNodes = #{ category_data_json };

    $(document).ready(function () {
        $.fn.zTree.init($("#category_tree"), setting, zNodes);
        $('#is_edit_category_tree').on('click', function() {
            var _ztree = $.fn.zTree.getZTreeObj('category_tree');
            if ($(this).attr('checked')) {
                $(this).removeAttr('checked');
//                _ztree.setting.view.addHoverDom = null;
//                _ztree.setting.view.removeHoverDom = null;
                _ztree.setEditable(false);
            } else {
                $(this).attr('checked', 'checked');
//                _ztree.setting.view.addHoverDom = add_hover_dom;
//                _ztree.setting.view.removeHoverDom = remove_hover_dom;
                _ztree.setEditable(true);
            }
        });
    });
</script>
<style type="text/css">
    .ztree li span.button.switch.level0 {
        visibility: hidden;
        width: 1px;
    }

    .ztree li ul.level0 {
        padding: 0;
        background: none;
    }

    /*.ztree li span.button.add {*/
        /*margin-left: 2px;*/
        /*margin-right: -1px;*/
        /*background-position: -144px 0;*/
        /*vertical-align: top;*/
        /**vertical-align: middle;*/
    /*}*/

    .ztree * {
        font-size: 14px;
    }
</style>
    EOF

    raw category_tree_js_tag
  end

  def image_tag_content_with_options(options={})
    image_content_tag = nil
    if options[:alt].blank? and options[:width].present? and options[:height].present?
      options[:alt] = "Image #{options[:width]}x#{options[:height]}"
    end

    options[:crop] = 'fill'
    if options[:fb_user_id].present?
      image_content_tag = facebook_profile_image_tag(options[:fb_user_id], options)
    elsif options[:image_url].present?
      if options[:image_url].match(Settings.IMAGE_URI_PREFIX)
        path_nodes = options[:image_url].split('/')
        image_content_tag = cl_image_tag("#{path_nodes[-2]}/#{path_nodes[-1]}", options)
      else
        image_content_tag = image_tag(options[:image_url], options)
      end
    end
    return image_content_tag
  end

end
