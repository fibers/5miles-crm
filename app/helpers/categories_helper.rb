module CategoriesHelper

  def category_attr_value(category, key, value)
    case key
      when 'parent_id'
        category.parent.blank? ? Settings.TOP_CATEGORY.invert[Settings.TOP_CATEGORY["Top Category"]] : self.parent.title
      when 'status'
        Settings.CATEGORY_STATUSES.invert[value]
      when 'gender'
        Settings.USER_GENDER.invert[value]
      else
        value
    end
  end

end
