module ProductsHelper
  def product_thumb_image_tag(product)
    if product.images.any?
      image_count_content = ''
      if product.images.size > 1
        image_count_content = content_tag(:span, product.images.size, style: 'width:20px;top: 0;right: 0;position: absolute;background-color: rgba(255, 255, 255, 0.8);')
      end
      content_tag :div, style: 'position: relative;' do
        image_path = product.images.order(is_first: :desc)[0].image_path
        product_title_tag = content_tag(:span, product.title, class: "help-block text-center")
        link_to(image_tag_content_100_100(image_url: image_path), images_product_path(product), method: :get, remote: true) + image_count_content + product_title_tag
      end
    end
  end

  def product_attr_value(product, attr)
    case attr
      when 'state'
        Settings.PRODUCT_STATUSES.invert[product.state]
      when 'internal_status'
        Settings.PRODUCT_INTERNAL_STATUSES.invert[product.internal_status]
      when 'shipping_method'
        Settings.SHIP_MODE.invert[product.shipping_method]
      when 'share'
        product.share == 0 ? 'No' : 'Yes'
      else
        product.send(attr)
    end
  end

  def product_wice_grid_risk_ids(grid)
    risk_product_ids = []
    grid.current_page_records.each do |p|
      risk_product_ids << p.id.to_s if p.risk.present?
    end
    return risk_product_ids
  end

  def product_wice_grid_ids_with_risk_user(grid)
    product_ids = []
    user_item_ids = {}
    grid.current_page_records.each do |p|
      if user_item_ids[p.user_id].blank?
        user_item_ids[p.user_id] = []
      end
      user_item_ids[p.user_id] << p.id.to_s
    end
    User.where(user_id:user_item_ids.keys).where(internal_status: Settings.USER_INTERNAL_STATUSES.Risk).select(:id, :user_id).each do |user|
      product_ids |= user_item_ids[user.user_id]
    end

    return product_ids
  end

  def product_wice_grid_get_offer_count_by_item_ids_as_json(item_ids)
    item_offer_counts = []
    if item_ids.present?
      ret = BackendApiREST.get_offer_count_by_item_ids(item_ids)
      if ret[0]
        ret = JSON.load(ret[1])
        item_offer_count = ret["objects"]
      end
    end
    return item_offer_count.to_json
  end

  def product_wice_grid_get_item_ids_edited_after_audit(item_ids)
    BkItem.where(item_id: item_ids).where(is_edited: true).pluck(:item_id)
  end

end
