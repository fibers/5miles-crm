module Templates::EmailsHelper
  def email_template_attr_value(email, attr)
    case attr
      when 'content'
        raw email.content
      when 'status'
        Settings.EMAIL_TEMP_STATUSES.invert[email.status]
      when 'notification_type'
        Settings.USER_SUBSCRIPTIONS.NOTIFICATION_TYPE.invert[email.notification_type]
      else
        email.send(attr)
    end
  end
end
