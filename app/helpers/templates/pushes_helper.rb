module Templates::PushesHelper
  def push_template_attr_value(push, attr)
    case attr
      when 'status'
        Settings.FMMC_STATUSES.invert[push.status]
      when 'flag'
        Settings.PUSH_REMIND_TYPES.invert[push.flag]
      when 'notification_type'
        Settings.USER_SUBSCRIPTIONS.NOTIFICATION_TYPE.invert[push.notification_type]
      else
        push.send(attr)
    end
  end
end
