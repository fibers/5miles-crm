module Templates::SystemMessagesHelper
  def system_message_template_attr_value(sm, attr)
    case attr
      when 'status'
        Settings.FMMC_STATUSES.invert[sm.status]
      else
        sm.send(attr)
    end
  end
end
