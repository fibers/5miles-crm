module Tools::HomeBannerHelper
  def home_banner_thumb_image(home_banner)
    if home_banner.present? and home_banner.image_url.present?
      cl_image_tag(home_banner.image_url.sub(Settings.IMAGE_URI_PREFIX, ''), alt: Settings.CAMPAIGN.LAUNCH_NAME.capitalize,
                   width: Settings.CAMPAIGN.THUMBNAIL_IMAGE.HomeBanner.With, height: Settings.CAMPAIGN.THUMBNAIL_IMAGE.HomeBanner.Height, crop: :fill)
    end
  end
end
