module Tools::ItemsHelper
  def product_thumb_image_tag_from_json(item)
    if item['images'].any?
      image_path = item['images'][0]['imageLink']
      image_tag_content_200_200(image_url: image_path, alt: "Image for #{item['id']}")
    end
  end
end
