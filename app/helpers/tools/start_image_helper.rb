module Tools::StartImageHelper
  def launch_thumb_image(launch)
    if launch.present? and launch.image_url.present?
      cl_image_tag(launch.image_url.sub(Settings.IMAGE_URI_PREFIX, ''), alt: Settings.CAMPAIGN.LAUNCH_NAME.capitalize,
                   width: Settings.CAMPAIGN.THUMBNAIL_IMAGE.Launch.With, height: Settings.CAMPAIGN.THUMBNAIL_IMAGE.Launch.Height, crop: :fill)
    end
  end
end
