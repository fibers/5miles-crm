module TraceLogsHelper
  def trace_log_display_content(log)
    content = JSON.load(log.op_content)
    case log.op_type
      when Settings.BACKEND_TRACE_LOG.Type.Modify
        content = JSON.load(log.op_content)
        if content.is_a?(Hash) and content['from'].present? and content['to'].present?
          content_tag(:table, class: 'table table-bordered table-striped .table-condensed') do
            content_tag(:tbody) do
              content_tag(:tr, content_tag(:td, 'From', class: 'col-md-2') + content_tag(:td, content['from'])) +
                  content_tag(:tr, content_tag(:td, 'To', class: 'col-md-2') + content_tag(:td, content['to']))
            end
          end
        else
          content
        end
      else
        content
    end
  end
end
