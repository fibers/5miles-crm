module UserFeedbackHelper
  def user_feedback_get_feedback_ticket_json(feedback_ids=[])
    feedback_tickets = {}
    BkUserFeedbackZendeskTicket.select(:feedback_id, :ticket_id).where(feedback_id: feedback_ids).each do |rt|
      feedback_tickets[rt.feedback_id.to_s] = {ticket_id: rt.ticket_id}
    end
    feedback_tickets.to_json
  end
end
