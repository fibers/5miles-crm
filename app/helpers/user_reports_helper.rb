module UserReportsHelper
  def user_report_get_report_ticket_json(report_ids=[])
    report_tickets = {}
    BkUserReportZendeskTicket.select(:report_id, :ticket_id).where(report_id: report_ids).each do |rt|
      report_tickets[rt.report_id.to_s] = {ticket_id: rt.ticket_id}
    end
    report_tickets.to_json
  end
end
