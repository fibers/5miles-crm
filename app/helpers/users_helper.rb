module UsersHelper

  def isTabActive(tab_name)
    @active_tab == tab_name ? 'active':''
  end

  def user_thumb_image_tag(user)
    image_content_tag = image_tag_content_100_100(image_url: user.portrait)
    link_to(image_content_tag, portrait_user_path(user), method: :get,
            remote: true) + content_tag(:span, user.nickname, class: "help-block text-center")
  end

  def user_attr_key(user, key)
    case key
      when 'os'
        'register_os'
      when 'os_version'
        'register_os_version'
      when 'app_version'
        'register_app_version'
      when 'device'
        'register_device'
      else
        key
    end.humanize
  end

  def user_attr_value(user, attr)
    case attr
      when 'gender'
        Settings.USER_GENDER.invert[user.gender]
      else
        user.send(attr)
    end
  end

  def user_wice_grid_risk_ids(grid)
    risk_user_ids = []
    grid.current_page_records.each do |u|
      risk_user_ids << u.id.to_s if u.user_info.present? and u.user_info.internal_status == Settings.USER_INTERNAL_STATUSES.Risk
    end
    return risk_user_ids
  end
end
