class EntityBrand < ApiBase
  has_many :items, class_name: 'EntityItem', foreign_key: 'brand_id'
end
