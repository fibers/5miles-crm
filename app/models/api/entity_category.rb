class EntityCategory < ApiBase

  has_many :related_categories, class_name: 'EntityItemcategory', foreign_key: 'cat_id'
  has_many :products, class_name: 'EntityItem', through: :related_categories

  has_many :children, class_name: 'EntityCategory', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'EntityCategory', foreign_key: 'parent_id'

  default_scope { order(:order_num) }

  scope :siblings, ->(parent_id) { where(parent_id: parent_id) }
  scope :valid, -> { where.not(status: Settings.CATEGORY_STATUSES.Invalid) }
  scope :published, -> { where(status: Settings.CATEGORY_STATUSES.Published) }

  before_create :set_default_value

  def publish?
    self.status == Settings.CATEGORY_STATUSES.Published
  end

  def draft?
    self.status == Settings.CATEGORY_STATUSES.Draft
  end

  def invalid?
    self.status == Settings.CATEGORY_STATUSES.Invalid
  end

  def self.to_dropdown
    EntityCategory.where.not(status: Settings.CATEGORY_STATUSES.Invalid).map { |e| [e.title, e.id] }
  end

  private

  def set_default_value
    self.id = EntityCategory.unscoped.last.id + 1
    self.status = Settings.CATEGORY_STATUSES.Draft

    if self.parent_id.nil?
      self.parent_id = Settings.TOP_CATEGORY["Top Category"]
    end
    if self.description.nil?
      self.description = ''
    end
    if self.slug.nil?
      self.slug = ''
    end
    if self.short_url.nil?
      self.short_url = ''
    end

    # set default order
    siblings = self.parent.blank? ? EntityCategory.siblings(Settings.TOP_CATEGORY["Top Category"]) : self.parent.children
    self.order_num = siblings.last.order_num + 1

    if self.weight.present?
      self.weight_updated_at = Time.current
    end
  end

  # def get_titles
  #   chain= []
  #   chain.push(self.title)
  #   n = self
  #   while p = n.parent do
  #     chain.push(p.title)
  #     n = p
  #   end
  #   chain.reverse.join(' / ')
  # end

end
