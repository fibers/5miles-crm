class EntityFacebookprofile < ApiBase
  has_one :user, class_name: 'EntityUser', foreign_key: 'fb_user_id'
end
