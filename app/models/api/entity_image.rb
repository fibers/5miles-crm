class EntityImage < ApiBase
  belongs_to :product, class_name: 'EntityItem', foreign_key: 'item_id'

  def self.get_item_first_image(item_id)
    where(item_id: item_id).order(is_first: :desc).limit(1)[0]
  end
end
