class EntityItem < ApiBase

  belongs_to :user, class_name: 'EntityUser', foreign_key: 'user_id'
  belongs_to :brand, class_name: 'EntityBrand', foreign_key: 'brand_id'

  has_many :images, class_name: 'EntityImage', foreign_key: 'item_id'

  # has_many :related_tags, class_name: 'CommodityCommodityproductTags', foreign_key: 'commodityproduct_id'
  # has_many :tags, class_name: 'CommodityTag', through: :related_tags
  #
  has_many :related_categories, class_name: 'EntityItemcategory', foreign_key: 'item_id'
  has_many :categories, class_name: 'EntityCategory', through:  :related_categories

  has_many :pvs, class_name: 'OdsPageView', foreign_key: 'item_id'
  has_many :offers, class_name: 'OdsOffer', foreign_key: 'item_id'
  has_many :audits, class_name: 'BkItemAuditHistory', foreign_key: 'item_id'
  has_one  :bk_item, class_name: 'BkItem', foreign_key: 'item_id'

  def generate_copy
    item_copy = {}
    item_copy['desc'] = self.desc
    item_copy['media'] = self.mediaLink
    item_copy['original_price'] = self.original_price
    item_copy['price'] = self.price
    item_copy['shipping_method'] = self.shipping_method
    item_copy['category'] = self.categories[0].title
    item_copy['brand'] = self.brand_name
    item_copy['title'] = self.title
    item_copy['images'] = self.images.order(is_first: :desc).pluck(:image_path)
    item_copy['local_price'] = self.local_price
    item_copy['country'] = self.country
    item_copy['region'] = self.region
    item_copy['city'] = self.city

    return item_copy
  end
end
