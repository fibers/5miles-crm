class EntityItemcategory < ApiBase
  belongs_to :product, class_name: 'EntityItem', foreign_key: 'item_id'
  belongs_to :category, class_name: 'EntityCategory', foreign_key: 'cat_id'
end
