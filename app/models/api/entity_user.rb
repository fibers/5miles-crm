class EntityUser < ApiBase

  has_many :products, class_name: 'EntityItem', foreign_key: 'user_id'
  has_many :feedbacks, class_name: 'FeedbackUserfeedback', foreign_key: 'user_id'
  has_many :subsciptions, class_name: 'FmmcUserSubscription', foreign_key: 'user_id'
  belongs_to  :facebook_profile, class_name: 'EntityFacebookprofile', foreign_key: 'fb_user_id'
  has_one :user_info, class_name: 'User', foreign_key: 'user_id'

  def is_disabled?
    self.state == Settings.USER_STATE.Forbidden
  end

  def disable_user
    BackendApiREST.disable_user(self.id)
  end

  def self.valid_scope
    where(state: Settings.USER_STATE.Valid)
  end

  def self.disabled_scope
    where(state: Settings.USER_STATE.Forbidden)
  end

  def is_robot?
    self.is_robot == 1
  end

end
