class FmmcCron < ApiBase

  belongs_to :frequency, class_name: 'FmmcFrequencyPeriod', foreign_key: 'frequency_period_id'
  belongs_to :task, class_name: 'FmmcTask', foreign_key: 'task_id'

  validates :name, uniqueness: true


  def html_description
    created_at = "<b>Created</b>: #{self.created_at}"
    updated_at = "<b>Updated</b>: #{self.updated_at}"
    schedule = ""
    case self.recurring
      when 0 then
        if self.at == '00:00'
          schedule = self.frequency_quantity.send(self.frequency.name).from_now()
        elsif
          # TODO: Doesn't consider time zone as the factor.
          schedule = Time.now.tomorrow.to_date +  self.frequency_quantity.send(self.frequency.name)
        end
      when 1 then
        schedule= "#{self.at} every #{self.frequency_quantity} #{self.frequency.name} in #{self.tz} time"
      when 2 then
        schedule ="Run immediately by the api calling."
    end

    execute_at = "<b>Execute</b>: #{schedule}"
    [created_at, updated_at, execute_at].join('<br/>')
  end

end
