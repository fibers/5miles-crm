class FmmcFrequencyPeriod < ApiBase

  # has_one :cron , class_name: 'FmmcCron', foreign_key: 'frequency_period_id'

  def self.get_id_by_frequency(frequency)
    send('find_by_name', frequency).id
  end

  def self.get_frequency_by_id(id)
    send('find_by_id', id).name
  end
end
