class FmmcMailTemplate < ApiBase
  has_many :email_recommendations, class_name: 'EmailRecommendation', foreign_key: 'email_template_id'

  validates :name, uniqueness: true

  def self.normal_scope
    where.not("fmmc_mail_template.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end

  def self.manual_scope
    where("fmmc_mail_template.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end
end
