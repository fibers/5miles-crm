class FmmcPushTemplate < ApiBase

  validates :name, uniqueness: true
  validate :validate_payload

  before_create :set_default_value
  before_update :set_default_value

  def set_default_value
    if self.daily_quota.blank?
      self.daily_quota = -1
    end
  end

  def validate_payload
    pl= JSON.load(self.payload)
    if pl.size < 2
      errors.add(:payload, 'format is error.')
    else
      pl.each_pair do |key, value|
        if key.blank? or value.blank?
          errors.add(:payload, 'key or value is empty.')
          break
        end
      end
    end
  end

  class << self
    def normal_scope
      where.not("fmmc_push_template.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
    end

    def manual_scope
      where("fmmc_push_template.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
    end
  end
end
