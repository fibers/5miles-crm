class FmmcSmsTemplate < ApiBase
  validates :name, uniqueness: true

  def self.normal_scope
    where.not("fmmc_sms_template.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end

  def self.manual_scope
    where("fmmc_sms_template.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end
end
