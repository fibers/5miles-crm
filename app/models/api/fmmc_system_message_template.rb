class FmmcSystemMessageTemplate < ApiBase

  validates :name, uniqueness: true

  before_create :set_default_value

  def set_default_value
    if self.sm_type.nil?
      self.sm_type = Settings.TEMP_SYSTEM_MESSAGE_TYPES.Normal
    end
  end

  def self.normal_scope
    where.not("fmmc_system_message_template.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end

  def self.manual_scope
    where("fmmc_system_message_template.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end
end
