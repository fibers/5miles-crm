class FmmcTask < ApiBase

  has_one :cron, class_name: 'FmmcCron', foreign_key: 'task_id'
  has_one :trigger, class_name: 'FmmcTrigger', foreign_key: 'task_id'

  belongs_to :mail_template, class_name: 'FmmcMailTemplate', foreign_key: 'mail_template_id'
  belongs_to :push_template, class_name: 'FmmcPushTemplate', foreign_key: 'push_template_id'
  belongs_to :system_message_template, class_name: 'FmmcSystemMessageTemplate', foreign_key: 'system_message_template_id'
  belongs_to :frequency, class_name: 'FmmcFrequencyPeriod', foreign_key: 'target_register_interval_period_id'

  validates :name, uniqueness: true

  before_create :set_default_value

  def set_default_value
    if self.target_gender.nil?
      self.target_gender = ''
    end
    if self.target_country.nil?
      self.target_country = ''
    end

    if self.target_region.nil?
      self.target_region = ''
    end

    if self.target_city.nil?
      self.target_city = ''
    end

    if self.target_uid_tail.nil?
      self.target_uid_tail = ''
    end
  end

  def html_description
    created_at = "<b>Created</b>: #{self.created_at}"
    updated_at = "<b>Updated</b>: #{self.updated_at}"
    clazz = "<b>Clazz</b>: #{self.clazz}"
    target_country = "<b>Country</b>: #{self.target_country}"
    [created_at, updated_at, clazz, target_country].join('<br/>')
  end

  def country
    self.target_country.present? ? self.target_country : Settings.ALL_USERS_STRING
  end

  def send_target
    target = Settings.ALL_USERS_STRING
    if self.target_city.present?
      target = "Country(#{self.target_country}), Region(#{self.target_region}), City(#{self.target_city})"
    elsif self.target_region.present?
      target = "Country(#{self.target_country}), Region(#{self.target_region})"
    elsif self.target_country.present?
      target = "Country(#{self.target_country})"
    end
  end

  def gender
    gender_list = []

    self.target_gender.split(',').each do |value|
      gender_list << Settings.USER_GENDER.invert[value.to_i]
    end

    return gender_list.sort.join(',')
  end

  def sm_content
    self.system_message_template.content if self.system_message_template.present?
  end

  def sm_action
    self.system_message_template.action if self.system_message_template.present?
  end

  def push_title
    self.push_template.title if self.push_template.present?
  end

  def push_content
    self.push_template.content if self.push_template.present?
  end

  def push_payload
    self.push_template.payload if self.push_template.present?
  end

  def mail_subject
    self.mail_template.subject if self.mail_template.present?
  end

  def registered_time
    if self.target_register_interval_type.present? and self.target_register_interval_period_id.present? and self.target_register_interval_quantity.present?
      case self.target_register_interval_type
        when Settings.REGISTERED_TIME_TYPE.Ago
          "#{self.target_register_interval_quantity} #{self.frequency.name.pluralize(self.target_register_interval_quantity)} Ago"
        when Settings.REGISTERED_TIME_TYPE.Within
          "Within #{self.target_register_interval_quantity} #{self.frequency.name.pluralize(self.target_register_interval_quantity)}"
      end
    end
  end

  def self.normal_scope
    where.not("fmmc_task.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end

  def self.manual_scope
    where("fmmc_task.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end

end
