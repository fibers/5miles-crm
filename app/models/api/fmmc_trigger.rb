class FmmcTrigger <ApiBase
  belongs_to :task, class_name: 'FmmcTask', foreign_key: 'task_id'
  belongs_to :period, class_name: 'FmmcFrequencyPeriod', foreign_key: 'interval_period_id'

  validates :name, uniqueness: true

  def self.normal_scope
    where.not("fmmc_trigger.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end

  def self.manual_scope
    where("fmmc_trigger.name like ?", "#{Settings.MANUAL_MESSAGES_PREFIX.MANUAL_PREFIX_PATTERN}%")
  end

  def normal_trigger_time
    if self.trigger_type == Settings.TRIGGER_TYPES.Delayed
      if self.at.present?
        self.at
      else
        "#{self.interval_quantity} #{self.period.name.pluralize(self.interval_quantity)}"
      end
    end
  end

  def manual_trigger_time
    if self.trigger_type == Settings.TRIGGER_TYPES.Delayed
      self.created_at + self.interval_quantity.send(self.period.name)
    else
      self.created_at
    end
  end
end
