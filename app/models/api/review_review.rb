class ReviewReview < ApiBase
  belongs_to :reviewer, class_name: 'EntityUser', foreign_key: 'reviewer_id'
  belongs_to :reviewee, class_name: 'EntityUser', foreign_key: 'target_user'
end
