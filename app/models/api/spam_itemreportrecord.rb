class SpamItemreportrecord < ApiBase
  belongs_to :product, class_name: 'EntityItem', foreign_key: 'item_id'
  belongs_to :reporter, class_name: 'EntityUser', foreign_key: 'reporter_id'
end
