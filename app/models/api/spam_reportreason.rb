class SpamReportreason < ApiBase
  scope :item_type, -> { where(report_type: Settings.Report.ReasonType.Item).pluck(:reason_content, :id).to_h }
  scope :user_type, -> { where(report_type: Settings.Report.ReasonType.User).pluck(:reason_content, :id).to_h }
  scope :review_type, -> { where(report_type: Settings.Report.ReasonType.Review).pluck(:reason_content, :id).to_h }

  class << self
    def item_report_reasons
      Settings.PRODUCT_REPORT_REASONS.merge(self.item_type)
    end

    def user_report_reasons
      Settings.USER_REPORT_REASONS.merge(self.user_type)
    end

    def review_report_reasons
      self.review_type
    end
  end
end
