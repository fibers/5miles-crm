class SpamReviewreportrecord < ApiBase
  belongs_to :review, class_name: 'ReviewReview', foreign_key: 'review_id'
  belongs_to :reporter, class_name: 'EntityUser', foreign_key: 'reporter_id'
end
