class SpamUserreportrecord < ApiBase
  belongs_to :user, class_name: 'EntityUser', foreign_key: 'user_id'
  belongs_to :reporter, class_name: 'EntityUser', foreign_key: 'reporter_id'

end
