class ApiBase < ActiveRecord::Base
  self.abstract_class = true
  self.pluralize_table_names = false
  self.establish_connection :"#{Rails.env}_api"

  include Utils::BackendTraceLogOps::ModelTraceLogOps
  after_create :trace_log_after_create
  before_update :trace_log_before_update
  after_update :trace_log_after_update
  before_destroy :trace_log_before_destroy
end
