class Admin < BackendBase

  has_secure_password
  validates :username, length: {minimum: 1, maximum: 20}, uniqueness: {case_sensitive: false}

  has_many :schedules, class_name: 'BkOperatorSchedule', foreign_key: 'operator_id'

  scope :valid_scope, -> { where(state: Settings.BACKEND_USER.States.Enabled) }

  before_save :process_admin_before_save_callback
  before_update :process_admin_before_update_callback

  def super?
    self.is_super_admin
  end

  def promote
    self.is_super_admin = true
    self.save
  end

  def demote
    self.is_super_admin = false
    self.save
  end

  def is_disabled?
    self.state == Settings.BACKEND_USER.States.Disabled
  end

  def is_enabled?
    self.state == Settings.BACKEND_USER.States.Enabled
  end

  def process_username
    self.username.downcase!
    self.username.strip!
  end

  def process_admin_before_save_callback
    process_username
  end

  def process_admin_before_update_callback
    process_username
  end

  class << self
    def review_operators_for_select
      where(state: Settings.BACKEND_USER.States.Enabled)
          .where("admins.review_roles like '%#{Settings.BACKEND_USER.Roles.Agent}%'")
          .pluck(:username, :id)
    end
  end

end
