class BkItem < BackendBase
  belongs_to :operator, class_name: 'Admin', foreign_key: 'operator_id'
  belongs_to :img_assignment, class_name: 'Admin', foreign_key: 'img_assigned_to'
  belongs_to :assignment, class_name: 'Admin', foreign_key: 'assigned_to'
  belongs_to :item, class_name: 'EntityItem', foreign_key: 'item_id'
end
