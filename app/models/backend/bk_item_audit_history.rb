class BkItemAuditHistory < BackendBase

  def self.last_review(item_id)
    BkItemAuditHistory.where(item_id: item_id).order(id: :desc).limit(1)[0]
  end
end
