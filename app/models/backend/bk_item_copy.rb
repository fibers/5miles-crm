require 'audit/auto_audit_service'

class BkItemCopy < BackendBase

  def self.get_latest(item_id)
    BkItemCopy.where(item_id: item_id).order(id: :desc).first
  end

  def self.get_limit2(item_id)
    ret = BkItemCopy.where(item_id: item_id).order(id: :desc).limit(2)
    if ret.size < 2
      raise AutoAuditService::WorkFlowContinueException.new("BkItemCopy only one(item_id: #{item_id})")
    end
    return ret
  end
end
