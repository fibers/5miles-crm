class BkOperatorSchedule < BackendBase
  belongs_to :operator, class_name: 'Admin', foreign_key: 'operator_id'

  scope :valid_scope, -> { where(state: Settings.BACKEND_USER.ScheduleStates.Valid) }

  before_create :set_default_value

  private
  def set_default_value
    if self.state.nil?
      self.state = Settings.BACKEND_USER.ScheduleStates.Invalid
    end
  end
end
