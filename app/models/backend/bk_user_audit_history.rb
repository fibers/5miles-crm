class BkUserAuditHistory < BackendBase
  belongs_to :user, class_name: 'EntityUser', foreign_key: 'user_id'
  belongs_to :admin, class_name: 'Admin', foreign_key: 'admin_id'
end
