class EmailRecommendation < BackendBase
  belongs_to :email_template, class_name: 'FmmcMailTemplate', foreign_key: 'email_template_id'

  before_create :set_default_value

  private
  def set_default_value
    self.status = Settings.EMAIL_STATUSES.Send_Request
    self.error_user_total = 0
    self.error_user_ids = ''
  end
end
