class ItemOperationRule < BackendBase
  belongs_to :target_rule, class_name: 'ItemOperationRule', foreign_key: 'target_rule_id'
  has_many   :source_rules, class_name: 'ItemOperationRule', foreign_key: 'target_rule_id'

  def is_enabled?
    return self.status
  end

  def is_keep_audit_state?
    return self.is_keep_audit_state
  end

  def self.operation_types
    operation_types = self.all.map { |rule| rule.name }
    operation_types << Settings.ITEM_TASK_TYPE.NotDefined
    return operation_types
  end

end
