class ItemTask < BackendBase
  has_many :comments, class_name: 'ItemTaskComment', foreign_key: 'task_id'
  belongs_to :item, class_name: 'EntityItem', foreign_key: 'item_id'
  belongs_to :user, class_name: 'EntityUser', foreign_key: 'user_id'
  belongs_to :rule, class_name: 'ItemOperationRule', foreign_key: 'rule_id'
  belongs_to :bk_item_copy, class_name: 'BkItemCopy', foreign_key: 'bk_item_copy_id'

  def comments_text
    if self.comments
      return self.comments.map {|c| c.comment}.join("\n")
    else
      return ''
    end
  end

  def create_comment(comment_text, commenter)
    comment = self.comments.create
    comment.commenter = commenter.present? ? commenter : self.processor
    comment.comment = comment_text
    comment.save()
  end

  def complete(processor)
    self.state = Settings.ITEM_TASK_STATE.Completed
    self.completed_at = Time.now
    self.save()

    text = '%s completed this task' % processor
    self.create_comment(text, processor)
  end

  def visible_attributes
    attrs = self.attributes.clone
    ['comments'].each { |attr_name| attrs.delete(attr_name) }
    return attrs
  end

  class << self
    def is_has_opened_task(item_id)
      where(item_id: item_id).where.not(state: Settings.ITEM_TASK_STATE.Completed).size > 0
    end

    def get_opened_task_by_item_id(item_id)
      where(item_id: item_id).where.not(state: Settings.ITEM_TASK_STATE.Completed).last
    end
  end

end
