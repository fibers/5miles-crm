class ItemTaskComment < BackendBase
  belongs_to :task, class_name: 'ItemTask', foreign_key: 'task_id'
end
