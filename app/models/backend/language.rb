class Language < BackendBase

  validates_presence_of :iso_code, :title, :status
  validates :iso_code, presence: true, length: {minimum: 2, maximum: 2}

end
