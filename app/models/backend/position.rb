class Position < BackendBase
  before_create :set_default_value

  def set_default_value
    if self.display_order.nil?
      self.display_order = 0
    end
  end
end
