class User < BackendBase
  belongs_to :user, class_name: 'EntityUser', foreign_key: 'user_id'

  def self.risk_scope
    where(internal_status: Settings.USER_INTERNAL_STATUSES.Risk)
  end
end
