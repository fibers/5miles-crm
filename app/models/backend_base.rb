class BackendBase < ActiveRecord::Base
  self.abstract_class = true

  include Utils::BackendTraceLogOps::ModelTraceLogOps
  after_create :trace_log_after_create
  before_update :trace_log_before_update
  after_update :trace_log_after_update
  before_destroy :trace_log_before_destroy
end