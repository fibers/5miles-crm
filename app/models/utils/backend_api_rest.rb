class BackendApiREST

  extend Utils::BackendTraceLogOps::ApiTraceLogOps

  class << self

    @@URI_BASE = Settings.BackendApiRest.HOST
    @@API_KEY = Settings.BackendApiRest.KEY
    @@HTTP_PATH_PREFIX = "/be_api/v2"
    @@HEADERS = {'Authorization' => Settings.BackendApiRest.KEY, 'Content-Type' => 'application/json'}

    def disable_user(user_id)
      trace_log_content_from = EntityUser.find(user_id)
      trace_log_target = "disable_user"
      trace_log_business_id = user_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/disable_user/"
      payload = {}
      payload['user_id'] = user_id.to_i
      payload['action'] = "disable"

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityUser.find(user_id)
          trace_log_content = get_model_diff_content(trace_log_content_from, trace_log_content_to)
          if trace_log_content.present?
            trace_log_after_modify(trace_log_target, trace_log_business_id, trace_log_content)
          end
          return true
        else
          report_error(response, request, result)
          return false
        end
      end
    end

    def review_item(item_id, action_id, prev_state)
      trace_log_content_from = EntityItem.find(item_id)
      trace_log_target = "review_item"
      trace_log_business_id = item_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/review_item/"
      payload = {}
      payload['item_id'] = item_id.to_i
      case action_id
        when Settings.PRODUCT_INTERNAL_STATUSES.Approve
          payload['action'] = "approve"
          payload["state"] = prev_state
        when Settings.PRODUCT_INTERNAL_STATUSES.ApproveRisk
          payload['action'] = "approve_risk"
          payload["state"] = prev_state
        when Settings.PRODUCT_INTERNAL_STATUSES.Disapprove
          payload['action'] = "disapprove"
        else
          raise("Dose not support #{Settings.PRODUCT_INTERNAL_STATUSES.invert[action_id]} action")
      end

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityItem.find(item_id)
          trace_log_content = get_model_diff_content(trace_log_content_from, trace_log_content_to)
          if trace_log_content.present?
            trace_log_after_modify(trace_log_target, trace_log_business_id, trace_log_content)
          end
          return true
        else
          report_error(response, request, result)
          return false
        end
      end
    end

    def user_devices_info(user_id)
      http_path = "#{@@HTTP_PATH_PREFIX}/user_devices/?user_id=#{user_id}"
      RestClient.get("http://#{@@URI_BASE}#{http_path}", @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def set_item_first_image(item_id, image_id)
      trace_log_content = {}
      trace_log_content['from'] = EntityImage.get_item_first_image(item_id)
      trace_log_content['to'] = {}
      trace_log_target = "set_item_first_image"
      trace_log_business_id = item_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/set_first_image/"
      payload = {}
      payload['item_id'] = item_id.to_i
      payload['image_id'] = image_id.to_i


      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content['to'] = EntityImage.get_item_first_image(item_id)
          trace_log_after_modify(trace_log_target, trace_log_business_id, trace_log_content)
          #latest_bk_item_copy = BkItemCopy.get_latest(item_id)
          item = EntityItem.find(item_id)
          item_copy = item.generate_copy.to_json
          # if latest_bk_item_copy.blank?
          #   BkItemCopy.create(item_id: item.id, user_id: item.user_id, content: item_copy, is_latest: true)
          # else
          #   latest_bk_item_copy.save_item_copy(item_copy)
          # end
          BkItemCopy.create(item_id: item.id, user_id: item.user_id, content: item_copy)
          return true
        else
          report_error(response, request, result)
          return false
        end
      end
    end

    def lbs_item_update(item_id, lat, lon)
      trace_log_content_from = EntityItem.find(item_id)
      trace_log_user_content_from = EntityUser.find(trace_log_content_from.user_id)
      trace_log_target = "lbs_item_update"

      http_path = "#{@@HTTP_PATH_PREFIX}/lbs_item_update/"
      payload = {}
      payload['item_id'] = item_id.to_i
      payload['lat'] = lat
      payload['lon'] = lon
      # api 接口中有is_update_owner参数，但是这里只是更新item的位置，如果更新用户的请用lbs_user_update(user_id, lat, lon)接口
      payload['is_update_owner'] = false


      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityItem.find(item_id)
          trace_log_content = get_model_diff_content(trace_log_content_from, trace_log_content_to)
          if trace_log_content.present?
            trace_log_after_modify("#{trace_log_target}:item", trace_log_content_from.id, trace_log_content)
          end
          trace_log_user_content_to = EntityUser.find(trace_log_user_content_from.id)
          trace_log_content = get_model_diff_content(trace_log_user_content_from, trace_log_user_content_to)
          if trace_log_content.present?
            trace_log_after_modify("#{trace_log_target}:user", trace_log_user_content_from.id, trace_log_content)
          end
          return true
        else
          report_error(response, request, result)
          return false
        end
      end
    end

    def lbs_user_update(user_id, lat, lon)
      trace_log_content_from = EntityUser.find(user_id)
      trace_log_target = "lbs_user_update"

      http_path = "#{@@HTTP_PATH_PREFIX}/lbs_user_update/"
      payload = {}
      payload['user_id'] = user_id.to_i
      payload['lat'] = lat
      payload['lon'] = lon

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityUser.find(user_id)
          trace_log_content = get_model_diff_content(trace_log_content_from, trace_log_content_to)
          if trace_log_content.present?
            trace_log_after_modify(trace_log_target, trace_log_content_from.id, trace_log_content)
          end
          return true
        else
          report_error(response, request, result)
          return false
        end
      end
    end

    def unlist_item(item_id)
      trace_log_content_from = EntityItem.find(item_id)
      trace_log_target = "unlist_item"

      http_path = "#{@@HTTP_PATH_PREFIX}/unlist_item/"
      payload = {}
      payload['item_id'] = item_id.to_i

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityItem.find(item_id)
          trace_log_content = get_model_diff_content(trace_log_content_from, trace_log_content_to)
          if trace_log_content.present?
            trace_log_after_modify("#{trace_log_target}", trace_log_content_from.id, trace_log_content)
          end
          return true
        else
          report_error(response, request, result)
          return false
        end
      end
    end

    def clear_portrait(user_id)
      trace_log_content_from = EntityUser.find(user_id)
      trace_log_target = "clear_portrait"

      http_path = "#{@@HTTP_PATH_PREFIX}/clear_portrait/"
      payload = {}
      payload['user_id'] = user_id.to_i

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityUser.find(user_id)
          trace_log_content = get_model_diff_content(trace_log_content_from, trace_log_content_to)
          if trace_log_content.present?
            trace_log_after_modify("#{trace_log_target}", trace_log_content_from.id, trace_log_content)
          end
          return true
        else
          report_error(response, request, result)
          return false
        end
      end
    end

    def update_product_category(product_id, category_id)
      product_id = product_id.to_i
      category_id = category_id.to_i
      trace_log_content_from = EntityItem.find(product_id).categories.first
      trace_log_content = {}
      trace_log_content['from'] = trace_log_content_from
      trace_log_target = "update_product_category"
      trace_log_business_id = product_id

      http_path = "#{@@HTTP_PATH_PREFIX}/update_product_category/"
      payload = {}
      payload['product_id'] = product_id
      payload['category_id'] = category_id

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityItem.find(product_id).categories.first
          trace_log_content['to'] = trace_log_content_to
          trace_log_after_modify(trace_log_target, trace_log_business_id, trace_log_content)
          return true
        else
          report_error(response, request, result)
          return false
        end
      end
    end

    def get_offers_by_item_id(item_id)
      item_id = item_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/get_offers_by_item_id/"
      payload = {}
      payload['item_id'] = item_id

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def get_offer_count_by_item_ids(item_ids)

      http_path = "#{@@HTTP_PATH_PREFIX}/get_offer_count_by_item_ids/"
      payload = {}
      payload['item_ids'] = item_ids.to_json

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def batch_update_item_weight(item_ids, weight)
      trace_log_content_from = EntityItem.where(id: item_ids).select(:id, :weight, :weight_updated_at).to_a
      trace_log_content = {}
      trace_log_content['from'] = trace_log_content_from
      trace_log_target = "batch_update_item_weight"
      trace_log_business_id = nil

      http_path = "#{@@HTTP_PATH_PREFIX}/batch_update_item_weight/"
      payload = {}
      payload['item_ids'] = item_ids.to_json
      payload['weight'] = weight

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityItem.where(id: item_ids).select(:id, :weight, :weight_updated_at).to_a
          trace_log_content['to'] = trace_log_content_to
          trace_log_after_modify(trace_log_target, trace_log_business_id, trace_log_content)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def get_user_statistic_data(user_id)
      user_id = user_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/get_user_statistic_data/"
      payload = {}
      payload['user_id'] = user_id

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def batch_update_user_weight(user_ids, weight)
      trace_log_content_from = EntityUser.where(id: user_ids).select(:id, :weight, :weight_updated_at).to_a
      trace_log_content = {}
      trace_log_content['from'] = trace_log_content_from
      trace_log_target = "batch_update_user_weight"
      trace_log_business_id = nil

      http_path = "#{@@HTTP_PATH_PREFIX}/batch_update_user_weight/"
      payload = {}
      payload['user_ids'] = user_ids.to_json
      payload['weight'] = weight

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityUser.where(id: user_ids).select(:id, :weight, :weight_updated_at).to_a
          trace_log_content['to'] = trace_log_content_to
          trace_log_after_modify(trace_log_target, trace_log_business_id, trace_log_content)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def batch_update_category_weight(cat_id, weight)
      cat_id = cat_id.to_i
      trace_log_content_from = EntityCategory.find(cat_id)
      trace_log_target = "batch_update_category_weight"
      trace_log_business_id = cat_id

      http_path = "#{@@HTTP_PATH_PREFIX}/batch_update_category_weight/"
      payload = {}
      payload['cat_id'] = cat_id
      payload['weight'] = weight

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          trace_log_content_to = EntityCategory.find(cat_id)
          trace_log_content = get_model_diff_content(trace_log_content_from, trace_log_content_to)
          trace_log_after_modify(trace_log_target, trace_log_business_id, trace_log_content)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def get_offered_item_count_by_user_id(user_id)
      user_id = user_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/get_offered_item_count_by_user_id/"
      payload = {}
      payload['user_id'] = user_id

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def get_offered_items_by_user_id(user_id)
      user_id = user_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/get_offered_items_by_user_id/"
      payload = {}
      payload['user_id'] = user_id

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def get_offer_count_by_user_id(user_id)
      user_id = user_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/get_offer_count_by_user_id/"
      payload = {}
      payload['user_id'] = user_id

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    def get_offers_by_user_id(user_id)
      user_id = user_id.to_i

      http_path = "#{@@HTTP_PATH_PREFIX}/get_offers_by_user_id/"
      payload = {}
      payload['user_id'] = user_id

      RestClient.post("http://#{@@URI_BASE}#{http_path}", payload, @@HEADERS) do |response, request, result|
        if result.is_a?(Net::HTTPOK)
          return [true, response.body]
        else
          report_error(response, request, result)
          return [false, nil]
        end
      end
    end

    private
    def report_error(response, request, result)
      Rails.logger.error result
      Rails.logger.error response.body
      Rails.logger.error request.inspect
      Raven.capture_message("result: #{result}, response: #{response.body}, request: #{request}")
    end
  end
end
