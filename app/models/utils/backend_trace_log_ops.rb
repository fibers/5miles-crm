module Utils
  module BackendTraceLogOps
    class << self
      def get_current_operator_id
        EmployeeManager.instance.current_employee_id
      end

      def get_model_diff_content(from, to)
        is_changed_flag = false
        op_content = {}
        op_content['from'] = {}
        op_content['to'] = {}
        from.class.column_names.each do |column_name|
          if to.__send__(column_name) != from.__send__(column_name)
            is_changed_flag = true
            op_content['from'][column_name] = from.__send__(column_name)
            op_content['to'][column_name] = to.__send__(column_name)
          end
        end
        is_changed_flag ? op_content : {}
      end

      # operator_id: integer, op_type: string, op_target: string, op_business_id: integer, op_content: text
      def create_trace_log(op_type, op_target, op_business_id=nil, op_content="")
        if TraceLog.table_name != op_target
          operator_id = get_current_operator_id()
          trace_log_params = {}
          trace_log_params['operator_id'] = operator_id
          trace_log_params['op_type'] = op_type
          trace_log_params['op_target'] = op_target
          trace_log_params['op_business_id'] = op_business_id
          trace_log_params['op_content'] = op_content.to_json
          TraceLogWorker.perform_async(trace_log_params.to_json)
        end
      end
    end
  end
end
