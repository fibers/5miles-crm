module Utils
  module BackendTraceLogOps
    module ApiTraceLogOps
      # target: string, business_id: integer, content: hash
      def trace_log_after_add(target, business_id, content)
        add_trace_log_module_prefix(target, business_id, content) do |target, business_id, content|
          Utils::BackendTraceLogOps.create_trace_log(Settings.BACKEND_TRACE_LOG.Type.Add,
                                                     target,
                                                     business_id,
                                                     content)
        end
      end

      # target: string, business_id: integer, content: hash(from, to)
      def trace_log_after_modify(target, business_id, content)
        add_trace_log_module_prefix(target, business_id, content) do |target, business_id, content|
          Utils::BackendTraceLogOps.create_trace_log(Settings.BACKEND_TRACE_LOG.Type.Modify,
                                                     target,
                                                     business_id,
                                                     content)
        end
      end

      # target: string, business_id: integer, content: hash
      def trace_log_before_delete(target, business_id, content)
        add_trace_log_module_prefix(target, business_id, content) do |target, business_id, content|
          Utils::BackendTraceLogOps.create_trace_log(Settings.BACKEND_TRACE_LOG.Type.Delete,
                                                     target,
                                                     business_id,
                                                     content)
        end
      end

      def get_model_diff_content(from, to)
        Utils::BackendTraceLogOps.get_model_diff_content(from, to)
      end

      private
      def add_trace_log_module_prefix(target, business_id, content, &block)
        target = "#{self.name}.#{target}"
        block.call(target, business_id, content)
      end
    end
  end
end
