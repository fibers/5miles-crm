module Utils
  module BackendTraceLogOps
    module ModelTraceLogOps
      def trace_log_after_create
        Utils::BackendTraceLogOps.create_trace_log(Settings.BACKEND_TRACE_LOG.Type.Add,
                                                   self.class.table_name,
                                                   self.id,
                                                   self)
      end

      def trace_log_before_update
        @before_update_record = self.class.find(self.id)
      end

      def trace_log_after_update
        op_content = Utils::BackendTraceLogOps.get_model_diff_content(@before_update_record, self)
        if op_content.present?
          Utils::BackendTraceLogOps.create_trace_log(Settings.BACKEND_TRACE_LOG.Type.Modify,
                                                     self.class.table_name,
                                                     self.id,
                                                     op_content)
        end
      end

      def trace_log_before_destroy
        Utils::BackendTraceLogOps.create_trace_log(Settings.BACKEND_TRACE_LOG.Type.Delete,
                                                   self.class.table_name,
                                                   self.id,
                                                   self)
      end
    end
  end
end
