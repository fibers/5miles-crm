module Utils
  module BkBadUserOps
    class << self
      def update_count(options={})
        options.symbolize_keys!
        is_success_flag = false
        message = ''
        bk_bad_user = options[:bk_bad_user]
        begin
          current_time = Time.current
          if bk_bad_user.blank?
            if options[:user_id].blank?
              is_success_flag = false
              message = "bk_bad_user and user_id are all empty."
              break
            end
            bk_bad_user = BkBadUser.find_by(user_id: options[:user_id])
            if bk_bad_user.blank?
              user_status = Settings.USER_STATE.Valid
              unless DisabledUser.select(:id).find_by(user_id: options[:user_id]).blank?
                user_status = Settings.USER_STATE.Forbidden
              end
              bk_bad_user = BkBadUser.new
              bk_bad_user.user_id = options[:user_id]
              bk_bad_user.total_count = 0
              bk_bad_user.day_count = 0
              bk_bad_user.status = user_status
              # bk_bad_user.total_count_updated_at = nil
              # bk_bad_user.day_count_updated_at = nil
            end
          end

          bk_bad_user.total_count += 1
          bk_bad_user.total_count_updated_at = current_time
          if (current_time.to_i - bk_bad_user.day_count_updated_at.to_i) > 1.day
            bk_bad_user.day_count += 1
            bk_bad_user.day_count_updated_at = current_time
          end

          unless bk_bad_user.save
            is_success_flag = false
            message = "Failed to update count."
            break
          end
          is_success_flag = true
          message = "Update count successfully."
        end while false

        return [is_success_flag, message, bk_bad_user]
      end

      def update_status(user_id, status)
        bk_bad_user = BkBadUser.find_by(user_id: user_id)

        if bk_bad_user.present?
          bk_bad_user.update(status: status)
        end
      end

      def update_count_by_item_operation_rule_id(rule_id, options={})
        is_success_flag = false
        message = ""
        bk_bad_user = nil
        rule_id = rule_id.to_i

        if Settings.BadUser.ItemOperationRuleIds.include?(rule_id)
          is_success_flag, message, bk_bad_user = update_count(options)
        else
          is_success_flag = false
          message = "Item operation rules of bad user do not include this rule id(#{rule_id})"
          bk_bad_user = nil
        end
      end
    end
  end
end
