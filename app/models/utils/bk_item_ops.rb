module Utils
  module BkItemOps
    class << self
      def update_item_state(bk_item, item)
        if bk_item.present? and bk_item.item_state != item.state
          bk_item.update!(prev_item_state: bk_item.item_state, item_state: item.state)
        end
      end

      def update_item_audited_at(bk_item)
        if bk_item.present?
          bk_item.update!(prev_audited_at: bk_item.audited_at, audited_at: Time.current)
        end
      end

      def update_item_operator_id(bk_item, op_id)
        if bk_item.present?
          bk_item.update!(operator_id: op_id)
        end
      end

      def update_item_audit_state(bk_item, item, audit_state, operator_name, audit_history_comment, options={})
        options.symbolize_keys!
        item_risk = ''
        if Settings.PRODUCT_INTERNAL_STATUSES.ApproveRisk == audit_state and options[:item_risk].present?
          item_risk = options[:item_risk]
        end

        item_options = {}
        item_options[:internal_status] = audit_state
        item_options[:update_time] = Time.current
        item_options[:risk] = item_risk
        item.update!(item_options)

        if bk_item.present?
          bk_item_options = {}
          bk_item_options[:review_state] = item.internal_status
          bk_item_options[:is_edited] = false
          bk_item_options[:reason] = item_risk
          bk_item_options[:update_time] = item.update_time
          if bk_item.item_state != item.state
            bk_item_options[:prev_item_state] = bk_item.item_state
            bk_item_options[:item_state] = item.state
          end
          bk_item.update!(bk_item_options)
        end

        bk_item_audit_history = BkItemAuditHistory.new
        bk_item_audit_history.item_id = item.id
        bk_item_audit_history.admin = operator_name
        bk_item_audit_history.action_type = Settings.PRODUCT_INTERNAL_STATUSES.invert[audit_state]
        bk_item_audit_history.action_type_id = audit_state
        bk_item_audit_history.reason = audit_history_comment
        bk_item_audit_history.save!
      end
    end
  end
end
