module Utils
  module BkItemTaskOps
    class << self

      def delete_item_task_worker_job(task)
        is_exist = false
        is_success = false

        if task.task_jid.present?
          is_exist, is_success = Utils::BkOps.sidekiq_delete_worker_job(ItemTaskWorker, task.task_jid)
          task.update(task_jid: '')
        end

        return [is_exist, is_success]
      end

      def create_task_comment(task, comment_text, admin_username, is_update_comment_to_item_audit_history=true)
        task.create_comment(comment_text, admin_username)
        # TODO: save comment to item history too
        if is_update_comment_to_item_audit_history
          item = task.item
          current_state = item.state
          bk_item_audit_history = BkItemAuditHistory.new
          bk_item_audit_history.item_id = item.id
          bk_item_audit_history.admin = admin_username
          bk_item_audit_history.action_type = Settings.PRODUCT_INTERNAL_STATUSES.invert[item.internal_status]
          bk_item_audit_history.action_type_id = item.internal_status
          bk_item_audit_history.reason = comment_text
          bk_item_audit_history.prev_state = current_state
          bk_item_audit_history.save!
        end
      end

      def create(rule, item_id, admin_username)

        if not rule
          Rails.logger.info "Item operation rule does not exist."
          raise "rule no found!"
        end

        is_success_flag = false
        message = ""
        task = nil
        item = EntityItem.find(item_id)
        if ItemTask.is_has_opened_task(item.id)
          message = "This item had been in review task."
          is_success_flag = false
        else
          task = ItemTask.new
          task.item_id = item.id
          task.user_id = item.user_id
          task.processor = admin_username
          task.rule_id = rule.id
          task.task_type = rule.name
          task.bk_item_copy = BkItemCopy.get_latest(task.item_id)
          task.save()

          comment_text = "Operator '#{task.processor}' mark this item as '#{task.task_type}' rule, and task id is #{task.id}"

          item = task.item
          bk_item = BkItem.find_by_item_id(item.id)
          if rule.is_keep_audit_state?
            bk_item_audit_history = BkItemAuditHistory.new
            bk_item_audit_history.item_id = item.id
            bk_item_audit_history.admin = admin_username
            bk_item_audit_history.action_type = Settings.PRODUCT_INTERNAL_STATUSES.invert[item.internal_status]
            bk_item_audit_history.action_type_id = item.internal_status
            bk_item_audit_history.reason = comment_text
            bk_item_audit_history.save!
          else
            Utils::BkItemOps.update_item_audit_state(bk_item,
                                                     item,
                                                     Settings.PRODUCT_INTERNAL_STATUSES.InReview,
                                                     admin_username,
                                                     comment_text)
          end
          create_task_comment(task, comment_text, admin_username, false)

          audit_item_options = {}
          audit_item_options[:action_type] = Settings.Audit.ActionType.ItemTaskCreate
          audit_item_options[:item_id] = task.item_id
          audit_item_options[:item_task_id] = task.id
          audit_item_options[:item_task_rule_id] = task.rule_id
          audit_item_options[:item_task_state] = task.state
          audit_item_options = AuditLog.instance.add_bk_item_information(bk_item, audit_item_options)
          AuditLog.instance.audit_item_log(audit_item_options)

          message = "Create item task successfully."
          is_success_flag = true
        end
        return [is_success_flag, message, task]
      end

      def notify_user(task, admin_username, push_message=nil, email_title=nil, email_and_system_message=nil)
        push_message = task.rule.push_message if push_message.nil?
        email_title = task.rule.email_title if email_title.nil?
        email_and_system_message = task.rule.email_and_system_message if email_and_system_message.nil?
        # TODO: notify user by calling FMMC
        user = task.user
        if user.present? and not user.is_robot?
          item = task.item
          data = {}
          data['mail_subject'] = email_title
          data['nickname'] = user.nickname
          data['message'] = email_and_system_message
          data['item_image'] = item.images[0].image_path
          data['item_title'] = item.title
          data['item_price'] = item.local_price
          data['fuzzy_item_id'] = item.fuzzy_item_id
          data['push_message'] = push_message
          seller_data = {}
          seller_data['target_user'] = user.id.to_s
          seller_data['data'] = data
          # FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemLowWeightSeller, seller_data)
          Utils::BkNotifyHelper.notify_user(Settings.PRODUCT_AUDIT_STATE.LowerWeight, item.state, trigger_tmp_name: Settings.TRIGGER_TEMP_NAMES.ItemLowWeightSeller, send_data: seller_data)
        end
        text = '%s notified owner with push: %s ' % [admin_username, push_message]
        create_task_comment(task, text, admin_username)
        text = '%s notified owner with email and system message: %s (email title: %s) ' % \
      [admin_username, email_and_system_message, email_title]
        create_task_comment(task, text, admin_username)
      end

      def lower_weight(task, admin_username)
        new_weight = task.rule.lower_weight_value
        current_weight = task.item.weight
        is_success_flag = false
        message = ''
        if current_weight != new_weight
          if new_weight < Settings.WEIGHT.Min or new_weight > Settings.WEIGHT.Max
            is_success_flag = false
            message = "weight value #{new_weight} is invaid."
            Rails.logger.info message
          else
            ret = BackendApiREST.batch_update_item_weight([task.item_id], new_weight)
            result = JSON.load(ret[1])
            if result.present? && result["meta"]["result"] == "ok"
              is_success_flag = true
              Rails.logger.info 'update weight successfully.'
              if (new_weight < current_weight)
                UserStats.instance.saveRightDownCnt(task.user_id)
              end
            else
              is_success_flag = false
              message = "Failed to update weight"
              Rails.logger.info "update weight failed: #{ret}"
            end
          end
        else
          is_success_flag = true
        end

        if is_success_flag
          text = '%s changed weight of this item from %d to %d. ' % \
        [admin_username, current_weight, new_weight]
          create_task_comment(task, text, admin_username)
          task.state = Settings.ITEM_TASK_STATE.ActionExecuted
          task.save()
          audit_item_options = {}
          audit_item_options[:item_id] = task.item_id
          audit_item_options[:action_type] = Settings.Audit.ActionType.ItemTaskLowWeight
          audit_item_options[:item_task_id] = task.id
          audit_item_options[:item_task_rule_id] = task.rule_id
          audit_item_options[:item_task_state] = task.state
          AuditLog.instance.audit_item_log(audit_item_options)
        end

        return [is_success_flag, message]

      end

      def take_down(task, admin_username)
        # TODO: take down this item by calling API
        push_message = task.rule.push_message
        email_title = task.rule.email_title
        email_and_system_message = task.rule.email_and_system_message
        text = '%s take down this item. ' % [admin_username]
        # Warning: create_task_comment must be before Utils::ItemOps.disapprove, otherwise, disapprove to approve will be fault.
        create_task_comment(task, text, admin_username)
        is_success_flag, message = Utils::ItemOps.disapprove(task.item_id, admin_username, push_message, email_title, email_and_system_message)
        if is_success_flag
          task.complete(admin_username)
          audit_item_options = {}
          audit_item_options[:item_id] = task.item_id
          audit_item_options[:action_type] = Settings.Audit.ActionType.ItemTaskTakeDown
          audit_item_options[:item_task_id] = task.id
          audit_item_options[:item_task_rule_id] = task.rule_id
          audit_item_options[:item_task_state] = task.state
          AuditLog.instance.audit_item_log(audit_item_options)
        end

        return [is_success_flag, message]
      end

      def execute_action(task, admin_username, push_message=nil, email_title=nil, email_and_system_message=nil)
        is_success_flag = false
        message = ''
        task.update(execute_action_at: Time.current, task_jid: '')

        if task.rule.action.to_s == 'lower_weight'
          if task.rule.notify_user?
            notify_user(task, admin_username, push_message, email_title, email_and_system_message)
          end
          is_success_flag, message = lower_weight(task, admin_username)
          if is_success_flag and task.rule.delay_time.present?
            data = {item_task_id: task.id, item_operation_rule_id: task.rule.id, execute_action_at: task.execute_action_at.to_i}
            task_jid = ItemTaskWorker.perform_in(task.rule.delay_time.__send__(:hours), data.to_json)
            if task_jid.present?
              task.update(task_jid: task_jid)
            end
          end
        elsif task.rule.action.to_s == 'take_down'
          is_success_flag, message = take_down(task, admin_username)
        end

        Utils::BkBadUserOps.update_count_by_item_operation_rule_id(task.rule_id, user_id: task.user_id)

        if is_success_flag
          message = "Execute item task successfully"

          bk_item = task.item.bk_item
          Utils::BkItemOps.update_item_audited_at(bk_item)

          audit_item_options = {}
          audit_item_options[:item_id] = task.item_id
          audit_item_options[:action_type] = Settings.Audit.ActionType.ItemTaskExecute
          audit_item_options[:item_task_id] = task.id
          audit_item_options[:item_task_rule_id] = task.rule_id
          audit_item_options[:item_task_state] = task.state
          audit_item_options = AuditLog.instance.add_bk_item_information(bk_item, audit_item_options)
          AuditLog.instance.audit_item_log(audit_item_options)
        end

        return [is_success_flag, message]
      end

      def complete(task, admin_username, options={})
        options.symbolize_keys!
        is_success_flag = false
        message = ""
        begin
          if options[:is_ok] == true
            is_change_weight = false
            weight = options[:weight].to_i
            original_weight = task.item.weight
            if options[:weight].present? and weight != original_weight
              is_change_weight = true
              product_ids = []
              product_ids << task.item_id
              ret = BackendApiREST.batch_update_item_weight(product_ids, weight)
              result = JSON.load(ret[1])
              unless result["meta"]["result"] == "ok"
                is_success_flag = false
                message = "Failed to update weight to #{weight}."
                break
              end
            end
            text = ""
            completed_comment = options[:comment].blank? ? "OK" : options[:comment]
            if is_change_weight
              text = "#{admin_username} approves the item with comment('#{completed_comment}') and changes its weight from #{original_weight} to #{weight}"
            else
              text = "#{admin_username} approves the item with comment('#{completed_comment}')"
            end

            is_success_flag, message = Utils::ItemOps.approve(task.item_id, admin_username, "OK", false)
            if is_success_flag
              create_task_comment(task, text, admin_username)
              task.complete(admin_username)
            end
          else
            rule = options[:item_operation_rule]
            task.state = Settings.ITEM_TASK_STATE.Created
            task.processor = admin_username
            task.rule = rule
            task.task_type = rule.name
            unless task.save()
              is_success_flag = false
              message = "Failed to change task rule."
              break
            end
            comment_text = "Operator '#{task.processor}' mark this item as '#{task.task_type}' rule, and task id is #{task.id}"
            item = task.item
            bk_item = BkItem.find_by_item_id(item.id)
            if rule.is_keep_audit_state?
              bk_item_audit_history = BkItemAuditHistory.new
              bk_item_audit_history.item_id = item.id
              bk_item_audit_history.admin = admin_username
              bk_item_audit_history.action_type = Settings.PRODUCT_INTERNAL_STATUSES.invert[item.internal_status]
              bk_item_audit_history.action_type_id = item.internal_status
              bk_item_audit_history.reason = comment_text
              bk_item_audit_history.save!
            else
              Utils::BkItemOps.update_item_audit_state(bk_item,
                                                       item,
                                                       Settings.PRODUCT_INTERNAL_STATUSES.InReview,
                                                       admin_username,
                                                       comment_text)
            end
            create_task_comment(task, comment_text, admin_username, false)

            is_success_flag = true
            message = "Change task rule successfully."

            delete_item_task_worker_job(task)

            if options[:is_execute]
              is_success_flag, message = execute_action(task, admin_username)
              if is_success_flag
                message = "Change task rule to '#{rule.name}' and execute it successfully."
              end
            end
          end
        end while false

        if is_success_flag
          audit_item_options = {}
          audit_item_options[:item_id] = task.item_id
          audit_item_options[:action_type] = Settings.Audit.ActionType.ItemTaskComplete
          audit_item_options[:item_task_id] = task.id
          audit_item_options[:item_task_rule_id] = task.rule_id
          audit_item_options[:item_task_state] = task.state
          AuditLog.instance.audit_item_log(audit_item_options)
        else
          Rails.logger.info message
        end
        return [is_success_flag, message]
      end

    end
  end
end
