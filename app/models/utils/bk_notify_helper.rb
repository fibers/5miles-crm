module Utils
  module BkNotifyHelper
    class << self
      @@decision_matrix = {}
      # Listing       Settings.PRODUCT_STATUSES.Listing : 0
      # Unlisted      Settings.PRODUCT_STATUSES.Unlisted: 6
      # Unavailable(UserDeleted)   Settings.PRODUCT_STATUSES.Unavailable: 5
      # MarkasSold       Settings.PRODUCT_STATUSES.Sold: 1
      # Unapproved(OffShelf)       Settings.PRODUCT_STATUSES.Unapproved: 4
      begin
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndLowWeight] = {}
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndLowWeight][Settings.PRODUCT_STATUSES.Listing] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndLowWeight][Settings.PRODUCT_STATUSES.Unlisted] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndLowWeight][Settings.PRODUCT_STATUSES.Unavailable] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndLowWeight][Settings.PRODUCT_STATUSES.Sold] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndLowWeight][Settings.PRODUCT_STATUSES.Unapproved] = false

        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndHighWeight] = {}
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndHighWeight][Settings.PRODUCT_STATUSES.Listing] = true
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndHighWeight][Settings.PRODUCT_STATUSES.Unlisted] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndHighWeight][Settings.PRODUCT_STATUSES.Unavailable] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndHighWeight][Settings.PRODUCT_STATUSES.Sold] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveAndHighWeight][Settings.PRODUCT_STATUSES.Unapproved] = false

        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveRisk] = {}
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveRisk][Settings.PRODUCT_STATUSES.Listing] = true
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveRisk][Settings.PRODUCT_STATUSES.Unlisted] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveRisk][Settings.PRODUCT_STATUSES.Unavailable] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveRisk][Settings.PRODUCT_STATUSES.Sold] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.ApproveRisk][Settings.PRODUCT_STATUSES.Unapproved] = false

        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.LowerWeight] = {}
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.LowerWeight][Settings.PRODUCT_STATUSES.Listing] = true
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.LowerWeight][Settings.PRODUCT_STATUSES.Unlisted] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.LowerWeight][Settings.PRODUCT_STATUSES.Unavailable] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.LowerWeight][Settings.PRODUCT_STATUSES.Sold] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.LowerWeight][Settings.PRODUCT_STATUSES.Unapproved] = false

        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.Disapprove] = {}
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.Disapprove][Settings.PRODUCT_STATUSES.Listing] = true
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.Disapprove][Settings.PRODUCT_STATUSES.Unlisted] = true
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.Disapprove][Settings.PRODUCT_STATUSES.Unavailable] = false
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.Disapprove][Settings.PRODUCT_STATUSES.Sold] = true
        @@decision_matrix[Settings.PRODUCT_AUDIT_STATE.Disapprove][Settings.PRODUCT_STATUSES.Unapproved] = true
      end

      def notify_user(after_auditing_state, item_state, options={})
        if determine_msg_sending(after_auditing_state, item_state)
          FmmcREST.notify(options[:trigger_tmp_name], options[:send_data])
        end
      end

      private
      def determine_msg_sending (after_auditing_state, item_state)
        @@decision_matrix[after_auditing_state][item_state]
      end
    end
  end
end