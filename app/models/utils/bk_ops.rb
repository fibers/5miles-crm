require 'singleton'

require 'edit_distance'
require 'item_risk_keywords'
require 'render_anywhere'
require 'sidekiq/api'

module Utils
  module BkOps
    class << self
      # from rails_autolink gem
      AUTO_LINK_RE = %r{
              (?: ((?:ed2k|ftp|http|https|irc|mailto|news|gopher|nntp|telnet|webcal|xmpp|callto|feed|svn|urn|aim|rsync|tag|ssh|sftp|rtsp|afs|file):)// | www\. )
              [^\s<\u00A0"]+
            }ix
      AUTO_LINK_RE_2 = %r{[^\s]+\.[^\s]+/[^\s]*}ix

      AUTO_EMAIL_LOCAL_RE = /[\w.!#\$%&'*\/=?^`{|}~+-]/
      AUTO_EMAIL_RE = /[\w.!#\$%+-]\.?#{AUTO_EMAIL_LOCAL_RE}*@[\w-]+(?:\.[\w-]+)+/

      @@keyword_pattern = nil

      def render_template_content(content, *args)
        options = args.extract_options!
        RenderTemplateContent.instance.render(inline: content, locals: options)
      end

      def assign_operator()
        # 随机选取非管理员的审核人员
        operators = {}
        get_operators.each do |operator|
          operators[operator.id.to_s] = operator
        end
        operators[operators.keys().sample()]
      end

      def get_operators()
        operators = []
        begin
          current_time = Time.current
          operator_ids = []
          BkOperatorSchedule.valid_scope
              .where("bk_operator_schedules.start_time <= ?", current_time)
              .where("? <= bk_operator_schedules.end_time", current_time)
              .select(:operator_id)
              .each do |schedule|
            operator_ids << schedule.operator_id
          end
          if operator_ids.present?
            Admin.select(:id, :username, :email).where(id: operator_ids).each do |admin|
              operators << admin
            end
          end
          if operators.present?
            break
          end

          Admin.valid_scope.select(:id, :username, :email)
              .where.not(user_type: Settings.BACKEND_USER.Types.System)
              .where("admins.review_roles like '%#{Settings.BACKEND_USER.Roles.Agent}%' OR admins.review_roles like '%#{Settings.BACKEND_USER.Roles.Manager}%'")
              .where.not(email: ['', nil]).each do |admin|
            operators << admin
          end
          if operators.present?
            break
          end

          # default to dongdan
          admin_id = 11
          admin = Admin.select(:id, :username, :email).find_by_id(admin_id)
          if admin.present?
            operators << admin
          end
        end while false

        return operators
      end

      def similarity_check(str_a, str_b, percent=0.5)
        max_length = [str_a.length, str_b.length].max
        if max_length == 0
          return [true, 1.0]
        else
          str_a = str_a.downcase
          str_b = str_b.downcase

          distance = str_a.levenshtein_distance(str_b).to_f
          similarity = 1 - (distance / max_length)
          return [similarity > percent ? true : false, similarity]
        end
      end

      def bk_html_diff(old_version, new_version)
        BkHTMLDiff.diff(old_version, new_version)
      end

      def sidekiq_find_job(worker_clazz, jid)
        scheduled_set = Sidekiq::ScheduledSet.new
        job = scheduled_set.find_job(jid)

        if job.blank?
          queue_name = worker_clazz.get_sidekiq_options["queue"]
          queue = Sidekiq::Queue.new(queue_name)
          job = queue.find_job(jid)
        end

        return job
      end

      def sidekiq_delete_worker_job(worker_clazz, jid)
        is_exist = false
        is_success = false

        job = sidekiq_find_job(worker_clazz, jid)

        if job.present?
          is_exist = true
          is_success = job.delete
        end

        return [is_exist, is_success]
      end

      def sidekiq_queue_scheduled_job(jid)
        is_exist = false
        is_success = false

        scheduled_set = Sidekiq::ScheduledSet.new
        job = scheduled_set.find_job(jid)
        if job.present?
          is_exist = true
          job.add_to_queue
          is_success = true
        end

        return [is_exist, is_success]
      end

      def is_include_keywords(text)
        is_include = false
        kw_list = []
        re_pattern = BackendConfiguration::ItemRiskKeywords.instance.get_re_pattern()
        if re_pattern.blank?
          if @@keyword_pattern.blank?
            keywords_list = []
            Utils::ItemRiskKeywords.item_risk_keywords.each do |kw|
              keywords_list << kw
            end
            Rails.logger.info keywords_list

            keywords_str = keywords_list.join("|")
            sub_pattern = %r{#{keywords_str}}i
            @@keyword_pattern = %r{\b#{sub_pattern}\b}
            # Rails.logger.info "keyword_pattern: #{@@keyword_pattern}"
          end
          re_pattern = @@keyword_pattern
        end
        if re_pattern.present?
          kw_list = text.scan(re_pattern)
          if kw_list.present?
            is_include = true
          end
        end

        return [is_include, kw_list]
      end

      def process_include_keywords_item(item)
        comment_text = ""
        title_is_include, title_kw_list = is_include_keywords(item.title)
        desc_is_include, desc_kw_list = is_include_keywords(item.desc)
        is_include = false
        if title_is_include or desc_is_include
          is_include = true
          kw_list = title_kw_list | desc_kw_list
          item_data = {}
          item_data['keyword_list'] = kw_list
          item_data['title'] = item.title if title_is_include
          item_data['desc'] = item.desc if desc_is_include
          comment_text = <<-EOF
Keywords:  #{item_data['keyword_list'].inspect}
          EOF
          if item_data['title'].present?
            comment_text = <<-EOF
#{comment_text.chomp}
Title with keywords: #{item_data['title']}
            EOF
          end
          if item_data['desc'].present?
            comment_text = <<-EOF
#{comment_text.chomp}
Desc with keywords:  #{item_data['desc']}
            EOF
          end
        end
        message = "The item(id: #{item.id}) does not include risk keywords"
        begin
          if is_include
            message = "The item(id: #{item.id}) includes risk keywords"
            item_operation_rule_name = "SYSTEM_Risk Item Filtering"
            rule = ItemOperationRule.find_by_name(item_operation_rule_name)
            if rule.blank?
              message = "Item operation rule(name:'#{item_operation_rule_name}') does not exist."
              break
            end
            unless rule.is_enabled?
              message = "Item operation rule(name:'#{item_operation_rule_name}') has been disabled."
              break
            end

            task = ItemTask.get_opened_task_by_item_id(item.id)
            if task.blank?
              is_success_flag, message, task = Utils::BkItemTaskOps.create(rule,
                                                                           item.id,
                                                                           EmployeeManager.instance.current_employee_name)
              task.create_comment(comment_text, EmployeeManager.instance.current_employee_name)
              Utils::BkItemTaskOps.execute_action(task, EmployeeManager.instance.current_employee_name)
            else
              ret = Utils::BkItemTaskOps.complete(task, EmployeeManager.instance.current_employee_name, item_operation_rule: rule)
              if ret[0]
                task.create_comment(comment_text, EmployeeManager.instance.current_employee_name)
                Utils::BkItemTaskOps.execute_action(task, EmployeeManager.instance.current_employee_name)
              end
            end
          end
          if is_include
            save_item_risk_words(item.id, title_kw_list, desc_kw_list)
          end
        end while false
        return [ret, message]
      end

      # 工作task
      def filter_contact_info(item_id)
        item_detail_page_prefix = "http://506.5miles.io/products"
        unless Rails.env == "production"
          item_detail_page_prefix = "http://54.65.41.175/products"
        end
        item_data = {}
        phone_number_list = []
        EntityItem.where(id: item_id)
            .select(:id, :title, :desc, :modified_at)
            .each do |item|
          if item.title.present?
            ret = find_contact_info(item.title)
            if ret.present?
              item_data['title'] = item.title
              phone_number_list |= ret
            end
          end
          if item.desc.present?
            ret = find_contact_info(item.desc)
            if ret.present?
              item_data['desc'] = item.desc
              phone_number_list |= ret
            end
          end
          if item_data.present?
            item_data['contact_info_list'] = phone_number_list
            item_data['url'] = "#{item_detail_page_prefix}/#{item.id}"
            item_data['modified_at'] = item.modified_at
            item_data['id'] = item.id
          end
        end
        # puts item_data.inspect

        return item_data
      end

      #private

      # ################################################
      def find_contact_info(text)
        contact_info = []
        begin
          ret = find_phone_us(text)
          if ret.present?
            contact_info |= ret
            break
          end
          ret = find_web_url(text)
          if ret.present?
            contact_info << ret
            break
          end
          ret = find_email(text)
          if ret.present?
            contact_info << ret
            break
          end
        end while false

        return contact_info
      end

      def find_email(text)
        AUTO_EMAIL_RE.match(text).to_s
      end

      def find_web_url(text)
        ret = AUTO_LINK_RE.match(text) || AUTO_LINK_RE_2.match(text)
        ret.to_s
      end

      # filter phone number
      def find_phone_us(text)
        #puts "------s"
        text = "phone_test#{text}"
        text = text.downcase
        text = text.gsub(/\$[\d\.]+/, '')
        result = extract_phone_us(text)
        if result.blank?
          # puts "empty"
          text = extract_num(text)
          #puts text
          result = extract_phone_us(text)
          #puts result
        end
        #puts "------e"
        return result
      end

      def extract_phone_us(text)
        result = []
        text.scan(/(?<=\D)[\d|\.|,|\*|\(|\/|\)|\s|[:blank:]|\-]{10,}/) do |phone|
          #puts "extract_phone_us: #{phone}"
          phone = phone.gsub(/[\.|,|\*|\(|\/|\)|\s|[:blank:]|\-]+/, '')
          #puts "phone: #{phone}, len=#{phone.length}"
          if phone.length == 10 and not phone.start_with?('201')
            result << phone
          elsif phone.length == 11 and phone.start_with?('1')
            result << phone[1..-1]
          elsif phone.length > 10
            result << phone[0..-1]
          end
        end
        return result
      end

      def extract_num(text)
        numbers = {
            'twenty' => '20', 'thirty' => '30', 'forty' => '40', 'fifty' => '50',
            'sixty' => '60', 'seventy' => '70', 'eighty' => '80', 'ninety' => '90',
            'zero' => '0', 'one' => '1', 'two' => '2', 'three' => '3',
            'four' => '4', 'five' => '5', 'six' => '6', 'seven' => '7',
            'eight' => '8', 'nine' => '9',
        }
        numbers.each do |key, value|
          reg = Regexp.new("(?<=[^a-z])#{key}")
          text = text.gsub(reg, value)
        end
        numbers.each do |key, value|
          reg = Regexp.new("(?<=[^a-z])#{key}")
          text = text.gsub(reg, value)
        end

        # replace I to 1: 7I3 to 713
        text = text.gsub(/(?<=[\d\-])(?i:i)(?=\d)/, '1')
        # replace o to 0: 2o2 to 202
        text = text.gsub(/(?<=\d){2,}(?i:o)/, '0')
        text = text.gsub(/(?i:o)(?=\d){2,}/, '0')
        text = text.gsub(/(?<=\d)x+/, '')
        return text
      end

      def test_parse()
        lines = [
            # 'feel free to text or call at area code 713-459-47fivetwo.',
            # 'feel free to text or call at area code 713-459-4752.',
            # 'call or text my name is John 972 598 5011',
            # 'call/text @ 214-673-355eight. Buick, ',
            # 'Please text me at 214-901-5989. Emails usually go to the Spam folder',
            # 'Call and reserve your slot! -John (469)835-five eight 12 Johnwardmusic.wordpress.com',
            # 'MY NAME IS PAUL CALL OR TXT ME @ 2144972077 Serious buyers',
            # 'Please call 501-4 42--8497 if interested. We live near Spring Creek and 75',
            # 'Please call or text 501-442-8497 if interested.   We live near Spring Creek and 75. <br>',
            # 'so lets talk txt me or email me 469-245 three nine one 4|',
            # 'CONTACT: Text (preferred) or voice 817-657-3695. Please leave a message as I screen my calls. Or email. ',
            # 'Call or Text me if Interested (817-884-6364)<br>',
            # '<95>Rsx Hub. **100<br>(213) 309-5558<br>',
            # 'Feel free to email or Text 972-seven8six-2428 With any questions Toys',
            # 'And some plywood  , Sale all together $ 500 obo , call 903-217-7602',
            # '100% Original Product *** CALL OR TEXT. 214 641 5620<br>',
            # 'Thanks! Seven13-Five05-984Seven ',
            # 'Thanks Seven13-505-7797',
            # 'call or text. 832*813*9309',
            # "MSRP: $1,100.00 *Not accepting trades* **Must come pick up in Saginaw** TEXT/CALL ME AT: 8one7-8four5-2sixsix3 *Due to work schedule, I'm asleep between 5pm-1am so don't expect a reply if you call/text between that time* ",
            # "It is new in the case. No trades $175 will same you a little more than the taxes. Call or text me at 8 one 7 - 7 one 4 - Eight 8 Five 6.",
            # "text me at 8three2, four seven three, 4500. I'm in the north side of Houston",
            # 'and exterior text me at 682 478 nine nine 0eight Chevy Silverado Tahoe GMC Sierra Yukon Denali ',
            # 'Call or text 7I3-6I4-I65O. k',
            # '- (214) 649-9226 Zack ',
            # ' Call or text 817xxx889xxx0341',
            # 'Sarah 7 l 3 . 922.382.',
            # 'with questions. $200.00  281-728-7432 the following ',
            # '$125 OBO Pick up only please Call/text/email EIGHT THREE TWO 808 6009 Thanks for looking!',
            # 'if interested call me or text me at two one four 9073722',
            # 'call or text me at 817-300 eighty three zero six ',
            # 'Email or Text 972-four89-767five',
            # 'Brand new factory seal ipad mini 3 64g wifi gold color ! Seriously buyer , no shipping, no PayPal, pu in McKinney Text :469288991seven',
            # 'All or text.. 1(682)551-9040 (DANIEL)',
        ]

        for line in lines
          puts line
          puts extract_num(line)
          puts "Phones ======> #{find_phone_us(line)}"
        end
      end

      # def send_message_to_slack(text)
      #   uri = URI("https://hooks.slack.com/services/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")
      #   req = Net::HTTP::Post.new(uri)
      #   payload = {}
      #   payload['text'] = text
      #   payload['channel'] = "@liuyanliang"
      #   payload['username'] = "#{Rails.env.capitalize}: backend_filter_phone"
      #   send_params = {}
      #   send_params['payload'] = payload.to_json
      #   req.set_form_data(send_params)
      #   res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
      #     http.request(req)
      #   end
      # end

      def send_mail_to_admins(item_data)
        if item_data.blank?
          return
        end

        email_head_part_html_filename_path = "~/filter_phone/email-head.html"
        email_tail_part_html_filename_path = "~/filter_phone/email-tail.html"

        email_head_part_html_filename_path = File.expand_path(email_head_part_html_filename_path)
        unless File.exist?(email_head_part_html_filename_path)
          Rails.logger.info "#{Time.current}: #{email_head_part_html_filename_path} does not exist."
          return
        end
        email_tail_part_html_filename_path = File.expand_path(email_tail_part_html_filename_path)
        unless File.exist?(email_tail_part_html_filename_path)
          Rails.logger.info "#{Time.current}: #{email_tail_part_html_filename_path} does not exist."
          return
        end

        admin_emails = []
        if Rails.env == "production"
          # admin_emails << "operations-3stone@wespoke.com"
          admin_emails << "hello@5milesapp.com"
        else
          admin_emails << "liuyanliang@wespoke.com"
        end

        email_text_list = []
        File.open(email_head_part_html_filename_path) do |f|
          email_head_part = f.read()
          email_text_list << email_head_part
        end
        email_text_list << "<tr>\n"
        email_text_list << "<td>#{item_data['id']}</td>\n"
        email_text_list << "<td>#{item_data['url']}</td>\n"
        email_text_list << "<td>#{item_data['title']}</td>\n"
        email_text_list << "<td>#{item_data['desc']}</td>\n"
        email_text_list << "</tr>\n"
        File.open(email_tail_part_html_filename_path) do |f|
          email_tail_part = f.read()
          email_text_list << email_tail_part
        end
        # puts email_text_list.join("")
        email_subject = "新商品中含有手机号的美国商品列表"
        email_params = {}
        email_params['from'] = Settings.FROM_EMAIL_DOMAINS.Promption.Subscirbe
        email_params['subject'] = email_subject
        email_params['text'] = email_text_list.join("")
        # parameters: to, from , template, text, subject, data
        for email_to in admin_emails
          unless FmmcREST.send_direct_mail(email_to, email_params)
            Rails.logger.info "Fail to send '#{email_subject}' to '#{email_to}'"
            Rails.logger.info "#{email_params.to_json}"
          end
        end
      end

      def save_item_risk_words(item_id, title_kw_list, desc_kw_list)
        bk_item = BkItemRiskWord.find_by(item_id: item_id)
        if bk_item.blank?
          bk_item = BkItemRiskWord.new
          bk_item.item_id = item_id
        end

        bk_item.title_risk_words = title_kw_list.nil? ? '' : title_kw_list.join(",")
        bk_item.desc_risk_words = desc_kw_list.nil? ? '' : desc_kw_list.join(",")
        bk_item.save!
      end
    end

    private
    class RenderTemplateContent
      include RenderAnywhere
      include Singleton
    end

    class BkHTMLDiff
      class << self
        include HTMLDiff
      end
    end

  end
end
