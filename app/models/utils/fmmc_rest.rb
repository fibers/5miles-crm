require 'uri'
require 'net/http'

class FmmcREST

  extend Utils::BackendTraceLogOps::ApiTraceLogOps

  class << self
    @@URI_BASE = Settings.FMMC.URI
    def push(target_user, *args)
      # parameters: target_user, template, title, text, data, payload, flag
      parameters = args.extract_options!
      parameters["target_user"] = target_user.to_s
      remote_execute('push', parameters.deep_stringify_keys)
    end

    def send_mail(target_user, *args)
      # parameters: target_user, from, template, text, subject, data
      parameters = args.extract_options!
      parameters["target_user"] = target_user.to_s
      remote_execute('send_mail', parameters.deep_stringify_keys)
    end

    def send_direct_mail(to, *args)
      # parameters: to, from , template, text, subject, data
      parameters = args.extract_options!
      parameters["to"] = to
      remote_execute('send_direct_mail', parameters.deep_stringify_keys)
    end

    def notify(trigger_name, *args)
      # parameters: target_user, cron_name, data
      parameters = args.extract_options!
      parameters["trigger_name"] = trigger_name
      remote_execute('notify', parameters.deep_stringify_keys)
    end

    def system_message(target_user, *args)
      # parameters: target_user, template, data, text, image, action, desc
      parameters = args.extract_options!
      parameters["target_user"] = target_user.to_s
      remote_execute('system_message', parameters.deep_stringify_keys)
    end

    private

    def remote_execute(*args)
      method = args.shift
      parameters = args.extract_options!
      uri = URI.join(@@URI_BASE, method)
      req = Net::HTTP::Post.new(uri)
      if parameters["data"].present?
        parameters["data"] = parameters["data"].to_json
      end
      if parameters["payload"].present?
        parameters["payload"] = parameters["payload"].to_json
      end

      req.set_form_data(parameters)
      req["Accept"] = "application/vnd.fmmc-v2.1+json"

      res = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(req)
      end

      if res.is_a?(Net::HTTPCreated) #Response code:201
        trace_log_target = "#{method}"
        trace_log_business_id = nil
        trace_log_content = parameters
        trace_log_after_add(trace_log_target, trace_log_business_id, trace_log_content)
        return true
      else
        Rails.logger.error "#{uri}, #{parameters.inspect}, RequestBody(#{req.body}),ResponseCode(#{res.code})"
        return false
      end

    end
  end
end
