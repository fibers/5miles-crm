module Utils
  module ItemOps
    class << self
      # NoChange: "NC", PrevState: "PS"
      # +------------------------------------------------------------------------+
      # |            | Listing  | Sold     | Unapproved| Unavailable |  Unlisted |
      # +------------------------------------------------------------------------+
      # |Approve     |   NC     |   NC     |   PS      |      NC     |   NC      |
      # +------------------------------------------------------------------------+
      # |Settings.PRODUCT_AUDIT_STATE.ApproveRisk |   NC     |   NC     |   PS      |      NC     |   NC      |
      # +------------------------------------------------------------------------+
      # |Disapprove  |Unapproved|Unapproved|   NC      |      NC     | Unapproved|
      # +------------------------------------------------------------------------+
      def approve(item_id, admin_username, reason="OK", is_notify_user=true, is_auto_approve=false)
        is_success_flag = true
        message = ""
        prev_state = nil
        current_state = nil
        item_id = item_id.to_i
        begin
          product = EntityItem.includes(:user, :images).find_by_id(item_id)
          if product.blank?
            is_success_flag = false
            message = "product(id:#{item_id}) does not exist."
            break
          end

          if product.internal_status == Settings.PRODUCT_INTERNAL_STATUSES.Approve
            is_success_flag = true
            message = "product(id:#{item_id}) has already been approved."
            break
          end

          current_state = product.state
          Rails.logger.warn "Current product #{item_id} state: #{current_state}"
          bk_item = BkItem.find_by_item_id(item_id)
          Utils::BkItemOps.update_item_state(bk_item, product)
          # lvyi remove
          # if current_state == Settings.PRODUCT_STATUSES.Unavailable
          #   is_success_flag = false
          #   message = "product(id:#{item_id}) has already been removed by owner."
          #   break
          # end
          if product.state == Settings.PRODUCT_STATUSES.Unapproved
            if bk_item.present?
              prev_state = bk_item.prev_item_state
            end
            if prev_state.blank?
              last_review = BkItemAuditHistory.last_review(item_id)
              if last_review.present? and last_review.action_type_id.present? and last_review.action_type_id == Settings.PRODUCT_INTERNAL_STATUSES.Disapprove
                prev_state = last_review.prev_state
              else
                prev_state = Settings.PRODUCT_STATUSES.Listing
              end
            end
            # call api
            is_success_flag = BackendApiREST.review_item(item_id, Settings.PRODUCT_INTERNAL_STATUSES.Approve, prev_state)
          else
            is_success_flag = true
          end
          if is_success_flag
            product.reload

            Utils::BkItemOps.update_item_audit_state(bk_item,
                                                     product,
                                                     Settings.PRODUCT_INTERNAL_STATUSES.Approve,
                                                     admin_username,
                                                     reason)
            Utils::BkItemOps.update_item_audited_at(bk_item)

            Utils::BkItemOps.update_item_operator_id(bk_item, admin_username)

            audit_item_options = {}
            audit_item_options[:action_type] = Settings.Audit.ActionType.Approve
            audit_item_options[:item_id] = product.id
            audit_item_options = AuditLog.instance.add_bk_item_information(bk_item, audit_item_options)
            AuditLog.instance.audit_item_log(audit_item_options)

            if product.user.present? and not product.user.is_robot? and is_notify_user == true
              # if product.state == Settings.PRODUCT_STATUSES.Listing
              data = {}
              data['item_image'] = product.images[0].image_path
              data['fuzzy_item_id'] = product.fuzzy_item_id
              seller_data = {}
              seller_data['target_user'] = product.user.id.to_s
              seller_data['data'] = {}
              seller_data['data'].merge!(data)
              # FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemApproveSeller, seller_data)
              if product.internal_status == Settings.Audit.ActionType.Approve
                if product.weight < 5
                  Utils::BkNotifyHelper.notify_user(Settings.PRODUCT_AUDIT_STATE.ApproveAndLowWeight, Settings.PRODUCT_STATUSES.Listing, trigger_tmp_name: Settings.TRIGGER_TEMP_NAMES.ItemApproveSeller, send_data: seller_data)
                else
                  Utils::BkNotifyHelper.notify_user(Settings.PRODUCT_AUDIT_STATE.ApproveAndHighWeight, Settings.PRODUCT_STATUSES.Listing, trigger_tmp_name: Settings.TRIGGER_TEMP_NAMES.ItemApproveSeller, send_data: seller_data)
                end
              end
              # end
            end

            # 设置用户的连续审核次数
            if is_auto_approve
              UserStats.instance.setApproveCountOnly(product)
            else
              UserStats.instance.setApproveCount(product)
            end

            message = "Approve the product '#{product.title}' successfully."
          else
            message = "Failed to approve the product '#{product.title}'."
          end
        end while false

        return [is_success_flag, message]
      end

      def approve_risk(item_id, admin_username, reason)
        is_success_flag = true
        message = ""
        prev_state = nil
        current_state = nil
        item_id = item_id.to_i

        begin
          product = EntityItem.includes(:user, :images).find_by_id(item_id)
          if product.blank?
            is_success_flag = false
            message = "product(id:#{item_id}) does not exist."
            break
          end

          if product.internal_status == Settings.PRODUCT_INTERNAL_STATUSES.Settings.PRODUCT_AUDIT_STATE.ApproveRisk
            is_success_flag = true
            message = "product(id:#{item_id}) has already been approved with risk, Please check it."
            break
          end

          current_state = product.state
          Rails.logger.warn "Current product #{item_id} state: #{current_state}"

          bk_item = BkItem.find_by_item_id(item_id)
          Utils::BkItemOps.update_item_state(bk_item, product)
          if current_state == Settings.PRODUCT_STATUSES.Unavailable
            is_success_flag = false
            message = "product(id:#{item_id}) has already been removed by owner."
            break
          end
          if product.state == Settings.PRODUCT_STATUSES.Unapproved
            if bk_item.present?
              prev_state = bk_item.prev_item_state
            end
            if prev_state.blank?
              last_review = BkItemAuditHistory.last_review(item_id)
              if last_review.present? and last_review.action_type_id.present? and last_review.action_type_id == Settings.PRODUCT_INTERNAL_STATUSES.Disapprove
                prev_state = last_review.prev_state
              else
                prev_state = Settings.PRODUCT_STATUSES.Listing
              end
            end
            # call api
            is_success_flag = BackendApiREST.review_item(item_id, Settings.PRODUCT_INTERNAL_STATUSES.Settings.PRODUCT_AUDIT_STATE.ApproveRisk, prev_state)
          else
            is_success_flag = true
          end

          if is_success_flag
            product.reload

            audit_options = {}
            audit_options[:item_risk] = reason
            Utils::BkItemOps.update_item_audit_state(bk_item,
                                                     product,
                                                     Settings.PRODUCT_INTERNAL_STATUSES.Settings.PRODUCT_AUDIT_STATE.ApproveRisk,
                                                     admin_username,
                                                     reason,
                                                     audit_options)
            Utils::BkItemOps.update_item_audited_at(bk_item)

            audit_item_options = {}
            audit_item_options[:action_type] = Settings.Audit.ActionType.Settings.PRODUCT_AUDIT_STATE.ApproveRisk
            audit_item_options[:item_id] = product.id
            audit_item_options = AuditLog.instance.add_bk_item_information(bk_item, audit_item_options)
            AuditLog.instance.audit_item_log(audit_item_options)

            # if product.state == Settings.PRODUCT_STATUSES.Listing
            #   send_offer_user_email_flag = false
            #   if product.state == Settings.PRODUCT_STATUSES.Listing.to_i
            #     send_offer_user_email_flag = true
            #   end

            data = {}
            data['message'] = reason
            data['item_image'] = product.images[0].image_path
            data['item_title'] = product.title
            data['item_price'] = product.local_price
            data['fuzzy_item_id'] = product.fuzzy_item_id
            if product.user.present? and not product.user.is_robot?
              seller_data = {}
              seller_data['target_user'] = product.user.id.to_s
              seller_data['data'] = {}
              seller_data['data']['item_url'] = "#{Settings.APP_SITE_ITEM_URI_PREFIX}#{product.fuzzy_item_id}?from=#{Settings.EMAIL_TEMP_NAMES.ItemApproveRiskSeller}"
              seller_data['data'].merge!(data)
              # FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemApproveRiskSeller, seller_data)
              Utils::BkNotifyHelper.notify_user(Settings.PRODUCT_AUDIT_STATE.ApproveRisk, product.state, trigger_tmp_name: Settings.TRIGGER_TEMP_NAMES.ItemApproveRiskSeller, send_data: seller_data)
            end
            # if send_offer_user_email_flag
            buyer_data = {}
            buyer_data['data'] = {}
            buyer_data['data']['item_url'] = "#{Settings.APP_SITE_ITEM_URI_PREFIX}#{product.fuzzy_item_id}?from=#{Settings.EMAIL_TEMP_NAMES.ItemApproveRiskBuyer}"
            buyer_data['data']['item_id'] = product.id
            buyer_data['data'].merge!(data)
            # FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemApproveRiskBuyer, buyer_data)
            Utils::BkNotifyHelper.notify_user(Settings.PRODUCT_AUDIT_STATE.ApproveRisk, product.state, trigger_tmp_name: Settings.TRIGGER_TEMP_NAMES.ItemApproveRiskBuyer, send_data: buyer_data)
            # end
            # end

            message = "Approve the product '#{product.title}' on risk(#{reason}) successfully."

            # 设置用户的连续审核次数
            UserStats.instance.setApproveCount(product)

          else
            message = "Failed to approve the product '#{product.title}' on risk(#{reason})"
          end
        end while false

        return [is_success_flag, message]
      end

      def disapprove(item_id, admin_username, push_message, email_subject, first_section)
        is_success_flag = true
        message = ""
        prev_state = nil
        current_state = nil
        item_id = item_id.to_i

        begin
          product = EntityItem.includes(:user, :images).find_by_id(item_id)
          if product.blank?
            is_success_flag = false
            message = "Product(id: #{item_id}) does not exist"
            break
          end

          if product.internal_status == Settings.PRODUCT_INTERNAL_STATUSES.Disapprove
            is_success_flag = true
            message = "product(id:#{item_id}) has already been disapproved, Please check it."
            break
          end

          current_state = product.state
          Rails.logger.warn "Current product #{item_id} state: #{current_state}"

          bk_item = BkItem.find_by_item_id(item_id)
          Utils::BkItemOps.update_item_state(bk_item, product)
          if product.state == Settings.PRODUCT_STATUSES.Listing or product.state == Settings.PRODUCT_STATUSES.Unlisted or product.state == Settings.PRODUCT_STATUSES.Sold
            prev_state = Settings.PRODUCT_STATUSES.Unapproved
            # call api
            is_success_flag = BackendApiREST.review_item(item_id, Settings.PRODUCT_INTERNAL_STATUSES.Disapprove, prev_state)
          else
            is_success_flag = true
          end

          if is_success_flag
            product.reload

            Utils::BkItemOps.update_item_audit_state(bk_item,
                                                     product,
                                                     Settings.PRODUCT_INTERNAL_STATUSES.Disapprove,
                                                     admin_username,
                                                     first_section)
            audit_item_options = {}
            audit_item_options[:action_type] = Settings.Audit.ActionType.Disapprove
            audit_item_options[:item_id] = product.id
            audit_item_options = AuditLog.instance.add_bk_item_information(bk_item, audit_item_options)
            AuditLog.instance.audit_item_log(audit_item_options)

            # if current_state == Settings.PRODUCT_STATUSES.Listing
            #   send_offer_user_email_flag = false
            #   if current_state == Settings.PRODUCT_STATUSES.Listing.to_i
            #     send_offer_user_email_flag = true
            #   end

            data = {}
            data['message'] = first_section
            data['item_image'] = product.images[0].image_path
            data['item_title'] = product.title
            data['item_price'] = product.local_price
            data['fuzzy_item_id'] = product.fuzzy_item_id
            if product.user.present? and not product.user.is_robot?
              seller_data = {}
              seller_data['target_user'] = product.user.id.to_s
              seller_data['data'] = {}
              seller_data['data']['nickname'] = product.user.nickname
              seller_data['data']['push_message'] = push_message
              seller_data['data']['mail_subject'] = email_subject
              seller_data['data'].merge!(data)
              # FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemDisapproveSeller, seller_data)
              Utils::BkNotifyHelper.notify_user(Settings.PRODUCT_AUDIT_STATE.Disapprove, current_state, trigger_tmp_name: Settings.TRIGGER_TEMP_NAMES.ItemDisapproveSeller, send_data: seller_data)
            end
            # if send_offer_user_email_flag
            buyer_data = {}
            buyer_data['data'] = {}
            buyer_data['data']['item_id'] = product.id
            buyer_data['data'].merge!(data)
            # FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemDisapproveBuyer, buyer_data)
            Utils::BkNotifyHelper.notify_user(Settings.PRODUCT_AUDIT_STATE.Disapprove, current_state, trigger_tmp_name: Settings.TRIGGER_TEMP_NAMES.ItemDisapproveBuyer, send_data: buyer_data)
            # end
            # end

            message = "Disapprove the product '#{product.title}' successfully."


            # 设置用户的审核不通过的次数
            UserStats.instance.setUnapproveCount(product.user_id)

          else
            message = "Failed to disapprove the product '#{product.title}'."
          end
        end while false

        return [is_success_flag, message]
      end
    end
  end
end
