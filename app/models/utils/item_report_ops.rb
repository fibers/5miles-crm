module Utils
  module ItemReportOps
    class << self
      def create_zendesk_ticket(item_report)
        is_success_flag = false
        message = ''
        begin
          if item_report.is_processed
            is_success_flag = false
            message = "This item report(id: #{item_report.id}) had been processed."
            break
          end
          reporter_id = item_report.reporter_id.to_i
          item_reported_id = item_report.item_id.to_i
          reporter = EntityUser.select(:id, :nickname, :email, :fuzzy_user_id).find(reporter_id)
          if reporter.blank? or reporter.email.blank?
            is_success_flag = false
            message = "The reporter of this item report(id: #{item_report.id}) has no email."
            break
          end

          item_reported = EntityItem.select(:id, :title, :fuzzy_item_id).find(item_reported_id)

          subject = "[5miles]Item Report from #{reporter.nickname}"

          comment = []
          comment << "Reporter: #{reporter.nickname}"
          comment << "Item reported: #{item_reported.title}(#{Settings.APP_SITE_ITEM_URI_PREFIX}#{item_reported.fuzzy_item_id})"
          comment << "Reason: #{SpamReportreason.item_report_reasons.invert[item_report.reason]}" if item_report.reason.present?
          comment << "Message: #{item_report.reason_content}" if item_report.reason_content.present?

          others = []
          others << "Item report ID: #{item_report.id}"
          others << "Item report: #{Settings.BossHost}/product_reports?grid%5Bf%5D%5Bid%5D%5Beq%5D=#{item_report.id}"
          others << "Reporter: #{Settings.BossHost}/users/#{reporter_id}"
          others << "Item reported: #{Settings.BossHost}/products/#{item_reported_id}"
          others << "Report time: #{item_report.created_at.to_s}"

          tags = []
          tags << Settings.ZendeskConfig.Ticket.Tags.Boss
          tags << Settings.ZendeskConfig.Ticket.Tags.ItemReport

          ticket_options = {}
          ticket_options[:subject] = subject
          ticket_options[:comment] = comment.join("\r\n")
          ticket_options[:tags] = tags
          ticket_options[:name] = reporter.nickname
          ticket_options[:email] = reporter.email
          ticket_options[:custom_fields] = {}
          ticket_options[:custom_fields][:others] = others.join("\r\n")
          ticket = Zendesk::Ticket.new
          ret = ticket.create(ticket_options)
          if ret.blank?
            is_success_flag = false
            message = "Failed to create zendesk ticket of this item report(id: #{item_report.id})."
            break
          end
          item_report.is_processed = true
          item_report.save

          ticket_id = ret['id']
          bk_report_ticket = BkItemReportZendeskTicket.new()
          bk_report_ticket.report_id = item_report.id
          bk_report_ticket.ticket_id = ticket_id
          bk_report_ticket.save

          is_success_flag = true
          message = "Create zendesk ticket of this item report(id: #{item_report.id}) successfully."
        end while false

        return [is_success_flag, message]
      end
    end
  end
end
