module Utils
  module UserFeedbackOps
    class << self
      def create_zendesk_ticket(user_feedback)
        is_success_flag = false
        message = ''
        begin
          if user_feedback.state
            is_success_flag = false
            message = "This user feedback(id: #{user_feedback.id}) had been processed."
            break
          end
          user_id = user_feedback.user_id.to_i
          user =  EntityUser.select(:id, :nickname, :email, :fuzzy_user_id).find(user_id)
          if user.blank? or user.email.blank?
            is_success_flag = false
            message = "The user of this feedback(id: #{user_feedback.id}) has no email."
            break
          end

          subject = "[5miles]Feedback from #{user.nickname}"

          comment = []
          comment << "#{user.nickname}: #{user_feedback.text}"

          others = []
          others << "Feedback ID: #{user_feedback.id}"
          others << "Feedback: #{Settings.BossHost}/user_feedback?grid%5Bf%5D%5Bid%5D%5Beq%5D=#{user_feedback.id}"
          others << "User: #{Settings.BossHost}/users/#{user_id}"
          others << "Os: #{user_feedback.os}"
          others << "Os Version: #{user_feedback.os_version}"
          others << "App Version: #{user_feedback.app_version}"
          others << "Device: #{user_feedback.device}"
          others << "Feedback Time: #{user_feedback.created_at.to_s}"

          tags = []
          tags << Settings.ZendeskConfig.Ticket.Tags.Boss
          tags << Settings.ZendeskConfig.Ticket.Tags.Feedback

          ticket_options = {}
          ticket_options[:subject] = subject
          ticket_options[:comment] = comment.join("\r\n")
          ticket_options[:tags] = tags
          ticket_options[:name] = user.nickname
          ticket_options[:email] = user.email
          ticket_options[:custom_fields] = {}
          ticket_options[:custom_fields][:others] = others.join("\r\n")
          ticket = Zendesk::Ticket.new
          ret = ticket.create(ticket_options)
          if ret.blank?
            is_success_flag = false
            message = "Failed to create zendesk ticket of this feedback(id: #{user_feedback.id})."
            break
          end
          user_feedback.state = true
          user_feedback.save

          ticket_id = ret['id']
          bk_feedback_ticket = BkUserFeedbackZendeskTicket.new()
          bk_feedback_ticket.feedback_id = user_feedback.id
          bk_feedback_ticket.ticket_id = ticket_id
          bk_feedback_ticket.save

          is_success_flag = true
          message = "Create zendesk ticket of this feedback(id: #{user_feedback.id}) successfully."
        end while false

        return [is_success_flag, message]
      end
    end
  end
end
