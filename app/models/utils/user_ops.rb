module Utils
  module UserOps
    class << self
      def disable(user_id, current_admin_id, current_admin_username, reason)
        is_success_flag = false
        message = ""
        current_admin_id = current_admin_id.to_i
        user_id = user_id.to_i
        begin
          if reason.blank?
            is_success_flag = false
            message = 'Please input reason firstly.'
            break
          end

          user = EntityUser.find_by_id(user_id)
          if user.blank?
            is_success_flag = false
            message = "User(id: #{user_id}) does not exist"
            break
          end
          if user.is_disabled?
            is_success_flag = false
            message = "User(id: #{user_id}) had been disabled."
            break
          end
          disabled_user = DisabledUser.new
          disabled_user.user = user
          disabled_user.admin_id = current_admin_id
          disabled_user.reason = reason
          trigger = FmmcTrigger.find_by_name(Settings.TRIGGER_TEMP_NAMES.DisableUser)
          if trigger.blank?
            is_success_flag = false
            message = "Trigger template(#{Settings.TRIGGER_TEMP_NAMES.DisableUser}) does not exist"
            break
          end
          unless user.disable_user
            is_success_flag = false
            message = "Failed to disable user"
            break
          end
          disabled_user.save!
          Utils::BkBadUserOps.update_status(user_id, Settings.USER_STATE.Forbidden)
          is_success_flag = true
          message = "Disable user Successfully"
          trigger_params = {}
          trigger_params['target_user'] = user.id
          trigger_params['data'] = {}
          trigger_params['data']['nickname'] = user.nickname
          trigger_params['data']['email'] = user.email
          trigger_params['data']['message'] = reason
          trigger_params['data']['allow_disabled'] = true
          FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.DisableUser, trigger_params)
          # 下架该用户的所有商品
          push_message = "Sorry! Your item is unable to be listed on 5miles."
          email_subject = "Your item failed to be listed on 5miles"
          first_section = "Sorry! Your item is unable to be listed on 5miles because your account is disabled."
          user.products.select(:id).find_each do |product|
            is_success_flag, message = Utils::ItemOps.disapprove(product.id, current_admin_username, push_message, email_subject, first_section)
          end
        end while false

        return [is_success_flag, message]
      end

      def risk(user_id, current_admin_id, current_admin_username, reason)
        is_success_flag = true
        message = ""
        current_admin_id = current_admin_id.to_i
        user_id = user_id.to_i
        begin
          if reason.blank?
            is_success_flag = false
            message = 'Please input reason firstly.'
            break
          end

          user = User.find_by_user_id(user_id)
          if user.present?
            user.update!(internal_status: Settings.USER_INTERNAL_STATUSES.Risk, reason: reason)
          else
            user = User.new
            user.user_id = user_id
            user.internal_status = Settings.USER_INTERNAL_STATUSES.Risk
            user.reason = reason
            user.save!
          end
          bk_user_audit_history = BkUserAuditHistory.new
          bk_user_audit_history.user_id = user_id
          bk_user_audit_history.admin_id = current_admin_id
          bk_user_audit_history.action_type = Settings.USER_INTERNAL_STATUSES.Risk
          bk_user_audit_history.reason = reason
          bk_user_audit_history.save!
          is_success_flag = true
          message = "Mark user risk successfully."

          UserStats.instance.saveRiskUser(user_id)
        end while false



        return [is_success_flag, message]
      end

      def normalize(user_id, current_admin_id, current_admin_username, reason="OK")
        is_success_flag = true
        message = ""
        current_admin_id = current_admin_id.to_i
        user_id = user_id.to_i

        begin
          if reason.blank?
            is_success_flag = false
            message = 'Please input reason firstly.'
            break
          end
          user = User.find_by_user_id(user_id)
          if user.blank?
            is_success_flag = true
            message = "The user has been normal."
            break
          end
          user.destroy!
          bk_user_audit_history = BkUserAuditHistory.new
          bk_user_audit_history.user_id = user_id
          bk_user_audit_history.admin_id = current_admin_id
          bk_user_audit_history.action_type = Settings.USER_INTERNAL_STATUSES.Normal
          bk_user_audit_history.reason = reason
          bk_user_audit_history.save!
          is_success_flag = true
          message = "Normalize the user successfully."

          UserStats.instance.clearRiskUser(user_id)

        end while false
        return [is_success_flag, message]
      end
    end
  end
end
