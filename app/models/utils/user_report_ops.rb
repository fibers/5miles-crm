module Utils
  module UserReportOps
    class << self
      def create_zendesk_ticket(user_report)
        is_success_flag = false
        message = ''
        begin
          if user_report.is_processed
            is_success_flag = false
            message = "This user report(id: #{user_report.id}) had been processed."
            break
          end
          reporter_id = user_report.reporter_id.to_i
          user_reported_id = user_report.user_id.to_i
          user_ids = []
          user_ids << reporter_id
          user_ids << user_reported_id
          reporter = nil
          user_reported = nil
          EntityUser.select(:id, :nickname, :email, :fuzzy_user_id).where(id: user_ids).each do |user|
            if reporter_id == user.id
              reporter = user
            end
            if user_reported_id == user.id
              user_reported = user
            end
          end
          if reporter.blank? or reporter.email.blank?
            is_success_flag = false
            message = "The reporter of this user report(id: #{user_report.id}) has no email."
            break
          end

          subject = "[5miles]User Report from #{reporter.nickname}"

          comment = []
          comment << "Reporter: #{reporter.nickname}"
          comment << "User reported: #{user_reported.nickname}(#{Settings.APP_SITE_USER_URI_PREFIX}#{user_reported.fuzzy_user_id})"
          comment << "Reason: #{SpamReportreason.user_report_reasons.invert[user_report.reason]}" if user_report.reason.present?
          comment << "Message: #{user_report.reason_content}" if user_report.reason_content.present?

          others = []
          others << "User report ID: #{user_report.id}"
          others << "User report: #{Settings.BossHost}/user_reports?grid%5Bf%5D%5Bid%5D%5Beq%5D=#{user_report.id}"
          others << "Reporter: #{Settings.BossHost}/users/#{reporter_id}"
          others << "User reported: #{Settings.BossHost}/users/#{user_reported_id}"
          others << "Report time: #{user_report.created_at.to_s}"

          tags = []
          tags << Settings.ZendeskConfig.Ticket.Tags.Boss
          tags << Settings.ZendeskConfig.Ticket.Tags.UserReport

          ticket_options = {}
          ticket_options[:subject] = subject
          ticket_options[:comment] = comment.join("\r\n")
          ticket_options[:tags] = tags
          ticket_options[:name] = reporter.nickname
          ticket_options[:email] = reporter.email
          ticket_options[:custom_fields] = {}
          ticket_options[:custom_fields][:others] = others.join("\r\n")
          ticket = Zendesk::Ticket.new
          ret = ticket.create(ticket_options)
          if ret.blank?
            is_success_flag = false
            message = "Failed to create zendesk ticket of this user report(id: #{user_report.id})."
            break
          end
          user_report.is_processed = true
          user_report.save

          ticket_id = ret['id']
          bk_report_ticket = BkUserReportZendeskTicket.new()
          bk_report_ticket.report_id = user_report.id
          bk_report_ticket.ticket_id = ticket_id
          bk_report_ticket.save

          is_success_flag = true
          message = "Create zendesk ticket of this user report(id: #{user_report.id}) successfully."
        end while false

        return [is_success_flag, message]
      end
    end
  end
end
