module Utils
  module UserReviewOps
    class << self

      def get_template_contents(trigger_name, review)
        template_contents = {}
        trigger = FmmcTrigger.includes(task: [:push_template, :system_message_template]).find_by_name(trigger_name)
        template_contents['push_content'] = Utils::BkOps.render_template_content(trigger.task.push_template.content,
                                                                                 push_content: nil,
                                                                                 is_update: review.last_updated_at > review.created_at)
        template_contents['system_message_content'] = Utils::BkOps.render_template_content(trigger.task.system_message_template.content,
                                                                                           system_message_content: nil,
                                                                                           reviewer_nickname: review.reviewer.nickname,
                                                                                           is_update: review.last_updated_at > review.created_at)
        return template_contents
      end

      def approve(review, current_admin_id, current_admin_username, system_message_content, push_content, is_send_reviewee)
        is_success_flag = false
        message = ""

        begin
          unless review.update(state: Settings.USER_REVIEW_STATES.Approve)
            is_success_flag = false
            message = "Failed to approve the review"
            break
          end

          trigger_name = Settings.TRIGGER_TEMP_NAMES.UserReviewReviewee
          reply_content = {}
          if is_send_reviewee
            reply_content['system_message_content'] = system_message_content
            reply_content['push_content'] = push_content
            if system_message_content.blank? or push_content.blank?
              template_contents = get_template_contents(trigger_name, review)
              reply_content['system_message_content'] = template_contents['system_message_content'] if system_message_content.blank?
              reply_content['push_content'] = template_contents['push_content'] if system_message_content.blank?
            end
          end

          bk_user_review_audit_history = BkUserReviewAuditHistory.new
          bk_user_review_audit_history.review_id = review.id
          bk_user_review_audit_history.admin_id = current_admin_id
          bk_user_review_audit_history.admin = current_admin_username
          bk_user_review_audit_history.action_type = Settings.USER_REVIEW_STATES.Approve
          bk_user_review_audit_history.reason = reply_content.to_json
          bk_user_review_audit_history.save

          if is_send_reviewee and review.reviewee.present?
            # Send to FMMC
            reviewee_data = {}
            reviewee_data['target_user'] = review.reviewee.id.to_s
            reviewee_data['source_user'] = review.reviewer.id.to_s
            reviewee_data['data'] = {}
            reviewee_data['data']['reviewer_portrait'] = review.reviewer.portrait
            reviewee_data['data']['review_score'] = review.score
            reviewee_data['data']['image_type'] = 1
            reviewee_data['data']['push_content'] = push_content
            reviewee_data['data']['system_message_content'] = system_message_content
            reviewee_data['data']['system_message_desc'] = review.comment
            reviewee_data['data']['is_update'] = review.last_updated_at > review.created_at
            reviewee_data['data']['reviewer_nickname'] = review.reviewer.nickname
            FmmcREST.notify(trigger_name, reviewee_data)
          end

          is_success_flag = true
          message = "Approve the review successfully"

        end while false

        return [is_success_flag, message]
      end

      def disapprove(review, current_admin_id, current_admin_username, reason)
        is_success_flag = false
        message = ""

        begin
          unless review.update(state: Settings.USER_REVIEW_STATES.Disapprove)
            is_success_flag = false
            message = "Failed to disapprove the review"
            break
          end

          bk_user_review_audit_history = BkUserReviewAuditHistory.new
          bk_user_review_audit_history.review_id = review.id
          bk_user_review_audit_history.admin_id = current_admin_id
          bk_user_review_audit_history.admin = current_admin_username
          bk_user_review_audit_history.action_type = Settings.USER_REVIEW_STATES.Disapprove
          bk_user_review_audit_history.reason = "reason: #{reason}"
          bk_user_review_audit_history.save

          is_success_flag = true
          message = "Disapprove the review successfully"

        end while false
        return [is_success_flag, message]
      end

    end
  end
end
