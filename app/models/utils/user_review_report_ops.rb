module Utils
  module UserReviewReportOps
    class << self
      def get_template_contents(trigger_name)
        template_contents = {}
        trigger = FmmcTrigger.includes(task: [:push_template, :system_message_template]).find_by_name(trigger_name)
        template_contents['push_content'] = Utils::BkOps.render_template_content(trigger.task.push_template.content,
                                                                                 push_content: nil)
        template_contents['system_message_content'] = Utils::BkOps.render_template_content(trigger.task.system_message_template.content,
                                                                                           system_message_content: nil)
        return template_contents
      end
      
      def process(report, current_admin_id, current_admin_username, system_message_content=nil, push_content=nil, is_send_flag=false, is_ok=true)
        is_success_flag = false
        message = ""

        begin
          unless report.update(is_processed: true)
            is_success_flag = false
            message = "Failed to process the report"
            break
          end

          trigger_name = is_ok ? Settings.TRIGGER_TEMP_NAMES.UserReviewReportOkReporter : Settings.TRIGGER_TEMP_NAMES.UserReviewReportDeleteReporter
          reply_content = {}
          if is_send_flag
            reply_content['system_message_content'] = system_message_content
            reply_content['push_content'] = push_content
            if system_message_content.blank? or push_content.blank?
              template_contents = get_template_contents(trigger_name)
              reply_content['system_message_content'] = template_contents['system_message_content'] if system_message_content.blank?
              reply_content['push_content'] = template_contents['push_content'] if system_message_content.blank?
            end
          end

          bk_user_review_report_audit_history = BkUserReviewReportAuditHistory.new
          bk_user_review_report_audit_history.review_report_id = report.id
          bk_user_review_report_audit_history.admin_id = current_admin_id
          bk_user_review_report_audit_history.admin = current_admin_username
          bk_user_review_report_audit_history.reply_content = reply_content.to_json
          bk_user_review_report_audit_history.save

          if is_send_flag and report.reporter.present?
            # Send to FMMC
            reviewer_data = {}
            reviewer_data['target_user'] = report.reporter.id.to_s
            reviewer_data['data'] = {}
            reviewer_data['data']['push_content'] = push_content
            reviewer_data['data']['system_message_content'] = system_message_content
            reviewer_data['data']['system_message_desc'] = report.review.comment
            reviewer_data['data']['review_score'] = report.review.score
            FmmcREST.notify(trigger_name, reviewer_data)
          end

          is_success_flag = true
          message = "Process the report successfully"

        end while false
        return [is_success_flag, message]
      end

      def create_zendesk_ticket(review_report)
        is_success_flag = false
        message = ''
        begin
          if review_report.is_processed
            is_success_flag = false
            message = "This review report(id: #{review_report.id}) had been processed."
            break
          end
          reporter_id = review_report.reporter_id.to_i
          review_reported_id = review_report.review_id.to_i
          reporter = EntityUser.select(:id, :nickname, :email, :fuzzy_user_id).find(reporter_id)
          if reporter.blank? or reporter.email.blank?
            is_success_flag = false
            message = "The reporter of this review report(id: #{review_report.id}) has no email."
            break
          end

          review_reported = ReviewReview.select(:id, :score, :comment).find(review_reported_id)

          subject = "[5miles]Review Report from #{reporter.nickname}"

          comment = []
          comment << "Reporter: #{reporter.nickname}"
          comment << "Review reported: #{review_reported.comment}(Score: #{review_reported.score})"
          comment << "Reason: #{SpamReportreason.review_report_reasons.invert[review_report.reason]}" if review_report.reason.present?
          comment << "Message: #{review_report.reason_content}" if review_report.reason_content.present?

          others = []
          others << "Review report ID: #{review_report.id}"
          others << "Review report: #{Settings.BossHost}/users/review_reports?grid%5Bf%5D%5Bid%5D%5Beq%5D=#{review_report.id}"
          others << "Reporter: #{Settings.BossHost}/users/#{reporter_id}"
          others << "Review reported: #{Settings.BossHost}/users/reviews?grid%5Bf%5D%5Bid%5D%5Beq%5D=#{review_reported_id}"
          others << "Report time: #{review_report.created_at.to_s}"

          tags = []
          tags << Settings.ZendeskConfig.Ticket.Tags.Boss
          tags << Settings.ZendeskConfig.Ticket.Tags.ReviewReport

          ticket_options = {}
          ticket_options[:subject] = subject
          ticket_options[:comment] = comment.join("\r\n")
          ticket_options[:tags] = tags
          ticket_options[:name] = reporter.nickname
          ticket_options[:email] = reporter.email
          ticket_options[:custom_fields] = {}
          ticket_options[:custom_fields][:others] = others.join("\r\n")
          ticket = Zendesk::Ticket.new
          ret = ticket.create(ticket_options)
          if ret.blank?
            is_success_flag = false
            message = "Failed to create zendesk ticket of this review report(id: #{review_report.id})."
            break
          end
          review_report.is_processed = true
          review_report.save

          ticket_id = ret['id']
          bk_report_ticket = BkReviewReportZendeskTicket.new()
          bk_report_ticket.report_id = review_report.id
          bk_report_ticket.ticket_id = ticket_id
          bk_report_ticket.save

          is_success_flag = true
          message = "Create zendesk ticket of this review report(id: #{review_report.id}) successfully."
        end while false

        return [is_success_flag, message]
      end

    end
  end
end
