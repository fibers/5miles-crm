require 'sneakers'

class ApiWorker

  include Sneakers::Worker

  # Define queue in subclass
  # from_queue 'queuename'

  def work(data)
    Raven.capture do
      data = JSON.load(data)
      logger.info "class: #{self.class}, data: #{data}"

      if EmployeeManager.instance.current_employee_id.blank?
        # 在所有写操作之前必须关联相关的operator
        @api_operator = Admin.find_by_username(Settings.SYSTEM_OPERATORS.Api)
        if @api_operator.blank?
          error_msg = "Operator(name:#{Settings.SYSTEM_OPERATORS.Api}) does not exist. data: #{data}"
          raise(error_msg)
        end
        EmployeeManager.instance.current_employee_id = @api_operator.id
        EmployeeManager.instance.current_employee_name = @api_operator.username
      end

      # 具体处理任务，该接口实现具体的处理方式
      process_api_work(data)

    end
    # 通知rabbitmq完成
    ack!
  end

  protected
  def process_api_work(data)
    raise("Please implement this interface")
  end
end
