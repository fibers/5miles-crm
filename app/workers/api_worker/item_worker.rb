require 'sneakers'
require 'api_worker'

require 'audit/auto_audit_service'

class ItemWorker < ApiWorker

  from_queue 'boss.item_to_review',
             :timeout_job_after => 60 #defalut 5

  def process_api_work(data)
    Rails.logger.info "api_work item=#{data.inspect}"

    # item_action
    # New（已通知）
    # Edit（已通知）
    # Relist
    # Unlist
    # Delete
    # MarkasSold

    item_action = data['item_action']
    item_id = data['item_id']
    operator_id = 0
    # 原始数据
    item = EntityItem.find(item_id)
    bk_item = AuditInfo.instance.save_audit_info(item_id, item, operator_id)

    if ['Edit', 'Unlist', 'Delete', 'MarkasSold'].include?(item_action)
      ItemTask.where(item_id: item_id)
          .where.not(state: Settings.ITEM_TASK_STATE.Completed)
          .where.not(task_jid: '')
          .where.not(execute_action_at: [nil, ''])
          .each do |item_task|
        if 'Edit' == item_action
          Rails.logger.info "Delete sidekiq job(jid: #{item_task.task_jid}, item_task_id: #{item_task.id}) because of item modified"
          Utils::BkItemTaskOps.delete_item_task_worker_job(item_task)
        else
          Rails.logger.info "Queue sidekiq job(jid: #{item_task.task_jid}), item_task_id: #{item_task.id}"
          comment_text = "Item is deleted/unlisted/sold by user, auto complete according to rule."
          Utils::BkItemTaskOps.create_task_comment(item_task, comment_text, EmployeeManager.instance.current_employee_name)
          Utils::BkOps.sidekiq_queue_scheduled_job(item_task.task_jid)
        end
      end
    end

    if ['New', 'Edit', nil].include?(item_action)
      audit_options = {}
      audit_options[:item] = item
      audit_options[:bk_item] = bk_item
      audit_options[:operator_id] = operator_id
      AutoAuditService::Manager.instance.exec_audit(data, audit_options)

      # item modified trigger
      AutoDispatchWorker.perform_async({type: "item_modified", item_id: data['item_id']}.to_json)
    end

  end


end
