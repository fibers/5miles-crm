require 'sneakers'
require 'api_worker'

class ReportWorker < ApiWorker

  from_queue 'boss.report_to_review',
             :timeout_job_after => 60 #defalut 5

  def process_api_work(data)
    # case data['report_type']
    #   when "item"
    #     process_item_report(data['report_id'])
    #   when "user"
    #     process_user_report(data['report_id'])
    #   else
    #     raise "Unknow report type: #{data}"
    # end
  end

  private
  def process_item_report(report_id)
    item_report = SpamItemreportrecord.find(report_id)

    # 在zendesk创建ticket
    item_report_reason = ''
    if item_report.reason.present?
      item_report_reason = Settings.PRODUCT_REPORT_REASONS.invert[item_report.reason]
    end
    subject = "Item Report(id: #{item_report.id}, Created Time: #{item_report.created_at})"
    comment = <<-EOF
      Reason:       #{item_report_reason}
      ReasonContent:#{item_report.reason_content}
    EOF

    ticket_data = {}
    ticket_data['subject'] = subject
    ticket_data['comment'] = comment
    ticket_data['name'] = item_report.reporter.nickname
    ticket_data['email'] = item_report.reporter.email
    ticket_data['custom_fields'] = {}
    ticket_data['custom_fields']['Others']= <<-EOF
ID:           #{item_report.id}
Item:         #{Settings.BossHost}/products/#{item_report.item_id}
Reporter:     #{Settings.BossHost}/users/#{item_report.reporter_id}
Created_at:   #{item_report.created_at}
    EOF
    ZendeskWorker.perform_async(ticket_data.to_json)
  end

  def process_user_report(report_id)
    user_report = SpamUserreportrecord.find(report_id)

    # 在zendesk创建ticket
    user_report_reason = ''
    if user_report.reason.present?
      user_report_reason = Settings.USER_REPORT_REASONS.invert[user_report.reason]
    end
    subject = "User Report(id: #{user_report.id}, Created Time: #{user_report.created_at})"
    comment = <<-EOF
      Reason:       #{user_report_reason}
      ReasonContent:#{user_report.reason_content}
    EOF

    ticket_data = {}
    ticket_data['subject'] = subject
    ticket_data['comment'] = comment
    ticket_data['name'] = user_report.reporter.nickname
    ticket_data['email'] = user_report.reporter.email
    ticket_data['custom_fields'] = {}
    ticket_data['custom_fields']['Others']= <<-EOF
ID:           #{user_report.id}
User:         #{Settings.BossHost}/users/#{user_report.user_id}
Reporter:     #{Settings.BossHost}/users/#{user_report.reporter_id}
Created_at:   #{user_report.created_at}
    EOF
    ZendeskWorker.perform_async(ticket_data.to_json)
  end
end
