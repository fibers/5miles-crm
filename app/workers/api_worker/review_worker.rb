require 'sneakers'
require 'api_worker'

class ReviewWorker < ApiWorker

  from_queue 'boss.review_to_audit',
             :timeout_job_after => 60 #defalut 5

  def process_api_work(data)
    review_id = data['review_id']
    review = ReviewReview.find(review_id)
    logger.info "The review(id: #{review_id}) score is #{review.score}"
    if review.score > 3
      logger.info "The review(id: #{review_id}) should be automatically approved"
      is_success_flag, message = Utils::UserReviewOps.approve(review,
                                                              @api_operator.id,
                                                              @api_operator.username,
                                                              nil,
                                                              nil,
                                                              true)
      logger.info "The review(id: #{review_id}), message: '#{message}'"
    end
  end

end
