class TaskWorker

  include Sidekiq::Worker
  sidekiq_options :retry => false

  def initialize
    # 在所有写操作之前必须关联相关的operator
    if EmployeeManager.instance.current_employee_id.blank?
      @task_operator = Admin.find_by_username(Settings.SYSTEM_OPERATORS.Task)
      if @task_operator.blank?
        raise("Operator(name:#{Settings.SYSTEM_OPERATORS.Task}) does not exist.")
      end
      EmployeeManager.instance.current_employee_id = @task_operator.id
      EmployeeManager.instance.current_employee_name = @task_operator.username
    end

    # subclass 初始化
    task_work_initialize()
  end

  def perform(data)
    data = JSON.load(data)
    logger.info "class: #{self.class}, data: #{data}"

    # 具体处理任务，该接口实现具体的处理方式
    process_task_work(data)
  end

  protected
  def task_work_initialize

  end

  def process_task_work(data)
    raise("Please implement this interface")
  end
end
