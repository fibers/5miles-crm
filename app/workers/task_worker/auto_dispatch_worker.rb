require 'task_worker'

class AutoDispatchWorker < TaskWorker
  sidekiq_options retry: false, queue: 'auto_dispatch'

  def process_task_work(data)
    AutoDispatchService::DispatchCtrlCenter.instance.deal_dispatch(data["type"], data)
  end

end
