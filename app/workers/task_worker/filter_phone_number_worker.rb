require 'task_worker'

class FilterPhoneNumberWorker < TaskWorker

  def process_task_work(data)

    item_id = data['item_id']

    AuditRules.instance.check_contact_info(item_id)
  end

end
