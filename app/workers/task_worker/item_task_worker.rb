require 'task_worker'

class ItemTaskWorker < TaskWorker

  sidekiq_options retry: false, queue: 'high'

  def task_work_initialize

  end

  def process_task_work(data)
    # data = {item_task_id: task.id, item_operation_rule_id: task.rule.id, execute_action_at: task.execute_action_at.to_i}
    task = ItemTask.find(data['item_task_id'])

    begin
      if task.state == Settings.ITEM_TASK_STATE.Completed
        logger.info "Item task(id: #{task.id}) had been completed."
        break
      end
      if task.rule_id != data['item_operation_rule_id']
        logger.info "Item task(id: #{task.id}) had changed rule(from #{data['item_operation_rule_id']} to #{task.rule_id})."
        break
      end
      if task.execute_action_at.to_i != data['execute_action_at']
        logger.info "Item task(id: #{task.id}) had executed action."
        break
      end
      rule = task.rule
      if rule.action_type.blank?
        logger.info "Item task(id: #{task.id})'s rule(id:#{task.rule_id}) has no action_type."
        break
      end
      process_item_task_work(task)
    end while false
  end

  private

  def process_item_task_work(task)
    rule = task.rule
    case rule.action_type
      when Settings.ItemOperationRules.ActionType.ApproveAndComplete
        is_success_flag = false
        message = ''
        comment = "No modify over time, auto approve by rule."
        if rule.target_weight.blank?
          is_success_flag, message = Utils::BkItemTaskOps.complete(task,
                                                                   EmployeeManager.instance.current_employee_name,
                                                                   is_ok: true,
                                                                   comment: comment)
        else
          is_success_flag, message = Utils::BkItemTaskOps.complete(task,
                                                                   EmployeeManager.instance.current_employee_name,
                                                                   is_ok: true,
                                                                   comment: comment,
                                                                   weight: rule.target_weight)
        end
        unless is_success_flag
          logger.info message
        end
      when Settings.ItemOperationRules.ActionType.ExchangeRule
        target_rule = rule.target_rule
        begin
          if target_rule.blank?
            logger.info "Rule(id: #{rule.id}) has no target rule"
            break
          end
          is_success_flag, message = Utils::BkItemTaskOps.complete(task,
                                                                   EmployeeManager.instance.current_employee_name,
                                                                   item_operation_rule: target_rule,
                                                                   is_execute: true)
          unless is_success_flag
            logger.info message
            break
          end
        end while false
      else
        logger.info "Rule action type(#{rule.action_type}) is not defined"
    end
  end
end
