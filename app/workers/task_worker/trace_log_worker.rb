require 'task_worker'

class TraceLogWorker < TaskWorker

  def process_task_work(data)
    if TraceLog.table_name != data['op_target']
      TraceLog.create(data)
    end
  end

end
