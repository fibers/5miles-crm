require 'task_worker'

class UnlistItemWorker < TaskWorker

  sidekiq_options retry: false, queue: 'high'

  def task_work_initialize

  end

  def process_task_work(data)

    item = EntityItem.find(data['item_id'])
    bk_item = BkItem.find_by_item_id(data['item_id'])
    case data['step']
      when 2
        unlist_item_step_2(data, item, bk_item)
      when 3
        unlist_item_step_3(data, item, bk_item)
      else
        unlist_item_step_1(data, item, bk_item)
    end
  end

  private

  def change_to_next_step(data, item, bk_item)
    data['step'] += 1
    current_jid = self.class.perform_at(data['at'], data.to_json)
    logger.info "current_jid: #{current_jid}"
    if current_jid.nil?
      raise("Failed to perform unlist item worker, data: #{data.inspect}")
    else
      bk_item.update!(unlist_task_jid: current_jid) if bk_item.present?
    end
  end

  def unlist_item_step_1(data, item, bk_item)
    # 需修正：有了上架事件的时候，下面的条件应该把Settings.PRODUCT_STATUSES.Unlisted去掉
    if item.state == Settings.PRODUCT_STATUSES.Listing or item.state == Settings.PRODUCT_STATUSES.Unlisted
      # 需修正：data['on_shelf_at']应该是该商品的上架时间，现在还没有该字段，所以先用创建时间代替
      data['on_shelf_at'] = item.created_at.to_i
      data['at'] = (Settings.UnlistItem.Threshold - Settings.UnlistItem.RemindThreshold).__send__(:days) + data['on_shelf_at']
      data['step'] = 1
      change_to_next_step(data, item, bk_item)
    else
      logger.info "Do not create delay task, because its item state of this item(id: #{data['item_id']}) is #{Settings.PRODUCT_STATUSES.invert[item.state]}"
    end
  end

  def unlist_item_step_2(data, item, bk_item)
    # 当该商品的商品状态为Listing时，提醒该商品的owner对该商品Renew、降价、改进描述，如果7日内不Renew或修改，将自动unlist
    if item.state == Settings.PRODUCT_STATUSES.Listing
      if item.user.present? and not item.user.is_robot?
        seller_data = {}
        seller_data['target_user'] = item.user.id.to_s
        seller_data['data'] = {}
        seller_data['data']['item_image'] = item.images[0].image_path
        seller_data['data']['fuzzy_item_id'] = item.fuzzy_item_id
        FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemUnlistRemindSeller, seller_data)
      end
    end

    # 需修正：下面的逻辑为了循环检测放到这里。
    # 1.等有了上架事件的时候，这里需要放到上面Listing状态的条件下
    # 2.不为Listing状态时，还得bk_item.update!(unlist_task_jid: '')
    data['modified_at'] = item.modified_at.to_i
    data['renewed_at'] = item.renewed_at.to_i
    data['at'] = Settings.UnlistItem.RemindThreshold.__send__(:days) + Time.now.to_i
    change_to_next_step(data, item, bk_item)
  end

  def unlist_item_step_3(data, item, bk_item)
    bk_item.update!(unlist_task_jid: '') if bk_item.present?
    if data['modified_at'] < item.modified_at.to_i or data['renewed_at'] < item.renewed_at.to_i
      logger.info "The owner of this item(id: #{data['item_id']}) has modified or renewed it"
    elsif item.state == Settings.PRODUCT_STATUSES.Listing
      # unlist 该商品
      ret = BackendApiREST.unlist_item(item.id)
      if ret
        # 通知该商品的owner
        if item.user.present? and not item.user.is_robot?
          seller_data = {}
          seller_data['target_user'] = item.user.id.to_s
          seller_data['data'] = {}
          seller_data['data']['item_url'] = "#{Settings.APP_SITE_ITEM_URI_PREFIX}#{item.fuzzy_item_id}"
          seller_data['data']['nickname'] = item.user.nickname
          seller_data['data']['item_image'] = item.images[0].image_path
          seller_data['data']['item_title'] = item.title
          seller_data['data']['item_price'] = item.local_price
          seller_data['data']['fuzzy_item_id'] = item.fuzzy_item_id
          FmmcREST.notify(Settings.TRIGGER_TEMP_NAMES.ItemUnlistSeller, seller_data)
        end
      end
    end

    # 需修正：现在没有上架事件，所以，为了循环检测，这里做一些类似step1的操作，直接跳转到step2
    if item.state == Settings.PRODUCT_STATUSES.Listing or item.state == Settings.PRODUCT_STATUSES.Unlisted
      data['on_shelf_at'] = Time.now.to_i
      data['at'] = (Settings.UnlistItem.Threshold - Settings.UnlistItem.RemindThreshold).__send__(:days) + data['on_shelf_at']
      data['step'] = 1
      change_to_next_step(data, item, bk_item)
    else
      logger.info "Do not create delay task, because its item state of this item(id: #{data['item_id']}) is #{Settings.PRODUCT_STATUSES.invert[item.state]}"
    end
  end
end
