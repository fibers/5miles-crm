require 'task_worker'

class ZendeskWorker < TaskWorker

  def task_work_initialize
  end

  def process_task_work(data)
    Zendesk::Ticket.update_local_tickets_with_lock()
  end
end

