class CreateAdsDpaFeed < ActiveRecord::Migration
  def change
    create_table :ads_dpa_feeds do |t|

        t.integer :batch_id, null: false, default:0
        t.string  :img_url, null: false, default:''
        t.integer :item_id, null: false, default:0

        t.integer :ctr_num, null: false, default:0
        t.integer :like_num, null: false, default:0
        t.integer :offer_num, null: false, default:0

        t.integer :state, null: false, default:0
        t.integer :sync_state, null: false, default:0
        t.string  :sync_action, null: false, default:''

        t.datetime :created_at
        t.timestamps
    end

    add_index :ads_dpa_feeds, :item_id ,:ctr_num
  end
end
