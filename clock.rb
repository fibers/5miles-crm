require_relative './config/boot'
require_relative './config/environment'
require 'clockwork'

module Clockwork

  handler do |job|
    case job
      when "auto_dispatch_worker"
        AutoDispatchWorker.perform_async({type: "dispatch_interval"}.to_json)
      when 'update_local_zendesk_tickets'
        ZendeskWorker.perform_async({}.to_json)
    end
  end

  # handler receives the time when job is prepared to run in the 2nd argument
  # handler do |job, time|
  #   puts "Running #{job}, at #{time}"
  # end
  every(1.minutes, 'auto_dispatch_worker')
  every(Settings.ZendeskConfig.Ticket.UpdateThreshold.minutes, 'update_local_zendesk_tickets')
end
