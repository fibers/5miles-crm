# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'backend'

set :scm, :git
set :repo_url, 'ssh://git@github.com/3rdStone/backend.git'

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/var/www/backend'
set :use_sudo, false

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :info

# Default value for :pty is false
# There is a known bug that prevents sidekiq from starting when pty is true
set :pty, false

set :sidekiq_role, [:worker]

set :sidekiq_processes, 2
set :sidekiq_options_per_process, ["--queue auto_dispatch", "--queue default --queue high"]

# rbenv
set :rbenv_type, :system
set :rbenv_ruby, '2.1.2'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml config/application.rb}
set :linked_files, %w{config/database.yml config/sidekiq.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

# which config files should be copied by deploy:setup_config
# see documentation in lib/capistrano/tasks/setup_config.cap
# for details of operations
set(:config_files, %w(
  database.yml
  unicorn.rb
  unicorn_init.sh
  sidekiq.yml
  sidekiq_init.sh
  api_workers_init.sh
))

# which config files should be made executable after copying
# by deploy:setup_config
set(:executable_config_files, %w(
  unicorn_init.sh
  sidekiq_init.sh
  api_workers_init.sh
))

# files which need to be symlinked to other parts of the
# filesystem. For example nginx virtualhosts, log rotation
# init scripts etc. The full_app_name variable isn't
# available at this point so we use a custom template {{}}
# tag and then add it at run time.
set(:symlinks, [
    {
        source: "unicorn_init.sh",
        link: "/etc/init.d/unicorn_{{full_app_name}}"
    },
    {
        source: "api_workers_init.sh",
        link: "/etc/init.d/api_workers_{{full_app_name}}"
    },
])

set(:root_owner_files, [])

set :conditionally_migrate, true

namespace :deploy do
  # make sure we're deploying what we think we're deploying
  # before :deploy, "deploy:check_revision"
  # only allow a deploy with passing tests to deployed
  # before :deploy, "deploy:run_tests"
  # compile assets locally then rsync
  # after 'deploy:symlink:shared', 'deploy:compile_assets_locally'
  after :finishing, 'deploy:cleanup'

  # remove the default nginx configuration as it will tend
  # to conflict with our configs.
  before 'deploy:setup_config', 'nginx:remove_default_vhost'

  # reload nginx to it will pick up any modified vhosts from
  # setup_config
  after 'deploy:setup_config', 'nginx:reload'

  # Restart monit so it will pick up any monit configurations
  # we've added
  # after 'deploy:setup_config', 'monit:restart'

  # rztree assert
  after 'assets:precompile', 'assets:non_digested_of_ztree'

  # As of Capistrano 3.1, the `deploy:restart` task is not called
  # automatically.
  after 'deploy:publishing', 'deploy:restart'

  before 'deploy:migrate', 'api_workers:stop'
  after 'deploy:publishing', 'api_workers:start'
  after 'deploy:publishing', 'clockwork:restart'
end
