# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

# role :app, %w{ec2-54-79-115-119.ap-southeast-2.compute.amazonaws.com}
# role :web, %w{ec2-54-79-115-119.ap-southeast-2.compute.amazonaws.com}
# role :db,  %w{fivemiles-db-au.ceetkbzx55gf.ap-southeast-2.rds.amazonaws.com}


# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

#server 'ec2-54-79-115-119.ap-southeast-2.compute.amazonaws.com', user: 'ec2-user', roles: %w{web app}, my_property: :my_value


# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
# set :ssh_options, {
#    user: 'ec2-user',
#    keys: %w(/Users/fibers/Documents/fivemiles-inst-kp.pem),
#    forward_agent: true,
#    auth_methods: %w(publickey)
# }
#
# And/or per server (overrides global)
# ------------------------------------
# server 'ec2-54-252-222-65.ap-southeast-2.compute.amazonaws.com',
#   user: 'ec2-user',
#   roles: %w{web app},
#   ssh_options: {
#     keys: %w(~/Documents/fivemiles-inst-kp.pem),
#     forward_agent: true,
#     auth_methods: %w(publickey)
#     # password: 'please use keys'
#   }


set :deploy_user, 'ec2-user'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call
set :branch, :master

# this should match the filename. E.g. if this is production.rb,
# this should be :production
set :stage, :production

# This is used in the Nginx VirtualHost to specify which domains
# the app should appear on. If you don't yet have DNS setup, you'll
# need to create entries in your local Hosts file for testing.
set :server_name, "506.5miles.io"

# used in case we're deploying multiple versions of the same
# app side by side. Also provides quick sanity checks when looking
# at filepaths
set :full_app_name, "#{fetch(:application)}_#{fetch(:stage)}"

# don't try and infer something as important as environment from
# stage name.
set :rails_env, :production

# number of unicorn workers, this will be reflected in
# the unicorn.rb and the monit configs
set :unicorn_worker_count, 5

# whether we're using ssl or not, used for building nginx
# config file
set :enable_ssl, false
set :pty, false

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.
# server 'ec2-54-252-222-65.ap-southeast-2.compute.amazonaws.com', user: 'ec2-user', roles: %w{web app db}, primary: true
#

server '52.64.91.222', user: 'ec2-user', roles: %w{web app db worker}, primary: true
