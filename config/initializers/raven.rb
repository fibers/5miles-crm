require 'raven'

Raven.configure do |config|
  config.dsn = 'https://a38dcd753e3c4212af0f9de74e31f92c:511d9efe06e343eda06a0126aaedb409@app.getsentry.com/30774'
  config.environments = %w[ test production ]
end
