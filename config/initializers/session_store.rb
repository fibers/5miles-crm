# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :cookie_store, key: '_fivemiles-backend_session', :expire_after => 1.hour
