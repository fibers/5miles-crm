Sidekiq.configure_server do |config|
  config.redis = { :url => "redis://#{Settings.Redis.Sidekiq.Host}:#{Settings.Redis.Sidekiq.Port}/#{Settings.Redis.Sidekiq.DB}" }
  config.error_handlers << Proc.new {|ex,ctx_hash| Raven.capture_exception(ex, ctx_hash) }
end

Sidekiq.configure_client do |config|
  config.redis = { :url => "redis://#{Settings.Redis.Sidekiq.Host}:#{Settings.Redis.Sidekiq.Port}/#{Settings.Redis.Sidekiq.DB}" }
end
