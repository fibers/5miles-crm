require 'sneakers'

Sneakers.configure :daemonize => true,
                   :runner_config_file => Rails.root.join('config/sneakers.conf.rb').to_s,
                   :amqp => Settings.AMPQ,
                   :log => Rails.root.join('log/sneakers.log').to_s,
                   :pid_path => Rails.root.join('tmp/pids/sneakers.pid').to_s

Sneakers.logger.level = Logger::INFO
