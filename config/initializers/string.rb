class String

  def to_slug
    chars = []
    Unicode::text_elements(self).each do |e|
      category = Unicode::abbr_categories(e)[0].to_s
      if category.start_with?('L', 'N') || '-_~'.include?(e)
        chars.push(e)
      elsif category.start_with?('Z')
        chars.append(' ')
      end
    end
    chars.join.strip.downcase.gsub(/[-\s]+/,'-')
  end

end