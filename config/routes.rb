Rails.application.routes.draw do

  require 'sidekiq/web'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == Settings.Sidekiq.username && password == Settings.Sidekiq.password
  end if Rails.env.production?
  mount Sidekiq::Web => '/sidekiq'

  get 'welcome/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # root 'welcome#index'
  root 'item_tasks#index'

  get 'index', to: redirect('welcome/index')

  get 'login' => 'welcome#login'
  post 'login' => 'welcome#create'

  get 'logout' => 'welcome#destroy'

  get 'logs' => 'trace_logs#index'

  get "fetch_product_feed" => "smart_ads/board#fetch_product_feed"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  namespace :users do
    resources :subscriptions, only: [:index, :new, :create, :edit, :update]
    resources :reviews do
      collection do
        get 'ajax', action: :review_ajax
      end
    end
    resources :review_reports do
      member do
        get 'create_zendesk_ticket', action: :create_zendesk_ticket
      end
      collection do
        get 'ajax', action: :review_report_ajax
      end
    end
  end
  resources :users, only: [:index, :show] do
    collection do
      # post 'enable'
      post 'disable'
      post 'weight', action: :update_weight
      post 'risk', action: :user_risk

      get 'map'
      get 'disabled_state', action: :index_disabled_users
      get 'risk_state', action: :index_risk_users
      get 'bad_state', action: :index_bad_users

      get 'ajax', action: :user_ajax
    end

    member do
      post 'weight', action: :update_weight
      get 'portrait'
      post 'email'
    end

  end


  resources :products, only: [:index, :show] do
    collection do
      get 'hot_items', action: :index_hot_items
      get 'weight', action: :index_weight
      post 'weight', action: :update_weight
      put 'weight', action: :update_weight
      post 'categories', action: :update_categories
      post 'categories_new', action: :update_categories_new
      put 'approve'
      # put 'disapprove'

      get 'map'
      get 'ajax', action: :product_ajax
    end

    member do
      get 'images'
      # put 'update_tags', action: :update_tags
      post 'categories', action: :update_categories
      get 'offers', action: :offers
      post 'weight', action: :update_weight
    end

    resource :state, controller: 'product_states', only: [:create] do
    end
  end

  resources :product_reports, only: [:index] do
    member do
      get 'feedback', action: :edit
      post 'feedback', action: :update
      get 'create_zendesk_ticket', action: :create_zendesk_ticket
    end
  end

  resources :user_reports, only: [:index] do
    member do
      get 'feedback', action: :edit
      post 'feedback', action: :update
      get 'create_zendesk_ticket', action: :create_zendesk_ticket
    end
  end

  # resources :tags, as: 'commodity_tags', except: [:show, :destroy] do
  #   member do
  #     get 'order'
  #   end
  #
  #   resource :state, controller: 'tag_states', only: [:create, :destroy]
  # end

  resources :categories, as: 'category', except: [:destroy] do
    collection do
      get 'treeview'
    end

    member do
      get 'publish', action: :publish_category
      get 'draft', action: :draft_category
      get 'disable', action: :disable_category
      get 'enable', action: :enable_category
    end
  end

  resources :admins, only: [:index, :new, :create, :destroy, :edit, :show, :update] do
    resource :super_state, controller: 'admin_super_states', only: [:create, :destroy]
    member do
      get 'password', action: :password_edit
      post 'password', action: :password_update
    end
    collection do
      resources :schedules, controller: 'operator_schedules'
      get 'item_risk_keywords', action: :edit_item_risk_keywords
      post 'item_risk_keywords', action: :update_item_risk_keywords
      # resources :configurations, controller: 'configurations'
      get 'white_list', action: :edit_white_list
      post 'white_list', action: :update_white_list
    end
  end

  resources :templates, only: [:index]
  namespace :templates do
    resources :pushes, except: [:destroy] do
      member do
        get 'enable'
        get 'disable'
      end
    end
    resources :emails, except: [:destroy] do
      member do
        get 'enable'
        get 'disable'
      end
    end
    resources :system_messages, except: [:destroy] do
      member do
        get 'enable'
        get 'disable'
      end
    end
    resources :smses, except: [:destroy] do
      member do
        get 'enable'
        get 'disable'
      end
    end
  end

  resource :manual, controller: 'manual', only: [:show]
  namespace :manual do
    resources :system_messages, only: [:index, :create, :new]
    resources :emails, only: [:index, :create, :new] do
      collection do
        get 'good_items', to: 'emails#new_good_items'
        post 'good_items', to: 'emails#create_good_items'
      end
    end
    resources :statistics, only:[:index]
  end

  resources :user_feedback, only: [:index] do
    member do
      get 'feedback', action: :edit
      post 'feedback', action: :update
      get 'create_zendesk_ticket', action: :create_zendesk_ticket
    end
  end


  namespace :fmmc do
    resources :crons, except: [:destroy] do
      member do
        get 'enable'
        get 'disable'
      end
    end
    resources :tasks, except: [:destroy] do
      member do
        get 'enable'
        get 'disable'
      end
    end
    resources :triggers, except: [:destroy] do
      member do
        get 'enable'
        get 'disable'
      end
    end
  end

  namespace :tools do
    resources :items, only: [:index]
    resource :start_image, controller: 'start_image', only: [:edit, :update, :destroy]
    resource :home_banner, controller: 'home_banner', only: [:edit, :update, :destroy]
    resource :recommend_items, controller: 'recommend_items', only: [:create, :new]
  end

  namespace :tools do
    resources :items, only: [:index]
  end

  resource :api, controller: 'api', only: [] do
    collection do
      post 'country'
      post 'category'
    end
  end

  resources :item_tasks, only: [:index, :show, :create] do
    member do
      post 'complete', action: :complete
      post 'execute_action', action: :execute_action
    end
    collection do
      post 'create_and_execute', action: :create_and_execute
    end

    resources :item_task_comments
  end

  resources :item_operation_rules, only: [:index, :new, :create, :show, :edit, :update] do
    member do
      get 'enable', action: :enable
      get 'disable', action: :disable
    end
  end

  namespace :smart_audit do
    resources :operators, only: [:index, :update] do
      post 'check_in'
      post 'check_out'
    end

    resources :board, only: [:index] do

      post 'change_category'

      post 'assign_data'

      collection do
        post 'pass_images',action: :pass_images
        get 'stat_info'
        get 'list_images'
        get 'list_items'
        post 'approve'
        post 'item_task_execute'
        post 'set_first_image'
      end
    end

    resources :owners, only: [:index]
    resources :tasks, only: [:index] do
      post 'quick_task_exec', action: :quick_task_exec
      member do
        post 'refresh', action: :query_task_state
      end
      collection do
        get 'get_rules'
        get 'get_img_audit_rules'
        get 'get_often_use_rules'
        post 'ajax', action: :item_task_ajax
      end
    end
  end

  namespace :smart_ads do

    resources :board, only: [:index] do

      collection do
        get 'update_info'
        get 'list_items'

        post "clear_current_feeds", action: :clear_product_feed
        post "select_feeds", action: :add_product_feed
      end
    end

  end

  # namespace :smart_review do
  #   # resources :bk_items, only: [:index, :show] do
  #   #   get 'assign', action: :get_operator_window
  #     #
  #     # post 'assign', action: :change_operator
  #     # post 'change_category'
  #     # post 'change_first_image'
  #     #
  #     # collection do
  #     #   get 'fetch_items', action: :fetch_items
  #     # end
  #   # end
  #   #
  #   # resources :bk_images, only: [:index] do
  #   #   post 'right_down' , action: :right_down
  #   #
  #   #   collection do
  #   #     post 'fetch_img_items'
  #   #     get 'stat_info'
  #   #     post 'set_items_imgs_pass'
  #   #   end
  #   # end
  #
  #   resources :users, only: [:index]
  #
  #   resources :tasks, only: [:index] do
  #     post 'quick_task_exec', action: :quick_task_exec
  #
  #     collection do
  #       get 'get_rules'
  #     end
  #   end
  # end


  get "es/list" => "es#list"
  get "es" => "es#index"

  get "my_506/bkuser" => "test#bkuser"
  get "my_506/execa" => "test#execa"
  get "my_506/q" => "test#test_q"
  get "my_506/agents" => "test#test_agents"
  get "my_506/empls" =>"test#test_empls"
  get "my_506/ph" =>"test#test_ph"
  get "my_506/wl" =>"test#test_wl"


end
