workers 1
amqp "#{Settings.AMPQ}"

before_fork do
  Sneakers::logger.info " ** im before-fork'en ** "
end


after_fork do
  Sneakers::logger.info " !! im after forke'n !! "
  ::Rails.application.eager_load!
  Sneakers::logger.info " ** eager load ** "
end
