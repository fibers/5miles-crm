class CreateAdmins < ActiveRecord::Migration

  def change
    create_table :admins do |t|
      t.string :username, null: false
      t.string :password_digest, null: false
      t.boolean :is_super_admin, null: false

      t.timestamps
    end

    add_index :admins, [:username], name: :idx_admin_username, unique: true, using: :btree

  end
end
