class CreateLanguages < ActiveRecord::Migration
  def change
    create_table :languages, force: true do |t|
      t.string :iso_code, limit: 2, null: false
      t.string :title, null: false
      t.boolean :status, null: false

      t.timestamps
    end

    add_index :languages, [:iso_code], name: :idx_language_iso_code, unique: true, using: :btree
  end
end
