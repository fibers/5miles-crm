class CreateLogs < ActiveRecord::Migration
  def change

    create_table :logs, force: true do |t|
      t.string :operator, null: false
      t.string :url, null: false

      t.timestamps
    end

    add_index :logs, [:operator], name: :idx_log_operator, using: :btree

  end
end
