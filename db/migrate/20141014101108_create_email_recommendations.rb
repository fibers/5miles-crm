class CreateEmailRecommendations < ActiveRecord::Migration
  def change
    create_table :email_recommendations do |t|
      t.string :to
      t.integer :gender
      t.integer :email_template_id
      t.string :product_ids
      t.string :remarks
      t.integer :user_total
      t.integer :error_user_total
      t.text :error_user_ids
      t.integer :status

      t.timestamps
    end
  end
end
