class CreateBackendPositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.string :name, null: false
      t.integer :parent_id, null: false
      t.integer :level, null: false

      t.timestamps
    end

    add_index :positions, :name
    add_index :positions, :parent_id

  end
end
