class AddDispalyOrderToPosition < ActiveRecord::Migration
  def change
    add_column :positions, :display_order, :integer
    add_index :positions, :display_order
  end
end
