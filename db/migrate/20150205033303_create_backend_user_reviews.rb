class CreateBackendUserReviews < ActiveRecord::Migration
  def change
    create_table :user_reviews do |t|
      t.integer :user_id, null: false
      t.integer :admin_id, null: false
      t.integer :action_type, null: false
      t.text :reason, null: false

      t.timestamps
    end
    add_index :user_reviews, :user_id
    add_index :user_reviews, :admin_id
  end
end
