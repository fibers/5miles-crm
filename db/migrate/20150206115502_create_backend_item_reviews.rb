class CreateBackendItemReviews < ActiveRecord::Migration
  def change
    create_table :item_reviews do |t|
      t.integer :item_id
      t.string :admin, null:false
      t.string :action_type, null:false
      t.text :reason, null:false

      t.timestamps
    end

    add_index :item_reviews, :item_id
    add_index :item_reviews, :created_at
  end
end
