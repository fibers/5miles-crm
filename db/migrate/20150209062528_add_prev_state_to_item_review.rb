class AddPrevStateToItemReview < ActiveRecord::Migration
  def change
    add_column :item_reviews, :action_type_id, :integer
    add_column :item_reviews, :prev_state, :integer
  end
end
