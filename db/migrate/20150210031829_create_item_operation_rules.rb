class CreateItemOperationRules < ActiveRecord::Migration

  def change
    create_table :item_operation_rules do |t|
      t.string      :name, index: true, unique: true
      t.boolean     :status, default: true
      t.string      :action, default: 'lower_weight'  # ['take_down', 'lower_weight']
      t.integer     :lower_weight_value, default: 0

      t.boolean     :notify_user, default: true
      t.string      :push_message
      t.string      :email_title
      t.text        :email_and_system_message
    end
  end

end
