class CreateItemTasks < ActiveRecord::Migration

  def change
    create_table :item_tasks do |t|
      t.references :item,  index: true
      t.references :user,  index: true
      t.string :processor, null: false, default: 'Unknown'
      t.references :rule, index: true
      t.string :task_type, null: false, default: Settings.ITEM_TASK_TYPE.NotDefined
      t.string :state, null: false, default: Settings.ITEM_TASK_STATE.Created
      t.timestamp :completed_at

      t.timestamps
    end

    add_index :item_tasks, [:task_type], name: :idx_item_task_type, using: :btree
    add_index :item_tasks, [:processor, :state], name: :idx_item_task_processor_state, using: :btree
    add_index :item_tasks, [:created_at], name: :idx_item_task_created_at, using: :btree

    create_table :item_task_comments do |t|
      t.references :task, index: true
      t.string  :commenter, null: false, default: 'Unknown'
      t.text  :comment, null: false, default: ''

      t.timestamps
    end

    add_index :item_task_comments, [:created_at], name: :idx_item_task_comment_created, using: :btree

  end

end
