class CreateBackendUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :user_id, null: false
      t.integer :internal_status, null: false
      t.text :reason

      t.timestamps
    end

    add_index :users, :user_id
    add_index :users, :internal_status
  end
end
