class CreateBackendDisabledUsers < ActiveRecord::Migration
  def change
    create_table :disabled_users do |t|
      t.integer :user_id, null:false
      t.integer :admin_id, null:false
      t.text :reason, null:false

      t.timestamps
    end

    add_index :disabled_users, :user_id
    add_index :disabled_users, :admin_id
  end
end
