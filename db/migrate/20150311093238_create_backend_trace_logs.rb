class CreateBackendTraceLogs < ActiveRecord::Migration
  def change
    create_table :trace_logs do |t|
      t.integer :operator_id, null: false
      t.string :op_type, null: false, limit: 20
      t.string :op_target, null: false
      t.integer :op_business_id
      t.text :op_content, null: false
      t.datetime :created_at
    end

    add_index :trace_logs, :operator_id
    add_index :trace_logs, :op_type
    add_index :trace_logs, :op_target
    add_index :trace_logs, :op_business_id
  end
end
