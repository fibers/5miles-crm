class AddTypeRolesEmailStateToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :user_type, :integer
    add_index :admins, :user_type

    add_column :admins, :roles, :text

    add_column :admins, :email, :string

    add_column :admins, :state, :integer, default: 1
    add_index :admins, :state
  end
end
