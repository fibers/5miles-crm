class CreateBackendBkItems < ActiveRecord::Migration
  def change
    create_table :bk_items do |t|
      t.integer :item_id, null: false
      t.integer :review_state, null: false
      t.integer :prev_item_state
      t.integer :operator_id
      t.boolean :is_edited, default: false
      t.text    :reason
      t.text    :assign_reason

      t.timestamps
    end

    add_index :bk_items, :item_id, unique: true
    add_index :bk_items, :review_state
    add_index :bk_items, :operator_id
  end
end
