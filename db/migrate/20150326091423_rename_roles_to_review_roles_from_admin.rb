class RenameRolesToReviewRolesFromAdmin < ActiveRecord::Migration
  def change
    rename_column :admins, :roles, :review_roles
  end
end
