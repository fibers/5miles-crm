class AddUserIdItemTitleUpdateTimeModifiedAtToBkItem < ActiveRecord::Migration
  def change
    add_column :bk_items, :user_id, :integer

    add_column :bk_items, :item_title, :string

    add_column :bk_items, :item_first_image_path, :string

    add_column :bk_items, :update_time, :datetime

    add_column :bk_items, :modified_at, :datetime
  end
end
