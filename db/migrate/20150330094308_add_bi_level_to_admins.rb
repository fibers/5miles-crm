class AddBiLevelToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :bi_level, :integer, default: 0
  end
end
