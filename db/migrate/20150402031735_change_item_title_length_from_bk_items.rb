class ChangeItemTitleLengthFromBkItems < ActiveRecord::Migration
  def change
    change_column :bk_items, :item_title, :string, limit: 500
  end
end
