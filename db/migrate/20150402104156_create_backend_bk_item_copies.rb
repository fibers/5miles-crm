class CreateBackendBkItemCopies < ActiveRecord::Migration
  def change
    create_table :bk_item_copies do |t|
      t.integer :item_id, null: false
      t.integer :user_id, null: false
      t.text :content, null: false
      t.boolean :is_latest, default: false
      t.timestamps
    end

    add_index :bk_item_copies, :item_id
    add_index :bk_item_copies, :user_id
  end
end
