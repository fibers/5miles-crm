class CreateBackendBkOperatorSchedules < ActiveRecord::Migration
  def change
    create_table :bk_operator_schedules do |t|
      t.integer :operator_id, null: false
      t.integer :state, null: false
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false

      t.timestamps
    end
  end
end
