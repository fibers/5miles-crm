class CreateBackendBkUsers < ActiveRecord::Migration
  def change
    create_table :bk_users do |t|
      t.integer :user_id, null: false
      t.integer :audit_cnt, null: false, default:0
      t.integer :risk_level, null: false, default:0
      t.integer :risk_desc, null: false, default:''
      t.integer :disabled, null: false, default:0
      t.integer :ctr_cnt, null: false, default:0
      t.integer :like_cnt, null: false, default:0
      t.integer :offer_cnt, null: false, default:0
      t.timestamps
    end

    add_index :bk_users, :user_id
  end

end
