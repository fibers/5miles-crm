class AddItemStateToBkItems < ActiveRecord::Migration
  def change
    add_column :bk_items, :item_state, :integer
  end
end
