class RenameItemReviewsToBkItemAuditHistories < ActiveRecord::Migration
  def change
    rename_table :item_reviews, :bk_item_audit_histories
  end
end
