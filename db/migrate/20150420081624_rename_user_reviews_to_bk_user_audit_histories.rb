class RenameUserReviewsToBkUserAuditHistories < ActiveRecord::Migration
  def change
    rename_table :user_reviews, :bk_user_audit_histories
  end
end
