class CreateBackendBkUserReviewAuditHistories < ActiveRecord::Migration
  def change
    create_table :bk_user_review_audit_histories do |t|
      t.integer :review_id, null: false
      t.integer :admin_id, null: false
      t.string :admin, null:false
      t.integer :action_type, null: false
      t.text :reason, null: false

      t.timestamps
    end

    add_index :bk_user_review_audit_histories, :review_id
    add_index :bk_user_review_audit_histories, :created_at
  end
end
