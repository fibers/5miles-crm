class AddPrevReviewStateToBkItem < ActiveRecord::Migration
  def change
    add_column :bk_items, :prev_review_state, :integer
  end
end
