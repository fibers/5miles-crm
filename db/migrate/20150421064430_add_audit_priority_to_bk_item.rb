class AddAuditPriorityToBkItem < ActiveRecord::Migration
  def change
    add_column :bk_items, :audit_priority, :integer
  end
end
