class AddFieldsToBkUsers < ActiveRecord::Migration
  def change
    add_column :bk_users, :disapproved_cnt, :integer,default: 0
    add_column :bk_users, :approved_cnt, :integer,default: 0
    add_column :bk_users, :right_down_cnt, :integer,default: 0
    add_column :bk_users, :item_cnt, :integer,default: 0
    add_column :bk_users, :risk_description, :text
  end
end
