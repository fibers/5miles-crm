class CreateBackendBkUserReviewReportAuditHistories < ActiveRecord::Migration
  def change
    create_table :bk_user_review_report_audit_histories do |t|
      t.integer :review_report_id, null: false
      t.integer :admin_id, null: false
      t.string :admin, null: false
      t.text :reply_content

      t.timestamps
    end

    add_index :bk_user_review_report_audit_histories, :review_report_id, unique: true
  end
end
