class Change1FieldToBkItems < ActiveRecord::Migration
  def change
    change_column :bk_items, :audit_priority, :integer, default:0
  end
end
