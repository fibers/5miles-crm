class AddModifiedTimeBeforeAuditToBkItems < ActiveRecord::Migration
  def change
    add_column :bk_items, :modified_time_before_audit, :datetime
  end
end
