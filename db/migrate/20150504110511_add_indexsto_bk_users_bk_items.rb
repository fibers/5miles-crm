class AddIndexstoBkUsersBkItems < ActiveRecord::Migration
  def change

    add_index :bk_items, :audit_tag
    add_index :bk_items, :item_state
    add_index :bk_items, :modified_at
  end
end
