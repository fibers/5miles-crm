class AddFieldImgOpIdToBkItems < ActiveRecord::Migration
  def change
    add_column :bk_items, :img_op_id, :integer, null: false, default: 0
    add_index :bk_items, :img_op_id
  end
end
