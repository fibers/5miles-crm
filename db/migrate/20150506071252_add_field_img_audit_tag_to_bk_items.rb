class AddFieldImgAuditTagToBkItems < ActiveRecord::Migration
  def change
    add_column :bk_items, :img_audit_tag, :string, null: false, default: ''
    add_index :bk_items, :img_audit_tag
  end
end
