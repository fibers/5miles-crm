class AddUnlistTaskJidToBkItems < ActiveRecord::Migration
  def change
    add_column :bk_items, :unlist_task_jid, :string, default: ''
  end
end
