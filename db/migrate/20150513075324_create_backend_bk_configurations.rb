class CreateBackendBkConfigurations < ActiveRecord::Migration
  def change
    create_table :bk_configurations do |t|
      t.string :name, null: false
      t.text :content, null: false

      t.timestamps
    end

    add_index :bk_configurations, :name, unique: true
  end
end
