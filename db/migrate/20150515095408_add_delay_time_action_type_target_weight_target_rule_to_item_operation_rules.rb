class AddDelayTimeActionTypeTargetWeightTargetRuleToItemOperationRules < ActiveRecord::Migration
  def change
    add_column :item_operation_rules, :delay_time, :integer
    add_column :item_operation_rules, :action_type, :integer
    add_column :item_operation_rules, :target_weight, :integer
    add_column :item_operation_rules, :target_rule_id, :integer
  end
end
