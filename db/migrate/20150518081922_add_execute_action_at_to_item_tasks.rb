class AddExecuteActionAtToItemTasks < ActiveRecord::Migration
  def change
    add_column :item_tasks, :execute_action_at, :datetime
    add_column :item_tasks, :task_jid, :string, null: false, default: ''

    add_index :item_tasks, :execute_action_at
  end
end
