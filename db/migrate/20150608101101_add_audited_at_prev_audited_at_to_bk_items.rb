class AddAuditedAtPrevAuditedAtToBkItems < ActiveRecord::Migration
  def change
    add_column :bk_items, :audited_at, :datetime
    add_column :bk_items, :prev_audited_at, :datetime
  end
end
