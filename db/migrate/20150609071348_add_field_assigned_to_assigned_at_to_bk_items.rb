class AddFieldAssignedToAssignedAtToBkItems < ActiveRecord::Migration
  def change
    add_column :bk_items, :img_assigned_to, :int, default: 0
    add_column :bk_items, :img_assigned_at, :datetime
    add_column :bk_items, :assigned_to, :int, default: 0
    add_column :bk_items, :assigned_at, :datetime
  end
end
