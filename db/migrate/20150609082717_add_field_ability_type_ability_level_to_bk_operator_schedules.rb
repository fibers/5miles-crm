class AddFieldAbilityTypeAbilityLevelToBkOperatorSchedules < ActiveRecord::Migration
  def change
    add_column :bk_operator_schedules, :ability_type, :int, default: 0
    add_column :bk_operator_schedules, :ability_level, :float, default: 1
  end
end
