class AddIsKeepAuditStateToItemOperationRules < ActiveRecord::Migration
  def change
    add_column :item_operation_rules, :is_keep_audit_state, :boolean, default: false
  end
end
