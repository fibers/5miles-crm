class AddAssignmentIndexToBkItems < ActiveRecord::Migration
  def change
    add_index :bk_items, :img_assigned_to
    add_index :bk_items, :assigned_to
  end
end
