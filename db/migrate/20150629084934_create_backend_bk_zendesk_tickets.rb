class CreateBackendBkZendeskTickets < ActiveRecord::Migration
  def change
    create_table :bk_zendesk_tickets do |t|
      t.integer :tid, null: false
      t.string :via
      t.string :status, limit: 50
      t.string :subject, limit: 500
      t.string :priority, limit: 50
      t.string :current_tags
      t.string :ticket_type, limit: 50
      t.string :req_name
      t.string :req_email
      t.string :submitter_name
      t.string :group_name
      t.string :assignee_name
      t.datetime :assigned_at
      t.datetime :solved_at
      t.string :others, limit: 500

      t.timestamps
    end

    add_index :bk_zendesk_tickets, :tid, unique: true
    add_index :bk_zendesk_tickets, :via
    add_index :bk_zendesk_tickets, :status
    add_index :bk_zendesk_tickets, :req_email
  end
end
