class CreateBackendBkReviewReportZendeskTickets < ActiveRecord::Migration
  def change
    create_table :bk_review_report_zendesk_tickets do |t|
      t.integer :report_id, null: false
      t.integer :ticket_id, null: false

      t.timestamps
    end

    add_index :bk_review_report_zendesk_tickets, :report_id
    add_index :bk_review_report_zendesk_tickets, :ticket_id
  end
end
