class AddFieldShowTypeToItemOperationRules < ActiveRecord::Migration
  def change
    add_column :item_operation_rules, :show_type, :int, default: 0
  end
end
