class CreateBkItemRiskWords < ActiveRecord::Migration
  def change
    create_table :bk_item_risk_words do |t|
      t.integer :item_id, null: false
      t.string :title_risk_words, default: ''
      t.string :desc_risk_words, default: ''

      t.timestamps
    end
    add_index :bk_item_risk_words, :item_id
  end
end
