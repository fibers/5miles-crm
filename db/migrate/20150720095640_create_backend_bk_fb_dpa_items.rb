class CreateBackendBkFbDpaItems < ActiveRecord::Migration
  def change
    create_table :bk_fb_dpa_items do |t|
      t.integer :item_id, null:false
      t.string :city_name, null:false, default:'', limit:50
      t.string :fb_img_link, null:false, default:'', limit: 250
      t.string :google_product_category, default:'', limit:250
      t.integer :item_state, null: false, default: 0
      t.integer :fetch_state, null: false, default: 0
      t.string :remarks, default:'', limit: 250
      t.string :reserved_1, default:'', limit: 250
      t.string :reserved_2, default:'', limit: 250
      t.string :reserved_3, default:'', limit: 250

      t.timestamps
    end
    add_index :bk_fb_dpa_items, [:item_id], name: 'idx_item_id', unique: true
  end
end
