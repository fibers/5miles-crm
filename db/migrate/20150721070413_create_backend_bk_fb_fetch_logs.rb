class CreateBackendBkFbFetchLogs < ActiveRecord::Migration
  def change
    create_table :bk_fb_fetch_logs do |t|
      t.string :city_name, null:false, default:'', limit:50
      t.datetime :fetch_start_time
      t.datetime :fetch_end_time
      t.string :deal_status, default: '', limit: 32
      t.integer :fetch_count, default: 0
      t.string :fetch_result, default: '', limit: 15000
      t.string :remarks, default:'', limit: 250
      t.string :reserved_1, default:'', limit: 250
      t.string :reserved_2, default:'', limit: 250
      t.string :reserved_3, default:'', limit: 250
    end
    add_index :bk_fb_fetch_logs, [:city_name, :fetch_end_time], name: 'idx_city_name_fetch_end_time'
  end

end
