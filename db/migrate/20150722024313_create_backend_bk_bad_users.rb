class CreateBackendBkBadUsers < ActiveRecord::Migration
  def change
    create_table :bk_bad_users do |t|
      t.integer :user_id, null: false
      t.integer :total_count, default: 0, null: false
      t.integer :day_count, default: 0, null: false
      t.integer :status, null: false
      t.datetime :total_count_updated_at
      t.datetime :day_count_updated_at

      t.timestamps
    end

    add_index :bk_bad_users, :user_id, unique: true
    add_index :bk_bad_users, :status
  end
end
