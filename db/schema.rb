# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150722024313) do

  create_table "admins", force: true do |t|
    t.string   "username",                    null: false
    t.string   "password_digest",             null: false
    t.boolean  "is_super_admin",              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_type"
    t.text     "review_roles"
    t.string   "email"
    t.integer  "state",           default: 1
    t.integer  "bi_level",        default: 0
  end

  add_index "admins", ["state"], name: "index_admins_on_state", using: :btree
  add_index "admins", ["user_type"], name: "index_admins_on_user_type", using: :btree
  add_index "admins", ["username"], name: "idx_admin_username", unique: true, using: :btree

  create_table "bk_bad_users", force: true do |t|
    t.integer  "user_id",                            null: false
    t.integer  "total_count",            default: 0, null: false
    t.integer  "day_count",              default: 0, null: false
    t.integer  "status",                             null: false
    t.datetime "total_count_updated_at"
    t.datetime "day_count_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_bad_users", ["status"], name: "index_bk_bad_users_on_status", using: :btree
  add_index "bk_bad_users", ["user_id"], name: "index_bk_bad_users_on_user_id", unique: true, using: :btree

  create_table "bk_configurations", force: true do |t|
    t.string   "name",       null: false
    t.text     "content",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_configurations", ["name"], name: "index_bk_configurations_on_name", unique: true, using: :btree

  create_table "bk_fb_dpa_items", force: true do |t|
    t.integer  "item_id",                                          null: false
    t.string   "city_name",               limit: 50,  default: "", null: false
    t.string   "fb_img_link",             limit: 250, default: "", null: false
    t.string   "google_product_category", limit: 250, default: ""
    t.integer  "item_state",                          default: 0,  null: false
    t.integer  "fetch_state",                         default: 0,  null: false
    t.string   "remarks",                 limit: 250, default: ""
    t.string   "reserved_1",              limit: 250, default: ""
    t.string   "reserved_2",              limit: 250, default: ""
    t.string   "reserved_3",              limit: 250, default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_fb_dpa_items", ["item_id"], name: "idx_item_id", unique: true, using: :btree

  create_table "bk_fb_fetch_logs", force: true do |t|
    t.string   "city_name",        limit: 50,    default: "", null: false
    t.datetime "fetch_start_time"
    t.datetime "fetch_end_time"
    t.string   "deal_status",      limit: 32,    default: ""
    t.integer  "fetch_count",                    default: 0
    t.string   "fetch_result",     limit: 15000, default: ""
    t.string   "remarks",          limit: 250,   default: ""
    t.string   "reserved_1",       limit: 250,   default: ""
    t.string   "reserved_2",       limit: 250,   default: ""
    t.string   "reserved_3",       limit: 250,   default: ""
  end

  add_index "bk_fb_fetch_logs", ["city_name", "fetch_end_time"], name: "idx_city_name_fetch_end_time", using: :btree

  create_table "bk_item_audit_histories", force: true do |t|
    t.integer  "item_id"
    t.string   "admin",          null: false
    t.string   "action_type",    null: false
    t.text     "reason",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "action_type_id"
    t.integer  "prev_state"
  end

  add_index "bk_item_audit_histories", ["created_at"], name: "index_bk_item_audit_histories_on_created_at", using: :btree
  add_index "bk_item_audit_histories", ["item_id"], name: "index_bk_item_audit_histories_on_item_id", using: :btree

  create_table "bk_item_copies", force: true do |t|
    t.integer  "item_id",                    null: false
    t.integer  "user_id",                    null: false
    t.text     "content",                    null: false
    t.boolean  "is_latest",  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_item_copies", ["item_id"], name: "index_bk_item_copies_on_item_id", using: :btree
  add_index "bk_item_copies", ["user_id"], name: "index_bk_item_copies_on_user_id", using: :btree

  create_table "bk_item_report_zendesk_tickets", force: true do |t|
    t.integer  "report_id",  null: false
    t.integer  "ticket_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_item_report_zendesk_tickets", ["report_id"], name: "index_bk_item_report_zendesk_tickets_on_report_id", using: :btree
  add_index "bk_item_report_zendesk_tickets", ["ticket_id"], name: "index_bk_item_report_zendesk_tickets_on_ticket_id", using: :btree

  create_table "bk_item_risk_words", force: true do |t|
    t.integer  "item_id",                       null: false
    t.string   "title_risk_words", default: ""
    t.string   "desc_risk_words",  default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_item_risk_words", ["item_id"], name: "index_bk_item_risk_words_on_item_id", using: :btree

  create_table "bk_items", force: true do |t|
    t.integer  "item_id",                                                null: false
    t.integer  "review_state",                                           null: false
    t.integer  "prev_item_state"
    t.integer  "operator_id"
    t.boolean  "is_edited",                              default: false
    t.text     "reason"
    t.text     "assign_reason"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "item_title",                 limit: 500
    t.string   "item_first_image_path"
    t.datetime "update_time"
    t.datetime "modified_at"
    t.integer  "item_state"
    t.integer  "prev_review_state"
    t.integer  "audit_priority",                         default: 0
    t.string   "audit_tag",                              default: ""
    t.datetime "modified_time_before_audit"
    t.integer  "img_op_id",                              default: 0,     null: false
    t.string   "img_audit_tag",                          default: "",    null: false
    t.string   "unlist_task_jid",                        default: ""
    t.datetime "audited_at"
    t.datetime "prev_audited_at"
    t.integer  "img_assigned_to",                        default: 0
    t.datetime "img_assigned_at"
    t.integer  "assigned_to",                            default: 0
    t.datetime "assigned_at"
  end

  add_index "bk_items", ["assigned_to"], name: "index_bk_items_on_assigned_to", using: :btree
  add_index "bk_items", ["audit_tag"], name: "index_bk_items_on_audit_tag", using: :btree
  add_index "bk_items", ["img_assigned_to"], name: "index_bk_items_on_img_assigned_to", using: :btree
  add_index "bk_items", ["img_audit_tag"], name: "index_bk_items_on_img_audit_tag", using: :btree
  add_index "bk_items", ["img_op_id"], name: "index_bk_items_on_img_op_id", using: :btree
  add_index "bk_items", ["item_id"], name: "index_bk_items_on_item_id", unique: true, using: :btree
  add_index "bk_items", ["item_state"], name: "index_bk_items_on_item_state", using: :btree
  add_index "bk_items", ["modified_at"], name: "index_bk_items_on_modified_at", using: :btree
  add_index "bk_items", ["operator_id"], name: "index_bk_items_on_operator_id", using: :btree
  add_index "bk_items", ["review_state"], name: "index_bk_items_on_review_state", using: :btree

  create_table "bk_operator_schedules", force: true do |t|
    t.integer  "operator_id",                            null: false
    t.integer  "state",                                  null: false
    t.datetime "start_time",                             null: false
    t.datetime "end_time",                               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ability_type",             default: 0
    t.float    "ability_level", limit: 24, default: 1.0
  end

  create_table "bk_review_report_zendesk_tickets", force: true do |t|
    t.integer  "report_id",  null: false
    t.integer  "ticket_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_review_report_zendesk_tickets", ["report_id"], name: "index_bk_review_report_zendesk_tickets_on_report_id", using: :btree
  add_index "bk_review_report_zendesk_tickets", ["ticket_id"], name: "index_bk_review_report_zendesk_tickets_on_ticket_id", using: :btree

  create_table "bk_user_audit_histories", force: true do |t|
    t.integer  "user_id",     null: false
    t.integer  "admin_id",    null: false
    t.integer  "action_type", null: false
    t.text     "reason",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_user_audit_histories", ["admin_id"], name: "index_bk_user_audit_histories_on_admin_id", using: :btree
  add_index "bk_user_audit_histories", ["user_id"], name: "index_bk_user_audit_histories_on_user_id", using: :btree

  create_table "bk_user_feedback_zendesk_tickets", force: true do |t|
    t.integer  "feedback_id", null: false
    t.integer  "ticket_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_user_feedback_zendesk_tickets", ["feedback_id"], name: "index_bk_user_feedback_zendesk_tickets_on_feedback_id", using: :btree
  add_index "bk_user_feedback_zendesk_tickets", ["ticket_id"], name: "index_bk_user_feedback_zendesk_tickets_on_ticket_id", using: :btree

  create_table "bk_user_report_zendesk_tickets", force: true do |t|
    t.integer  "report_id",  null: false
    t.integer  "ticket_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_user_report_zendesk_tickets", ["report_id"], name: "index_bk_user_report_zendesk_tickets_on_report_id", using: :btree
  add_index "bk_user_report_zendesk_tickets", ["ticket_id"], name: "index_bk_user_report_zendesk_tickets_on_ticket_id", using: :btree

  create_table "bk_user_review_audit_histories", force: true do |t|
    t.integer  "review_id",   null: false
    t.integer  "admin_id",    null: false
    t.string   "admin",       null: false
    t.integer  "action_type", null: false
    t.text     "reason",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_user_review_audit_histories", ["created_at"], name: "index_bk_user_review_audit_histories_on_created_at", using: :btree
  add_index "bk_user_review_audit_histories", ["review_id"], name: "index_bk_user_review_audit_histories_on_review_id", using: :btree

  create_table "bk_user_review_report_audit_histories", force: true do |t|
    t.integer  "review_report_id", null: false
    t.integer  "admin_id",         null: false
    t.string   "admin",            null: false
    t.text     "reply_content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_user_review_report_audit_histories", ["review_report_id"], name: "index_bk_user_review_report_audit_histories_on_review_report_id", unique: true, using: :btree

  create_table "bk_users", force: true do |t|
    t.integer  "user_id",                      null: false
    t.integer  "audit_cnt",        default: 0, null: false
    t.integer  "risk_level",       default: 0, null: false
    t.integer  "disabled",         default: 0, null: false
    t.integer  "ctr_cnt",          default: 0, null: false
    t.integer  "like_cnt",         default: 0, null: false
    t.integer  "offer_cnt",        default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "disapproved_cnt",  default: 0
    t.integer  "approved_cnt",     default: 0
    t.integer  "right_down_cnt",   default: 0
    t.integer  "item_cnt",         default: 0
    t.text     "risk_description"
  end

  add_index "bk_users", ["user_id"], name: "index_bk_users_on_user_id", using: :btree

  create_table "bk_zendesk_tickets", force: true do |t|
    t.integer  "tid",                        null: false
    t.string   "via"
    t.string   "status",         limit: 50
    t.string   "subject",        limit: 500
    t.string   "priority",       limit: 50
    t.string   "current_tags"
    t.string   "ticket_type",    limit: 50
    t.string   "req_name"
    t.string   "req_email"
    t.string   "submitter_name"
    t.string   "group_name"
    t.string   "assignee_name"
    t.datetime "assigned_at"
    t.datetime "solved_at"
    t.string   "others",         limit: 500
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bk_zendesk_tickets", ["req_email"], name: "index_bk_zendesk_tickets_on_req_email", using: :btree
  add_index "bk_zendesk_tickets", ["status"], name: "index_bk_zendesk_tickets_on_status", using: :btree
  add_index "bk_zendesk_tickets", ["tid"], name: "index_bk_zendesk_tickets_on_tid", unique: true, using: :btree
  add_index "bk_zendesk_tickets", ["via"], name: "index_bk_zendesk_tickets_on_via", using: :btree

  create_table "disabled_users", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "admin_id",   null: false
    t.text     "reason",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "disabled_users", ["admin_id"], name: "index_disabled_users_on_admin_id", using: :btree
  add_index "disabled_users", ["user_id"], name: "index_disabled_users_on_user_id", using: :btree

  create_table "email_recommendations", force: true do |t|
    t.string   "to"
    t.integer  "gender"
    t.integer  "email_template_id"
    t.string   "product_ids"
    t.string   "remarks"
    t.integer  "user_total"
    t.integer  "error_user_total"
    t.text     "error_user_ids"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "item_operation_rules", force: true do |t|
    t.string  "name"
    t.boolean "status",                   default: true
    t.string  "action",                   default: "lower_weight"
    t.integer "lower_weight_value",       default: 0
    t.boolean "notify_user",              default: true
    t.string  "push_message"
    t.string  "email_title"
    t.text    "email_and_system_message"
    t.integer "delay_time"
    t.integer "action_type"
    t.integer "target_weight"
    t.integer "target_rule_id"
    t.boolean "is_keep_audit_state",      default: false
    t.integer "show_type",                default: 0
  end

  create_table "item_task_comments", force: true do |t|
    t.integer  "task_id"
    t.string   "commenter",  default: "Unknown", null: false
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "item_task_comments", ["created_at"], name: "idx_item_task_comment_created", using: :btree
  add_index "item_task_comments", ["task_id"], name: "index_item_task_comments_on_task_id", using: :btree

  create_table "item_tasks", force: true do |t|
    t.integer  "item_id"
    t.integer  "user_id"
    t.string   "processor",         default: "Unknown",     null: false
    t.integer  "rule_id"
    t.string   "task_type",         default: "Not Defined", null: false
    t.string   "state",             default: "Created",     null: false
    t.datetime "completed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "execute_action_at"
    t.string   "task_jid",          default: "",            null: false
    t.integer  "bk_item_copy_id"
  end

  add_index "item_tasks", ["created_at"], name: "idx_item_task_created_at", using: :btree
  add_index "item_tasks", ["execute_action_at"], name: "index_item_tasks_on_execute_action_at", using: :btree
  add_index "item_tasks", ["item_id"], name: "index_item_tasks_on_item_id", using: :btree
  add_index "item_tasks", ["processor", "state"], name: "idx_item_task_processor_state", using: :btree
  add_index "item_tasks", ["rule_id"], name: "index_item_tasks_on_rule_id", using: :btree
  add_index "item_tasks", ["task_type"], name: "idx_item_task_type", using: :btree
  add_index "item_tasks", ["user_id"], name: "index_item_tasks_on_user_id", using: :btree

  create_table "languages", force: true do |t|
    t.string   "iso_code",   limit: 2, null: false
    t.string   "title",                null: false
    t.boolean  "status",               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "languages", ["iso_code"], name: "idx_language_iso_code", unique: true, using: :btree

  create_table "logs", force: true do |t|
    t.string   "operator",   null: false
    t.string   "url",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "logs", ["operator"], name: "idx_log_operator", using: :btree

  create_table "positions", force: true do |t|
    t.string   "name",          null: false
    t.integer  "parent_id",     null: false
    t.integer  "level",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "display_order"
  end

  add_index "positions", ["display_order"], name: "index_positions_on_display_order", using: :btree
  add_index "positions", ["name"], name: "index_positions_on_name", using: :btree
  add_index "positions", ["parent_id"], name: "index_positions_on_parent_id", using: :btree

  create_table "trace_logs", force: true do |t|
    t.integer  "operator_id",               null: false
    t.string   "op_type",        limit: 20, null: false
    t.string   "op_target",                 null: false
    t.integer  "op_business_id"
    t.text     "op_content",                null: false
    t.datetime "created_at"
  end

  add_index "trace_logs", ["op_business_id"], name: "index_trace_logs_on_op_business_id", using: :btree
  add_index "trace_logs", ["op_target"], name: "index_trace_logs_on_op_target", using: :btree
  add_index "trace_logs", ["op_type"], name: "index_trace_logs_on_op_type", using: :btree
  add_index "trace_logs", ["operator_id"], name: "index_trace_logs_on_operator_id", using: :btree

  create_table "users", force: true do |t|
    t.integer  "user_id",         null: false
    t.integer  "internal_status", null: false
    t.text     "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["internal_status"], name: "index_users_on_internal_status", using: :btree
  add_index "users", ["user_id"], name: "index_users_on_user_id", using: :btree

end
