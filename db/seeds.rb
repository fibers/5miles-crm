# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

unless Admin.exists?(1)
  Admin.create!(username: 'root', password: '1', is_super_admin: true)
end

unless Language.exists?(1)
  Language.create!(iso_code: 'en', title: 'English', status: true)
end
