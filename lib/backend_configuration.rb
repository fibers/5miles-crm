module BackendConfiguration
  extend self

  attr_reader :key, :key_prefix
  attr_reader :redis_host, :redis_port, :redis_db

  @key = "bk_configuration"
  @key_prefix = @key

  @redis_host = Settings.Redis.Boss.Host
  @redis_port = Settings.Redis.Boss.Port
  @redis_db = Settings.Redis.Boss.DB

  Rails.logger.debug "class: #{self.name}"

  def get_configuration(name)
    BkConfiguration.find_by_name(name)
  end

  def update_configuration(name, content)
    ret = get_configuration(name)
    if ret
      ret = ret.update(content: content)
      if ret
        redis = Redis.new(host: @redis_host, port: @redis_port, db: @redis_db)
        redis.hdel(@key, name)
        Rails.logger.info "#{self.name}: update '#{name}' successfully"
      end
    end

    return ret
  end

  def update_white_list(name, content)
    ret = get_configuration(name)
    if ret
      # 将白名单列表首先更新至数据库
      ret = ret.update(content: content)
      if ret
        arr = content.split(%r{\n|\r\n})
        if arr.size > 0
          WhiteList.instance.update_white_list(arr)
          Rails.logger.info "#{self.name}: update '#{name}' successfully"
        end
      end
    end
  end
end
