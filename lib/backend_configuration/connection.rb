module BackendConfiguration
  module Connection
    extend self
    attr_reader :redis

    Rails.logger.debug "class: #{self.name}"
    @redis = Redis.new(host: BackendConfiguration.redis_host, port: BackendConfiguration.redis_port, db: BackendConfiguration.redis_db)
  end
end
