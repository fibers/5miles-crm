require 'singleton'

require 'backend_configuration/connection'

module BackendConfiguration
  class ItemRiskKeywords
    include Singleton

    Rails.logger.debug "class: #{self.name}"

    def initialize()
      @name = Settings.BackendConfigurations.ItemRiskKeywords
      @redis = BackendConfiguration::Connection.redis
    end

    def get_content
      content = @redis.hget(BackendConfiguration.key, @name)
      if content.nil?
        ret = BackendConfiguration.get_configuration(@name)
        if ret
          @redis.hset(BackendConfiguration.key, @name, ret.content)
          content = ret.content
        end
      end
      return content
    end

    def get_re_pattern
      re_pattern = nil
      content = get_content()
      if content.present?
        re_pattern = create_re_pattern(content)
      end
      return re_pattern
    end

    private

    def create_re_pattern(content)
      keywords = content.split(%r{\n|\r\n}).join("|")
      sub_re_pattern = %r{#{keywords}}i
      re_pattern = %r{\b#{sub_re_pattern}\b}
      return re_pattern
    end
  end
end
