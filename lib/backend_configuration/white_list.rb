module BackendConfiguration
  class WhiteList
    include Singleton
    Rails.logger.debug "class: #{self.name}"

    def initialize()
      @key = "bk:#{Settings.BackendConfigurations.WhiteList}"
    end

    # 更新集合的@key内容
    def update_white_list(prams = [])
      $global_redis.multi do |multi|
        multi.del(@key)
        multi.sadd(@key, prams)
      end
    end

    #判断id是否是集合@key的成员
    def is_white_list_member?(id)
      $global_redis.sismember(@key, id.to_s)
    end
  end
end
