require 'singleton'

class AgentManager
  include Singleton

  def initialize()
    puts "AgentManager init"
  end

  # 签到，给用户排班表添加一条记录
  def checkin(operator_id)
    ck = get_agent_info(operator_id)
    puts ck
    # 用户已存在并且已经签到
    if ck.present? and checked_in?(ck)
      Rails.logger.info "Operator=#{operator_id} has already checked_in，no need to check in again！"
      return
    end

    # 用户存在未签到
    if ck.present? and not checked_in?(ck)
      # 更新用户签到状态
      BkOperatorSchedule.where(operator_id: operator_id)
          .where(state: 0).update_all("state = 1, start_time = now(), end_time = now()")
    else # 第一次check in，保存用户信息
      ck = BkOperatorSchedule.new
      ck.operator_id = operator_id
      ck.start_time = Time.current
      ck.end_time = Time.current
      ck.state = 1
      # 默认BOTH
      ck.ability_type = 2
      # 审核速度分级，默认1
      ck.ability_level = 1
      ck.save
    end
    # notify the auto_dispatch_service that the operator has checked_in
    # AutoDispatchService::DispatchCtrlCenter.instance.deal_dispatch(:check_in, operator_id: operator_id)
    AutoDispatchWorker.perform_async({type: "check_in", operator_id: operator_id}.to_json)
    # save the operator's info time
    $global_redis.hset("agent:#{operator_id}", "last_dispatch_time", Time.now.utc.to_s)
    Rails.logger.info "Operator=#{operator_id} checked in successfully！"
    AuditLog.instance.audit_check_in_log(operator_id: operator_id)
  end

  # 签退，修改用户排班表对应的数据
  def checkout(operator_id)
    process_cnt = BkOperatorSchedule.where(operator_id: operator_id)
                      .where(state: 1).update_all("state = 0, end_time = now()")

    if process_cnt > 0
      Rails.logger.info "Operator=#{operator_id} checked out successfully！"
      AuditLog.instance.audit_check_out_log(operator_id: operator_id)
      # rm the operator's info
      $global_redis.del("agent:#{operator_id}")
    else
      # 没有签到
      Rails.logger.info "Not checked in, no need to check out."
    end

  end

  # 获取用户的信息
  def get_agent_info(operator_id)
    BkOperatorSchedule.find_by(operator_id: operator_id)
  end


  # 获得当前签入,有未处理数据的agent
  def get_agents(operator_id)

    agents_data = self.get_stats(operator_id)
    puts "operator_id=#{operator_id}"
    puts "agents_data=#{agents_data.inspect}"

    # cks = BkOperatorSchedule.where(state: 1)
    #
    # for it in cks
    #   if agents_data[operator_id]
    #     puts "online=#{it.operator_id}"
    #     agents_data[it.operator_id]['online'] = true
    #   else
    #     agents_data[it.operator_id] = {"item_cnt"=>0,"id"=>it.operator_id,"username"=>'-'}
    #
    #     if operator_id==it.operator_id
    #       is_my = true
    #       agents_data[it.operator_id]['is_my'] = is_my
    #     end
    #   end
    # end
    Rails.logger.info agents_data.to_json
    return agents_data
  end

  # 获得所有agent的统计信息
  def get_stats(operator_id)
    dict_group={}
    Admin.where(id: dict_group.keys).select(:id, :username).find_each do |operator|
      dict_group[operator.id]['username'] = operator.username
    end
    return dict_group

  end


  # 已经checkin操作员的信息
  def get_checkin_agents()
    agents = BkOperatorSchedule.where(state: 1)
  end

  def get_checkin_agent_ids()

    agents = BkOperatorSchedule.where(state: 1)
    puts "get_checkin_agents=#{agents.inspect}"

    ids = []
    agents.each do |it|
      ids << it.operator_id
    end
    return ids
  end

  def get_agent_ids()
    agents = BkOperatorSchedule.all()
    puts "get_checkin_agents=#{agents.inspect}"

    ids = {}
    agents.each do |it|
      ids << it.operator_id
    end
    return ids
  end

  def checked_in?(agent)
    if agent.nil?
      return false
    else
      agent.state == 1
    end
  end
end