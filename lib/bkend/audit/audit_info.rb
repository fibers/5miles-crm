require 'singleton'
require 'audit/auto_audit_service'

class AuditInfo
  include Singleton

  def initialize()
    puts "AuditInfo init"
  end

  def save_audit_info(item_id, item, operator_id='0')

    bk_item = BkItem.find_by_item_id(item_id)

    # Rails.logger.debug "save_audit_info bk_item: #{bk_item.to_json}"
    # Rails.logger.debug "save_audit_info item: #{item.to_json}"
    self.create_item_snapshot(item)

    if bk_item.blank?
      bk_item = process_new_item(item_id, item, operator_id)

      # 累计商品数量
      UserStats.instance.saveUserItemCnt(bk_item.user_id)
    else
      bk_item = process_modified_item(item_id, item, bk_item, operator_id)
    end

    return bk_item

  end

  def create_item_snapshot(item)
    Rails.logger.info "create_item_snapshot: #{item.id}"

    item_copy = item.generate_copy.to_json
    BkItemCopy.create(item_id: item.id, user_id: item.user_id, content: item_copy)
  end

  private
  def process_new_item(item_id, item, operator_id)
    Rails.logger.info "process_new_item: #{item_id}"

    #item = EntityItem.find(data['item_id'])
    #selected_operator = get_operator()

    # 创建后台审核商品记录
    bk_item = BkItem.new
    bk_item.item_id = item.id
    bk_item.review_state = item.internal_status
    bk_item.prev_item_state = nil
    bk_item.item_state = item.state
    bk_item.operator_id = operator_id
    # bk_item.is_edited defaults false
    # bk_item.reason defaults null
    # bk_item.assign_reason defaults null
    bk_item.created_at = item.created_at
    bk_item.user_id = item.user_id
    bk_item.item_title = item.title
    bk_item.item_first_image_path = item.images.order(is_first: :desc)[0].image_path
    bk_item.update_time = item.update_time
    bk_item.modified_at = item.modified_at
    bk_item.modified_time_before_audit = item.modified_at
    bk_item.save!

    bk_item
  end

  private
  def process_modified_item(item_id, item, bk_item, operator_id)
    Rails.logger.info "process_modified_item: #{item_id}"
    #item = EntityItem.find(data['item_id'])

    is_edit_after_review = false
    if bk_item.update_time.blank?
      is_edit_after_review = false
    else
      if item.modified_at
        is_edit_after_review = item.modified_at > bk_item.update_time
      end
    end
    prev_bk_item_review_state = bk_item.review_state
    bk_item.review_state = item.internal_status
    bk_item.prev_review_state = prev_bk_item_review_state

    bk_item.prev_item_state = bk_item.item_state
    bk_item.item_state = item.state
    bk_item.is_edited = is_edit_after_review
    bk_item.item_title = item.title
    bk_item.item_first_image_path = item.images.order(is_first: :desc)[0].image_path
    bk_item.modified_at = item.modified_at
    if bk_item.prev_review_state != Settings.PRODUCT_INTERNAL_STATUSES.PendingReview
      bk_item.modified_time_before_audit = item.modified_at
    end

    bk_item.operator_id = 0
    bk_item.img_op_id = 0
    bk_item.img_audit_tag = ''
    bk_item.audit_tag = ''
    bk_item.img_assigned_to = 0
    bk_item.assigned_to = 0
    bk_item.save!

    bk_item

  end


end

