require 'singleton'
require 'logger'

class AuditLog
  include Singleton

  def initialize()
    Rails.logger.info "#{self.class} initialize"
    @audit_logger = Logger.new(Rails.root.join('log', 'audit.log'))
    @audit_logger.formatter = proc do |severity, datetime, progname, msg|
      "#{msg}\n"
    end
  end

  def add_operator_information(options={})
    options[:operator_id] = EmployeeManager.instance.current_employee_id
    options[:operator_name] = EmployeeManager.instance.current_employee_name

    return options
  end

  def add_bk_item_information(bk_item, options={})
    if bk_item.present?
      options[:item_modified_time_before_audit] = bk_item.modified_time_before_audit if options[:item_modified_time_before_audit].blank?
      options[:audited_at] = bk_item.audited_at if options[:audited_at].blank?
      options[:prev_audited_at] = bk_item.prev_audited_at if options[:prev_audited_at].blank?
    end

    return options
  end

  # params: operator_id, operator_name, operate_type, item_task_id, item_id
  def audit_item_log(*args)
    options = args.extract_options!
    options.symbolize_keys!

    if options[:operator_id].blank? or options[:operator_name].blank?
      options = add_operator_information(options)
    end

    if options[:item_modified_time_before_audit].blank? or options[:audited_at].blank? or options[:prev_audited_at].blank?
      bk_item = BkItem.select(:modified_time_before_audit, :audited_at, :prev_audited_at).find_by_item_id(options[:item_id])
      options = add_bk_item_information(bk_item, options)
    end

    if options[:item_modified_time_before_audit].present?
      options[:item_modified_time_before_audit] = options[:item_modified_time_before_audit].utc.iso8601
    end
    if options[:audited_at].present?
      options[:audited_at] = options[:audited_at].utc.iso8601
    end
    if options[:prev_audited_at].present?
      options[:prev_audited_at] = options[:prev_audited_at].utc.iso8601
    end

    print_audit_log(options)
  end

  def audit_check_in_log(options={})
    if options[:operator_id].blank? or options[:operator_name].blank?
      options = add_operator_information(options)
    end
    current_time = Time.current
    $global_redis.set("bk:checkin_at:#{options[:operator_id]}", current_time.to_i)
    options[:checkin_at] = current_time.utc.iso8601
    checkout_at = $global_redis.get("bk:checkout_at:#{options[:operator_id]}")
    options[:checkout_at] = checkout_at.nil? ? nil : Time.at(checkout_at.to_i).utc.iso8601
    print_audit_log(options)
  end

  def audit_check_out_log(options={})
    if options[:operator_id].blank? or options[:operator_name].blank?
      options = add_operator_information(options)
    end
    current_time = Time.current
    $global_redis.set("bk:checkout_at:#{options[:operator_id]}", current_time.to_i)
    options[:checkout_at] = current_time.utc.iso8601
    checkin_at = $global_redis.get("bk:checkin_at:#{options[:operator_id]}")
    options[:checkin_at] = checkin_at.nil? ? nil : Time.at(checkin_at.to_i).utc.iso8601
    print_audit_log(options)
  end

  private
  def print_audit_log(options={})
    if options[:operator_id].blank? or options[:operator_name].blank?
      options = add_operator_information(options)
    end
    
    @audit_logger.info "\
#{Time.now.utc.iso8601}\
|\
#{options[:action_type]}\
|\
#{options[:item_id]}\
|\
#{options[:operator_id]}\
|\
#{options[:operator_name]}\
|\
#{options[:audited_at]}\
|\
#{options[:prev_audited_at]}\
|\
#{options[:item_modified_time_before_audit]}\
|\
#{options[:item_task_id]}\
|\
#{options[:item_task_rule_id]}\
|\
#{options[:item_task_state]}\
|\
#{options[:checkin_at]}\
|\
#{options[:checkout_at]}\
"
  end
end
