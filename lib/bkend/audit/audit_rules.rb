require 'singleton'

class AuditRules
  include Singleton

  def initialize()
    puts "AuditRules init"
  end


  def white_list(item_id ,item ,bk_item)
    is_include = BackendConfiguration::WhiteList.instance.is_white_list_member?(item.user_id.to_s)
    if is_include
      is_success_flag, message = Utils::ItemOps.approve(
          item_id, EmployeeManager.instance.current_employee_name, "in white list, auto approve.", false, true)
      Rails.logger.info "message=#{message}"
    end
    return is_include
  end

  def last_task(item_id ,item ,bk_item)
    return ItemTask.is_has_opened_task(item_id)
  end

  def repeat_search(item_id ,item ,bk_item)

    # 检查当前商品与其他商品的title， description的相识程度


  end

  def check_contact_info(item_id)
    item_data = Utils::BkOps.filter_contact_info(item_id)
    is_include_contact_info = item_data.present?
    if is_include_contact_info
      begin
        comment_text = <<-EOF
ContactInfo:  #{item_data['contact_info_list'].inspect}
        EOF
        if item_data['title'].present?
          comment_text = <<-EOF
#{comment_text.chomp}
Title with contact info: #{item_data['title']}
          EOF
        end
        if item_data['desc'].present?
          comment_text = <<-EOF
#{comment_text.chomp}
Desc with contact info:  #{item_data['desc']}
          EOF
        end

        item_operation_rule_name = "SYSTEM_Contact Info. Filtering"
        TaskManager.instance.quick_task_exec(
            item_operation_rule_name, item_id, nil, comment_text)

      end while false
    end
    return is_include_contact_info
  end

  def credit_approve(item, bk_item)

    is_auto_approve, is_credit_task, priority, check_num ,dis_check_num=
        UserStats.instance.isAutoApprove(item.user_id)
    if is_auto_approve
      #
      #价格<=1.00时必须人工审核
      if item.price > 1.00
        is_success_flag, message = Utils::ItemOps.approve(
            item.id, EmployeeManager.instance.current_employee_name, "User approve count >=#{check_num} , auto approve.", false, true)

        Rails.logger.info "credit_approve!"
        bk_item.audit_tag = Settings.AUDIT_TAG.ByCredit
        bk_item.save
      end

    elsif is_credit_task
      operator_id = EmployeeManager.instance.current_employee_id
      rule_type = "Bad-Credit-User"
      TaskManager.instance.quick_task_exec(rule_type, item.id, operator_id, "Bad Credit User.")

      Rails.logger.info "credit_task!"
      bk_item.audit_tag = Settings.AUDIT_TAG.ByBadCreditUser
      bk_item.save
    end

    return is_auto_approve, is_credit_task, priority
  end


  def modify_check(item, bk_item)
    audit_workflow_ctrl = Settings.AUDIT_WORKFLOW_CTRL.Continue

    prev_bk_item_review_state = bk_item.prev_review_state

    limit2 = BkItemCopy.get_limit2(item.id)
    if limit2[0].content!= limit2[1].content
      item_copy = limit2[0].content
      latest_item_copy = limit2[1].content
      if prev_bk_item_review_state == Settings.PRODUCT_INTERNAL_STATUSES.Approve
        item_copy_json = JSON.load(item_copy)
        begin
          unless item_copy_json['price'].to_i > 1
            break
          end
          latest_item_copy_json = JSON.load(latest_item_copy)
          unless latest_item_copy_json['images'][0] == item_copy_json['images'][0]
            break
          end
          ret = Utils::BkOps.similarity_check(latest_item_copy_json['title'], item_copy_json['title'], 0.5)
          unless ret[0]
            break
          end
          ret = Utils::BkOps.similarity_check(latest_item_copy_json['desc'], item_copy_json['desc'], 0.5)
          unless ret[0]
            break
          end
          # ret = Utils::BkOps.filter_contact_info(item.id)
          # if ret.present?
          #   break
          # end

          Rails.logger.info "Modified small part, auto approve item(id: #{item.id})"
          is_success_flag, message = Utils::ItemOps.approve(item.id, EmployeeManager.instance.current_employee_name, "Modified small part, auto approve.", false ,true)

          audit_workflow_ctrl = Settings.AUDIT_WORKFLOW_CTRL.Break
        end while false
      end
    else
      # 用户虽然编辑了，但是没有修改快照关心的内容
      if prev_bk_item_review_state == Settings.PRODUCT_INTERNAL_STATUSES.Approve

          Rails.logger.info "User edited without modify, auto approve item(id: #{item.id})"
          is_success_flag, message = Utils::ItemOps.approve(item.id, EmployeeManager.instance.current_employee_name, "User edited without modify, auto approve.", false , true)

          audit_workflow_ctrl = Settings.AUDIT_WORKFLOW_CTRL.Break

      end
    end

    return audit_workflow_ctrl
  end
end
