require 'singleton'

module AutoAuditService


  class WorkFlowBreakException < StandardError
    attr_reader :reason
    def initialize(reason)
      @reason = reason
    end
  end
  class WorkFlowContinueException < StandardError
    attr_reader :reason
    def initialize(reason)
      @reason = reason
    end
  end

  class Manager
    include Singleton

    @@rules = []

    def initialize()
      puts "AutoAuditService init"

      @@rules.push(WhiteListRule.new)
      @@rules.push(RiskKeywordsRule.new)
      @@rules.push(LastTaskRule.new)
      @@rules.push(PhoneNumberRule.new)
      @@rules.push(ModifyRule.new)
      @@rules.push(RepeatSearchRule.new)

      @@rules.push(CreditApproveRule.new)

      puts "AutoAuditManager init ok"
    end

    def exec_audit(data, options={})

      item_id = data['item_id']
      operator_id = options[:operator_id]
      item = options[:item]
      bk_item = options[:bk_item]

      is_break = false

      # 先基于商品审核规则检查，后基于用户审核规则检查
      for rule in @@rules
        begin
          #logger.info "-----------------------------------"
          Rails.logger.info "rule exec start: #{rule}-#{item_id}"
          res = rule.exec(data, item ,bk_item)
          Rails.logger.info "rule exec ok: #{item_id}-#{res.inspect}"
          if res and Settings.AUDIT_WORKFLOW_CTRL.Break==res.wf_ctrl
            is_break = true
            break
          end
        rescue WorkFlowBreakException => e
          Rails.logger.info "rule exec break: #{item_id}"
          Rails.logger.warn "WorkFlow Break:#{$!} at:#{e}"
          is_break = true
          break
        rescue WorkFlowContinueException => e
          Rails.logger.info "rule exec break: #{item_id}"
          Rails.logger.warn "WorkFlow Break:#{$!} at:#{e}"
        rescue Exception => e
          Rails.logger.warn "WorkFlow error:#{e}"
          Rails.logger.error e.message
          Rails.logger.error e.backtrace.inspect
        ensure
          Rails.logger.info "rule exec end: #{item_id}"
        end
      end
      #Rails.logger.info "====================================="

      # 如果没有被自动审核规则拦截，需要分配处理人进行人工处理
      if is_break==false
        save_assign_info(data, item, bk_item)
      end

    end


    def save_assign_info(data, item, bk_item)

      #selected_operator = Utils::BkOps.assign_operator()
      #operator_id = selected_operator.id if selected_operator.present?
      #bk_item.operator_id = operator_id

    end

  end
end


# wf_ctrl:   continue,break
class AutoAuditResult
  attr_reader :wf_ctrl
  def initialize(wf_ctrl, opt_user='', opt_msg='', is_notify_user=false)
    @wf_ctrl = wf_ctrl
  end
end


class AutoAuditRule
  def exec(data, item,bk_item)
    return exec_item(data,item,bk_item)
  end
end



class WhiteListRule<AutoAuditRule
  def exec_item(data, item, bk_item)
    item_id = item['id']
    is_include = AuditRules.instance.white_list(item_id ,item ,bk_item)
    if is_include
      return AutoAuditResult.new(Settings.AUDIT_WORKFLOW_CTRL.Break)
    end
  end
end

class LastTaskRule<AutoAuditRule
  def exec_item(data, item, bk_item)
    item_id = item['id']
    has_task = AuditRules.instance.last_task(item_id ,item ,bk_item)
    if has_task
      raise AutoAuditService::WorkFlowBreakException.new("LastTaskRule has task:#{item_id}")
      #return AutoAuditResult.new(Settings.AUDIT_WORKFLOW_CTRL.Break)
    end
  end
end

class RepeatSearchRule<AutoAuditRule
  def exec_item(data, item, bk_item)
    item_id = item['id']
    has_task = AuditRules.instance.repeat_search(item_id ,item ,bk_item)
    if has_task
      return AutoAuditResult.new(Settings.AUDIT_WORKFLOW_CTRL.Break)
    end
  end
end

#促发检查是否含有电话号码的work
class PhoneNumberRule<AutoAuditRule
  def exec_item(data, item, bk_item)
    item_id = item['id']
    is_include_contact_info = AuditRules.instance.check_contact_info(item_id)
    if is_include_contact_info
      return AutoAuditResult.new(Settings.AUDIT_WORKFLOW_CTRL.Break)
    end
  end
end

#编辑数据，但没有修改重要内容
class ModifyRule<AutoAuditRule
  def exec_item(data, item,bk_item)
    wf_ctrl = AuditRules.instance.modify_check(item, bk_item)
    if wf_ctrl==Settings.AUDIT_WORKFLOW_CTRL.Break
      bk_item.audit_tag = Settings.AUDIT_TAG.ByModify
      bk_item.save
    end
    return AutoAuditResult.new(wf_ctrl)
  end
end

# 高危关键字审核
class RiskKeywordsRule<AutoAuditRule
  def exec_item(data, item,bk_item)
    is_include, message = Utils::BkOps.process_include_keywords_item(item)
    if is_include
      Rails.logger.info "RiskKeywordsRule=#{message}"
      return AutoAuditResult.new(Settings.AUDIT_WORKFLOW_CTRL.Break)
    end
    return AutoAuditResult.new(Settings.AUDIT_WORKFLOW_CTRL.Continue)
  end
end

# 连续审核通过的用户放行
class CreditApproveRule<AutoAuditRule
  def exec_item(data, item, bk_item)
    audit_workflow_ctrl = Settings.AUDIT_WORKFLOW_CTRL.Continue
    begin
      is_approve, is_disapprove, priority = AuditRules.instance.credit_approve(item, bk_item)
      if is_approve
        bk_item.audit_tag = Settings.AUDIT_TAG.ByCredit
        audit_workflow_ctrl = Settings.AUDIT_WORKFLOW_CTRL.Break
      end
      if is_disapprove
        bk_item.audit_tag = Settings.AUDIT_TAG.ByCreditDisapprove
        audit_workflow_ctrl = Settings.AUDIT_WORKFLOW_CTRL.Break
        break
      end
    end while false
    bk_item.audit_priority = priority
    bk_item.save!
    return AutoAuditResult.new(audit_workflow_ctrl)
  end
end
