require 'singleton'

class AutoDistributorService
  include Singleton

  def initialize()
    puts "AutoDistributorService init"
  end


  def distribute()

    # 查询待分配数据

    # 查询当前坐席

    # 根据规则计算分配

  end


  def fetch_items(fetch_size, operator_id)
    res = nil
    puts "operator_id=#{operator_id}"
    if operator_id
      res = BkItem.where(operator_id: 0)
                .where(img_audit_tag: Settings.AUDIT_TAG_IMG.ImgOk)
                .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                .order(audit_priority: :asc , modified_at: :desc)
                .limit(fetch_size)
                .update_all("operator_id=#{operator_id}")
    end
    puts "fetch_items=#{res}"
    res
  end

  def fetch_img_items(operator_id ,fetch_size)
    res = nil
    puts "operator_id=#{operator_id}"
    if operator_id
      res = BkItem.where(img_op_id: 0)
                .where(operator_id: 0)
                .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                .order(audit_priority: :asc , modified_at: :desc)
                .limit(fetch_size)
                .update_all("img_op_id=#{operator_id}")
    end
    return res
  end

  def bk_item_state(bk_item_obj)
    bk_item_obj.where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
         .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
    return bk_item_obj
  end

  # 指定某个操作员的信息
  def get_agent_info(operator_id)

    bk_items_group  = self.bk_item_state(BkItem).select("img_op_id, count(id) as item_cnt")
                          .where.not(audit_assign_type: '')
                          .where(operator_id: operator_id)

    puts "bk_items_group2=#{bk_items_group.inspect}"

    ids = {}
    bk_items_group.each do |it|
      ids << it.img_op_id
    end
    
    return EmployeeManager.instance.merge_agent_info(ids)
  end

  def get_assigned_info(operator_id)

    bk_items_group  = BkItem.select("operator_id, count(id) as item_cnt")
                          .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                          .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                          .where.not(operator_id: 0)
                          .group(:operator_id)
    #puts bk_items_group.to_json

    empls = EmployeeManager.instance.get_employees()
    #puts empls.inspect

    dict_group = {}
    bk_items_group.each do |it|
      #puts it.inspect

      op_id = it.operator_id

      self.build_assigned_info(it, operator_id, op_id , empls ,dict_group)
    end

    #puts dict_group.inspect

    return dict_group
  end

  def build_assigned_info(it, my_id,op_id, empls ,dict_group)

    #puts it.inspect

    is_my = false
    if op_id == nil
      op_id='-'
    elsif my_id==op_id
      is_my = true
    end

    dict_group[op_id] = {"item_cnt" => it.item_cnt, "id" => op_id}
    dict_group[op_id]['username'] = empls[op_id] ? empls[op_id]['empl_name'] : "-#{op_id}-"
    dict_group[op_id]['is_my'] = is_my
  end

end