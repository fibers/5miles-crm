module Audit
  module ManualAuditService
    class << self
      # 通用审核流程
      def approve(item_id, options={})
        is_success_flag, message = false, ""
        options.symbolize_keys!
        if options[:item_task_id].blank?
          is_success_flag, message = item_approve(item_id, options)
        else
          is_success_flag, message = item_task_approve_complete(item_id, options)
        end
        return [is_success_flag, message]
      end

      # 商品审核通过
      def item_approve(item_id, options={})
        is_success_flag, message = false, ""

        begin
          if item_id.nil?
            is_success_flag = false
            message = "The item id can not be empty!"
            break
          end
          is_success_flag, message = Utils::ItemOps.approve(item_id,
                                                            options[:operator_name],
                                                            reason="OK",
                                                            is_notify_user=true,
                                                            is_auto_approve=false)
          unless is_success_flag
            # item approve 失败
            break
          end
          unless options[:is_change_weight]
            # 不需要修改商品权重
            break
          end
          product_ids = []
          product_ids << item_id.to_i
          weight = options[:weight].to_i
          ret = BackendApiREST.batch_update_item_weight(product_ids, weight)
          puts ret.inspect
          result = JSON.load(ret[1])
          # Update API's item weiht NOT OK
          unless result["meta"]["result"] == "ok"
            is_success_flag = false
            message = "Approve the product successfully, but failed to update weight to #{weight}."
            break
          end
          is_success_flag = true
          message = "Approve the product successfully and update weight to #{weight}."
        end while false

        return [is_success_flag, message]
      end

      # Approve item with risk tag
      def item_approve_risk(item_id, options={})
        is_success_flag, message = false, "Not finished yet!"
        return [is_success_flag, message]
      end

      # Approve the item task and finish it
      def item_task_approve_complete(item_id, options={})
        is_success_flag, message = true, ""

        if options[:item_task_id].nil?
          is_success_flag = false
          message = "The task id can not be empty!"
        end

        if is_success_flag
          is_success_flag = false
          task = ItemTask.includes(:item, :rule).find(options[:item_task_id])
          options[:item_operation_rule] = task.rule
          options[:is_execute]= true # Execute task flag
          options[:is_ok]= true # Approve flag
          is_success_flag, message = Utils::BkItemTaskOps.complete(task,
                                                                   options[:operator_name],
                                                                   options)
        end

        return [is_success_flag, message]
      end

      # 修改task rule并完成
      def item_task_change_rule_execute(item_task_id, rule_name, options={})
        is_success_flag, message = true, ""

        begin
          if rule_name.blank?
            is_success_flag = false
            message = "The rule name can not be empty!"
            break
          end

          new_rule = ItemOperationRule.find_by_name(rule_name)
          if new_rule.blank?
            is_success_flag = false
            message = "The rule(name: #{rule_name} dose not exist."
            break
          end
          item_task = ItemTask.includes(:item, :rule).find(item_task_id)
          options[:item_operation_rule] = new_rule
          options[:is_execute]= true # Execute task flag

          is_success_flag, message = Utils::BkItemTaskOps.complete(item_task,
                                                                   options[:operator_name],
                                                                   options)
        end while false

        return [is_success_flag, message]
      end

      # Only create a task
      def item_task_create(item_id, rule_name, options={})
        is_success_flag, message, task = true, "", nil

        if rule_name.nil?
          is_success_flag = false
          message = "The rule name can not be empty!"
        end

        if is_success_flag
          is_success_flag = false
          rule = ItemOperationRule.find_by_name(rule_name)
          is_success_flag, message, task = Utils::BkItemTaskOps.create(rule,
                                                                       item_id,
                                                                       options[:operator_name])
        end

        return [is_success_flag, message, task]
      end

      # Execute the specified task with task_id
      def item_task_execute(item_task_id, options={})
        is_success_flag, message = true, ""

        if item_task_id.nil?
          is_success_flag = false
          message = "The item task id can not be empty!"
        end

        if is_success_flag
          is_success_flag = false
          task = ItemTask.includes(:item, :rule).find(item_task_id)
          # fill the message and email info with DB default config when not specified
          push_message = options[:push_message].nil? ? nil : task.rule.push_message
          email_title = options[:email_title].nil? ? nil : task.rule.email_title
          email_and_system_message = options[:email_and_system_message].nil? ? nil : task.rule.email_and_system_message
          is_success_flag, message = Utils::BkItemTaskOps.execute_action(task,
                                                                         options[:operator_name],
                                                                         push_message,
                                                                         email_title,
                                                                         email_and_system_message)
        end

        return [is_success_flag, message]
      end
    end
  end
end