require 'singleton'

class SmartviewManager
  include Singleton

  def initialize()
    puts "SmartviewManager init"
  end

  def get_check_cnt()
    return BkItem.where(audit_tag: [Settings.AUDIT_TAG.ByAuto,Settings.AUDIT_TAG.ByModify,
                                    Settings.AUDIT_TAG.ByCredit,Settings.AUDIT_TAG.ByContactInfo]).count(:id)
  end

  def get_today_check_cnt()
    return BkItem.where(audit_tag: [Settings.AUDIT_TAG.ByAuto,Settings.AUDIT_TAG.ByModify,
                                    Settings.AUDIT_TAG.ByCredit,Settings.AUDIT_TAG.ByContactInfo])
               .where(modified_at: (Time.now - 1.day)..Time.now).count(:id)
  end

  def get_all_pending_desc_cnt()
    return BkItem
               .where(img_audit_tag: Settings.AUDIT_TAG_IMG.ImgOk)
               .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
               .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview).count(:id)
  end

  def get_all_pending_img_cnt()
    return BkItem
               .where(operator_id: 0)
               .where(img_audit_tag: '')
               .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
               .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview).count(:id)
  end

  def get_rest_pending_cnt()
    return BkItem.where(operator_id: 0)
               .where(img_audit_tag: Settings.AUDIT_TAG_IMG.ImgOk)
               .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
               .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview).count(:id)
  end

  def fetch_items(fetch_size, operator_id)
    res = nil
    puts "operator_id=#{operator_id}"
    if operator_id
      res = BkItem.where(operator_id: 0)
                .where(img_audit_tag: Settings.AUDIT_TAG_IMG.ImgOk)
                .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                .order(audit_priority: :asc , modified_at: :desc)
                .limit(fetch_size)
                .update_all("operator_id=#{operator_id}")
    end
    puts "fetch_items=#{res}"
    res
  end

  def get_all_pending_data(offset:0, limit:10)

    puts "get_all_pending_data #{limit}, #{offset}"

    bk_items = BkItem.includes(:operator)
                   .where(img_audit_tag: Settings.AUDIT_TAG_IMG.ImgOk)
                   .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(audit_priority: :asc , modified_at: :desc)
                   .limit(limit)
                   .offset(offset)

    return self.merge_items(nil,bk_items,limit, offset)
  end

  def get_rest_pending_data(offset:0, limit:10)

    puts "get_rest_pending_data #{limit}, #{offset}"

    bk_items = BkItem.includes(:operator)
                   .where(operator_id: 0)
                   .where(img_audit_tag: Settings.AUDIT_TAG_IMG.ImgOk)
                   .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(audit_priority: :asc , modified_at: :asc)
                   .limit(limit)
                   .offset(offset)

    return self.merge_items(nil,bk_items,limit, offset)
  end

  def get_double_check_data(offset:0, limit:10)

    bk_items = BkItem.includes(:operator)
        .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
        .where(audit_tag: [Settings.AUDIT_TAG.ByAuto,Settings.AUDIT_TAG.ByModify,
                           Settings.AUDIT_TAG.ByCredit,Settings.AUDIT_TAG.ByContactInfo])
        .order(modified_at: :desc)
        .limit(limit)
        .offset(offset)

    return self.merge_items(nil,bk_items,limit, offset)
  end

  def get_today_check_data(offset:0, limit:10)

    bk_items = BkItem.includes(:operator)
                   .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                   .where(audit_tag: [Settings.AUDIT_TAG.ByAuto,Settings.AUDIT_TAG.ByModify,
                                      Settings.AUDIT_TAG.ByCredit,Settings.AUDIT_TAG.ByContactInfo])
                   .where(modified_at: (Time.now - 1.day)..Time.now)
                   .order(modified_at: :desc)
                   .limit(limit)
                   .offset(offset)

    return self.merge_items(nil,bk_items,limit, offset)
  end

  def get_smart_review_data(operator_id , offset:0, limit:10)
    #.where(img_audit_tag: Settings.AUDIT_TAG_IMG.ImgOk)

    bk_items = BkItem.includes(:operator)
        .where(operator_id: operator_id)
        .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
        .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
        .order(audit_priority: :asc , modified_at: :desc)
        .limit(limit)
        .offset(offset)

    return self.merge_items(operator_id, bk_items,limit, offset)
  end


  def get_audited_imgs_data(operator_id , offset, limit)

    bk_items = BkItem.includes(:operator)
                   .order(audit_priority: :asc , modified_at: :desc)
                   .limit(limit)
                   .offset(offset)

    return self.merge_imgs_items(operator_id, bk_items,limit, offset)
  end

  def fetch_img_items(operator_id ,fetch_size)
    res = nil
    puts "operator_id=#{operator_id}"
    if operator_id
      res = BkItem.where(img_op_id: 0)
                .where(operator_id: 0)
                .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                .order(audit_priority: :asc , modified_at: :desc)
                .limit(fetch_size)
                .update_all("img_op_id=#{operator_id}")
    end
    return res
  end

  def get_image_data(operator_id , offset, limit, list_order)

    #self.fetch_img_items(operator_id ,limit)
    bk_items = BkItem.includes(:operator)
                   .where(img_op_id: operator_id)
                   .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .where(img_audit_tag: '')
                   .order(audit_priority: :asc , modified_at: list_order.to_sym)
                  .limit(limit)
                  .offset(offset)

    puts "merge_imgs_items #{bk_items}"
    #puts "merge_imgs_items #{board.inspect}"
    #puts "merge_imgs_items #{board.to_json}"
    return self.merge_imgs_items(operator_id, bk_items,limit, offset)
  end

  def merge_imgs_items(operator_id, bk_items ,limit, offset)

    puts "merge_imgs_items #{operator_id}, #{limit}, #{offset}"


    pending_review_item_data = {}
    bk_items.each do |bk_item|
      item_id = bk_item.item_id
      pending_review_item_data[item_id] = {}
      pending_review_item_data[item_id]['id'] = bk_item.item_id

      if bk_item.operator
        pending_review_item_data[item_id]['reviewer'] = bk_item.operator.username
      elsif
      pending_review_item_data[item_id]['reviewer'] = '-'
      end

      pending_review_item_data[item_id]['is_edit_after_review'] = bk_item.is_edited
      if bk_item.audit_priority
        pending_review_item_data[item_id]['audit_priority'] = bk_item.audit_priority
      end

      pending_review_item_data[item_id]['audit_tag'] = bk_item.audit_tag
    end

    item_ids = pending_review_item_data.keys()
    if item_ids.present?
      EntityItem.where(id: item_ids).each do |item|
        item_id = item.id
        pending_review_item_data[item_id]['item_id'] = item_id
        pending_review_item_data[item_id]['user_id'] = item.user_id
        pending_review_item_data[item_id]['title'] = item.title
        pending_review_item_data[item_id]['desc'] = item.desc
        pending_review_item_data[item_id]['brand_name'] = item.brand_name
        pending_review_item_data[item_id]['local_price'] = item.local_price
        pending_review_item_data[item_id]['country'] = item.country
        pending_review_item_data[item_id]['region'] = item.region

        pending_review_item_data[item_id]['pending_review_time_utc'] = item.modified_at

        pending_review_item_data[item_id]['city'] = item.city
        pending_review_item_data[item_id]['state'] = item.state
        pending_review_item_data[item_id]['state_txt'] = Settings.PRODUCT_STATUSES.invert[item.state]

        pending_review_item_data[item_id]['images'] = []
      end

      EntityImage.where(item_id: item_ids)
          .order(is_first: :desc)
          .select(:id, :item_id, :image_path).find_each do |image|
        pending_review_item_data[image.item_id]['images'] << image.image_path
      end

    end

    item_data = {}
    item_data['data'] = pending_review_item_data.values()
    item_data["meta"]= {"limit"=>limit,"next"=>"offset=#{offset+limit}","offset"=>offset}

    return item_data
  end

  def merge_items(operator_id, bk_items ,limit, offset)

    puts "merge_items #{operator_id}, #{limit}, #{offset}"

    pending_review_item_data = {}

    for bk_item in bk_items
      item_id = bk_item.item_id
      pending_review_item_data[item_id] = {}
      pending_review_item_data[item_id]['id'] = bk_item.item_id

      if bk_item.operator
        pending_review_item_data[item_id]['reviewer'] = bk_item.operator.username
      elsif
      pending_review_item_data[item_id]['reviewer'] = '-'
      end

      pending_review_item_data[item_id]['is_edit_after_review'] = bk_item.is_edited
      if bk_item.audit_priority
        pending_review_item_data[item_id]['audit_priority'] = bk_item.audit_priority
      end

      pending_review_item_data[item_id]['audit_tag'] = bk_item.audit_tag
    end

    item_ids = pending_review_item_data.keys()
    if item_ids.present?
      EntityItem.where(id: item_ids).each do |item|
        item_id = item.id
        pending_review_item_data[item_id]['item_id'] = item_id
        pending_review_item_data[item_id]['user_id'] = item.user_id
        pending_review_item_data[item_id]['offer_count'] = 0
        pending_review_item_data[item_id]['title'] = item.title
        pending_review_item_data[item_id]['desc'] = item.desc
        pending_review_item_data[item_id]['brand_name'] = item.brand_name
        pending_review_item_data[item_id]['local_price'] = item.local_price
        pending_review_item_data[item_id]['country'] = item.country
        pending_review_item_data[item_id]['region'] = item.region
        pending_review_item_data[item_id]['city'] = item.city
        pending_review_item_data[item_id]['state'] = item.state
        pending_review_item_data[item_id]['state_txt'] = Settings.PRODUCT_STATUSES.invert[item.state]

        pending_review_item_data[item_id]['images'] = []

        pending_review_time = []
        if item.modified_at.present?
          pending_review_time << ((Time.current - item.modified_at)/1.minute).round(0)
          pending_review_time << "#{item.modified_at.to_s}(LA)"
          pending_review_time << "#{item.modified_at.in_time_zone('America/Chicago').to_s}(Dallas)"
          pending_review_time << "#{item.modified_at.in_time_zone('Asia/Shanghai').to_s}(Beijing)"
        else
          pending_review_time << 0
          pending_review_time << ''
          pending_review_time << ''
          pending_review_time << ''
        end
        pending_review_item_data[item_id]['pending_review_time'] = pending_review_time

        pending_review_item_data[item_id]['pending_review_time_utc'] = item.modified_at
      end

      EntityImage.where(item_id: item_ids)
          .order(is_first: :desc)
          .select(:id, :item_id, :image_path).find_each do |image|
        pending_review_item_data[image.item_id]['images'] << image.image_path
      end

      EntityItemcategory.includes(:category).where(item_id: item_ids).find_each do |itc|
        pending_review_item_data[itc.item_id]['category'] = itc.category.title
      end

      # todo dev
      # ret = BackendApiREST.get_offer_count_by_item_ids(item_ids.map { |id| id.to_i })
      # if ret.present?
      #   JSON.load(ret[1])['objects'].each do |ob|
      #     item_id = ob['item_id'].to_i
      #     offer_count = ob['count'].to_i
      #     pending_review_item_data[item_id]['offer_count'] = offer_count if offer_count != 0
      #   end
      # end

      ItemTask.where.not(state: Settings.ITEM_TASK_STATE.Completed).where(item_id: item_ids).select(:id, :item_id).each do |item_task|
        item_id = item_task.item_id
        pending_review_item_data[item_id]['item_task_ids'] = [] if pending_review_item_data[item_id]['item_task_ids'].blank?
        pending_review_item_data[item_id]['item_task_ids'] << item_task.id
      end
    end

    item_data = {}
    item_data['data'] = pending_review_item_data.values()
    item_data['current_operator'] = {}
    item_data['current_operator']['username'] = "-"

    if operator_id
      item_data['current_operator']['username'] = operator_id
    elsif
      item_data['current_operator']['username'] = 'no assgin'
    end

    item_data["meta"]= {"limit"=>limit,"next"=>"offset=#{offset+limit}","offset"=>offset}

    return item_data
  end

  def get_img_operators(operator_id)

    bk_items_group  = BkItem.select("img_op_id, count(id) as item_cnt")
                          .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                          .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                          .where(img_audit_tag: '')
                          .where(operator_id: 0)
                          .where.not(img_op_id: 0)
                          .group(:img_op_id)

    puts "bk_items_group2=#{bk_items_group.inspect}"

    empls = EmployeeManager.instance.get_employees()
    puts empls.inspect

    dict_group = {}
    bk_items_group.each do |it|

      op_id = it.img_op_id

      self.build_empl_info(it, operator_id,op_id, empls ,dict_group)
    end

    puts dict_group.inspect

    return dict_group
  end

  def build_empl_info(it, my_id,op_id, empls ,dict_group)

      #puts it.inspect

      is_my = false
      if op_id == nil
        op_id='-'
      elsif my_id==op_id
        is_my = true
      end

      dict_group[op_id] = {"item_cnt" => it.item_cnt, "id" => op_id}
      dict_group[op_id]['username'] = empls[op_id] ? empls[op_id]['empl_name'] : "-#{op_id}-"
      dict_group[op_id]['is_my'] = is_my

  end

  # 获得所有agent的统计信息
  def get_assign_info(operator_id)

    bk_items_group  = BkItem.select("operator_id, count(id) as item_cnt")
                          .where.not(item_state: Settings.PRODUCT_STATUSES.Unavailable)
                          .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                          .where.not(operator_id: 0)
                          .group(:operator_id)
    #puts bk_items_group.to_json

    empls = EmployeeManager.instance.get_employees()
    #puts empls.inspect

    dict_group = {}
    bk_items_group.each do |it|
      #puts it.inspect

      op_id = it.operator_id

      self.build_empl_info(it, operator_id, op_id , empls ,dict_group)
    end

    #puts dict_group.inspect

    return dict_group
  end


end