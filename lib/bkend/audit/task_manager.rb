require 'singleton'

class TaskManager
  include Singleton

  def initialize()
    puts "TaskManager init"
  end

  def opened_task_cnt()
    task_cnt  = ItemTask.where(state: [Settings.ITEM_TASK_STATE.Created,Settings.ITEM_TASK_STATE.ActionExecuted])
                    .where(processor:[Settings.SYSTEM_OPERATORS.Task, Settings.SYSTEM_OPERATORS.Api]).count(:id)

    return task_cnt
  end

  def img_other_task_cnt()
    ItemTask.where.not(state: Settings.ITEM_TASK_STATE.Completed)
            .where(task_type: "Image-Other(Smart Image)").count(:id)
  end

  def quick_task_exec(rule_name, item_id, empl_id, comment_text)

    if empl_id
      empl_name = EmployeeManager.instance.get_empl_name(empl_id)
    else
      empl_name = EmployeeManager.instance.current_employee_name
    end

    item_operation_rule_name = rule_name
    rule = ItemOperationRule.find_by_name(item_operation_rule_name)
    if rule.is_enabled?
      Rails.logger.info "Item operation rule(name:'#{item_operation_rule_name}') has been disabled."
    end

    is_success_flag, message, task = Utils::BkItemTaskOps.create(rule, item_id, empl_name)
    if task.present?
      Utils::BkItemTaskOps.execute_action(task, empl_name)

      if comment_text
        task.create_comment(comment_text, empl_name)
      end

    end
    Rails.logger.info message

  end

end