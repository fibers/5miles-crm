require 'singleton'
require_relative '../agent_manager'

module AutoDispatchService
  module ParamsConst
    OPER_AUDIT_VELOCITY = 1
    # image audit standard speed(vs1'): 90 (items/min)
    IMG_AUDIT_STD_SPEED = 90.0
    # IMG_AUDIT_STD_SPEED = 20.0
    # image description audit standard speed(vs2'): 4.3 (items / min)
    IMG_DESC_AUDIT_STD_SPEED = 6.8
    # dispatch interval time (minutes) in busy time
    DISPATCH_INTERVAL_IN_BUSY_TIME = 5
    # dispatch interval time (minutes) in idle time
    DISPATCH_INTERVAL_IN_IDLE_TIME = 0
    # pending review audit time alarm level in minutes
    PENDING_REVIEW_TIME_ALARM_LEVEL = 6
    # water level of item audit
    ITEM_AUDIT_WARTER_LEVEL = 100
    # Busy time standard
    UNASSIGNED_ITEM_COUNT_IN_BUSY_TIME = 20
  end

  class DispatchCtrlCenter
    include Singleton

    def initialize
      @triggers = {}
      @triggers[:check_in] = AutoDispatchService::CheckInTrigger.new
      @triggers[:finish_assigned] = AutoDispatchService::FinishAssignedItemTrigger.new
      @triggers[:dispatch_interval] = AutoDispatchService::CheckDispatchIntervalTrigger.new
      @triggers[:item_modified] = AutoDispatchService::ItemModifiedTrigger.new
    end

    def deal_dispatch(trigger_type, data={})
      trigger_type = trigger_type.to_sym
      data.symbolize_keys!
      obj = @triggers[trigger_type]
      if obj.nil?
        Rails.logger.info "Not found such a trigger type: #{trigger_type}"
      else
        ret = SmartAuditManager.instance.get_total_stat_info
        # puts ret.size
        # 图片未分配
        data[:item_unassigned_cnt] = ret["item_unassigned_cnt"]
        # 图片已分配待审核(img_pending_review)
        data[:img_assigned_unaudit_cnt] = ret["img_assigned_unaudit_cnt"]
        # 图片审核通过图文未分配
        data[:desc_unassigned_cnt] = ret["desc_unassigned_cnt"]
        # 图片审核通过图文已分配待审核
        data[:desc_assigned_unaudit_cnt] = ret["desc_assigned_unaudit_cnt"]
        obj.exec(data)
      end

    end

    def get_img_assignment_cnt(data={})
      (AutoDispatchService::ParamsConst::IMG_AUDIT_STD_SPEED *
          AutoDispatchService::ParamsConst::DISPATCH_INTERVAL_IN_BUSY_TIME *
          data[:operator_level]).round()
    end

    def get_desc_assignment_cnt(data={})
      (AutoDispatchService::ParamsConst::IMG_DESC_AUDIT_STD_SPEED *
          AutoDispatchService::ParamsConst::DISPATCH_INTERVAL_IN_BUSY_TIME *
          data[:operator_level]).round()
    end

    def is_desc_assignment?(data={})
      data[:item_unassigned_cnt] <= AutoDispatchService::ParamsConst::ITEM_AUDIT_WARTER_LEVEL
    end

    def is_busy_time?(data={})
      data[:item_unassigned_cnt].to_i + data[:desc_unassigned_cnt].to_i >= AutoDispatchService::ParamsConst::UNASSIGNED_ITEM_COUNT_IN_BUSY_TIME
    end

    def search_available_operator(data)
      checked_in_agents = AgentManager.instance.get_checkin_agents()
      return nil if checked_in_agents.size < 1

      op_workload = get_op_workload_stat(data)

      ret_op_id = -1
      ret_load = 2 << 64
      checked_in_agents.each do |agent|
        #actual_effort = now - last_dispatch_time
        actual_effort = 6
        if op_workload[agent[:operator_id]][:workload] > actual_effort
          Rails.logger.info "The #{agent[:operator_id]} over processing limit."
          # AutoDispatchService::DispatchCtrlCenter.instance.send_warning_email(agent[:operator_id], op_workload)
        elsif ret_load > op_workload[agent[:operator_id]][:workload]
          ret_op_id = agent[:operator_id]
          ret_load = op_workload[agent[:operator_id]][:workload]
        end
      end
      ret_op_id
    end

    def get_op_workload_stat(data)
      agents_stat = SmartAuditManager.instance.get_separated_stat_info()
      op_workload = Hash.new({pending_img_cnt: 0, pending_desc_cnt: 0, workload: 0})

      agents_stat.each do |stat|
        pending_img_cnt = stat["pending_img_cnt"]
        pending_desc_cnt = stat["pending_desc_cnt"]
        # 分配时已考虑了处理速度，本次计算不需要再考虑
        workload = (pending_img_cnt / AutoDispatchService::ParamsConst::IMG_AUDIT_STD_SPEED +
            pending_desc_cnt / AutoDispatchService::ParamsConst::IMG_DESC_AUDIT_STD_SPEED).round(2)

        op_workload[stat["id"]] = {pending_img_cnt: pending_img_cnt, pending_desc_cnt: pending_desc_cnt, workload: workload}
        Rails.logger.info "operator_id=#{stat["id"]}, pending_img_cnt=#{pending_img_cnt}, pending_desc_cnt=#{pending_desc_cnt}"
      end
      op_workload
    end

    def desc_assignment(operator_id, assigment_cnt)
      # 图文分配
      bk_item_ids = BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                     .where(assigned_to: 0)
                     .order(modified_at: :desc)
                     .limit(assigment_cnt)
                     .pluck(:id);

      row_cnt = BkItem.where(id: bk_item_ids)
                    .update_all("operator_id = 0, assigned_to=#{operator_id}, assigned_at=now()")
      Rails.logger.info "desc_assignment: should_assignment_count=#{assigment_cnt}, actual_assignment_cnt=#{row_cnt}, assigned_to=#{operator_id}"
      row_cnt
    end

    def img_assignment(operator_id, assigment_cnt)
      # 分配图片
      bk_item_ids = BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                    .where(assigned_to: 0)
                    .where(operator_id: 0)
                    .where(img_assigned_to: 0)
                    .where(img_op_id: 0)
                    .order(modified_at: :desc)
                    .limit(assigment_cnt)
                    .pluck(:id);

      row_cnt = BkItem.where(id: bk_item_ids)
                    .update_all("img_op_id=0, img_assigned_to=#{operator_id}, img_assigned_at=now()")
      Rails.logger.info "img_assignment: should_assignment_count=#{assigment_cnt}, actual_assignment_cnt=#{row_cnt}, assigned_to=#{operator_id}"
      row_cnt
    end

    # 单个item的图片分配
    # operator_id : 操作员ID
    # item_id ： 待分配item ID
    # first_flag : 是否第一次分配（或再分配？）
    def single_img_assignment(operator_id, item_id, first_flag = 1)
      # 分配图片
      row_cnt = BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                    .where(item_id: item_id)
                    .where(assigned_to: 0)
                    .where(operator_id: 0)
                    .update_all("img_op_id = 0, img_assigned_to=#{operator_id}, img_assigned_at=now()")
      if row_cnt > 0
        Rails.logger.info "single_img_assignment: item_id=#{item_id}, assigned_to=#{operator_id}"
      end

      # Update the last assignment time
      if row_cnt > 0
        $global_redis.hset("agent:#{operator_id}", "last_dispatch_time", Time.now.utc.to_s)
      end

      row_cnt
    end

    # 单个item的图文分配
    # operator_id : 操作员ID
    # item_id ： 待分配item ID
    # first_flag : 是否第一次分配（或再分配？）
    def single_item_assignment(operator_id, item_id, first_flag = 1)
      # 图片未分配的图文分配
      row_cnt = BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                    .where(item_id: item_id)
                    .update_all("operator_id=0, assigned_to=#{operator_id}, assigned_at=now()")
      if row_cnt > 0
        Rails.logger.info("single_desc_assignment: item_id=#{item_id}, assigned_to=#{operator_id}")
      end

      #Update the last assignment time
      if row_cnt > 0
        $global_redis.hset("agent:#{operator_id}", "last_dispatch_time", Time.now.utc.to_s)
      end
      row_cnt
    end

    def general_assignment(data)
      # is_desc_assignment = DispatchCtrlCenter.instance.is_desc_assignment?(data)
      # puts "is_desc_assignment: #{is_desc_assignment}"
      assign_cnt = 0
      if DispatchCtrlCenter.instance.is_desc_assignment?(data)
        # desc_assignment
        assign_cnt = desc_assignment(data[:operator_id], DispatchCtrlCenter.instance.get_desc_assignment_cnt(data))
      else
        # img_assignment
        assign_cnt = img_assignment(data[:operator_id], DispatchCtrlCenter.instance.get_img_assignment_cnt(data))
      end
      #Update the last assignment time
      if assign_cnt > 0
        $global_redis.hset("agent:#{data[:operator_id]}", "last_dispatch_time", Time.now.utc.to_s)
      end
    end

    def clear_assignment_info(item_id)
      BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
          .where(item_id: item_id)
          .update_all("img_assigned_to = 0, img_assigned_at = null, assigned_to = 0, assigned_at = null, img_op_id = 0, operator_id = 0")
    end


    def send_warning_email(operator_id, op_workload)
      # template = "<html><head><title></title></head><body>#{content}</body></html>"
      return if op_workload[operator_id].nil?
      empl_name = EmployeeManager.instance.get_empl_name(operator_id)
      assigned_unaudit_cnt = op_workload[operator_id][:pending_img_cnt] + op_workload[operator_id][:pending_desc_cnt]
      content = []
      email_subject = "【BOSS审核分配报警】#{empl_name} 已分配待审核商品数：#{assigned_unaudit_cnt},"
      content << email_subject
      content << "超出阈值"
      content << "</BR>"
      content << "报警时间："
      content << Time.current.in_time_zone("Asia/Shanghai").to_s
      content << "(北京时间)."

      text = "<html><head><title></title></head><body>#{content.join("")}</body></html>"
      puts text
      admin_emails = []
      if Rails.env == "production"
        admin_emails << "item-review-operations@wespoke.com"
      else
        admin_emails << "chenweibing@wespoke.com"
      end
      email_params = {}
      email_params['from'] = Settings.FROM_EMAIL_DOMAINS.Promption.Subscirbe
      email_params['subject'] = email_subject
      email_params['text'] = text
      # parameters: to, from , template, text, subject, data
      Rails.logger.info text
      # 暂停发邮件
      # for email_to in admin_emails
      #   unless FmmcREST.send_direct_mail(email_to, email_params)
      #     Rails.logger.info "Fail to send '#{email_subject}' to '#{email_to}'"
      #     Rails.logger.info "#{email_params.to_json}"
      #   end
      # end
    end
  end


  class BaseTrigger
    def exec(data={})
      before_exec(data)
      exec_dispatch(data)
      after_exec(data)
    end

    def before_exec(data={})
      Rails.logger.info("Before execute BaseTrigger dispatch ...")
    end

    def after_exec(data={})
      Rails.logger.info("After execute BaseTrigger dispatch ...")
    end


  end

  class CheckInTrigger < BaseTrigger
    def before_exec(data={})
      Rails.logger.info("Before execute CheckInTrigger dispatch ...")
    end

    def after_exec(data={})
      Rails.logger.info("After execute CheckInTrigger dispatch ...")
    end

    def exec_dispatch(data={})
      operator_id = data[:operator_id]
      return if operator_id.nil?

      rec = AgentManager.instance.get_agent_info(operator_id)
      # 审核员速度分级  L = 0.5, 1, 1.5 …
      data[:operator_level] = rec.ability_level

      puts ("Operator ##{operator_id} has checked in")
      Rails.logger.info("The ability levle of operator ##{operator_id} is #{data[:operator_level]}")

      # puts "图片未分配: #{data[:img_unassigned_cnt]}"
      # puts "图片已分配待审核(img_pending_review): #{data[:img_unaudit_cnt]}"
      # puts "图片审核通过图文未分配: #{data[:img_ok_unassigned_cnt]}"
      # puts "图片审核通过图文已分配待审核: #{data[:img_ok_unaudit_cnt]}"
      # puts "图文已分配待审核: #{data[:item_unaudit_cnt]}"

      op_workload = DispatchCtrlCenter.instance.get_op_workload_stat(data)
      #actual_effort = now - last_dispatch_time
      # 每次分配任务的最大处理时间（分钟）
      actual_effort = 6
      if op_workload[operator_id][:workload] > actual_effort
        Rails.logger.info("Over processing time limits, skip dispatch.")
        # AutoDispatchService::DispatchCtrlCenter.instance.send_warning_email(operator_id, op_workload)
      else
        DispatchCtrlCenter.instance.general_assignment(data)
      end

    end
  end

  class FinishAssignedItemTrigger < BaseTrigger
    def before_exec(data={})
      Rails.logger.info("Before execute FinishAssignedItemTrigger dispatch ...")
    end

    def after_exec(data={})
      Rails.logger.info("After execute FinishAssignedItemTrigger dispatch ...")
    end


    def exec_dispatch(data={})
      operator_id = data[:operator_id]
      return if operator_id.nil?

      rec = AgentManager.instance.get_agent_info(operator_id)
      # is checked_in state?
      if not AgentManager.instance.checked_in?(rec)
        Rails.logger.info("Operator ##{operator_id} not checked in. Skip dispatch.")
        return
      end

      my_group = SmartAuditManager.instance.get_my_group(operator_id)
      # still has pending assignment
      if my_group["image_audit"] != 0 or my_group["description_audit"] != 0
        puts "Operator ##{operator_id} still has pending assignment! Skip dispatch."
        return
      end

      # 审核员速度分级  L = 0.5, 1, 1.5 …
      data[:operator_level] = rec.ability_level
      Rails.logger.info("The ability levle of operator ##{operator_id} is #{data[:operator_level]}")

      DispatchCtrlCenter.instance.general_assignment(data)
    end
  end

  class ItemModifiedTrigger < BaseTrigger
    @@last_assigned_seq = -1

    def before_exec(data={})
      Rails.logger.info("Before execute ItemModifiedTrigger dispatch ...")
    end

    def after_exec(data={})
      Rails.logger.info("After execute ItemModifiedTrigger dispatch ...")
    end

    def exec_dispatch(data={})
      #is busy time?
      if DispatchCtrlCenter.instance.is_busy_time?(data)
        Rails.logger.info("In busy time, skip dispatch.")
        return
      end

      checked_in_agents = AgentManager.instance.get_checkin_agents()
      return nil if checked_in_agents.size < 1

      # 计算有任务的operator剩余工作量（需要的处理时间）
      op_workload = DispatchCtrlCenter.instance.get_op_workload_stat(data)

      checked_in_cnt = checked_in_agents.size

      (0 ... checked_in_cnt).each do |i|
        seq = get_next_assign_seq(checked_in_cnt)
        agent = checked_in_agents[seq]

        #actual_effort = now - last_dispatch_time
        # 每次分配任务的最大处理时间（分钟）
        actual_effort = 6
        # 小于处理的量
        if op_workload[agent[:operator_id]][:workload] <= actual_effort
          Rails.logger.info("Assignment for ##{agent[:operator_id]} as ItemModifiedTrigger, ItemID=#{data[:item_id]}.")
          # 审核员速度分级  L = 0.5, 1, 1.5 …
          data[:operator_id] = agent[:operator_id]
          data[:operator_level] = agent[:ability_level]
          DispatchCtrlCenter.instance.general_assignment(data)
          break
        end

      end
    end

    private
    def get_next_assign_seq(checked_in_cnt)
      return nil if checked_in_cnt <= 0
      @@last_assigned_seq += 1
      @@last_assigned_seq = @@last_assigned_seq.modulo(checked_in_cnt)
    end

  end

  class CheckDispatchIntervalTrigger < BaseTrigger
    def before_exec(data={})
      Rails.logger.info("Before execute CheckDispatchIntervalTrigger dispatch ...")
    end

    def after_exec(data={})
      Rails.logger.info("After execute CheckDispatchIntervalTrigger dispatch ...")
    end

    def exec_dispatch(data={})
      #is idle time?
      # if not DispatchCtrlCenter.instance.is_busy_time?(data)
      #   Rails.logger.info("In idle time, skip dispatch.")
      #   return
      # end

      checked_in_agents = AgentManager.instance.get_checkin_agents()
      return nil if checked_in_agents.size < 1

      # 计算有任务的operator剩余工作量（需要的处理时间）
      op_workload = DispatchCtrlCenter.instance.get_op_workload_stat(data)

      checked_in_agents.each do |agent|
        #actual_effort = now - last_dispatch_time
        # 每次分配任务的最大处理时间（分钟）
        actual_effort = 6
        last_dispatch_time = $global_redis.hget("agent:#{agent[:operator_id]}", "last_dispatch_time")
        next if last_dispatch_time.nil?

        diff = Time.now.utc - last_dispatch_time.to_time
        # 判断是否到达下次分配任务的时刻（忙时系统分配间隔 Tb = 300秒）
        if diff - AutoDispatchService::ParamsConst::DISPATCH_INTERVAL_IN_BUSY_TIME * 60 >= 0
          # 达到告警阈值，发告警邮件病跳过此次分配
          if op_workload[agent[:operator_id]][:workload] > actual_effort
            Rails.logger.info("Over processing time limits, send warning email and skip dispatch.")
            AutoDispatchService::DispatchCtrlCenter.instance.send_warning_email(agent[:operator_id], op_workload)
            # 跳过此次分配，更新最后分配时间
            $global_redis.hset("agent:#{agent[:operator_id]}", "last_dispatch_time", Time.now.utc.to_s)
          else
            Rails.logger.info("Assignment for ##{agent[:operator_id]} as CheckDispatchIntervalTrigger.")
            # 审核员速度分级  L = 0.5, 1, 1.5 …
            data[:operator_id] = agent[:operator_id]
            data[:operator_level] = agent[:ability_level]
            DispatchCtrlCenter.instance.general_assignment(data)
          end
        end
      end
    end
  end
end


