require 'singleton'

class EmployeeManager
  include Singleton

  def initialize()
    puts "EmployeeManager init"
    @empls = nil
  end

  def get_employees()
    # todo 需要缓存

    if not @empls
      @empls = {}
      Admin.all.find_each do |empl|
        @empls[empl.id] = {"empl_id"=>empl.id, "empl_name"=>empl.username}
      end
    end

    return @empls
  end

  def get_empl_name(empl_id)
    return self.get_employees()[empl_id]['empl_name']
  end


  def clear_catach

  end

  def get_employee(operator_id)
    return self.get_employees()[operator_id]
  end

  def current_employee_id=(id)
    Thread.current[:current_employee_id] = id.to_i
  end

  def current_employee_id
    Thread.current[:current_employee_id]
  end

  def current_employee_name=(name)
    Thread.current[:current_employee_name] = name
  end

  def current_employee_name
    Thread.current[:current_employee_name]
  end

end
