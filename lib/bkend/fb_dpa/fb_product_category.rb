module FbDPA
  class FbProductCategory
    class << self
      def generate_fb_product_feed(city_name)
        headers = %w(id condition description google_product_category image_link link price title shipping brand availability applink_ios_app_name applink_ios_url applink_iphone_url applink_iphone_app_name applink_ipad_url applink_ipad_app_name applink_android_url applink_android_package applink_android_app_name)
        fb_items = populate_fb_items(city_name)
        item_count = fb_items.size

        output_string = CSV.generate({col_sep: "\t"}) do |csv|
          csv << headers # Add new headers
          fb_items.each do |item|
            row = []
            headers.each do |col_name|
              row << item[col_name.to_sym]
            end
            csv.puts row
          end
        end
        return true, item_count, output_string
      end

      def clear_product_feed(city_name)
       ids = BkFbDpaItem.where(city_name: city_name).pluck(:id)
       delete_cnt = BkFbDpaItem.delete(ids)
       Rails.logger.info("city_name=#{city_name}, deleted ids in bk_fb_dpa_items: #{ids}")
       return true, "All item(s) delete successfully!"
      end

      def add_product_feed_batch(city_name, item_list)
        google_cat_map = Settings.CategoryMap.ToGoogle
        last_update_time = Time.zone.now().to_s
        failure_item_list = []

        item_list.each do |item|
          puts item.class
          fb_dba_item = BkFbDpaItem.new()
          fb_dba_item.item_id = item["id"]
          fb_dba_item.city_name = city_name
          fb_dba_item.fb_img_link = item['img_url']
          fb_dba_item.google_product_category = google_cat_map["CAT_ID_#{item['cat_id']}"].to_s
          fb_dba_item.item_state = Settings.PRODUCT_STATUSES.Listing
          fb_dba_item.fetch_state = 0
          fb_dba_item.remarks = ''
          fb_dba_item.reserved_1 = ''
          fb_dba_item.reserved_2 = ''
          fb_dba_item.reserved_3 = ''
          fb_dba_item.created_at = last_update_time
          fb_dba_item.updated_at = last_update_time

          begin
            fb_dba_item.save
          rescue =>e
            Rails.logger.info e.inspect
            failure_item_list << item["id"]
          end
        end
        Rails.logger.info("Failure item list: #{failure_item_list}") unless failure_item_list.size == 0
        success_item_cnt = item_list.size - failure_item_list.size
        return true, "Product feed added successfully!", success_item_cnt
      end

      # private
      def populate_fb_items(city_name)
        dpa_items = BkFbDpaItem.where(city_name: city_name)
                        .where(item_state: Settings.PRODUCT_STATUSES.Listing)
                        .select(:id, :item_id, :fb_img_link, :google_product_category)


        dpa_item_hash = {}
        dpa_items.each do |item|
          h = {}
          h[:fb_img_link] = item.fb_img_link
          h[:google_product_category] = item.google_product_category
          dpa_item_hash[item.item_id] = h
        end

        entity_items = EntityItem.where(id: dpa_item_hash.keys)

        fb_items = []
        entity_items.each do |item|
          row = {}
          row[:id] = item.id.to_s
          row[:condition] = "used"
          row[:title] = item.title.blank? ? nil : item.title.split(%r{\r\n|\n}).join(" ")
          row[:description] = item.desc.blank? ? row[:title] : item.desc.split(%r{\r\n|\n}).join(" ")
          row[:google_product_category] = dpa_item_hash[item.id][:google_product_category]
          row[:link] = "#{Settings.APP_SITE_ITEM_URI_PREFIX}#{item.fuzzy_item_id}"
          row[:image_link] = dpa_item_hash[item.id][:fb_img_link]
          row[:shipping] = nil
          row[:price] = "#{item.price} #{item.currency}"
          row[:brand] = item.brand_name.blank? ? "5miles" : item.brand_name
          row[:availability] = (item.state == Settings.PRODUCT_STATUSES.Listing ? "in stock" : "out of stock")

          row[:applink_ios_url] = "#{Settings.AppLink.iOS.URL_PREFIX}/#{item.fuzzy_item_id}"
          row[:applink_ios_app_name] = "#{Settings.AppLink.iOS.APP_NAME}"

          row[:applink_iphone_url] = "#{Settings.AppLink.iOS.URL_PREFIX}/#{item.fuzzy_item_id}"
          row[:applink_iphone_app_name] = "#{Settings.AppLink.iOS.APP_NAME}"

          row[:applink_ipad_url] = "#{Settings.AppLink.iOS.URL_PREFIX}/#{item.fuzzy_item_id}"
          row[:applink_ipad_app_name] = "#{Settings.AppLink.iOS.APP_NAME}"

          row[:applink_android_url] = "#{Settings.AppLink.Android.URL_PREFIX}/#{item.fuzzy_item_id}"
          row[:applink_android_package] = "#{Settings.AppLink.Android.PACKAGE}"
          row[:applink_android_app_name] = "#{Settings.AppLink.Android.APP_NAME}"

          fb_items << row
        end
        return fb_items
      end

    end
  end
end
