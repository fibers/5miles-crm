require 'singleton'

class SmartAuditManager
  include Singleton

  def initialize()
    puts "SmartAuditManager init"
  end

  # 获取指定agent的待审核图片和待审核图片的数量
  def get_my_group(op_id)
    imgage_audit_cnt = BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                           .where(assigned_to: 0)
                           .where(img_assigned_to: op_id)
                           .where(img_op_id: 0).size
    description_audit_cnt = BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                                .where(assigned_to: op_id)
                                .where(operator_id: 0).size
    return {"image_audit" => imgage_audit_cnt, "description_audit" => description_audit_cnt}
  end

  def get_all_pending_group()
    ret = self.get_total_stat_info

    unassigned_pending = 0
    assigned_pending = 0
    desc_unassigned = 0
    item_unassigned = 0
    img_assigned_unaudit = 0
    desc_assigned_unaudit = 0
    if not ret.nil?
      unassigned_pending = ret["item_unassigned_cnt"] + ret["desc_unassigned_cnt"]
      assigned_pending = ret["img_assigned_unaudit_cnt"] + ret["desc_assigned_unaudit_cnt"]

      desc_unassigned = ret['desc_unassigned_cnt']
      item_unassigned = ret['item_unassigned_cnt']
      img_assigned_unaudit = ret['img_assigned_unaudit_cnt']
      desc_assigned_unaudit = ret['desc_assigned_unaudit_cnt']
    end

    all_pending_cnt = unassigned_pending + assigned_pending

    return {"all_pending" => all_pending_cnt,
            "unassigned_pending" => unassigned_pending,
            "assigned_pending" => assigned_pending,
            "desc_unassigned" => desc_unassigned,
            "item_unassigned"=> item_unassigned,
            "img_assigned_unaudit" => img_assigned_unaudit,
            "desc_assigned_unaudit" => desc_assigned_unaudit}
  end

  def get_audit_operators(operator_id)
    audit_stat = self.get_separated_stat_info
    dict_group = {}
    audit_stat.each do |rec|
      op_id = rec["id"]
      dict_group[op_id] = {"item_cnt" => rec["pending_img_cnt"].to_i + rec["pending_desc_cnt"].to_i, "id" => op_id}
      dict_group[op_id]['username'] = rec["username"]
      dict_group[op_id]['is_my'] = op_id.equal?(operator_id)
    end
    puts dict_group
    return dict_group
  end

  def list_unassigned_pending_items(offset: 0, limit: 10)
    bk_items = BkItem.includes(:assignment)
                   .where("((assigned_to = 0 and img_assigned_to = 0) or (assigned_to = 0 and img_assigned_to <> 0 and img_op_id <> 0))")
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(modified_at: :desc)
                   .limit(limit)
                   .offset(offset)
    puts bk_items
    return self.merge_items(nil, bk_items, limit, offset)
  end

  def list_all_pending_items(offset: 0, limit: 10)
    bk_items = BkItem.includes(:assignment)
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(modified_at: :desc)
                   .limit(limit)
                   .offset(offset)
    puts bk_items
    return self.merge_items(nil, bk_items, limit, offset)
  end

  def list_assigned_pending_items(offset: 0, limit: 10)
    bk_items = BkItem.includes(:assignment)
                   .where("((assigned_to <> 0 and operator_id = 0) or (assigned_to = 0 and img_assigned_to <> 0 and img_op_id = 0))")
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(modified_at: :desc)
                   .limit(limit)
                   .offset(offset)
    puts bk_items
    return self.merge_items(nil, bk_items, limit, offset)
  end

  def list_items(operator_id, offset: 0, limit: 10)
    puts "get_rest_pending_data #{operator_id}, #{limit}, #{offset}"
    bk_items = BkItem.includes(:assignment)
                   .where(assigned_to: operator_id)
                   .where(operator_id: 0)
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(modified_at: :desc)
                   .limit(limit)
                   .offset(offset)
    puts bk_items
    return self.merge_items(operator_id, bk_items, limit, offset)
  end


  def get_assigned_data(operator_id, assign_type, offset: 0, limit: 10)
    bk_items = BkItem.includes(:assignment)
                   .where(assigned_to: operator_id)
                   .where.not(operator_id: 0)
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(modified_at: :asc)
                   .limit(limit)
                   .offset(offset)

    return self.merge_items(operator_id, bk_items, limit, offset)
  end

  def list_images(operator_id, offset: 0, limit: 10)
    puts "list_images #{operator_id}, #{limit}, #{offset}"
    bk_items = BkItem.includes(:img_assignment)
                   .where(img_assigned_to: operator_id)
                   .where(img_op_id: 0)
                   .where(assigned_to: 0)
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(modified_at: :desc)
                   .limit(limit)
                   .offset(offset)
    puts bk_items
    return self.merge_imgs_items(operator_id, bk_items, limit, offset)
  end


  def get_image_data(operator_id, offset, limit, list_order)
    #self.fetch_img_items(operator_id ,limit)
    bk_items = BkItem.includes(:img_assignment)
                   .where(img_assigned_to: operator_id)
                   .where(img_op_id: 0)
                   .where(assigned_to: 0)
                   .where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
                   .order(modified_at: list_order.to_sym)
                   .limit(limit)
                   .offset(offset)

    puts "merge_imgs_items #{bk_items}"
    #puts "merge_imgs_items #{board.inspect}"
    #puts "merge_imgs_items #{board.to_json}"
    return self.merge_imgs_items(operator_id, bk_items, limit, offset)
  end

  def merge_imgs_items(operator_id, bk_items, limit, offset)

    puts "merge_imgs_items #{operator_id}, #{limit}, #{offset}"


    pending_review_item_data = {}
    bk_items.each do |bk_item|
      item_id = bk_item.item_id
      pending_review_item_data[item_id] = {}
      pending_review_item_data[item_id]['id'] = bk_item.item_id

      if bk_item.img_assignment
        pending_review_item_data[item_id]['reviewer'] = bk_item.img_assignment.username
      elsif pending_review_item_data[item_id]['reviewer'] = '-'
      end

      pending_review_item_data[item_id]['is_edit_after_review'] = bk_item.is_edited
      if bk_item.audit_priority
        pending_review_item_data[item_id]['audit_priority'] = bk_item.audit_priority
      end

      pending_review_item_data[item_id]['audit_tag'] = bk_item.audit_tag
    end

    item_ids = pending_review_item_data.keys()
    if item_ids.present?
      EntityItem.where(id: item_ids).each do |item|
        item_id = item.id
        pending_review_item_data[item_id]['item_id'] = item_id
        pending_review_item_data[item_id]['user_id'] = item.user_id
        pending_review_item_data[item_id]['title'] = item.title
        pending_review_item_data[item_id]['desc'] = item.desc
        pending_review_item_data[item_id]['brand_name'] = item.brand_name
        pending_review_item_data[item_id]['local_price'] = item.local_price
        pending_review_item_data[item_id]['price'] = item.price
        pending_review_item_data[item_id]['country'] = item.country
        pending_review_item_data[item_id]['region'] = item.region

        pending_review_item_data[item_id]['pending_review_time_utc'] = item.modified_at

        pending_review_item_data[item_id]['city'] = item.city
        pending_review_item_data[item_id]['state'] = item.state
        pending_review_item_data[item_id]['state_txt'] = Settings.PRODUCT_STATUSES.invert[item.state]

        pending_review_item_data[item_id]['images'] = []
      end

      EntityImage.where(item_id: item_ids)
          .order(is_first: :desc)
          .select(:id, :item_id, :image_path).find_each do |image|
        pending_review_item_data[image.item_id]['images'] << image.image_path
      end

    end


    item_data = {}
    item_data['current_operator'] = {}
    item_data['current_operator']['username'] = "-"
    if operator_id
      item_data['current_operator']['username'] = EmployeeManager.instance.get_empl_name(operator_id)
    else
      item_data['current_operator']['username'] = 'no assgin'
    end
    item_data['data'] = pending_review_item_data.values()
    count = pending_review_item_data.values().size
    item_data["meta"]= {"limit" => limit,"count"=>count, "next" => "offset=#{offset+limit}", "offset" => offset}

    return item_data
  end

  def merge_items(operator_id, bk_items, limit, offset)

    # puts "merge_items #{operator_id}, #{limit}, #{offset}"

    pending_review_item_data = {}

    for bk_item in bk_items
      item_id = bk_item.item_id
      pending_review_item_data[item_id] = {}
      pending_review_item_data[item_id]['id'] = bk_item.item_id

      if bk_item.assignment
        pending_review_item_data[item_id]['reviewer'] = bk_item.assignment.username
      elsif pending_review_item_data[item_id]['reviewer'] = '-'
      end

      pending_review_item_data[item_id]['is_edit_after_review'] = bk_item.is_edited
      if bk_item.audit_priority
        pending_review_item_data[item_id]['audit_priority'] = bk_item.audit_priority
      end

      pending_review_item_data[item_id]['audit_tag'] = bk_item.audit_tag
    end

    item_ids = pending_review_item_data.keys()
    if item_ids.present?
      EntityItem.where(id: item_ids).each do |item|
        item_id = item.id

        pending_review_item_data[item_id]['item_id'] = item_id
        pending_review_item_data[item_id]['user_id'] = item.user_id
        pending_review_item_data[item_id]['offer_count'] = 0
        pending_review_item_data[item_id]['title'] = item.title
        pending_review_item_data[item_id]['desc'] = item.desc
        pending_review_item_data[item_id]['brand_name'] = item.brand_name
        pending_review_item_data[item_id]['local_price'] = item.local_price
        pending_review_item_data[item_id]['price'] = item.price
        pending_review_item_data[item_id]['country'] = item.country
        pending_review_item_data[item_id]['region'] = item.region
        pending_review_item_data[item_id]['city'] = item.city
        pending_review_item_data[item_id]['state'] = item.state
        pending_review_item_data[item_id]['state_txt'] = Settings.PRODUCT_STATUSES.invert[item.state]
        pending_review_item_data[item_id]['weight'] = item.weight.to_i

        pending_review_item_data[item_id]['images'] = []

        pending_review_time = []
        if item.modified_at.present?
          pending_review_time << ((Time.current - item.modified_at)/1.minute).round(0)
          pending_review_time << "#{item.modified_at.to_s}(LA)"
          pending_review_time << "#{item.modified_at.in_time_zone('America/Chicago').to_s}(Dallas)"
          pending_review_time << "#{item.modified_at.in_time_zone('Asia/Shanghai').to_s}(Beijing)"
        else
          pending_review_time << 0
          pending_review_time << ''
          pending_review_time << ''
          pending_review_time << ''
        end
        pending_review_item_data[item_id]['pending_review_time'] = pending_review_time

        pending_review_item_data[item_id]['pending_review_time_utc'] = item.modified_at
      end

      EntityImage.where(item_id: item_ids)
          .order(is_first: :desc)
          .select(:id, :item_id, :image_path).each do |image|
        item_image = {}
        item_image['img_url'] = image.image_path
        item_image['img_id'] = image.id
        item_image['is_new'] = false
        pending_review_item_data[image.item_id]['images'] << item_image
      end

      EntityItemcategory.includes(:category).where(item_id: item_ids).find_each do |itc|
        pending_review_item_data[itc.item_id]['category'] = itc.category.title
        pending_review_item_data[itc.item_id]['cat_id'] = itc.cat_id
      end

      # todo dev
      # ret = BackendApiREST.get_offer_count_by_item_ids(item_ids.map { |id| id.to_i })
      # if ret.present?
      #   JSON.load(ret[1])['objects'].each do |ob|
      #     item_id = ob['item_id'].to_i
      #     offer_count = ob['count'].to_i
      #     pending_review_item_data[item_id]['offer_count'] = offer_count if offer_count != 0
      #   end
      # end

      ItemTask.where.not(state: Settings.ITEM_TASK_STATE.Completed)
          .where(item_id: item_ids)
          .select(:id, :item_id, :task_type, :bk_item_copy_id)
          .order(id: :desc)
          .each do |item_task|
        item_id = item_task.item_id
        if pending_review_item_data[item_id]['item_task_id'].blank?
          pending_review_item_data[item_id]['item_task_id'] = item_task.id
          pending_review_item_data[item_id]['item_task_type'] = item_task.task_type
          pending_review_item_data[item_id]['is_pending_review_task'] = true if pending_review_item_data[item_id]['is_edit_after_review'] == false
          if item_task.bk_item_copy.present?
            item_copy = JSON.load(item_task.bk_item_copy.content).symbolize_keys!
            pending_review_item_data[item_id]['title'] = Utils::BkOps.bk_html_diff(item_copy[:title],
                                                                                   pending_review_item_data[item_id]['title'])
            pending_review_item_data[item_id]['desc'] = Utils::BkOps.bk_html_diff(item_copy[:desc],
                                                                                  pending_review_item_data[item_id]['desc'])
            pending_review_item_data[item_id]['images'].each do |item_image|
              if item_copy[:images].include?(item_image['img_url'])
                item_image['is_new'] = false
              else
                item_image['is_new'] = true
              end
            end
          end
        end
      end
    end

    item_data = {}
    item_data['data'] = pending_review_item_data.values()
    item_data['current_operator'] = {}
    item_data['current_operator']['username'] = "-"
    item_data['item_risk_words'] = get_item_risk_words(item_ids)

    if operator_id
      item_data['current_operator']['username'] = EmployeeManager.instance.get_empl_name(operator_id)
    else
      item_data['current_operator']['username'] = 'no assgin'
    end
    count = pending_review_item_data.values().size
    item_data["meta"]= {"limit" => limit,"count"=>count, "next" => "offset=#{offset+limit}", "offset" => offset}

    return item_data
  end

  # 获取总的待审核及待分配信息
  def get_total_stat_info()
    sql = "select
    ifnull(sum(case when t.assigned_to = 0  and t.operator_id = 0 and t.img_assigned_to = 0 and t.img_op_id = 0 then 1 else 0 end),0) item_unassigned_cnt,
    ifnull(sum(case when t.assigned_to = 0  and t.operator_id = 0 and t.img_assigned_to <> 0 and t.img_op_id = 0 then 1 else 0 end),0) img_assigned_unaudit_cnt,
    ifnull(sum(case when t.assigned_to = 0  and t.operator_id = 0 and t.img_assigned_to <> 0 and t.img_op_id <> 0 then 1 else 0 end),0) desc_unassigned_cnt,
    ifnull(sum(case when t.assigned_to <> 0  and t.operator_id = 0 then 1 else 0 end),0) desc_assigned_unaudit_cnt,
    count(1) total_cnt
    from bk_items t
    where t.review_state = #{Settings.PRODUCT_INTERNAL_STATUSES.PendingReview}"
    # return the hash result
    BkItem.connection.select_all(sql)[0]
  end

  #获取每个operator的待审核图片和图文数量
  def get_separated_stat_info()
    sql = "select a.* from (select t1.id, t1.username, ifnull(t2.pending_img_cnt, 0) pending_img_cnt, ifnull(t3.pending_desc_cnt,0)
    pending_desc_cnt from admins t1
    left join (select t.img_assigned_to, sum(case when t.img_op_id = 0 then 1 else 0 end ) pending_img_cnt
    from bk_items t where t.review_state = #{Settings.PRODUCT_INTERNAL_STATUSES.PendingReview} and t.assigned_to = 0 group by t.img_assigned_to
    ) t2 on t1.id = t2.img_assigned_to
    left join (select t.assigned_to, sum(case when operator_id = 0 then 1 else 0 end) pending_desc_cnt
    from bk_items t where t.review_state = #{Settings.PRODUCT_INTERNAL_STATUSES.PendingReview} and t.assigned_to <> 0 group by t.assigned_to
    ) t3 on t1.id = t3.assigned_to) a
    where (a.pending_img_cnt <> 0 or a.pending_desc_cnt <> 0)
    order by 1"

    BkItem.connection.select_all(sql)

  end

  def get_item_info(item_id)
    BkItem.find_by(item_id: item_id)
  end

  def assign_data(operator_id)
    # notify the auto_dispatch_service that the operator has finished all assigned item
    AutoDispatchWorker.perform_async({type: "finish_assigned", operator_id: operator_id}.to_json)
  end

  def reassign_data(operator_id)
    cnt = 0
    # 图文重新分配
    cnt = BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
        .where(operator_id: 0)
        .where(assigned_to: operator_id)
        .update_all("img_assigned_to=0,img_assigned_at=null,assigned_to=0,assigned_at=null,img_op_id=0,img_audit_tag=''")
    # 图片未重新分配
    cnt += BkItem.where(review_state: Settings.PRODUCT_INTERNAL_STATUSES.PendingReview)
        .where(assigned_to: 0)
        .where(img_assigned_to: operator_id)
        .update_all("img_assigned_to=0,img_assigned_at=null,img_op_id=0,img_audit_tag=''")

    Rails.logger.info "All items(#{cnt}) assigned to ##{operator_id} have been reassigned."
  end

  private
  def get_item_risk_words(item_ids)
    item_risk_words = Hash.new("")
    if item_ids.present?
      set_title_risk_words = Set.new();
      set_desc_risk_words = Set.new();
      BkItemRiskWord.where(item_id: item_ids).each do |item|
        # TODO: remove duplication and sort
        item.title_risk_words.to_s.split(',').each do |word|
          set_title_risk_words.add(word) unless word.blank?
        end
        item.desc_risk_words.to_s.split(',').each do |word|
          set_desc_risk_words.add(word) unless word.blank?
        end
      end
      item_risk_words[:title_risk_words_list] = set_title_risk_words.to_a.sort.join(",")
      item_risk_words[:desc_risk_words_list] = set_desc_risk_words.to_a.sort.join(",")
    end
    return item_risk_words
  end
end