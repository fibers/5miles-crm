require 'singleton'

class UserStats
  include Singleton

  def initialize()
    puts "UserStats init"
  end

  def isAutoApprove(user_id)
    ok_check_mun = 6

    dis_check_mun = 2


    bu = BkUser.find_by_id(user_id)
    auto_approve = false
    is_credit_task = false
    priority = 0
    if bu
      # 判断风险用户，以及信用条件
      if bu.risk_level>0
        auto_approve = false
      elsif bu.audit_cnt>=ok_check_mun
        #连续通过的次数
        Rails.logger.debug "max audit_cnt=#{bu.audit_cnt}"
        auto_approve = true
      end

      # if bu.disapproved_cnt>=dis_check_mun
      #   is_credit_task = true
      # end

      # 计算排序优先级
      priority = bu.right_down_cnt * (-1) +
          bu.approved_cnt * 1 +
          bu.disapproved_cnt * (-2) +
          bu.risk_level * (-100)

      if priority < 0
        is_credit_task = true
      end

    end
    return auto_approve,is_credit_task,priority,ok_check_mun,dis_check_mun
  end

  def setApproveCount(item)
    item.reload
    user_id = item.user_id
    Rails.logger.debug "setApproveCount item.weight=#{item.weight}"
    if item.weight>4
      self.saveApproveCount(user_id)
    # else
    #   self.saveRightDownCnt(user_id)
    end
  end

  def saveApproveCount(user_id)
    res = BkUser.where(id:user_id).update_all('audit_cnt = audit_cnt + 1 ,`approved_cnt`=`approved_cnt`+1')
    res_parse(res, user_id)
  end

  def setApproveCountOnly(item)
    item.reload
    user_id = item.user_id
    Rails.logger.debug "setApproveCountOnly item.weight=#{item.weight}"
    if item.weight>4
      self.saveApproveCountOnly(user_id)
    # else
    #   self.saveRightDownCnt(user_id)
    end
  end

  def saveApproveCountOnly(user_id)
    res = BkUser.where(id:user_id).update_all('`approved_cnt`=`approved_cnt`+1')
    res_parse(res, user_id)
  end

  def saveUserItemCnt(user_id)
    res = BkUser.where(id: user_id).update_all("item_cnt = item_cnt+1")
    res_parse(res, user_id)
  end

  def saveRightDownCnt(user_id)
    res = BkUser.where(id: user_id).update_all("right_down_cnt = right_down_cnt+1 ,audit_cnt = 0")
    res_parse(res, user_id)
  end

  def saveRiskUser(user_id ,description='')
    res = BkUser.where(id: user_id).update_all("risk_level = 1")
    res_parse(res, user_id)
  end

  def clearRiskUser(user_id)
    res = BkUser.where(id: user_id).update_all("risk_level = 0")
    res_parse(res, user_id)
  end

  def setUnapproveCount(user_id)
    res = BkUser.where(id: user_id).update_all('disapproved_cnt = disapproved_cnt+1,audit_cnt = 0')
    res_parse(res, user_id)
  end

  def saveCountZero(user_id)
    res = BkUser.where(id: user_id).update_all('audit_cnt = 0')
    res_parse(res, user_id)
  end

  def res_parse(res, user_id)
    Rails.logger.debug "update res=#{res}"
    if res==0
      self.initUser(user_id)
      #self.saveCountZero(user_id)
    end
  end

  def initUser(user_id)
    bu = BkUser.new
    bu.id = user_id
    bu.user_id = user_id
    bu.save
  end

end