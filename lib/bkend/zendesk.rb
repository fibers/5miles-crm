require 'zendesk_api'

module Zendesk

  class << self
    def client(retry_flag=true)
      client = ZendeskAPI::Client.new do |config|
        config.url = Settings.Zendesk.Url
        config.token = Settings.Zendesk.Token
        config.username = Settings.Zendesk.UserName
        config.retry = retry_flag

        # require 'logger'
        # config.logger = Logger.new(STDOUT)
      end

      return client
    end

    def get_redis_instance
      $global_redis
    end

  end

end

require_relative 'zendesk/ticket'
