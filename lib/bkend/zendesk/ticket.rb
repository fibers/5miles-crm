module Zendesk
  class Ticket
    class << self
      MUTEX_LOCK_KEY = "zendesk_ticket_lock".freeze()
      FROM_TIME_KEY = "zendesk_ticket_from_time".freeze()

      def update_local_tickets_with_lock()
        execute_action_with_lock() do
          from_time = Zendesk.get_redis_instance.get(FROM_TIME_KEY).to_i
          ret = update_local_tickets_by_from_time(from_time)
          if ret.present?
            next_from_time = Time.zone.parse(ret[-1]['updated_at']).to_i
            Zendesk.get_redis_instance.set(FROM_TIME_KEY, next_from_time)
          end
        end
      end

      # 一般不调用该接口，尽量使用update_local_tickets_with_lock()
      def update_local_tickets_by_from_time(from_time)
        Rails.logger.info("#{self.name}: from_time = #{from_time}")

        @client ||= Zendesk.client()
        ret = @client.tickets.incremental_export(from_time).fetch()
        ticket_data = {}
        ret.each do |ticket|
          ticket_as_json = {}
          ticket_as_json[:tid] = ticket['id']
          ticket_as_json[:via] = ticket['via'] || ''
          ticket_as_json[:status] = ticket['status'] || ''
          ticket_as_json[:subject] = ticket['subject'] || ''
          ticket_as_json[:priority] = ticket['priority'] || ''
          ticket_as_json[:current_tags] = ticket['current_tags'] || ''
          ticket_as_json[:ticket_type] = ticket['ticket_type'] || ''
          ticket_as_json[:req_name] = ticket['req_name'] || ''
          ticket_as_json[:req_email] = ticket['req_email'] || ''
          ticket_as_json[:submitter_name] = ticket['submitter_name'] || ''
          ticket_as_json[:group_name] = ticket['group_name'] || ''
          ticket_as_json[:assignee_name] = ticket['assignee_name'] || ''
          ticket_as_json[:assigned_at] = ticket['assigned_at']
          ticket_as_json[:solved_at] = ticket['solved_at']
          ticket_as_json[:created_at] = ticket['created_at']
          ticket_as_json[:updated_at] = ticket['updated_at']

          # 目前本地不存储定制的other信息
          # others_key = "field_#{Settings.Zendesk.CustomFields.Others}"
          # ticket_as_json[:others] = ticket[others_key] || ''
          ticket_as_json[:others] = ''
          ticket_data[ticket_as_json[:tid]] = ticket_as_json
        end

        if ticket_data.present?
          ticket_ids = ticket_data.keys()
          included_ticket_data = {}
          BkZendeskTicket.where(tid: ticket_ids).find_each do |ticket|
            included_ticket_data[ticket.tid] = ticket
          end
          included_ticket_ids = included_ticket_data.keys()
          added_ticket_ids = ticket_ids - included_ticket_ids

          included_ticket_data.values().each do |ticket|
            ticket_as_json = ticket_data[ticket.tid]
            ticket.status = ticket_as_json[:status]
            ticket.priority = ticket_as_json[:priority]
            ticket.group_name = ticket_as_json[:group_name]
            ticket.assignee_name = ticket_as_json[:assignee_name]
            ticket.assigned_at = ticket_as_json[:assigned_at]
            ticket.solved_at = ticket_as_json[:solved_at]
            ticket.updated_at = ticket_as_json[:updated_at]
            ticket.save
          end
          added_ticket_ids.each do |tid|
            ticket_as_json = ticket_data[tid]
            ticket = BkZendeskTicket.new(ticket_as_json)
            ticket.save
          end

        end

        return ret
      end

      private

      def execute_action_with_lock(*args, &block)
        if Zendesk.get_redis_instance.set(MUTEX_LOCK_KEY, 1, nx: true)
          begin
            block.call(*args)
          ensure
            Zendesk.get_redis_instance.del(MUTEX_LOCK_KEY)
          end
        else
          Rails.logger.info("Failed to lock '#{MUTEX_LOCK_KEY}'.")
        end
      end

    end

    def initialize
      @client = Zendesk.client()
    end

    def create(options={})
      # params: (subject, comment, tags, name, email, custom_fields)
      # 1. tags类型是数组，且成员是不带空格的字符串
      # 2. name 和email是用户的用户名和邮箱
      # 3. custom_fields是hash没醒，具体的定制的key参考Settings.Zendesk.CustomFields的underscore格式.比如custom_fields={others: 100}

      options.symbolize_keys!
      requester = {}
      requester[:name] = options[:name]
      requester[:email] = options[:email]

      ticket = {}
      ticket[:subject] = options[:subject]
      ticket[:comment] = {}
      ticket[:comment][:value] = options[:comment]
      ticket[:submitter_id] = @client.current_user.id
      ticket[:priority] = "urgent"
      ticket[:tags] = options[:tags] if options[:tags].present?
      ticket[:requester] = requester if requester.present?
      if options[:custom_fields].present?
        ticket_fields = []
        options[:custom_fields].each_pair do |key, value|
          field = {}
          field[Settings.Zendesk.CustomFields[key.to_s().camelcase()]] = value
          ticket_fields << field
        end
        ticket[:custom_fields] = ticket_fields
      end

      ret = @client.tickets.create(ticket)
      return ret
    end
  end
end
