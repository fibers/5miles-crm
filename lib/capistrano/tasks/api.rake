namespace :api_workers do
  %w(start stop).each do |task_name|
    desc "#{task_name} api workers"
    task task_name do
      on roles(:worker), in: :sequence, wait: 5 do
        sudo "/etc/init.d/api_workers_#{fetch(:full_app_name)} #{task_name}"
      end
    end
  end
end
