namespace :clockwork do
  desc 'Commands for clockwork'
  %w(start stop restart run).each do |command|
    task command.to_sym do
      on roles(:worker), in: :sequence, wait: 5 do
        within current_path do
            with rails_env: fetch(:rails_env) do
                execute :bundle, :exec, :clockworkd, "-c #{current_path}/clock.rb --pid-dir=#{current_path}/tmp/pids --log --log-dir=#{current_path}/log #{command}"
            end
        end
      end
    end
  end
end
