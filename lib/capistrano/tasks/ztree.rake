namespace :deploy do
  namespace :assets do
    task :non_digested_of_ztree do
      on roles(:app) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, "non_digested_of_ztree"
          end
        end
      end
    end
  end
end
