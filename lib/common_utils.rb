module CommonUtils

  def self.create_re_pattern(content)
    keywords = content.split(%r{\n|\r\n}).join("|")
    sub_re_pattern = %r{#{keywords}}i
    re_pattern = %r{\b#{sub_re_pattern}\b}
    return re_pattern
  end
end