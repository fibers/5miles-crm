var ListItem = React.createClass({
    getInitialState: function() {
        return {fromId: "",toId:""};
    },
    openFromUser: function() {
        var id = this.props.item.from.id;
        window.open("http://5milesapp.com/person/" + id);
    },
    openToUser: function() {
        var id = this.props.item.to.id;
        window.open("http://5milesapp.com/person/" + id);
    },
    openProduct: function() {
        var id = this.props.item.product.id;
        window.open("http://5milesapp.com/item/" + id);
    },
    render: function () {
        var userFromObj = <img className="chat_user_img" src={this.props.item.from.img} onClick={this.openFromUser} />;
        var userToObj = <img className="chat_user_img" src={this.props.item.to.img} onClick={this.openToUser}/>;
        var bodyObj = <div className="chat_body_con">{this.props.item.body}</div>;
        var subObj = <div className="chat_body_sub">{this.props.item.time}&nbsp;&nbsp;|&nbsp;&nbsp;{this.props.item.distance}</div>;
        var productObj = <img className="chat_img" src={this.props.item.product.img} onClick={this.openProduct}/>;
        if (this.props.item.from.prop == "SELLER") {
            return (
                <li className="row chat_li">
                    <div className="chat_li_item">
                        <div className="col-xs-2 chat_li_user text-center">
                            {userFromObj}
                            {userToObj}
                        </div>
                        <div className="col-xs-6 chat_li_body text-left">
                            {bodyObj}
                            {subObj}
                        </div>
                        <div className="col-xs-4 text-center">
                            {productObj}
                        </div>
                    </div>
                </li>
             )
        } else {
            return (
                <li className="row chat_li chat_li-right">
                    <div className="chat_li_item chat_li_item-right">
                        <div className="col-xs-4 text-center">
                            {productObj}
                        </div>
                        <div className="col-xs-6 chat_li_body text-left">
                            {bodyObj}
                            {subObj}
                        </div>
                        <div className="col-xs-2 chat_li_user text-center">
                            {userFromObj}
                            {userToObj}
                        </div>
                    </div>
                </li>
                );
        }
    }
});

//滚动列表布局
var List = React.createClass({
    render: function () {
        var items = this.props.chatList.map(function (chat) {
                return (
                    <ListItem item={chat} />
                );
        });
        return (
            <ul className="chat_ul container">
                {items}
            </ul>
            );
    }
});
//首页布局
var MainPage = React.createClass({
    render: function () {
        return (
            <div className="chat">
                 <List chatList={this.props.chatData}/>
            </div>
            );
    }
});
//创建App对象
var Room = React.createClass({
    //react内部方法，初始化对象变量，返回值可以通过this.state调用
    getInitialState: function() {
        return {
            chatData: [
            ]
        }
    },
    //react内部方法,初始化完成后立即调用一次
    componentDidMount: function() {
        var newItem = {"from":{"id":"5ko33Z7AoN","name":"Alex","prop":"SELLER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428542320/ylrvob6zlxs2uhpfllhq.jpg"},
            "to":{"id":"5ko39XEDRN","name":"Bear","prop":"BUYER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428420685/c7jmdz2cr7dtn4feydqx.jpg"},
            "body":"It does need a bulb but you can buy it onlineIt does need a bulb but you can buy it online",
            "time":"2015-04-09 11：28","distance":"6miles",
            "product":{"id":"2pj9aP8DeAyPdNnV","img":"http://res.cloudinary.com/fivemiles/image/upload/w_400/v1428546105/pnly8dopzzvgyztyvxoh.jpg"}};

        var data = [
            {"from":{"id":"5ko33Z7AoN","name":"Alex","prop":"SELLER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428542320/ylrvob6zlxs2uhpfllhq.jpg"},
             "to":{"id":"5ko39XEDRN","name":"Bear","prop":"BUYER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428420685/c7jmdz2cr7dtn4feydqx.jpg"},
             "body":"It does need a bulb but you can buy it onlineIt does need a bulb but you can buy it onlineIt does need a bulb but you can buy it onlineIt does need a bulb but you can buy it online",
             "time":"2015-04-09 11：28","distance":"6miles",
             "product":{"id":"2pj9aP8DeAyPdNnV","img":"http://res.cloudinary.com/fivemiles/image/upload/w_400/v1428546105/pnly8dopzzvgyztyvxoh.jpg"}},
            {"from":{"id":"5ko33Z7AoN","name":"Alex","prop":"SELLER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428542320/ylrvob6zlxs2uhpfllhq.jpg"},
             "to":{"id":"5ko39XEDRN","name":"Bear","prop":"BUYER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428420685/c7jmdz2cr7dtn4feydqx.jpg"},
             "body":"It does need a bulb but you can buy it online",
             "time":"2015-04-09 11：28","distance":"6miles",
             "product":{"id":"2pj9aP8DeAyPdNnV","img":"http://res.cloudinary.com/fivemiles/image/upload/w_400/v1428546105/pnly8dopzzvgyztyvxoh.jpg"}},
            {"from":{"id":"5ko33Z7AoN","name":"Alex","prop":"BUYER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428420685/c7jmdz2cr7dtn4feydqx.jpg"},
             "to":{"id":"5ko39XEDRN","name":"Bear","prop":"SELLER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428542320/ylrvob6zlxs2uhpfllhq.jpg"},
             "body":"It does need a bulb but you can buy it online",
             "time":"2015-04-09 11：28","distance":"6miles",
             "product":{"id":"2pj9aP8DeAyPdNnV","img":"http://res.cloudinary.com/fivemiles/image/upload/w_400/v1428546105/pnly8dopzzvgyztyvxoh.jpg"}},
            {"from":{"id":"5ko33Z7AoN","name":"Alex","prop":"BUYER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428420685/c7jmdz2cr7dtn4feydqx.jpg"},
             "to":{"id":"5ko39XEDRN","name":"Bear","prop":"SELLER","img":"http://res.cloudinary.com/fivemiles/image/upload/w_100,h_100,c_thumb,g_face/v1428542320/ylrvob6zlxs2uhpfllhq.jpg"},
             "body":"It does need a bulb but you can buy it online",
             "time":"2015-04-09 11：28","distance":"6miles",
             "product":{"id":"2pj9aP8DeAyPdNnV","img":"http://res.cloudinary.com/fivemiles/image/upload/w_400/v1428546105/pnly8dopzzvgyztyvxoh.jpg"}},
            ];
        this.setState({"chatData":data});
        var _self = this;
        setInterval(function() {
            if (data.length > 100) {
                data.shift();
            }
            data.push(newItem);
            _self.setState({"chatData":data});
            var bodyHei = document.body.scrollHeight;
            var scrollTop = document.body.scrollTop;
            var clientHei = document.documentElement.clientHeight;
            var liHei = $(".chat_li").last().height();
            var scrollHei = scrollTop + clientHei + liHei + 6;
            if (bodyHei == scrollHei) {
                scrollTo(0,document.body.scrollHeight);
            }
        },2000);
    },
    render: function() {
        return <MainPage chatData={this.state.chatData}/>;
    }
});
//渲染入口
React.render(<Room/>, document.getElementById("chat"));