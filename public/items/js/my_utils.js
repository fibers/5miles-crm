
function debug(s){
    console.debug(s);
}
(function($) {
    // the code of this function is from
    // http://lucassmith.name/pub/typeof.html
    $.type = function(o) {
        var _toS = Object.prototype.toString;
        var _types = {
            'undefined': 'undefined',
            'number': 'number',
            'boolean': 'boolean',
            'string': 'string',
            '[object Function]': 'function',
            '[object RegExp]': 'regexp',
            '[object Array]': 'array',
            '[object Date]': 'date',
            '[object Error]': 'error'
        };
        return _types[typeof o] || _types[_toS.call(o)] || (o ? 'object' : 'null');
    };
    // the code of these two functions is from mootools
    // http://mootools.net
    var $specialChars = { '\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"': '\\"', '\\': '\\\\' };
    var $replaceChars = function(chr) {
        return $specialChars[chr] || '\\u00' + Math.floor(chr.charCodeAt() / 16).toString(16) + (chr.charCodeAt() % 16).toString(16);
    };
    $.toJSON = function(o) {
        var s = [];
        switch ($.type(o)) {
            case 'undefined':
                return 'undefined';
                break;
            case 'null':
                return 'null';
                break;
            case 'number':
            case 'boolean':
            case 'date':
            case 'function':
                return o.toString();
                break;
            case 'string':
                return '"' + o.replace(/[\x00-\x1f\\"]/g, $replaceChars) + '"';
                break;
            case 'array':
                for (var i = 0, l = o.length; i < l; i++) {
                    s.push($.toJSON(o[i]));
                }
                return '[' + s.join(',') + ']';
                break;
            case 'error':
            case 'object':
                for (var p in o) {
                    s.push(p + ':' + $.toJSON(o[p]));
                }
                return '{' + s.join(',') + '}';
                break;
            default:
                return '';
                break;
        }
    };
    $.evalJSON = function(s) {
        if ($.type(s) != 'string' || !s.length) return null;
        return eval('(' + s + ')');
    };
})(jQuery);
/**
 * jquery.confirm.js
 * @author 云淡然 http://qianduanblog.com
 * @version  1.0
 * 2013年10月23日16:57:49
 */
(function($, undefined) {
	var _,
		prefix = "jquery_confirm____",
		// 是否加载了css
		isLoadCss = 0,
		// 空函数
		emptyFn = function() {},
		// 确认框队列
		// confirmQueue = [],
		defaults = {
			// 样式
			//css: "http://static.qianduanblog.com/css/jquery.confirm/default.min.css?v=" + Math.ceil(new Date() / 86400000),
            css: "/static/css/jquery.confirm.css?v=" + Math.ceil(new Date() / 86400000),
			// 确认框内容
			content: "确认吗？",
			// 确认按钮文字
			sureButton: "确认",
			// 取消按钮文字
			cancelButton: "取消",
			// 位置
			position: {},
			// 自动打开
			autoOpen: false,
			// 动画持续时间
			duration: 123,
			// 打开确认框回调
			onopen: emptyFn,
			// 单击了确认或者取消回调
			onclick: emptyFn,
			// 确认回调
			onsure: emptyFn,
			// 取消回调
			oncancel: emptyFn,
			// 关闭确认框回调
			onclose: emptyFn
		},
		$bg, $confirm, $body, $sure, $cancel, callback = emptyFn,
		options;

	$bg = $('<div class="' + prefix + 'bg"></div>').appendTo("body");
	$confirm = $('<div class="' + prefix + '" style="display:none"><div class="' + prefix + 'body"></div><div class="' + prefix + 'footer"><button class="button button-primary ' + prefix + 'sure">确认</button><button class="button button-error ' + prefix + 'cancel">取消</button></div></div>').appendTo("body");
	$body = $("." + prefix + "body", $confirm);
	$sure = $("." + prefix + "sure", $confirm);
	$cancel = $("." + prefix + "cancel", $confirm);

    //console.info($bg)
    console.info("init confirm ok")

	$.confirm = function() {
		var args = arguments,
			argL = args.length;

        //alert(args[0])
		// ""
		if (_isStrOrNum(args[0])) {
            //alert("str")
			options = $.extend({}, defaults);
            options.content = args[0];
			options.onclick = $.isFunction(args[1]) ? args[1] : emptyFn;
			// 加载样式
            //alert( options.css)
			//alert(_open)
            if (!isLoadCss) {
				$('<link rel="stylesheet" href="' + options.css + '" />').appendTo("head").load(function() {
                    //alert(1111)
					_open();
				});
			} else {
                //alert(2222)
				_open();
			}
		}
		// object
		else if ($.type(args[0]) == "object") {
            //alert("obj")
			options = $.extend({}, defaults, args[0]);
			// 加载样式
			if (!isLoadCss) {
				$('<link rel="stylesheet" href="' + options.css + '" />').appendTo("head").load(function() {
					isLoadCss = 1;
					if (options.autoOpen) {
						_open();
					}
				});
			} else {
				if (options.autoOpen) {
					_open();
				}
			}
		}
	}

	// 单击确认
	$sure.click(function() {
		options.onsure();
		options.onclick(true);
		_close();
	});


	// 单击取消
	$cancel.click(function() {
		options.oncancel();
		options.onclick(false);
		_close();
	});


	/**
	 * 打开确认框
	 * @return {undefined}
	 * @version 1.0
	 * 2013年10月23日15:49:35
	 */

	function _open() {
        console.info($bg)
		$bg.fadeIn(options.duration, function() {
			var theW = $confirm.width(),
				theH = $confirm.height(),
				winW = $(window).width(),
				winH = $(window).height(),
				theL = (winW - theW) / 2,
				theT = (winH - theH) / 3;
            //alert($body)
			$body.html(options.content);
			$sure.html(options.sureButton);
			$cancel.html(options.cancelButton);

			if (options.position.left !== undefined) theL = options.position.left;
			if (options.position.top !== undefined) theT = options.position.top;

            //alert(theL+""+theT)
			$confirm.css({
				left: theL,
				top: theT
			}).fadeIn(options.duration, function() {
				options.onopen();
			});
		});
	}

	/**
	 * 关闭确认框
	 * @return {undefined}
	 * @version 1.0
	 * 2013年10月23日15:48:50
	 */

	function _close() {
		$confirm.fadeOut(options.duration, function() {
			$bg.fadeOut(options.duration, function() {
				options.onclose();
			});
		});
	}
	/**
	 * 判断值是否为字符串或者数值
	 * @param  {String/Number} 字符串或数值
	 * @return {Boolean}
	 * @version 1.0
	 * 2013年9月23日15:23:04
	 */
	function _isStrOrNum(val) {
		return $.type(val) == "string" || $.type(val) == "number";
	}

	$.confirm.defaults = defaults;
})($);


$.extend({
    getUrlVars: function(){
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = decodeURIComponent(hash[1]);
        }
        return vars;
    },
    getUrlVar: function(name){
        return $.getUrlVars()[name];
    }
});


/**
 * Implementation of queryString object in JS.
 * It's not guaranteed to handle UTF8 encoding properly;
 * feel free to add/test it.
 */
var queryString = function (loc) {

    this.params = new Object();
    this.loc = loc;

    this.url = loc.protocol + "//" + loc.host + "" + loc.pathname;

    qs = this.loc.search.substring(1, this.loc.search.length);
    if (qs) {
        qs = qs.replace(/\+/g, ' ');
        var args = qs.split('&');

        for (var i = 0; i < args.length; i++) {
            var index = args[i].indexOf('=');
            if (index > 0) {
                var name = decodeURIComponent(args[i].substr(0, index));
                var value = decodeURIComponent(args[i].substr(index + 1));


                if(this.params[name] && typeof(this.params[name])=="string"){

                    var tempVal = this.params[name]
                    this.params[name] = [];

                    this.params[name].push(tempVal);
                    this.params[name].push(value);
                    debug(" new arr ");

                }else if(this.params[name] && typeof(this.params[name])=="object"){

                    this.params[name].push(value);

                    //debug(this.params[name]);
                }else{
                    this.params[name] = value;
                }
                debug(name+":"+value);
                debug(typeof(this.params[name]));
            }
        }
    }

}

queryString.prototype.get = function (name, defaultVal) {
    var value = this.params[name];
    if (value)
        return value;
    else
    if (defaultVal) {
        return defaultVal;
    } else {
        return null;
    }
}

queryString.prototype.add = function (name, value) {

    this.params[name] = value;
}

queryString.prototype.remove = function (name) {
    this.params[name] = null;
}
queryString.prototype.getAll = function () {
    return this.params;
}
queryString.prototype.toString = function () {
    var str = "?";

    for (var name in this.params) {
        var value = this.params[name];
        if (value != null) {
            str = str + name + "=" + encodeURIComponent(value) + "&";
        }
    }
    if (this.url) {
        return this.url + str;
    }
    return str;
}
queryString.prototype.go = function () {

    if (this.url) {
        this.loc.href = this.toString();
        //alert(this.loc.href);

        this.loc = null;
    }

}


function sys_getScrollTop() {
    var scrollTop = 0;
    if (document.documentElement && document.documentElement.scrollTop) {
        scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) {
        scrollTop = document.body.scrollTop;
    }
    return scrollTop;
}


function sys_check_res_data(data , is_check_data ,is_not_alert){
    console.info("sys_check_res_data:",data)
    if(data.res_code==1){
        str = "错误："+data.msg;
        if(is_not_alert==false){
            alert(str)
        }
        throw(str);
    }
    var res = data.result;
    console.info("sys_check_res_data res:",res)
    if(is_check_data){
        if(is_check_data==true){
            if(!res){
                throw "错误：result = null"
            }
        }
    }
    return res;
}



function sys_show_templ(templ_name, show_name ,data){
    try{
        $("#"+show_name).html("");

        $("#"+templ_name).tmpl(data).appendTo("#"+show_name);
    }catch(e){
        console.error("sys_show_templ error："+templ_name+"|"+ show_name +"|e:"+ e.description);
        throw e;
    }
}
function sys_append_templ(templ_name, show_name ,data){
    try{
        $("#"+templ_name).tmpl(data).appendTo("#"+show_name);
    }catch(e){
        console.error("sys_show_templ error："+templ_name+"|"+ show_name +"|e:"+ e.description);
        throw e;
    }
}
function sys_set_select_value(html_name){
    var sel = $("#"+html_name);
    var val = sel.attr("data-val");
    if(val){
        sel.val(val)
    }
    var val = sel.attr("data_val");
    if(val){
        sel.val(val)
    }
    var val = sel.attr("data-id");
    if(val){
        sel.val(val)
    }
}

function sys_build_select_remote(html_name, ops_url ,name_field ,get_data , html_sub_name,get_sub_data ,init_ok ,def_value){

    if(ops_url){
        $.get(ops_url, {}, function (data, textStatus, XMLHttpRequest) {
            sys_check_res_data(data);
            var result = null;
            var list_data =null;
            if(get_data){
                result = data.result
                list_data = get_data(data.result)
            }else{
                list_data = data.result;
            }
            //console.info(list_data)
            //alert(def_value)
            var sel = init(html_name,list_data ,def_value)      //.multiselect();
            if(get_sub_data){
                sel.change(function (){
                    $("#"+html_sub_name).html("")
                    var sel_id = sel.val()
                    list_data = get_sub_data(result , sel_id)
                    init(html_sub_name,list_data)
                })
                sel.change();
            }
            if(init_ok)init_ok();
        });
    }

    function init(html_name, ops_list ,def_value){
        var sel = $("#"+html_name);
        sel.append("<option value='-1' selected>--所有--</option>");
        for (i = 0; i < ops_list.length; i++) {
            var it = ops_list[i]
            //console.info(it)
            var txt = "";
            if (typeof(name_field)=="string"){
                txt = it[name_field]
            }else if (typeof(name_field)=="function"){
                txt = name_field(it);
            }
            var sled = ops_list[i].sled;
            var ooo= null;
            if (sled == 'true' || def_value==it.id) {
                ooo = sel.append("<option value='" + it.id+ "' selected>" + txt + "</option>");
            } else {
                ooo = sel.append("<option value='" + it.id + "' title='"+txt+"("+it.id+")'>" + txt + "(" + it.id + ")" + "</option>");
            }
            var opp = $("#"+html_name+" > option:last")
            opp.attr("js_data",JSON.stringify(it));
            //console.info(ooo.attr("js_data"));

        }
        setTimeout(function(){
            if(sel.attr("data-id")){
                sel.val(sel.attr("data-id"))
            }
            if(sel.attr("data-val")){
                sel.val(sel.attr("data-val"))
            }
        },100)

        //console.info(sel)
        //sel.onchange(function(){
            //paramSearch.setNewVal();
        //})
        return sel;
    }
}



//var sys_localStorage = false;
var sys_local_cache = function () {
    return{
        localStorage_enabled:false,
        init:function(){
        },
        getVal:function (key){
            if(('localStorage' in window) && window['localStorage'] !== null){
                sys_local_cache.localStorage_enabled = true;

                return localStorage.getItem(key);
            }else{
                alert('天啊，你还在用这么土的浏览器');
                sys_local_cache.localStorage_enabled = false;
            }
        },
        setVal:function(key,value){
            if(sys_local_cache.localStorage_enabled){

                //JSON.stringify(strvalue)
                var strname="pay_stats_"+location.host;
                //json形式存储
                var strvalue={'selPage':$('#selPage').val()};
                localStorage.setItem(key,value);
            }
        }
    }
}();




function sys_country_sel_init(selCountry , selCity , base_key ,def_country){
        sys_build_select_remote(selCountry, "/country/list_all?", "name_cn", function get_data(result){
            return result.country_data;
        },selCity,function get_sub_data(result , parent_id){
            console.info(result)
            var new_list=[];
            for(var i=0;i<result.city_data.length;i++){
                var it = result.city_data[i];
                if(it.country_id==parent_id){
                    new_list.push(it)
                }
            }
            return new_list;
        },function init_ok(){
            paramCountry= sys_module_head_params_cahce().init(base_key+"_params_country",selCountry);
            paramCity= sys_module_head_params_cahce().init(base_key+"_params_city",selCity);
        },def_country);
}

$(document).ajaxSend(function(evt, request, settings){
	$("#loading").show();
	//$("#ajaxMsg").append("<li id='liStartMsg'>开始请求: " + settings.url + "</li>");
	//$("#loading").fadeOut("slow");

});

	//$( document ).ajaxComplete(function( event,request, settings ) {
	//	$( "#ajaxMsg" ).append( "<li>Request Complete.</li>" );
	//});
$(document).ajaxSuccess(function(evt, request, settings) {
	//$( "#ajaxMsg" ).append("<li id='liSuccMsg'>请求成功!"+(new Date())+"</li>");
	//$("#loading").hide();
    $( "#loading").fadeOut("slow");
	//$( "#liSuccMsg").remove();
});

$(document).ajaxError(function(evt, request, settings) {
	//$("#loading").hide();
    $( "#loading").fadeOut("slow");
	//$( "#liSuccMsg").remove();
});



function sys_cleanHtml(s_html){
    return s_html.replace(/<[^>].*?>/g,"");
}





function sys_img_onload(){

    var timer  = null;
    var innerHeight = (window.innerHeight||document.documentElement.clientHeight);
    var height = innerHeight + 40;
    var images = [];

    function detect(){
        var scrollTop = (window.pageYOffset||document.documentElement.scrollTop) - 20;
        for( var i=0,l=images.length; i<l; i++ ){
            var img = images[i];
            var offsetTop = img.el.offsetTop;
            if( !img.show && scrollTop < offsetTop+img.height && scrollTop+height > offsetTop ){
                img.el.setAttribute('src', img.src);
                img.show = true;
            }
            if (ISWP && (img.el.width*1 > sw)){//兼容WP
                img.el.width = sw;
            }
        }
    }

    var ISWP = !!(navigator.userAgent.match(/Windows\sPhone/i));
    var ping_apurl = false;
    function onScroll(){
        clearTimeout(timer);
        timer = setTimeout(detect, 100);
    }
    function onLoad(){
        var imageEls = document.getElementsByTagName('img');
        var pcd = document.getElementById("page-content");
        if (pcd.currentStyle){
            sw = pcd.currentStyle.width;
        }else if (typeof getComputedStyle != "undefined"){
            sw = getComputedStyle(pcd).width;
        }
        sw = 1*(sw.replace("px", ""));
        for( var i=0,l=imageEls.length; i<l; i++ ){
            var img = imageEls.item(i);
            if(!img.getAttribute('data-src') ) continue;
            images.push({
                el     : img,
                src    : img.getAttribute('data-src'),
                height : img.offsetHeight,
                show   : false
            });
        }
        detect();
    }
    if( window.addEventListener ){
        window.addEventListener('scroll', onScroll, false);
        window.addEventListener('load', onLoad, false);
        document.addEventListener('touchmove', onScroll, false);
    }
    else {
        window.attachEvent('onscroll', onScroll);
        window.attachEvent('onload', onLoad);
    }
}

String.prototype.replaceAll = function(search, replace){
    var regex = new RegExp(search, "g");
    return this.replace(regex, replace);
}

function sys_replace_all_tmpl(id){
    var oo = $("#"+id);
    if (oo.attr("replace_init")=="ok"){
        return
    }
    var html = oo.html();
    var l = String.fromCharCode("{".charCodeAt())
    var l2 = l+l;
    var r = String.fromCharCode("}".charCodeAt())
    var r2 = r+r;
    html = html.replaceAll("{!{",l2)
    html = html.replaceAll("}!}",r2)
    //console.info(html)
    oo.html(html)
    oo.attr("replace_init","ok")
}


function sys_load_page_data(url, params , load_next ,load_complete ,next_fnc){

    //params['load_next'] = load_next;
    $.get(url, params, function (data, textStatus, XMLHttpRequest) {
        try{
            sys_check_res_data(data,true,true)
            var items = data.result.list_data
            setTimeout(function () {
                console.info(data)

                if(items.length==0){
                    sys_replace_all_tmpl("tmpl_list_empty");
                    sys_show_templ("tmpl_list_empty", "list_content", {});
                    return
                }

                if(load_next){
                    sys_append_templ("tmpl_list_item", "list_content", items);
                }else{
                    sys_show_templ("tmpl_list_item", "list_content", items);
                }

                console.info("is next:"+data.result.list_page.is_next)
                // todo 是否有下一页

                $(".list_item_next").remove()
                if (data.result.list_page.is_next){
                    console.info("is next")

                    sys_append_templ("tmpl_list_next", "list_content", {});
                    if (next_fnc){
                        next_fnc();
                    }
                }

                if (load_complete){
                    load_complete()
                }

                //$(".div_loadding").remove()
                $("#circleG").hide()
            }, 300);

        }catch(e){
            sys_replace_all_tmpl("tmpl_list_empty");
            sys_show_templ("tmpl_list_empty", "list_content", {"msg":"数据加载时出现问题，稍后再试！"});
        }
    });
}

function mergeObj(o1,o2){
       for(var key in o2){
           o1[key]=o2[key]
       }
       return o1;
   }

Date.prototype.format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


/**
* approx distance between two points on earth ellipsoid
* @param {Object} lat1
* @param {Object} lng1
* @param {Object} lat2
* @param {Object} lng2
*/
function getFlatternDistance(lat1,lng1,lat2,lng2){
    var f = getRad((lat1 + lat2)/2);
    var g = getRad((lat1 - lat2)/2);
    var l = getRad((lng1 - lng2)/2);

    var sg = Math.sin(g);
    var sl = Math.sin(l);
    var sf = Math.sin(f);

    var s,c,w,r,d,h1,h2;
    var a = EARTH_RADIUS;
    var fl = 1/298.257;

    sg = sg*sg;
    sl = sl*sl;
    sf = sf*sf;

    s = sg*(1-sl) + (1-sf)*sl;
    c = (1-sg)*(1-sl) + sf*sl;

    w = Math.atan(Math.sqrt(s/c));
    r = Math.sqrt(s*c)/w;
    d = 2*w*a;
    h1 = (3*r -1)/2/c;
    h2 = (3*r +1)/2/s;

    return d*(1 + fl*(h1*sf*(1-sg) - h2*(1-sf)*sg));
}

var EARTH_RADIUS = 6378137.0; //单位M
var PI = Math.PI;

function getRad(d){
    return d*PI/180.0;
}

function getGreatCircleDistance(lat1,lng1,lat2,lng2){
    var radLat1 = getRad(lat1);
    var radLat2 = getRad(lat2);

    var a = radLat1 - radLat2;
    var b = getRad(lng1) - getRad(lng2);

    var s = 2*Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
    s = s*EARTH_RADIUS;
    s = Math.round(s*10000)/10000.0;

    return s;
}


// map.js  start
var $G, $O, $M, $L, $I;
(function (undefined) {
    //O = function (id) {
    //    return "string" == typeof id ? document.getElementById(id) : id;
    //};
    MP = {
        y: 39.9,
        x: 116.4,
        point: function (y, x) {
            return new google.maps.LatLng(y, x);
        },
        getCanvas: function (id) {
            var mapid = id ? id : 'map_canvas';
            return document.getElementById(mapid);
        },
        options: function (center, z) {
            return {
                zoom: z ? z : 14,
                center: center ? center : this.getCenter(),
                navigationControl: true,
                scaleControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        }
    }

    M = {
        mark: function (map, latLng, title) {
            if (title)
                return new google.maps.Marker({
                    icon: this.icon,
                    position: latLng,
                    map: map,
                    title: title
                });
            else
                return new google.maps.Marker({
                    //icon: this.icon,
                    position: latLng,
                    map: map
                });
        }
    }

    I = {
        infos: [],
        add: function (info, latLng, w, h) {
            if (w && h)
                return new google.maps.InfoWindow({
                    content: info,
                    size: new google.maps.Size(w, h),
                    position: latLng
                });
            else if (latLng)
                return new google.maps.InfoWindow({
                    content: info,
                    position: latLng
                });
            else
                return new google.maps.InfoWindow({
                    content: info
                });
        }
    }

//event 事件
    L = {
        listen: null,
        add: function (dom, event, fn) {
            return google.maps.event.addDomListener(dom, event, fn);
        },
        addOnce: function (dom, event, fn) {
            return google.maps.event.addListenerOnce(dom, event, fn)
        }
    }

    $G = MP;
    //$O = O;
    $M = M;
    $L = L;
    $I = I;
})();
// map.js  end



function format_time(s){
    return new Date(new Date(parseInt(s+'000'))).format("MM-dd hh:mm");
}
function format_time_zone(ts , zone){

    var new_ts = parseInt(ts+'000')

    var offset_h = new Date().getTimezoneOffset()/60;  //-8

    if(offset_h*-1==zone){
        // 同一时区
    }else{
        new_ts = new_ts + (offset_h+zone) *3600*1000
    }

    /*console.info(new Date().getTimezoneOffset())

    if(new Date().getTimezoneOffset())

    var offset = new Date(parseInt(ts+'000')).getTimezoneOffset()
    console.info("offset="+offset)
    console.info(new Date(parseInt(ts+'000')).toLocaleString())*/

    return new Date(new_ts).format("MM-dd hh:mm");
}

function diff_time(c,u){
    interval = (u-c)
    var h = Math.floor(interval / 3600);
    var m = Math.floor(interval % 3600 / 60);
    if(h==0 && m==0){
        return "-"
    }
    return h + "h" + m + "m"
}